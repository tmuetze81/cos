/*
 * Copyright (c) 2021 Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <cassert>
#include <iostream>
#include <vector>
#include "edge.hpp"
#include "rectangle.hpp"
#include "rectangulation.hpp"
#include "vertex.hpp"
#include "wall.hpp"

Rectangulation::Rectangulation(int n, Type type, std::vector<Pattern> &patterns) : n_(n), type_(type), patterns_(patterns)
{
  // M1 - Initialize
  set_all_vertical();
  o_.push_back(Direction::none);
  s_.push_back(-1);
  for (int j = 1; j <=n_;  ++j)
  {
    o_.push_back(Direction::left);
    s_.push_back(j);
  }
}

void Rectangulation::set_all_vertical()
{
  // initialize the 3n+1 edges, 2n+2 vertices, n rectangles and n+3 walls
  std::vector<Edge> edges(3*n_+2, Edge());
  std::vector<Vertex> vertices(2*n_+3, Vertex());
  std::vector<Rectangle> rectangles(n_+1, Rectangle());
  std::vector<Wall> walls(n_+4, Wall());
  // set edge, vertex, rectangle and wall "0"
  edges[0].init(Edge::Dir::none,0,0,0,0,0,0,0);
  vertices[0].init(0,0,0,0);
  rectangles[0].init(0,0,0,0);
  walls[0].init(0,0);
  // set edge properties on the top side of the rectangulation
  for (int i = 1; i <= n_-1; ++i)
    edges[i].init(Edge::Dir::hor,i,i+1,i-1,i+1,0,i,1);
  edges[n_].init(Edge::Dir::hor,n_,n_+1,n_-1,0,0,n_,1);
  // set vertex properties on the top side of the rectangulation
  vertices[1].init(0,1,2*n_+1,0);
  vertices[n_+1].init(0,0,3*n_+1,n_);
  for (int i = 2; i <= n_; ++i)
    vertices[i].init(0,i,2*n_+i,i-1);
  // set edge properties on the bottom side of the rectangulation
  for (int i = 2; i <= n_-1; ++i)
    edges[n_+i].init(Edge::Dir::hor,n_+i+1,n_+i+2,n_+i-1,n_+i+1,i,0,2);
  edges[n_+1].init(Edge::Dir::hor,n_+2,n_+3,0,n_+2,1,0,2);
  edges[2*n_].init(Edge::Dir::hor,2*n_+1,2*n_+2,2*n_-1,0,n_,0,2);
  // set vertex properties on the bottom side of the rectangulation
  vertices[n_+2].init(2*n_+1,n_+1,0,0);
  vertices[2*n_+2].init(3*n_+1,0,0,2*n_);
  for (int i = 2; i <= n_; ++i)
    vertices[n_+i+1].init(2*n_+i,n_+i,0,n_+i-1);
  // set edge properties of the vertical edges
  edges[2*n_+1].init(Edge::Dir::ver,n_+2,1,0,0,0,1,3);
  edges[3*n_+1].init(Edge::Dir::ver,2*n_+2,n_+1,0,0,n_,0,n_+3);
  for (int i = 2; i <= n_; ++i)
    edges[2*n_+i].init(Edge::Dir::ver,n_+i+1,i,0,0,i-1,i,i+2);
  // set rectangle properties
  for (int i = 1; i <= n_; ++i)
    rectangles[i].init(i+1,n_+i+2,n_+i+1,i);
  // set wall parameters
  walls[1].init(1,n_+1);
  walls[2].init(n_+2,2*n_+2);
  for (int i = 1; i <= n_+1; ++i)
    walls[i+2].init(n_+i+1, i);
  init(vertices, walls, edges, rectangles);
}

void Rectangulation::init(std::vector<Vertex> &vertices, std::vector<Wall> &walls, std::vector<Edge> &edges, std::vector<Rectangle> &rectangles)
{
  edges_ = edges;
  vertices_ = vertices;
  walls_ = walls;
  rectangles_ = rectangles;
}

void Rectangulation::print_data()
{
  // output data structures as in the corresponding figure in the paper
  std::cout << "edges:" << std::endl;
  int i = 0;
  for (auto e : edges_)
  {
    std::cout << "\t" << i << ". ";
    switch(e.dir_)
      {
      case(Edge::Dir::hor): std::cout << "hor "; break;
      case(Edge::Dir::ver): std::cout << "ver "; break;
      case(Edge::Dir::none):  std::cout << "none "; break;
      }
      std::cout << e.tail_ << " " << e.head_ << " " << e.prev_ << " " << e.next_ << " " << e.left_ << " " << e.right_ << " " << e.wall_ << std::endl;
      i++;
  }
  i = 0;
  std::cout << "vertices:" << std::endl;
  for (auto v : vertices_)
  {
    std::cout << "\t" << i << ". " << v.north_ << " " << v.east_ << " " << v.south_ << " " << v.west_ << " ";
    switch(v.type_)
      {
        case(Vertex::Type::none): std::cout << "none" << std::endl; break;
        case(Vertex::Type::corner): std::cout << "corner" << std::endl; break;
        case(Vertex::Type::bottom): std::cout << "bottom" << std::endl; break;
        case(Vertex::Type::top):  std::cout << "top" << std::endl; break;
        case(Vertex::Type::left): std::cout << "left" << std::endl; break;
        case(Vertex::Type::right):  std::cout << "right" << std::endl; break;
      }
      i++;
  }
  i = 0;
  std::cout << "walls:" << std::endl;
  for (auto w : walls_)
  {
    std::cout << "\t" << i << ". " << w.first_ << " " << w.last_ << std::endl;
    i++;
  }
  i = 0;
  std::cout << "rectangles:" << std::endl;
  for (auto r : rectangles_)
  {
    std::cout << "\t" << i << ". " << r.neast_ << " " << r.seast_ << " " << r.swest_ << " " << r.nwest_ << std::endl;
    i++;
  }
}

void Rectangulation::print_coordinates()
{
  switch(type_)
  {
    case(Type::generic):  print_coordinates_generic();  break;
    case(Type::diagonal): print_coordinates_diagonal(); break;
    case(Type::baligned): print_coordinates_diagonal(); break;  // block-aligned rectangulations are a subset of diagonal rectangulations
  }
}

void Rectangulation::print_coordinates_generic()
{
  // Run the greedy algorithm for finding an equispaced grid to place the vertices of the rectangles.
  // For finding the x-coordinates of the grid, we do a sweep from west to east.
  std::vector<int> active_vertices;
  std::vector<int> vertex_x_coord(2*n_+3,-1);
  // start with every vertex which lies on the western side as an active vertex
  int a = 0;
  for (auto v : vertices_)
  {
    int side_edge_id;
    if (v.type_ == Vertex::Type::right)
      side_edge_id = v.north_;
    else if (v.type_ == Vertex::Type::corner)
      side_edge_id = std::max(v.north_,v.south_);
    else
    {
      a++;
      continue;
    }
    if (edges_[side_edge_id].left_ == 0)
    {
      active_vertices.push_back(a);
    }
    a++;
  }
  // propagate active vertices
  int x_value=0;
  while (active_vertices.size() != 0)
  {
    // set x-coordinate for active vertices
    for(auto idx : active_vertices)
      vertex_x_coord[idx]=x_value;
    x_value++;
    std::vector<int> new_active_vertices;
    // propagate east
    for(auto idx : active_vertices)
    {
      if (vertices_[idx].east_!=0)
      {
        int alpha = vertices_[idx].east_;
        new_active_vertices.push_back(edges_[alpha].head_);
      }
    }
    // propagate north and south
    std::vector<int> new_active_vertices_copy(new_active_vertices);
    for (auto idx : new_active_vertices_copy)
    {
      Vertex propagate_from=vertices_[idx];
      // propagate north
      while (propagate_from.north_!=0)
      {
        Edge e=edges_[propagate_from.north_];
        propagate_from=vertices_[e.head_];
        new_active_vertices.push_back(e.head_);
      }
      propagate_from=vertices_[idx];
      // propagate south
      while (propagate_from.south_!=0)
      {
        Edge e=edges_[propagate_from.south_];
        propagate_from=vertices_[e.tail_];
        new_active_vertices.push_back(e.tail_);
      }
    }
    active_vertices=new_active_vertices;
  }
  // For finding the y-coordinates of the grid, we do the same from south to north.
  active_vertices.clear();
  std::vector<int> vertex_y_coord(2*n_+3,-1);
  // start with every vertex which lies on the southern side as an active vertex
  a = 0;
  for (auto v : vertices_)
  {
    int side_edge_id;
    if (v.type_ == Vertex::Type::top)
      side_edge_id = v.east_;
    else if (v.type_ == Vertex::Type::corner)
      side_edge_id = std::max(v.east_,v.west_);
    else
    {
      a++;
      continue;
    }
    if (edges_[side_edge_id].right_ == 0)
    {
      active_vertices.push_back(a);
    }
    a++;
  }
  // propagate active vertices
  int y_value=0;
  while (active_vertices.size() != 0)
  {
    // set y-coordinate for active vertices
    for(auto idx : active_vertices)
      vertex_y_coord[idx]=y_value;
    y_value++;
    std::vector<int> new_active_vertices;
    // propagate north
    for(auto idx : active_vertices)
    {
      if (vertices_[idx].north_!=0)
      {
        int alpha = vertices_[idx].north_;
        new_active_vertices.push_back(edges_[alpha].head_);
      }
    }
    // propagate west and east
    std::vector<int> new_active_vertices_copy(new_active_vertices);
    for (auto idx : new_active_vertices_copy)
    {
      Vertex propagate_from=vertices_[idx];
      // propagate east
      while (propagate_from.east_!=0)
      {
        Edge e=edges_[propagate_from.east_];
        propagate_from=vertices_[e.head_];
        new_active_vertices.push_back(e.head_);
      }
      propagate_from=vertices_[idx];
      // propagate west
      while (propagate_from.west_!=0)
      {
        Edge e=edges_[propagate_from.west_];
        propagate_from=vertices_[e.tail_];
        new_active_vertices.push_back(e.tail_);
      }
    }
    active_vertices = new_active_vertices;
  }
  bool is_first = true;
  bool is_second = true;
  for(auto r : rectangles_)
  {
    if (is_first)
    {
      is_first = false;
      continue;
    }
    if (!is_second)
      std::cout << " | ";  // separator between rectangles
    else
      is_second = false;
    std::cout << vertex_x_coord[r.swest_] << " " << vertex_y_coord[r.swest_] << " ";  // bottom-left corner
    std::cout << vertex_x_coord[r.neast_] << " " << vertex_y_coord[r.neast_];         // top-right corner
  }
  std::cout << std::endl;
}

void Rectangulation::DFS_BL(int vertex_id, int& val, std::vector<int>& vertex_x_coord, std::vector<int>& vertex_y_coord)
{
  // We grow the binary tree rooted in the bottom-left corner by a DFS-like procedure.
  // We then check the order in which the diagonal is intersected by the leaves of this tree,
  // and then propagate the value of the vertex coordinates upwards.
  Vertex v = vertices_[vertex_id];
  int top_vertex = edges_[v.north_].head_;
  int right_vertex = edges_[v.east_].head_;
  // check if the DFS hits the diagonal while moving up, in that case fix the x-coordinate and increment the value by 1
  if (vertices_[top_vertex].type_ == Vertex::Type::corner || vertices_[top_vertex].type_ == Vertex::Type::bottom || vertices_[top_vertex].type_ == Vertex::Type::left)
  {
    vertex_x_coord[vertex_id] = val;
    val++;
  }
  // otherwise recurse on the next top vertex
  else
  {
    DFS_BL(top_vertex, val, vertex_x_coord, vertex_y_coord);
    vertex_x_coord[vertex_id] = vertex_x_coord[top_vertex];
  }
  // check if the DFS hits the diagonal while moving right, in that case fix the y-coordinate and increment the value by 1
  if (vertices_[right_vertex].type_ == Vertex::Type::corner || vertices_[right_vertex].type_ == Vertex::Type::bottom || vertices_[right_vertex].type_ == Vertex::Type::left)
  {
    vertex_y_coord[vertex_id] = n_-val;
    val++;
  }
  // otherwise recurse on the next right vertex
  else
  {
    DFS_BL(right_vertex, val, vertex_x_coord,vertex_y_coord);
    vertex_y_coord[vertex_id] = vertex_y_coord[right_vertex];
  }
}
void Rectangulation::DFS_TR(int vertex_id, int& val, std::vector<int>& vertex_x_coord, std::vector<int>& vertex_y_coord)
{
  // This function is a symmetric version of DFS_BL, but starting in the top-right corner.
  Vertex v = vertices_[vertex_id];
  int left_vertex = edges_[v.west_].tail_;
  int bottom_vertex = edges_[v.south_].tail_;
  if (vertices_[bottom_vertex].type_ == Vertex::Type::corner || vertices_[bottom_vertex].type_ == Vertex::Type::right || vertices_[bottom_vertex].type_ == Vertex::Type::top)
  {
    vertex_x_coord[vertex_id] = n_-val;
    val++;
  }
  else
  {
    DFS_TR(bottom_vertex, val, vertex_x_coord,vertex_y_coord);
    vertex_x_coord[vertex_id] = vertex_x_coord[bottom_vertex];
  }
  if (vertices_[left_vertex].type_ == Vertex::Type::corner || vertices_[left_vertex].type_ == Vertex::Type::right || vertices_[left_vertex].type_ == Vertex::Type::top)
  {
    vertex_y_coord[vertex_id] = val;
    val++;
  }
  else
  {
    DFS_TR(left_vertex, val, vertex_x_coord, vertex_y_coord);
    vertex_y_coord[vertex_id] = vertex_y_coord[left_vertex];
  }
}

void Rectangulation::print_coordinates_diagonal()
{
  std::vector<int> vertex_x_coord(2*n_+3,-1);
  std::vector<int> vertex_y_coord(2*n_+3,-1);
  // find bottom-left corner and top-right corner ids
  int BL = -1;
  int TR = -1;
  for (int i = 1; i < 2*n_+3; ++i)
  {
    Vertex v = vertices_[i];
    if (v.north_ == 0 && v.east_ == 0 && v.type_==Vertex::Type::corner)
      TR = i;
    else if (v.south_ == 0 && v.west_ == 0 && v.type_==Vertex::Type::corner)
      BL = i;
  }
  assert(BL != -1 && TR != -1);
  int val = 0;
  DFS_BL(BL,val, vertex_x_coord, vertex_y_coord);
  val = 0;
  DFS_TR(TR,val, vertex_x_coord, vertex_y_coord);
  bool is_first = true;
  bool is_second = true;
  for(auto r : rectangles_)
  {
    if (is_first)
    {
      is_first = false;
      continue;  // skip non-used 0th rectangle
    }
    if (!is_second)
      std::cout << " | ";  // don't print separator before first rectangle
    else
      is_second = false;
    std::cout << vertex_x_coord[r.swest_] << " " << vertex_y_coord[r.swest_] << " ";
    std::cout << vertex_x_coord[r.neast_] << " " << vertex_y_coord[r.neast_];
  }
  std::cout << std::endl;
}

bool Rectangulation::next()
{
  // Run one iteration of the memoryless algorithm, return true if the next rectangulation is not the identity, false otherwise.
  // M3 - Select rectangle
  int j = s_[n_];
  if ((j==1) || ((n_ == 2) && (type_ == Type::baligned)))
    return false;
  // M4 - Jump rectangle
  switch(type_)
  {
    case(Type::generic):
      next_generic(j, o_[j]);
      while (contains_pattern(j))
        next_generic(j, o_[j]);
      break;
    case(Rectangulation::Type::diagonal):
      next_diagonal(j,o_[j]);
      while (contains_pattern(j))
        next_diagonal(j, o_[j]);
      break;
    case(Rectangulation::Type::baligned):
      next_baligned(j,o_[j]);
      while (contains_pattern(j))
        next_baligned(j, o_[j]);
      break;
  }
  // M5 - Update o and s
  s_[n_]=n_;
  if ((type_ == Type::baligned) && (o_[j-1] == Direction::left) && is_bottom_based(j-1))
  {
    o_[j-1] = Direction::right;
    s_[j-1] = s_[j-2];
    s_[j-2] = j-2;
  }
  if (o_[j] == Direction::left && is_bottom_based(j))
  {
    o_[j]=Direction::right;
    s_[j]=s_[j-1];
    s_[j-1]=j-1;
  }
  if ((type_ == Type::baligned) && (o_[j-1] == Direction::right) && is_right_based(j-1))
  {
    o_[j-1] = Direction::left;
    s_[j-1] = s_[j-2];
    s_[j-2] = j-2;
  }
  if ((o_[j] == Direction::right) && is_right_based(j))
  {
    o_[j] = Direction::left;
    s_[j] = s_[j-1];
    s_[j-1] = j-1;
  }
  return true;
}

bool Rectangulation::is_bottom_based(int j)
{
  int a = rectangles_[j].nwest_;
  int alpha = vertices_[a].south_;
  int b = rectangles_[j].swest_;
  if (edges_[alpha].left_ == 0)
      return true;
  if (type_ == Rectangulation::Type::baligned && a == rectangles_[j-1].neast_ && b == rectangles_[j-1].seast_)
  {
    int c = rectangles_[j-1].nwest_;
    int gamma = vertices_[c].south_;
    if (edges_[gamma].left_ == 0)
        return true;
  }
  return false;
}

bool Rectangulation::is_right_based(int j)
{
    int a = rectangles_[j].nwest_;
    int alpha = vertices_[a].east_;
    int b = rectangles_[j].neast_;
    if (edges_[alpha].left_ == 0)
        return true;
    if (type_ == Type::baligned && a == rectangles_[j-1].swest_ && b == rectangles_[j-1].seast_ )
    {
        int c = rectangles_[j].nwest_;
        int gamma = vertices_[c].east_;
        if (edges_[gamma].left_ == 0)
            return true;
    }
    return false;
}

void Rectangulation::remHead(int beta)
{
  // Step 1 - Prepare
  int alpha = edges_[beta].prev_;
  int gamma = edges_[beta].next_;
  int a = edges_[beta].tail_;
  // Step 2 - Update edges/vertices
  if (alpha != 0)
    edges_[alpha].next_ = gamma;
  if (gamma != 0)
    edges_[gamma].prev_ = alpha;
  edges_[gamma].tail_ = a;
  if (edges_[beta].dir_ == Edge::Dir::hor)
    vertices_[a].east_ = gamma;
  else
    vertices_[a].north_ = gamma;
  // Step 3 - Update wall
  int x = edges_[beta].wall_;
  if (edges_[beta].head_ == walls_[x].last_)
    walls_[x].last_ = a;
}

void Rectangulation::remTail(int beta)
{
  // Step 1 - Prepare
  int alpha = edges_[beta].prev_;
  int gamma = edges_[beta].next_;
  int a = edges_[beta].head_;
  // Step 2 - Update edges/vertices
  if (alpha != 0)
    edges_[alpha].next_ = gamma;
  if (gamma != 0)
    edges_[gamma].prev_ = alpha;
  edges_[alpha].head_ = a;
  if (edges_[beta].dir_ == Edge::Dir::hor)
    vertices_[a].west_ = alpha;
  else
    vertices_[a].south_ = alpha;
  // Step 3 - Update wall
  int x = edges_[beta].wall_;
  if (edges_[beta].tail_ == walls_[x].first_)
    walls_[x].first_=a;
}

void Rectangulation::insBefore(int beta, int a, int gamma)
{
  // Step 1 - Prepare
  int alpha = edges_[gamma].prev_;
  int b = edges_[gamma].tail_;
  // Step 2 - Update edges/vertices
  edges_[beta].tail_ = b;
  edges_[beta].head_ = a;
  edges_[beta].prev_ = alpha;
  edges_[beta].next_ = gamma;
  edges_[gamma].tail_ = a;
  edges_[gamma].prev_ = beta;
  if (alpha != 0)
    edges_[alpha].next_ = beta;
  if (edges_[gamma].dir_ == Edge::Dir::hor)
  {
    edges_[beta].dir_ = Edge::Dir::hor;
    vertices_[a].west_ = beta;
    vertices_[a].east_ = gamma;
    vertices_[b].east_ = beta;
  }
  else
  {
    assert(edges_[gamma].dir_ == Edge::Dir::ver);
    edges_[beta].dir_ = Edge::Dir::ver;
    vertices_[a].south_ = beta;
    vertices_[a].north_ = gamma;
    vertices_[b].north_ = beta;
  }
  // Step 3 - Update wall
  edges_[beta].wall_ = edges_[gamma].wall_;
}

void Rectangulation::insAfter(int alpha, int a, int beta)
{
  // Step 1 - Prepare
  int gamma = edges_[alpha].next_;
  int b = edges_[alpha].head_;
  // Step 2 - Updates edges/vertices
  edges_[beta].tail_ = a;
  edges_[beta].head_ = b;
  edges_[beta].prev_ = alpha;
  edges_[beta].next_ = gamma;
  edges_[alpha].head_ = a;
  edges_[alpha].next_ = beta;
  if (gamma != 0)
    edges_[gamma].prev_ = beta;
  if (edges_[alpha].dir_ == Edge::Dir::hor)
  {
    edges_[beta].dir_ = Edge::Dir::hor;
    vertices_[a].west_ = alpha;
    vertices_[a].east_ = beta;
    vertices_[b].west_ = beta;
  }
  else
  {
    assert(edges_[alpha].dir_ == Edge::Dir::ver);
    edges_[beta].dir_ = Edge::Dir::ver;
    vertices_[a].south_ = alpha;
    vertices_[a].north_ = beta;
    vertices_[b].south_ = beta;
  }
  // Step 3 - Update wall
  edges_[beta].wall_ = edges_[alpha].wall_;
}

void Rectangulation::Wjump_hor(int j, Direction dir, int alpha)
{
  if (dir == Direction::left)
  {
    // Step 1 - Prepare
    int a = rectangles_[j].nwest_;
    int beta = vertices_[a].west_;
    assert(alpha == edges_[beta].prev_);
    int k = edges_[alpha].left_;
    // Step 2 - Flip and update rectangles
    remHead(beta);
    insAfter(alpha,a,beta);
    edges_[beta].left_ = k;
    edges_[beta].right_ = j;
  }
  else
  {
    assert(dir == Direction::right);
    // Step 1 - Prepare
    int a = rectangles_[j].nwest_;
    int beta = vertices_[a].east_;
    assert(alpha == edges_[beta].next_);
    int k = edges_[alpha].left_;
    int alpha_prime = edges_[beta].prev_;
    int l = edges_[alpha_prime].right_;
    // Step 2 - Flip and update rectangles
    remTail(beta);
    insBefore(beta,a,alpha);
    edges_[beta].left_ = k;
    edges_[beta].right_ = l;
  }
}

void Rectangulation::Wjump_ver(int j, Direction dir, int alpha)
{
  if (dir == Direction::right)
  {
    // Step 1 - Prepare
    int a = rectangles_[j].nwest_;
    int beta = vertices_[a].north_;
    assert(alpha == edges_[beta].next_);
    int k = edges_[alpha].left_;
    // Step 2 - Flip and update rectangles
    remTail(beta);
    insBefore(beta,a,alpha);
    edges_[beta].left_ = k;
    edges_[beta].right_ = j;
  }
  else
  {
    assert(dir == Direction::left);
    // Step 1 - Prepare
    int a = rectangles_[j].nwest_;
    int beta = vertices_[a].south_;
    assert(alpha == edges_[beta].prev_);
    int k = edges_[alpha].left_;
    int alpha_prime = edges_[beta].next_;
    int l = edges_[alpha_prime].right_;
    // Step 2 - Flip and update rectangles
    remHead(beta);
    insAfter(alpha,a,beta);
    edges_[beta].left_ = k;
    edges_[beta].right_ = l;
  }
}

void Rectangulation::Sjump(int j, Direction d, int alpha)
{
  if (d == Direction::left)
  {
    // Step 1 - Prepare
    int a = rectangles_[j].nwest_;
    int b = rectangles_[j].swest_;
    int c = rectangles_[j].neast_;
    int alpha_prime = vertices_[a].west_;
    int beta = vertices_[a].east_;
    int beta_prime = vertices_[b].west_;
    int gamma = vertices_[c].south_;
    int delta = vertices_[a].south_;
    int c_prime = edges_[beta_prime].tail_;
    int k = edges_[alpha].left_;
    int l = edges_[gamma].right_;
    int x = edges_[delta].wall_;
    // Step 2 - Flip
    remTail(beta);
    remHead(beta_prime);
    insBefore(beta, a, alpha);
    insAfter(gamma, b, beta_prime);
    edges_[delta].dir_ = Edge::Dir::hor;
    edges_[delta].tail_ = a;
    edges_[delta].head_ = b;
    vertices_[a].east_ = delta;
    vertices_[a].west_ = 0;
    vertices_[a].type_ = Vertex::Type::right;
    vertices_[b].east_ = 0;
    vertices_[b].west_ = delta;
    vertices_[b].type_ = Vertex::Type::left;
    walls_[x].first_ = a;
    walls_[x].last_ = b;
    // Step 3 - Update rectangles
    rectangles_[j].neast_ = b;
    rectangles_[j].swest_ = c_prime;
    rectangles_[j-1].neast_ = c;
    rectangles_[j-1].swest_ = a;
    int nu = vertices_[c].west_;
    while (nu != alpha_prime)
    {
      edges_[nu].right_ = j-1;
      nu = edges_[nu].prev_;
    }
    nu = vertices_[c_prime].north_;
    while (nu != alpha)
    {
      edges_[nu].right_= j;
      nu=edges_[nu].next_;
    }
    edges_[beta].left_ = k;
    edges_[beta].right_ = j;
    edges_[beta_prime].left_ = j-1;
    edges_[beta_prime].right_ = l;
  }
  else
  {
    assert(d == Direction::right);
    // this code is obtained by mirroring the case Direction::left along the main diagonal
    // Step 1 - Prepare
    int a = rectangles_[j].nwest_;
    int b = rectangles_[j].neast_;
    int c = rectangles_[j].swest_;
    int alpha_prime = vertices_[a].north_;
    int beta = vertices_[a].south_;
    int beta_prime = vertices_[b].north_;
    int gamma = vertices_[c].east_;
    int delta = vertices_[a].east_;
    int c_prime = edges_[beta_prime].head_;
    int k = edges_[alpha].left_;
    int l = edges_[gamma].right_;
    int x = edges_[delta].wall_;
    // Step 2 - Flip
    remHead(beta);
    remTail(beta_prime);
    insAfter(alpha, a, beta);
    insBefore(beta_prime, b, gamma);
    edges_[delta].dir_ = Edge::Dir::ver;
    edges_[delta].head_ = a;
    edges_[delta].tail_ = b;
    vertices_[a].south_ = delta;
    vertices_[a].north_ = 0;
    vertices_[a].type_ = Vertex::Type::bottom;
    vertices_[b].south_ = 0;
    vertices_[b].north_ = delta;
    vertices_[b].type_ = Vertex::Type::top;
    walls_[x].last_ = a;
    walls_[x].first_ = b;
    // Step 3 - Update rectangles
    rectangles_[j].swest_ = b;
    rectangles_[j].neast_ = c_prime;
    rectangles_[j-1].swest_ = c;
    rectangles_[j-1].neast_ = a;
    int nu = vertices_[c].north_;
    while (nu != alpha_prime)
    {
      edges_[nu].right_ = j-1;
      nu = edges_[nu].next_;
    }
    nu = vertices_[c_prime].west_;
    while (nu != alpha)
    {
      edges_[nu].right_ = j;
      nu=edges_[nu].prev_;
    }
    edges_[beta].left_ = k;
    edges_[beta].right_ = j;
    edges_[beta_prime].left_= j-1;
    edges_[beta_prime].right_= l;
  }
}

void Rectangulation::Tjump_hor(int j, Direction dir, int alpha)
{
  if (dir == Direction::left)
  {
    // Step 1 - Prepare
    int a = rectangles_[j].nwest_;
    int b = edges_[alpha].head_;
    int c = rectangles_[j].neast_;
    int alpha_prime = vertices_[a].west_;
    int beta = vertices_[a].east_;
    int beta_prime = vertices_[a].south_;
    int gamma = vertices_[c].south_;
    int gamma_prime = vertices_[b].south_;
    int k = edges_[beta_prime].left_;
    int l = edges_[gamma].right_;
    int m = edges_[alpha].right_;
    int x = edges_[alpha].wall_;
    int y = edges_[gamma_prime].wall_;
    // Step 2 - Flip
    remTail(beta);
    remTail(beta_prime);
    insAfter(alpha, a, beta);
    insAfter(gamma, b ,beta_prime);
    edges_[beta].head_ = b;
    edges_[gamma_prime].head_ = a;
    vertices_[a].south_ = gamma_prime;
    vertices_[b].west_ = beta;
    walls_[x].last_ = b;
    walls_[y].last_ = a;
    // Step 3 - Update rectangles
    rectangles_[j].neast_ = b;
    rectangles_[k].neast_ = c;
    rectangles_[m].neast_ = a;
    int nu = vertices_[c].west_;
    while (nu != alpha_prime)
    {
      edges_[nu].right_ = k;
      nu = edges_[nu].prev_;
    }
    edges_[beta].left_ = k;
    edges_[beta_prime].right_ = l;
  }
  else
  {
    assert(dir == Direction::right);
    // Step 1 - Prepare
    int a = rectangles_[j].nwest_;
    int b = rectangles_[j].neast_;
    int alpha_prime = vertices_[a].west_;
    int gamma_prime = vertices_[a].south_;
    int beta = vertices_[a].east_;
    int beta_prime = vertices_[b].north_;
    int c = edges_[beta_prime].head_;
    int k = edges_[beta].left_;
    int l = edges_[alpha].left_;
    int m = edges_[alpha_prime].right_;
    int x = edges_[alpha_prime].wall_;
    int y = edges_[gamma_prime].wall_;
    // Step 2 - Flip
    remTail(beta);
    remTail(beta_prime);
    insAfter(alpha, a, beta);
    insAfter(gamma_prime, b ,beta_prime);
    edges_[alpha_prime].head_ = b;
    edges_[beta_prime].head_ = a;
    vertices_[a].south_ = beta_prime;
    vertices_[b].west_ = alpha_prime;
    walls_[x].last_ = b;
    walls_[y].last_ = a;
    // Step 3 - Update rectangles
    rectangles_[j].neast_ = c;
    rectangles_[k].neast_ = a;
    rectangles_[m].neast_ = b;
    int nu = vertices_[c].west_;
    while (nu != alpha)
    {
      edges_[nu].right_ = j;
      nu = edges_[nu].prev_;
    }
    edges_[beta].left_ = l;
    edges_[beta_prime].right_ = j;
  }
}

void Rectangulation::Tjump_ver(int j, Direction dir, int alpha)
{
  // this code is obtained by mirroring Tjump_hor along the main diagonal
  if (dir == Direction::right)
  {
    // Step 1 - Prepare
    int a = rectangles_[j].nwest_;
    int b = edges_[alpha].tail_;
    int c = rectangles_[j].swest_;
    int alpha_prime = vertices_[a].north_;
    int beta = vertices_[a].south_;
    int beta_prime = vertices_[a].east_;
    int gamma = vertices_[c].east_;
    int gamma_prime = vertices_[b].east_;
    int k = edges_[beta_prime].left_;
    int l = edges_[gamma].right_;
    int m = edges_[alpha].right_;
    int x = edges_[alpha].wall_;
    int y = edges_[gamma_prime].wall_;
    // Step 2 - Flip
    remHead(beta);
    remHead(beta_prime);
    insBefore(beta, a, alpha);
    insBefore(beta_prime, b , gamma);
    edges_[beta].tail_ = b;
    edges_[gamma_prime].tail_ = a;
    vertices_[a].east_ = gamma_prime;
    vertices_[a].type_ = Vertex::Type::right;
    vertices_[b].north_ = beta;
    vertices_[b].type_ = Vertex::Type::top;
    walls_[x].first_ = b;
    walls_[y].first_ = a;
    // Step 3 - Update rectangles
    rectangles_[j].swest_ = b;
    rectangles_[k].swest_ = c;
    rectangles_[m].swest_ = a;
    int nu = vertices_[c].north_;
    while (nu != alpha_prime)
    {
      edges_[nu].right_ = k;
      nu = edges_[nu].next_;
    }
    edges_[beta].left_ = k;
    edges_[beta_prime].right_ = l;
  }
  else
  {
    assert(dir == Direction::left);
    // Step 1 - Prepare
    int a = rectangles_[j].nwest_;
    int b = rectangles_[j].swest_;
    int alpha_prime = vertices_[a].north_;
    int gamma_prime = vertices_[a].east_;
    int beta = vertices_[a].south_;
    int beta_prime = vertices_[b].west_;
    int c = edges_[beta_prime].tail_;
    int k = edges_[beta].left_;
    int l = edges_[alpha].left_;
    int m = edges_[alpha_prime].right_;
    int x = edges_[alpha_prime].wall_;
    int y = edges_[gamma_prime].wall_;
    // Step 2 - Flip
    remHead(beta);
    remHead(beta_prime);
    insBefore(beta, a, alpha);
    insBefore(beta_prime, b , gamma_prime);
    edges_[alpha_prime].tail_ = b;
    edges_[beta_prime].tail_ = a;
    vertices_[a].east_ = beta_prime;
    vertices_[a].type_ = Vertex::Type::right;
    vertices_[b].north_ = alpha_prime;
    vertices_[b].type_ = Vertex::Type::top;
    walls_[x].first_ = b;
    walls_[y].first_ = a;
    // Step 3 - Update rectangles
    rectangles_[j].swest_ = c;
    rectangles_[k].swest_ = a;
    rectangles_[m].swest_ = b;
    int nu = vertices_[c].north_;
    while (nu != alpha)
    {
      edges_[nu].right_ = j;
      nu = edges_[nu].next_;
    }
    edges_[beta].left_ = l;
    edges_[beta_prime].right_ = j;
  }
}

void Rectangulation::next_generic(int j, Direction dir)
{
  // N1 - Prepare
  int a = rectangles_[j].nwest_;
  if (dir == Direction::left && vertices_[a].type_ == Vertex::Type::bottom)
  {
    // Preparing horizontal left jump
    int alpha = vertices_[a].west_;
    int beta = vertices_[a].south_;
    int b = edges_[beta].tail_;
    int c = edges_[alpha].tail_;
    // N2 - Execute horizontal left jump
    if (vertices_[c].type_ == Vertex::Type::top)
    {
      int gamma = vertices_[c].west_;
      Wjump_hor(j, Direction::left, gamma);
    }
    else if (vertices_[b].type_ == Vertex::Type::left)
    {
      int gamma = vertices_[b].west_;
      Tjump_hor(j, Direction::left, gamma);
    }
    else
    {
      assert(vertices_[b].type_ == Vertex::Type::top);
      int gamma = vertices_[c].south_;
      Sjump(j, Direction::left, gamma);
    }
  }
  else if (dir == Direction::right && vertices_[a].type_ == Vertex::Type::bottom)
  {
    // Prepare horizontal right jump
    int alpha = vertices_[a].east_;
    int b = edges_[alpha].head_;
    // N3 - Execute horizontal right jump
    if (vertices_[b].type_ == Vertex::Type::top)
    {
      int gamma = vertices_[b].east_;
      Wjump_hor(j, Direction::right, gamma);
    }
    else
    {
      assert(vertices_[b].type_ == Vertex::Type::left);
      int k = edges_[alpha].left_;
      int c = rectangles_[k].nwest_;
      int gamma = vertices_[c].east_;
      Tjump_hor(j, Direction::right, gamma);
    }
  }
  else if (dir == Direction::right && vertices_[a].type_ == Vertex::Type::right)
  {
    // Prepare vertical right jump
    int alpha = vertices_[a].north_;
    int beta = vertices_[a].east_;
    int b = edges_[beta].head_;
    int c = edges_[alpha].head_;
    // N4 - Execute vertical right jump
    if (vertices_[c].type_ == Vertex::Type::left)
    {
      int gamma = vertices_[c].north_;
      Wjump_ver(j, Direction::right, gamma);
    }
    else if (vertices_[b].type_ == Vertex::Type::top)
    {
      int gamma = vertices_[b].north_;
      Tjump_ver(j, Direction::right, gamma);
    }
    else
    {
      assert(vertices_[b].type_ == Vertex::Type::left);
      int gamma = vertices_[c].east_;
      Sjump(j, Direction::right, gamma);
    }
  }
  else
  {
    assert(dir == Direction::left && vertices_[a].type_ == Vertex::Type::right);
    // Prepare vertical left jump
    int alpha = vertices_[a].south_;
    int b = edges_[alpha].tail_;
    // N5 - Execute vertical left jump
    if (vertices_[b].type_ == Vertex::Type::left)
    {
      int gamma = vertices_[b].south_;
      Wjump_ver(j, Direction::left, gamma);
    }
    else
    {
      assert(vertices_[b].type_ == Vertex::Type::top);
      int k = edges_[alpha].left_;
      int c = rectangles_[k].nwest_;
      int gamma = vertices_[c].south_;
      Tjump_ver(j, Direction::left, gamma);
    }
  }
}

void Rectangulation::next_diagonal(int j, Direction dir)
{
  // N1 - Prepare
  int a = rectangles_[j].nwest_;
  if (dir == Direction::left && vertices_[a].type_ == Vertex::Type::bottom)
  {
    // Prepare horizontal left jump
    int alpha = vertices_[a].south_;
    int b = edges_[alpha].tail_;
    // N2 - Horizontal left jump
    if (vertices_[b].type_ == Vertex::Type::left)
    {
      int gamma = vertices_[b].west_;
      Tjump_hor(j, Direction::left, gamma);
    }
    else
    {
      assert(vertices_[b].type_ == Vertex::Type::top);
      int c = rectangles_[j-1].swest_;
      int gamma = vertices_[c].north_;
      Sjump(j, Direction::left, gamma);
    }
  }
  else if (dir == Direction::right && vertices_[a].type_ == Vertex::Type::bottom)
  {
    // Prepare horizontal right jump
    int alpha = vertices_[a].east_;
    // N3 - Horizontal right jump
    int k = edges_[alpha].left_;
    int b = rectangles_[k].neast_;
    int gamma = vertices_[b].west_;
    Tjump_hor(j, Direction::right, gamma);
  }
  else if (dir == Direction::right && vertices_[a].type_ == Vertex::Type::right)
  {
    // Prepare vertical right jump
    int alpha = vertices_[a].east_;
    int b = edges_[alpha].head_;
    // N4 - Vertical right jump
    if (vertices_[b].type_ == Vertex::Type::top)
    {
      int gamma = vertices_[b].north_;
      Tjump_ver(j, Direction::right, gamma);
    }
    else
    {
      assert(vertices_[b].type_ == Vertex::Type::left);
      int c = rectangles_[j-1].neast_;
      int gamma = vertices_[c].west_;
      Sjump(j, Direction::right, gamma);
    }
  }
  else
  {
    assert(dir == Direction::left && vertices_[a].type_ == Vertex::Type::right);
    // Prepare vertical left jump
    int alpha = vertices_[a].south_;
    // N5 - Vertical left jump
    int k = edges_[alpha].left_;
    int b = rectangles_[k].swest_;
    int gamma = vertices_[b].north_;
    Tjump_ver(j, Direction::left, gamma);
  }
}

void Rectangulation::next_baligned(int j, Direction dir)
{
  // N1 - Prepare
  int a = rectangles_[j].nwest_;
  unlock(j, dir);
  if (dir == Direction::left && vertices_[a].type_ == Vertex::Type::bottom)
  {
    int alpha = vertices_[a].south_;
    int b = edges_[alpha].tail_;
    if (vertices_[b].type_ == Vertex::Type::left)
    {
      // N2 - Horizontal left jump (T/TS)
      int gamma = vertices_[b].west_;
      Tjump_hor(j, Direction::left, gamma);
      a = rectangles_[j].nwest_;
      alpha = vertices_[a].south_;
      b = edges_[alpha].tail_;
      int c = rectangles_[j-1].swest_;
      gamma = vertices_[c].north_;
      int c_prime = rectangles_[j].seast_;
      if (vertices_[b].type_ == Vertex::Type::top && (vertices_[c_prime].type_ == Vertex::Type::left || (j == n_ && edges_[gamma].left_ == 0)))
        Sjump(j, Direction::left, gamma);
      lock(j, Edge::Dir::hor);
    }
    else
    {
      assert(vertices_[b].type_ == Vertex::Type::top);
      // N3 - Horizontal left jump (ST/D)
      int c = rectangles_[j-1].swest_;
      int gamma = vertices_[c].north_;
      Sjump(j, Direction::left, gamma);
      gamma = vertices_[c].north_;
      int k = edges_[gamma].left_;
      int c_prime = rectangles_[k].swest_;
      int gamma_prime =  vertices_[c_prime].north_;
      Tjump_ver(j, Direction::left, gamma_prime);
      c = rectangles_[j-1].swest_;
      gamma = vertices_[c].north_;
      int a = edges_[gamma].head_;
      if (vertices_[a].type_ == Vertex::Type::bottom)
      {
        assert(k == j-2);
        c_prime = rectangles_[j-2].swest_;
        gamma_prime = vertices_[c_prime].north_;
        Sjump(j-1, Direction::left, gamma_prime);
      }
      lock(j-1, Edge::Dir::hor);
    }
  }
  else if (dir == Direction::right && vertices_[a].type_ == Vertex::Type::bottom)
  {
    int alpha = vertices_[a].east_;
    // N4 - Horizontal right jump (T/TS)
    int k = edges_[alpha].left_;
    int b = rectangles_[k].neast_;
    int gamma = vertices_[b].west_;
    Tjump_hor(j, Direction::right, gamma);
    a = rectangles_[j].nwest_;
    alpha = vertices_[a].south_;
    b = edges_[alpha].tail_;
    int beta = vertices_[b].south_;
    gamma = vertices_[b].west_;
    int c = edges_[beta].tail_;
    int c_prime = edges_[gamma].tail_;
    if (vertices_[c].type_ == Vertex::Type::top && vertices_[c_prime].type_ == Vertex::Type::right)
    {
      assert(k == j-2);
      int gamma_prime = vertices_[a].west_;
      Sjump(j-1, Direction::right, gamma_prime);
    }
    lock(j, Edge::Dir::ver);
  }
  else if (dir == Direction::right && vertices_[a].type_ == Vertex::Type::right)
  {
    int alpha = vertices_[a].east_;
    int b = edges_[alpha].head_;
    if (vertices_[b].type_ == Vertex::Type::top)
    {
      // N5 - Vertical right jump (T/TS)
      int gamma = vertices_[b].north_;
      Tjump_ver(j, Direction::right, gamma);
      a = rectangles_[j].nwest_;
      alpha = vertices_[a].east_;
      b = edges_[alpha].head_;
      int c = rectangles_[j-1].neast_;
      gamma = vertices_[c].west_;
      int c_prime = rectangles_[j].seast_;
      int e = rectangles_[j-1].nwest_;
      if (vertices_[b].type_ == Vertex::Type::left && (vertices_[c_prime].type_ == Vertex::Type::top || (j == n_ and !(vertices_[e].type_ == Vertex::Type::right and edges_[gamma].tail_==e))))
        Sjump(j, Direction::right, gamma);
      lock(j, Edge::Dir::ver);
    }
    else
    {
      assert(vertices_[b].type_ == Vertex::Type::left);
      // N6 - Vertical right jump (ST/D)
      int c = rectangles_[j-1].neast_;
      int gamma = vertices_[c].west_;
      Sjump(j, Direction::right, gamma);
      gamma = vertices_[c].west_;
      int k = edges_[gamma].left_;
      int c_prime = rectangles_[k].neast_;
      int gamma_prime = vertices_[c_prime].west_;
      Tjump_hor(j, Direction::right, gamma_prime);
      c = rectangles_[j-1].neast_;
      gamma = vertices_[c].west_;
      int a = edges_[gamma].tail_;
      if (vertices_[a].type_ == Vertex::Type::right)
      {
        assert(k == j-2);
        c_prime = rectangles_[j-2].neast_;
        gamma_prime = vertices_[c_prime].west_;
        Sjump(j-1, Direction::right, gamma_prime);
      }
      lock(j-1, Edge::Dir::ver);
    }
  }
  else
  {
    assert(dir == Direction::left && vertices_[a].type_ == Vertex::Type::right);
    int alpha = vertices_[a].south_;
    // N7 - Vertical left jump (T/TS)
    int k = edges_[alpha].left_;
    int b = rectangles_[k].swest_;
    int gamma = vertices_[b].north_;
    Tjump_ver(j, Direction::left, gamma);
    a = rectangles_[j].nwest_;
    alpha = vertices_[a].east_;
    b = edges_[alpha].head_;
    int beta = vertices_[b].east_;
    gamma = vertices_[b].north_;
    int c = edges_[beta].head_;
    int c_prime = edges_[gamma].head_;
    if (vertices_[c].type_ == Vertex::Type::left && vertices_[c_prime].type_ == Vertex::Type::bottom)
    {
      assert(k == j-2);
      int gamma_prime = vertices_[a].north_;
      Sjump(j-1, Direction::left, gamma_prime);
    }
    lock(j, Edge::Dir::hor);
  }
}

void Rectangulation::lock(int j, Edge::Dir dir)
{
  if (dir == Edge::Dir::hor)
  {
    // L1 - Prepare
    int a = rectangles_[j].neast_;
    int b = rectangles_[j].swest_;
    int c = rectangles_[j].seast_;
    int alpha = vertices_[a].west_;
    int beta = vertices_[b].east_;
    if ((vertices_[b].type_ != Vertex::Type::right) || (vertices_[c].type_ != Vertex::Type::left) || (edges_[beta].head_ != c))
      return;
    // L2 - Lock if necessary
    int d = rectangles_[j+1].seast_;
    if (vertices_[d].type_ == Vertex::Type::top)
      Sjump(j+1, Direction::right, alpha);
  }
  else
  {
    assert(dir == Edge::Dir::ver);
    // this code is obtained from the previous case by mirroring along the main diagonal
    // L1 - Prepare
    int a = rectangles_[j].swest_;
    int b = rectangles_[j].neast_;
    int c = rectangles_[j].seast_;
    int alpha = vertices_[a].north_;
    int beta = vertices_[b].south_;
    if ((vertices_[b].type_ != Vertex::Type::bottom) || (vertices_[c].type_ != Vertex::Type::top) || (edges_[beta].tail_ != c))
      return;
    // L2 - Lock if necessary
    int d = rectangles_[j+1].seast_;
    if (vertices_[d].type_ == Vertex::Type::left)
      Sjump(j+1, Direction::left, alpha);
  }
}

void Rectangulation::unlock(int j, Direction dir)
{
  if (dir == Direction::right)
  {
    // U1 - Prepare
    int a = rectangles_[j].neast_;
    int b = rectangles_[j].seast_;
    int c = rectangles_[j].swest_;
    int gamma = vertices_[c].north_;
    // U2 - Unlock if necessary
    if (vertices_[a].type_ == Vertex::Type::bottom && vertices_[b].type_ == Vertex::Type::top)
      Sjump(j+1, Direction::left, gamma);
  }
  else
  {
    // this code is obtained from the previous case by mirroring along the main diagonal
    // U1 - Prepare
    int a = rectangles_[j].swest_;
    int b = rectangles_[j].seast_;
    int c = rectangles_[j].neast_;
    int gamma = vertices_[c].west_;
    // U2 - Unlock if necessary
    if (vertices_[a].type_ == Vertex::Type::right && vertices_[b].type_ == Vertex::Type::left)
      Sjump(j+1, Direction::right, gamma);
  }
}

bool Rectangulation::contains_pattern(int j)
{
  // check containment of each possible pattern
  for (auto p : patterns_)
  {
    switch (p) {
      case Pattern::brick_leftright: if (contains_brick_leftright(j)) return true; break;
      case Pattern::brick_rightleft: if (contains_brick_rightleft(j)) return true; break;
      case Pattern::brick_bottomtop: if (contains_brick_bottomtop(j)) return true; break;
      case Pattern::brick_topbottom: if (contains_brick_topbottom(j)) return true; break;
      case Pattern::wmill_clockwise: if (contains_wmill_clockwise(j)) return true; break;
      case Pattern::wmill_counterclockwise: if (contains_wmill_counterclockwise(j)) return true; break;
      case Pattern::H_vertical: if (contains_H_vertical(j)) return true; break;
      case Pattern::H_horizontal: if (contains_H_horizontal(j)) return true; break;
    }
  }
  return false;
}

bool Rectangulation::contains_brick_leftright(int j)
{
  // C1 - Prepare
  int a = rectangles_[j].nwest_;
  if (vertices_[a].type_ == Vertex::Type::bottom)
    return false;
  assert(vertices_[a].type_ == Vertex::Type::right);
  // C2 - Check
  int alpha = vertices_[a].south_;
  int b = edges_[alpha].tail_;
  if (vertices_[b].type_ == Vertex::Type::left)
    return true;
  else
    return false;
}

bool Rectangulation::contains_brick_rightleft(int j)
{
  // C1 - Prepare
  int a = rectangles_[j].nwest_;
  if (vertices_[a].type_ == Vertex::Type::bottom)
    return false;
  assert(vertices_[a].type_ == Vertex::Type::right);
  // C2 - Check
  int alpha = vertices_[a].north_;
  int b = edges_[alpha].head_;
  if (vertices_[b].type_ == Vertex::Type::left)
    return true;
  else
    return false;
}

bool Rectangulation::contains_brick_bottomtop(int j)
{
  // this code is obtained by mirroring contains_brick_leftright along the main diagonal
  // C1 - Prepare
  int a = rectangles_[j].nwest_;
  if (vertices_[a].type_ == Vertex::Type::right)
    return false;
  assert(vertices_[a].type_ == Vertex::Type::bottom);
  // C2 - Check
  int alpha = vertices_[a].east_;
  int b = edges_[alpha].head_;
  if (vertices_[b].type_ == Vertex::Type::top)
    return true;
  else
    return false;
}

bool Rectangulation::contains_brick_topbottom(int j)
{
  // this code is obtained by mirroring contains_brick_rightleft along the main diagonal
  // C1 - Prepare
  int a = rectangles_[j].nwest_;
  if (vertices_[a].type_ == Vertex::Type::right)
    return false;
  assert(vertices_[a].type_ == Vertex::Type::bottom);
  // C2 - Check
  int alpha = vertices_[a].west_;
  int b = edges_[alpha].tail_;
  if (vertices_[b].type_ == Vertex::Type::top)
    return true;
  else
    return false;
}

bool Rectangulation::contains_wmill_clockwise(int j)
{
  // C1 - Prepare
  int a = rectangles_[j].nwest_;
  if (vertices_[a].type_ == Vertex::Type::bottom)
    return false;
  assert(vertices_[a].type_ == Vertex::Type::right);
  // C2 - Check
  int alpha = vertices_[a].north_;
  int x = edges_[alpha].wall_;
  int b = walls_[x].last_;
  int beta = vertices_[b].east_;
  int y = edges_[beta].wall_;
  int c = walls_[y].last_;
  int gamma = vertices_[c].south_;
  int z = edges_[gamma].wall_;
  int d = walls_[z].first_;
  int delta = vertices_[d].west_;
  if (edges_[delta].right_ == j)
    return true;
  else
    return false;
}

bool Rectangulation::contains_wmill_counterclockwise(int j)
{
  // this code is obtained by mirroring contains_wmill_clockwise along the main diagonal
  // C1 - Prepare
  int a = rectangles_[j].nwest_;
  if (vertices_[a].type_ == Vertex::Type::right)
    return false;
  assert(vertices_[a].type_ == Vertex::Type::bottom);
  // C2 - Check
  int alpha = vertices_[a].west_;
  int x = edges_[alpha].wall_;
  int b = walls_[x].first_;
  int beta = vertices_[b].south_;
  int y = edges_[beta].wall_;
  int c = walls_[y].first_;
  int gamma = vertices_[c].east_;
  int z = edges_[gamma].wall_;
  int d = walls_[z].last_;
  int delta = vertices_[d].north_;
  if (edges_[delta].right_ == j)
    return true;
  else
    return false;
}

bool Rectangulation::contains_H_vertical(int j)
{
  // C1 - Prepare
  int a = rectangles_[j].nwest_;
  if (vertices_[a].type_ == Vertex::Type::bottom)
    return false;
  assert(vertices_[a].type_ == Vertex::Type::right);
  int b = rectangles_[j].swest_;
  // C2 - Go up
  while ((vertices_[b].type_ != Vertex::Type::bottom) && (vertices_[b].type_ != Vertex::Type::corner))
  {
    // C3 - Go left
    int c = b;
    while ((vertices_[c].type_ != Vertex::Type::right) && (vertices_[c].type_ != Vertex::Type::corner))
    {
      if (vertices_[c].type_ == Vertex::Type::top)
      {
        // C4 - Go up
        int d = c;
        while ((d != b) && (vertices_[d].type_ != Vertex::Type::bottom) && (vertices_[d].type_ != Vertex::Type::corner))
        {
          if (vertices_[d].type_ == Vertex::Type::left)
            return true;
          int delta = vertices_[d].north_;
          d = edges_[delta].head_;
        }
      }
      int gamma = vertices_[c].west_;
      c = edges_[gamma].tail_;
    }
    int beta = vertices_[b].north_;
    b = edges_[beta].head_;
  }
  return false;
}

bool Rectangulation::contains_H_horizontal(int j)
{
  // this code is obtained by mirroring contains_H_vertical along the main diagonal
  // C1 - Prepare
  int a = rectangles_[j].nwest_;
  if (vertices_[a].type_ == Vertex::Type::right)
    return false;
  assert(vertices_[a].type_ == Vertex::Type::bottom);
  int b = rectangles_[j].neast_;
  // C2 - Go left
  while ((vertices_[b].type_ != Vertex::Type::right) && (vertices_[b].type_ != Vertex::Type::corner))
  {
    // C3 - Go up
    int c = b;
    while ((vertices_[c].type_ != Vertex::Type::bottom) && (vertices_[c].type_ != Vertex::Type::corner))
    {
      if (vertices_[c].type_ == Vertex::Type::left)
      {
        // C4 - Go left
        int d = c;
        while ((d != b) && (vertices_[d].type_ != Vertex::Type::right) && (vertices_[d].type_ != Vertex::Type::corner))
        {
          if (vertices_[d].type_ == Vertex::Type::top)
            return true;
          int delta = vertices_[d].west_;
          d = edges_[delta].tail_;
        }
      }
      int gamma = vertices_[c].north_;
      c = edges_[gamma].head_;
    }
    int beta = vertices_[b].west_;
    b = edges_[beta].tail_;
  }
  return false;
}
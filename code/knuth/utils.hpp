/*
 * Copyright (c) 2020 Arturo Merino, Ondrej Micka, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_HH
#define UTILS_HH

#include <string>
#include <vector>
#include <cassert>
#include <iostream>

// modified modulo which returns positive result even for negative a (unlike % operator)
inline int mod(int a, int b) {
  return (a % b + b) % b;
}

// compute the GCD of a and b and integers ca and cb such that a*ca+b*cb=GCD() using extended Euclid's algorithm
int gcd(int a, int b);
int ext_gcd(int a, int b ,int &ca, int &cb);

// compute multiplicative inverse of a mod b
int inverse(int a, int b);

// compute the n-th Catalan number modulo 2n+1 using Segner's recurrence relation in time O(n^2)
int catalan(int n);

// integer factorization; the factors are returned in increasing order and with their multiplicities
// running time is O(n)
void factor(int n, std::vector<int> &factors, std::vector<int> &mult);

// check if given number is prime
bool is_prime_power(int n);

// print bitstring (without newline)
void print_bitstring(const std::vector<bool> &x);

// macro that allows us run more complicated code in debug mode without pesky #ifdefs
// usage: if (DEBUG) {...}
#ifdef NDEBUG
#define DEBUG 0
#else
#define DEBUG 1
#endif

#endif
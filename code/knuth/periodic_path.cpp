/*
 * Copyright (c) 2020 Arturo Merino, Ondrej Micka, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <cassert>
#include <iostream>
#include <tuple>
#include <vector>
#include "periodic_path.hpp"
#include "tree.hpp"
#include "utils.hpp"

Periodic_path::Periodic_path(int n, int s) : n_(n), m_(2*n+1), s_(s), x_(m_), match_(m_), centroid_tree_(nullptr), current_tree_(nullptr), subtrees_(n+1, nullptr) {}

Periodic_path::~Periodic_path() {
  delete_subtrees();
  delete_trees();
}

void Periodic_path::init(const std::vector<bool> &start) {
  assert(start.size() == m_ && "length of first bitstring is different from 2n+1");

  x_ = start;

  // count 1s and 0s in x_
  int ones, zeros;
  std::tie(ones, zeros) = count_bits(x_);
  assert(
    (((ones == n_) && (zeros == n_+1)) || ((ones == n_+1) && (zeros == n_))) &&
    "invalid bitstring, bad number of 0s and 1s"
  );
  has_more_ones_ = (ones > zeros);

  dyck_shift_ = dyck_shift(x_);
  build_match_vector();

  // build the trees
  delete_trees();   // get rid of previous trees
  std::vector<bool> x_rotated(m_-1, 0);
  for (int i = 0; i < m_-1; ++i) {
    x_rotated[i] = x_[mod(i + dyck_shift_, m_)];
  }
  centroid_tree_ = new Tree(x_rotated);
  current_tree_ = new Tree(x_rotated);

  // reroot centroid tree to one of the centroids and order subtrees according to rule (T1)
  centroid_tree_->normalize();
  // fill the list of subtrees (also deletes previous subtrees if there were any)
  create_subtrees();
  selected_subtree_ = select_subtree();

  // are we on a reversed path?
  reversed_steps_remaining_ = locate_reversed_path();
  on_reversed_path_ = (reversed_steps_remaining_ != -1);

  assert(check_match_consistency() && "match_ (or x_) is not consistent");
}

void Periodic_path::init(const std::string &start) {
  assert(start.length() == m_ && "length of first bitstring is different from 2n+1");

  // convert string to vector of bits and call the "proper" init
  std::vector<bool> bitstring(m_, false);

  for (int i = 0; i < m_; ++i) {
    assert((start[i] == '0' || start[i] == '1') && "only 0s and 1s allowed in bitstring");
    bitstring[i] = (start[i] == '1');
  }

  init(bitstring);
}

int Periodic_path::dyck_shift(const std::vector<bool> &bitstring) {
  // find the leftmost lowest point of the lattice path, that is where the Dyck path starts
  int height = 0;   // height of lattice path before reading bit i
  int min = 0;
  int min_idx = 0;

  for (int i = 0; i < m_; ++i) {
    // update global minimum
    if (height < min) {
      min = height;
      min_idx = i;
    }
    // take next step
    if (bitstring[i])  height++;
    else               height--;
  }

  assert(bitstring[min_idx] && "first step of Dyck word should be 1");

  int shift = min_idx;

  // If there are more 1s than 0s, then we are not done yet.
  // It may happen that the extra 1 is in the middle of the path,
  // but it must always be at the minimum height.
  if (has_more_ones_) {
    height = 0;
    for (int i = 0; i < m_; ++i) {
      int j = mod(i+shift, m_);
      if (height == 0) {
        min_idx = j;
      }
      if (bitstring[j])  height++;
      else               height--;
    }

    assert(bitstring[min_idx] && "first step should be the trailing 1 in 'more 1s' case");
    assert((height == 1) && "we should end at height 1, since we read one extra 1");
    shift = mod(min_idx+1, m_);
    assert(bitstring[shift] && "first step of Dyck word should be 1");
  }

  return shift;
}

void Periodic_path::build_match_vector() {
  std::vector<int> ones;  // stack of 1-bit positions from x_

  // ignore trailing bit
  match_[(dyck_shift_ - 1 + m_) % m_] = -1;

  // match all remaining bits
  for (int j = 0; j < m_-1; j++) {  // cyclically iterate over x, starting at dyck_shift_
    int i = (j + dyck_shift_) % m_;
    if (x_[i]) {
      ones.push_back(i);
    }
    else {
      match_[i] = ones.back();
      match_[ones.back()] = i;
      ones.pop_back();
    }
  }
}

int Periodic_path::locate_reversed_path() {
  // We walk backwards a few steps and check if any of the bitstrings
  // encountered reverses the path we are located on. This can be detected
  // by looking for a pullable tree that is in the spanning tree T_n.
  int steps;
  for (steps = 0; steps < 5; steps++) {
    apply_f_inverse();
    if (is_applicable(Operation::PULL)) {
      break;
    }
  }
  // undo changes
  for (int i = 0; i < steps; ++i) {
    apply_f();
  }
  if (steps < 5) {   // if terminated by break, there is one extra f_inverse
    apply_f();
  }

  return (steps < 5) ? steps : -1;
}

void Periodic_path::create_subtrees() {
  std::list<Tree::Subtree *> subtree_list;
  centroid_tree_->make_subtree_list(subtree_list);

  // clear previous contents
  delete_subtrees();
  std::fill(subtrees_.begin(), subtrees_.end(), nullptr);

  // put subtrees into appropriate places
  for (Tree::Subtree *s : subtree_list) {
    int id = s->get_root()->get_id();
    subtrees_[id] = s;
  }
}

bool Periodic_path::is_rotation_of(const std::vector<bool> &bitstring, int &right) {
  // check length first
  if (bitstring.size() != m_) return false;

  // check number of 0s and 1s next
  int zeros, ones = 0;
  std::tie(ones, zeros) = count_bits(bitstring);
  if (has_more_ones_ && (ones != n_+1))    return false;
  if (!has_more_ones_ && (zeros != n_+1))  return false;

  // now check all bits, by normalizing strings through
  // rotations according to their Dyck shift
  int bs_dyck_shift = dyck_shift(bitstring);
  for (int k = 0; k < m_; ++k) {
    int i = mod(dyck_shift_+k, m_);
    int j = mod(bs_dyck_shift+k, m_);
    if (x_[i] != bitstring[j])  return false;
  }
  right = mod(dyck_shift_ - bs_dyck_shift, m_);
  return true;
}

int Periodic_path::next() {
  // We sometimes flip more than one bit during this function, but the flips cancel out
  // apart from one single flip that is effectively done.
  // So we keep a list of flipped positions and calculate the effective flip at the end.
  std::vector<int> flips;

  if (on_reversed_path_) {
    // we are on a reversed path
    assert((reversed_steps_remaining_ >= 0) && (reversed_steps_remaining_ <= 4) && "reversed path step counter has invalid value");
    if (reversed_steps_remaining_ > 0) {
      // we continue moving backwards
      flips.push_back(apply_f_inverse());
      reversed_steps_remaining_--;
    }
    else {
      // from start of reversed path jump forward to after the gluing part
      skip_reversed_path(true, flips);
      on_reversed_path_ = false;
    }
  }
  else if (is_applicable(Operation::PULL)) {
    if (DEBUG) std::cout << "pull" << std::endl;
    // we need to jump to another periodic path using the pull operation
    pull(flips);
    flips.push_back(apply_f());
  }
  else if (is_applicable(Operation::PUSH)) {
    if (DEBUG) std::cout << "push" << std::endl;
    // We need to jump to another periodic path using the push operation,
    // landing on a reversed path. We first do the push which gets us back
    // to the tree from where we pulled.
    // Then the actual target bitstring is at the end of the reversed path.
    push(flips);
    skip_reversed_path(true, flips);
    // We are now at the correct place to start moving backwards along a reversed path.
    on_reversed_path_ = true;
    reversed_steps_remaining_ = 4;
  }
  else {
    // no pull/push, so simply apply f
    flips.push_back(apply_f());
  }

  assert(check_match_consistency() && "match_ (or x_) is not consistent");

  // now figure out the effectively flipped bit, by finding the
  // first position that was flipped an odd number of times
  for (int pos : flips) {
    if (std::count(flips.cbegin(), flips.cend(), pos) % 2 == 1) {
      return pos;
    }
  }

  assert(false && "some bit is supposed to be flipped odd number of times");
  return -1;
}

void Periodic_path::rotate_right(int r) {
  std::vector<bool> tmp_x(m_, false);
  std::vector<int> tmp_match(m_, -1);
  for (int i = 0; i < m_; ++i) {
    int j = mod(i + r, m_);
    tmp_x[j] = x_[i];
    // as match_[i] is an index, we must also adjust the value itself (except for the trailing bit)
    if (match_[i] == -1) {
      tmp_match[j] = -1;
    }
    else {
      tmp_match[j] = mod(match_[i] + r, m_);
    }
  }

  x_ = tmp_x;
  match_ = tmp_match;
  dyck_shift_ = mod(dyck_shift_ + r, m_);

  assert(check_match_consistency() && "inconsistent match_ after rotation");
}

int Periodic_path::apply_f() {
  int flip;   // flip position to return

  int i,j, trail;   // positions of: initial bit of the Dyck word, its matched partner bit, trailing 0
  i = dyck_shift_;
  j = match_[i];
  trail = mod(dyck_shift_-1, m_);

  assert(j != -1 && "bit from Dyck word cannot be trailing 0");

  // initial 1 in the Dyck word becomes a 0 and its matched 0 becomes a 1
  // 1u0v0 --> 0u1v0 = u1v00 (after adjusting the Dyck shift)

  if (!has_more_ones_) {
    // we do the first step 1u0v0 --> 1u1v0 = u1v01
    assert(x_[i] && !x_[j] && "initial bit of Dyck word should be 1 and matched bit 0");
    assert(!x_[trail] && "trailing bit has to be 0");

    // flip bit
    x_[j] = 1;
    flip = j;

    // rematch bits
    match_[i] = -1;
    match_bits(j, trail);

    // update Dyck shift
    dyck_shift_++;
    dyck_shift_ %= m_;

    // update current tree
    current_tree_->rotate();
  }
  else {
    // we do the second step u1v01 --> u1v00
    // just flip the trailing bit
    assert(x_[trail] && "trailing bit has to be 1");
    x_[trail] = 0;
    flip = trail;
  }
  // update info on which of the two steps we are in
  has_more_ones_ = !has_more_ones_;

  return flip;
}

int Periodic_path::apply_f_inverse() {
  int flip;   // flip position to return

  int j,i, trail;   // positions of: last bit of the Dyck word, its matched partner bit, trailing 0
  trail = mod(dyck_shift_-1, m_);
  j = mod(trail-1, m_);
  i = match_[j];

  assert(i != -1 && "bit from Dyck word cannot be trailing 0");

  // trailing 0 becomes a 1 and the 1 matched to last 0 becomes a 0
  // u1v00 --> u0v01 = 1u0v0 (after adjusting the Dyck shift)

  if (!has_more_ones_) {
    // we do the step u1v00 --> u1v01
    // just flip the trailing bit
    assert(!x_[trail] && "trailing bit has to be 0");
    x_[trail] = 1;
    flip = trail;
  }
  else {
    // we do the step u1v01 --> u0v01 = 1u0v0
    assert(x_[i] && !x_[j] && "last bit of Dyck word should be 0 and matched bit 1");
    assert(x_[trail] && "trailing bit has to be 1");

    // flip bit
    x_[i] = 0;
    flip = i;

    // rematch bits
    match_[j] = -1;
    match_bits(i, trail);

    // update Dyck shift
    dyck_shift_ = mod(dyck_shift_-1, m_);

    // update current tree
    current_tree_->rotate_inverse();
  }
  // update info on which of the two steps we are in
  has_more_ones_ = !has_more_ones_;

  return flip;
}

void Periodic_path::skip_reversed_path(bool forward, std::vector<int> &flips) {
  // skipping the reversed path takes 5 applications of f or f^{-1}
  for (int i = 0; i < 5; ++i) {
    flips.push_back(forward ? apply_f() : apply_f_inverse());
  }
}

void Periodic_path::pull(std::vector<int> &flips) {
  // shifted x_ has the form 110u0v0
  // the pull operation turns it into 101u0v0
  assert((get_tree_operation() == Operation::PULL) && "tree is not pullable");
  int i = (dyck_shift_+1) % m_;
  int j = (dyck_shift_+2) % m_;
  x_[i] = 0;
  x_[j] = 1;
  match_bits(j, match_[dyck_shift_]);
  match_bits(dyck_shift_, i);

  // update the trees
  modify_tree(Operation::PULL);

  flips.push_back(i);
  flips.push_back(j);
}

void Periodic_path::push(std::vector<int> &flips) {
  // shifted x_ has the form 101u0v0
  // the push operation turns it into 110u0v0
  assert((get_tree_operation() == Operation::PUSH) && "tree is not pushable");
  int i = (dyck_shift_+1) % m_;
  int j = (dyck_shift_+2) % m_;
  x_[i] = 1;
  x_[j] = 0;
  match_bits(dyck_shift_, match_[j]);
  match_bits(i, j);

  // update the trees
  modify_tree(Operation::PUSH);

  flips.push_back(i);
  flips.push_back(j);
}

void Periodic_path::modify_tree(Operation op) {
  assert((op != Operation::NONE) && "NONE operation on tree is invalid");

  if (op == Operation::PULL) {
    Tree::Vertex *vc = to_centroid_tree(current_tree_->get_root()->get_left_child()->get_left_child());
    current_tree_->pull();
    if (vc->is_left_child()) {
      centroid_tree_->pull_to_root(vc);
    }
    else {
      centroid_tree_->pull_from_root(vc);
    }
  }
  else {
    Tree::Vertex *vc = to_centroid_tree(current_tree_->get_root()->get_left_child());
    current_tree_->push();
    if (vc->is_right_child()) {
      centroid_tree_->push_to_root(vc);
    }
    else {
      centroid_tree_->push_from_root(vc);
    }
  }

  centroid_tree_->normalize();
  create_subtrees();
  selected_subtree_ = select_subtree();
}

Tree::Subtree *Periodic_path::select_subtree() {
  if (centroid_tree_->has_two_centroids()) {
    return select_subtree_two_centroids();
  }
  else {
    return select_subtree_one_centroid();
  }
}

Tree::Subtree *Periodic_path::select_subtree_one_centroid() {
  // star defines no arcs in the spanning tree
  if (centroid_tree_->is_star())  return nullptr;

  // shortcut to root's children
  const std::list<Tree::Vertex *> &children = centroid_tree_->get_root()->get_children();
  // constructing the list of subtrees won't make the time complexity worse
  // but makes the work easier
  std::vector<Tree::Subtree *> subtrees;
  std::vector<int> subtree_sizes;
  for (Tree::Vertex *c : children) {
    Tree::Subtree *s = get_subtree(c);
    subtrees.push_back(s);
    subtree_sizes.push_back(s->num_edges_);
  }
  const int num_subtrees = subtrees.size();

  // condition (i): t_i = q1 and t_{i-1} = q0
  for (int i = 0; i < num_subtrees; ++i) {
    int j = mod(i-1, num_subtrees);
    Tree::Subtree *ti = subtrees[i];
    Tree::Subtree *tj = subtrees[j];
    if (
      (ti->bitstring_ == Tree::Subtree::Q_TREES[1]) &&
      (tj->bitstring_ == Tree::Subtree::Q_TREES[0])) {
        return ti;
      }
  }

  // condition (ii): t_i \in {q2, q4} and t_{i+1} \in {q0, q1, q2}
  for (int i = 0; i < num_subtrees; ++i) {
    int j = mod(i+1, num_subtrees);
    Tree::Subtree *ti = subtrees[i];
    Tree::Subtree *tj = subtrees[j];

    // first check t_i
    bool ok = false;
    for (int q : {2, 4}) {
      if (ti->bitstring_ == Tree::Subtree::Q_TREES[q]) {
        ok = true;
        break;
      }
    }
    if (!ok) continue;
    // now check t_{i+1}
    for (int q : {0, 1, 2}) {
      if (tj->bitstring_ == Tree::Subtree::Q_TREES[q]) {
        return ti;
      }
    }
  }

  // condition (iii): t_i \not\in {q0, q1, q2, q4}
  for (Tree::Subtree *ti : subtrees) {
    bool ok = true;
    for (int q : {0, 1, 2, 4}) {
      if (ti->bitstring_ == Tree::Subtree::Q_TREES[q]) {
        ok = false;
        break;
      }
    }
    if (ok) return ti;
  }

  // condition (iv): t_i != q_0
  // that is, root of t_i is leaf
  for (Tree::Vertex *c : children) {
    if (!c->is_leaf()) {
      return get_subtree(c);
    }
  }
  assert (false && "non-star must have at least one non-leaf subtree");
  return nullptr;
}

Tree::Subtree *Periodic_path::select_subtree_two_centroids() {
  Tree::Vertex *root = centroid_tree_->get_root();
  // in dumbbell case, we return the other centroid (which is the left child)
  if (centroid_tree_->is_dumbbell())  {
    return get_subtree(root->get_left_child());
  }
  // simply return the first subtree that is not a leaf and that is not the subtree
  // containing the other centroid (this is the first among the children, which we have to skip)
  for (auto it = std::next(root->get_children().cbegin()); it != root->get_children().cend(); ++it) {
    Tree::Vertex *v = *it;
    if (!v->is_leaf()) {
      return get_subtree(v);
    }
  }
  assert(false && "tree with two centroids must have non-leaf subtree");
  return nullptr;
}

std::pair<Tree::Vertex *, Periodic_path::Operation> Periodic_path::select_leaf(Tree::Subtree *subtree) {
  if (!subtree) {
    return std::make_pair(nullptr, Operation::NONE);
  }
  assert((subtree->num_edges_ > 1) && "cannot select leaf to pull/push from single-edge subtree");
  assert((subtree->get_tree() == centroid_tree_) && "subtree must be a subtree of the centroid tree");

  // push rightmost leaf in case of dumbbell (see rule (D))
  if (centroid_tree_->is_dumbbell()) {
    return std::make_pair(subtree->get_rightmost_leaf(), Operation::PUSH);
  }

  // First there are the cases when the subtree is of the form 11..1 t 00..0 and t is one of the special q_i trees.
  // We refer to t as "appendix" and to the path it is attached to as "initial path".
  // All of the q_i trees start with a single edge, so we must be careful about the boundary between the initial path
  // and the appendix. The initial path is chosen ***maximally*** in our implementation, so its length includes this
  // one edge belonging to q_i.
  int appendix_size = subtree->num_edges_ - subtree->initial_path_length_;
  // if the appendix is small enough, we try whether we match some of the special cases
  if (appendix_size <= Tree::Subtree::Q_TREE_MAX_SIZE) {
    // if end of the initial path is a leaf, then the entire subtree is a path
    if (subtree->initial_path_end_->is_leaf()) {
      // q_1 is a 2-path and subtree cannot be only leaf => subtree matches q_1
      assert((subtree->initial_path_length_ >= 2) && "subtree is a path, and must have at least 2 edges");
      // pull the leftmost leaf
      return std::make_pair(subtree->get_leftmost_leaf(), Operation::PULL);
    }

    // build bitstring representation of the appendix subtree (we know it is of constant size)
    // the bitstring is built adding the 1...0 from the terminal edge of the initial path
    std::list<bool> appendix_string;
    centroid_tree_->get_subtree_bitstring(subtree->initial_path_end_, appendix_string);

    // rule (q137), without q_1 case which was treated before
    for (int i : {3, 7}) {
      if (appendix_string == Tree::Subtree::Q_TREES[i]) {
        // pull leftmost leaf
        return std::make_pair(subtree->get_leftmost_leaf(), Operation::PULL);
      }
    }
    // rule (q24)
    for (int i : {2, 4}) {
      if (appendix_string == Tree::Subtree::Q_TREES[i]) {
        // push rightmost leaf
        return std::make_pair(subtree->get_rightmost_leaf(), Operation::PUSH);
      }
    }
    // rule (q8)
    if (appendix_string == Tree::Subtree::Q_TREES[8]) {
      // pull rightmost leaf
      return std::make_pair(subtree->get_rightmost_leaf(), Operation::PULL);
    }
    // rule (q5)
    if (appendix_string == Tree::Subtree::Q_TREES[5]) {
      Tree::Vertex *leaf = *std::next(subtree->leaves_.begin());
      // push middle leaf
      return std::make_pair(leaf, Operation::PUSH);
    }
  }

  // remaining rules are based on tree potential
  if (centroid_tree_->get_potential() % 2 == 0) {
    // potential is even
    // rule (e)
    return std::make_pair(subtree->get_leftmost_leaf(), Operation::PULL);
  }
  else {
    // potential is odd
    // get rightmost leaf
    Tree::Vertex *leaf = subtree->get_rightmost_leaf();
    if (leaf->is_thin_leaf()) {
      // rule (o1): pull rightmost leaf if it is thin
      return std::make_pair(leaf, Operation::PULL);
    }
    else {
      assert(leaf->is_thick_leaf() && "leaf must be thin or thick");
      // rule (o2): push rightmost leaf if it is thick
      return std::make_pair(leaf, Operation::PUSH);
    }
  }
}

bool Periodic_path::is_applicable(Operation op) {
  assert((op != Operation::NONE) && "NONE is not an applicable tree operation");
  // first check whether tree is even pullable or pushable
  if (op != get_tree_operation()) return false;

  return (is_down_arc(op) || is_up_arc(op));
}

bool Periodic_path::is_down_arc(Operation op) {
  // star has no arcs leading to trees with lower potential
  if (centroid_tree_->is_star()) return false;

  Tree::Vertex *current_root = current_tree_->get_root();
  // We first check whether the current root is in the correct subtree.
  // The tricky part here is to deal with symmetry. Also, we have to be extra
  // careful when the current root is also the root of the centroid tree.
  Tree::Subtree *current_subtree;
  // If the current root is also the root of the centroid tree, then the selected subtree must be the first subtree.
  if (to_centroid_tree(current_root) == centroid_tree_->get_root()) {
    current_subtree = get_subtree(current_root->get_left_child());
  }
  else {
    current_subtree = find_subtree(current_root);
  }

  if (current_subtree != selected_subtree_) {
    // Even though we are not in the selected subtree, there is still a chance, namely symmetry.
    // There may be an automorphism of the tree such that current_subtree == selected_subtree_.
    // In this case we have to adjust the current tree accordingly.
    if (!deal_with_symmetry(current_subtree)) {
      return false;
    }
  }

  // recompute current root and subtree in case they changed
  current_root = current_tree_->get_root();
  if (to_centroid_tree(current_root) == centroid_tree_->get_root()) {
    current_subtree = get_subtree(current_root->get_left_child());
  }
  else {
    current_subtree = find_subtree(current_root);
  }

  // now we are in the correct subtree, so we have to check that the leaf given by rule (T3) is the current one
  Tree::Vertex *cut_leaf;   // leaf that will be moved by op
  if (op == Operation::PULL) {
    cut_leaf = current_root->get_left_child()->get_left_child();
  }
  else {
    cut_leaf = current_root->get_left_child();
  }
  cut_leaf = to_centroid_tree(cut_leaf);

  assert(cut_leaf->is_leaf() && "vertex moved by pull/push is not a leaf");

  // both leaf and operation must match the ones given by rule (T3) from the paper
  return (std::make_pair(cut_leaf, op) == select_leaf(current_subtree));
}

bool Periodic_path::is_up_arc(Operation op) {
  // 1. perform the given operation (only on trees)
  // 2. check if the modified tree has an arc going to myself
  // 3. undo tree operation

  Operation inv_op = inverse_operation(op);
  modify_tree(op);

  bool answer = is_down_arc(inv_op);

  modify_tree(inv_op);

  return answer;
}

Tree::Subtree *Periodic_path::find_subtree(Tree::Vertex *v) {
  assert((v != nullptr) && "cannot find subtree for nullptr");
  v = to_centroid_tree(v);  // make sure v is a vertex of the centroid tree

  if (v->get_parent() == nullptr) return nullptr;

  Tree::Vertex *pred = v;
  v = v->get_parent();
  while(v->get_parent() != nullptr) {
    pred = v;
    v = v->get_parent();
  }
  return get_subtree(pred);
}

bool Periodic_path::deal_with_symmetry(Tree::Subtree* current_subtree) {
  if (centroid_tree_->has_two_centroids()) {
    return deal_with_symmetry_two_centroids(current_subtree);
  }
  else {
    return deal_with_symmetry_one_centroid(current_subtree);
  }
}

bool Periodic_path::deal_with_symmetry_one_centroid(Tree::Subtree *current_subtree) {
  int sym = centroid_tree_->get_symmetry();
  if (!sym) return false;   // no symmetry => no problem
  // We know that if we rotate root's childer by sym, we get an isomorphic tree.
  // Thus, if the current subtree is k*sym possitions to the right of selected_subtree, it is actually
  // the same as selected_subtree.
  // Note 1: It is not enough that current_subtree is isomorphic to selected_subtree_.
  // Note 2: By rule (T2) selected_subtree_ is the *first* subtree such that [...], which is why we only
  // go through the right siblings of the selected subtree.

  Tree::Vertex *v = selected_subtree_->get_root();
  int shift = 0;
  assert((!v->is_right_child()) && "tree has a symmetry but selected subtree is last child -- this should not happen");

  do {
    // ignore the first value of v since we wouldn't be here if current_subtree == selected_subtree_
    v = v->get_right_sibling();
    shift += 1;
    // we found current subtree among root's children
    if (v == current_subtree->get_root()) {
      if (shift % sym == 0) {
        // current_subtree is indeed same as selected_subtree_ up to symmetry
        return true;
      }
      else {
        // no symmetry can map selected_subtree_ to current_subtree
        return false;
      }
    }
  } while (!v->is_right_child());

  // current subtree is somewhere to the left of the selected subtree
  return false;
}

bool Periodic_path::deal_with_symmetry_two_centroids(Tree::Subtree *current_subtree) {
  Tree::Vertex *current_root = current_tree_->get_root();

  // deal with dumbbells first
  if (centroid_tree_->is_dumbbell()) {
    // in the dumbbell case, we only push when the current root is one of the centroids and its second child is the other centroid

    auto centroids = centroid_tree_->get_centroids();
    // The default choice of the leaf to push is the rightmost leaf of the second centroid's subtree.
    // If the current root is the second centroid, then we must "pass" without symmetry.
    if (current_root == to_current_tree(centroids.second)) {
      return false;
    }
    // We are the first centroid; second child (index 1) has to be the other centroid.
    if (current_root->get_child(1) != to_current_tree(centroids.second)) {
      return false;
    }
    symmetry_reroot(centroids.second);
    return true;
  }

  // The tree being symmetrical means that rooting it in either of the two centroids gives the same tree.
  // Thus, the first subtree of the centroid tree is isomorphic to the rest of the tree.
  // The selected subtree is never the first subtree (unless for a dumbbell).
  // So, if the root is not in the first subtree, we know that it cannot be the selected tree up to symmetry.
  if (current_subtree != get_subtree(centroid_tree_->get_root()->get_left_child())) {
    return false;
  }

  // If the current root is the centroid's tree root, then symmetry won't help us, since symmetry would place
  // the current root into the first subtree.
  if (to_centroid_tree(current_root) == centroid_tree_->get_root()) {
    return false;
  }

  // Now check whether the tree actually has symmetries (this is more
  // expensive than the previous checks, that's why we don't do it first).
  if (!centroid_tree_->get_symmetry()) {
    return false;
  }

  // Is the current root the second centroid?
  bool is_second_centroid = (to_centroid_tree(current_root) == centroid_tree_->get_centroids().second);

  // We need to find in which second centroid's subtree our current root is.
  Tree::Vertex *active_subtree;
  if (is_second_centroid) {
    // if the current root is the second centroid, it is easy
    active_subtree = to_centroid_tree(current_root->get_left_child());
    if (active_subtree == centroid_tree_->get_root()) {
      // This would mean that the selected subtree is the subtree with the other centroid, whic his not possible (unless the tree is dumbbell).
      return false;
    }
  }
  else {
    // otherwise we move up to the root
    active_subtree = to_centroid_tree(current_root);
    while (!active_subtree->get_parent()->get_parent()->is_root()) {
      active_subtree = active_subtree->get_parent();
    }
  }

  // Iff both active_subtree and selected_subtree_ are the i-th child of their parent => they are isomorphic.
  if (selected_subtree_->get_root()->get_position() != (1+active_subtree->get_position())) {
    return false;
  }
  // We want to reroot the current tree to get an isomorphic tree but "well-rooted" with respect to the centroid tree.
  // First we actually need to find the image of the current root via the automorphism, since it will become the new root.
  Tree::Vertex *new_root_cen;
  if (is_second_centroid) {
    new_root_cen = centroid_tree_->get_root();
  }
  else {
    // we will move up from current_root and leave breadcrumbs so we can find our way back
    std::vector<int> breadcrumbs;
    Tree::Vertex *v = to_centroid_tree(current_root);
    while (v != active_subtree) {
      breadcrumbs.push_back(v->get_position());
      v = v->get_parent();
    }

    new_root_cen = centroid_tree_->get_root();
    // first descend into the appropriate subtree
    new_root_cen = new_root_cen->get_child(1 + active_subtree->get_position());
    // now follow the breadcrumbs
    while (breadcrumbs.size() > 0) {
      new_root_cen = new_root_cen->get_child(breadcrumbs.back());
      breadcrumbs.pop_back();
    }
  }

  symmetry_reroot(new_root_cen);
  return true;
}

void Periodic_path::symmetry_reroot(Tree::Vertex *new_root) {
  Tree::Vertex *current_root_cur = current_tree_->get_root();
  Tree::Vertex *new_root_cen = to_centroid_tree(new_root);
  // reroot the tree -- we need to choose the first child of the root carefully
  Tree::Vertex *first_child_cen = to_centroid_tree(current_root_cur->get_left_child());
  Tree::Vertex *current_root_cen = to_centroid_tree(current_root_cur);
  // get image of the first child via the automorphism
  // in the centroid tree the first child may be a parent of the current root
  if (current_root_cen->get_parent() == first_child_cen) {
    first_child_cen = new_root_cen->get_parent();
  }
  else {
    int pos = first_child_cen->get_position();
    // first child of centroid's root corresponds to the parent of
    // the second centroid under the automorphism
    if (new_root_cen == centroid_tree_->get_root()) ++pos;
    // the same goes for the current root, but the other way around
    if (current_root_cen == centroid_tree_->get_root()) --pos;
    assert((pos >= 0) && (pos < new_root_cen->get_children().size()) && "pos is out of range of permissible values");
    first_child_cen = new_root_cen->get_child(pos);
  }
  current_tree_->reroot(to_current_tree(new_root_cen));
  current_tree_->make_first_subtree(to_current_tree(first_child_cen));
}

Periodic_path::Operation Periodic_path::inverse_operation(Operation op) {
  switch (op) {
    case (Operation::PULL): return Operation::PUSH;
    case (Operation::PUSH): return Operation::PULL;
    case (Operation::NONE): return Operation::NONE;
  }
  assert (false && "invalid operation");
  return Operation::NONE;
}

std::pair<int, int> Periodic_path::count_bits(std::vector<bool> bitstring) {
  int ones = 0;
  int zeros = 0;
  for (bool b: bitstring) {
    if (b)  ones++;
    else    zeros++;
  }
  return std::make_pair(ones, zeros);
}

bool Periodic_path::operator==(const Periodic_path &p) const {
  if (this->n_ != p.n_) {
    return false;
  }

  // we rotate both vectors by their Dyck shift and compare them bit by bit
  for (int i = 0; i < n_; ++i) {
    int j = mod(i + this->dyck_shift_, m_);
    int k = mod(i + p.dyck_shift_, m_);

    if (this->x_[j] != p.x_[k]) {
      return false;
    }
  }
  return true;
}

#ifndef NDEBUG
void Periodic_path::print_x() {
  for (bool b : x_) {
    std::cout << b;
  }
  std::cout << std::endl;
}

void Periodic_path::draw_trees(const std::string &centroid_fn, const std::string &current_fn) {
  if (centroid_fn != "") centroid_tree_->draw(centroid_fn);
  if (current_fn != "") current_tree_->draw(current_fn);
}

bool Periodic_path::check_match_consistency() {
  std::vector<int> closings;
  // first check the entry for the trailing bit
  if (match_[mod(dyck_shift_-1, m_)] != -1)  return false;

  // traverse rotated x_ and check the matched pairs of bits
  for (int k = 0; k < m_-1; ++k) {
    int i = mod(k+dyck_shift_, m_);
    int j = match_[i];

    // first check that match of match of i is i again
    if (j < 0)  return false;
    if (match_[j] != i)  return false;

    // i and j should not be the same
    if (x_[i] == x_[j]) return false;

    // now check that matched pairs are not interleaved etc.
    if (x_[i]) {
      // x_[i] is opening parentheses, so we expect the closing one later
      closings.push_back(j);
    }
    else {
      // x_[i] is closing parentheses, so it should be on the stack now
      if (closings.back() != i) return false;
      closings.pop_back();
    }
  }
  // stack should be empty
  if (closings.size() > 0)  return false;
  return true;
}
#endif

bool operator==(const std::vector<bool> &vec, const std::list<bool> &list) {
  if (vec.size() != list.size()) {
    return false;
  }
  std::vector<bool> tmp(list.cbegin(), list.cend());
  return vec == tmp;
}

/*
 * Copyright (c) 2016 Joe Sawada, Aaron Williams, Dennis Wong, and Daniel Gabric
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//---------------------------------------------------------------------------
// Listings of 7 de Bruijn sequences via O(n) time and space successor rules
// Four of the rules are based on the PCR and three are based on the CCR
//
// Research by Joe Sawada, Aaron Williams, Dennis Wong, and Daniel Gabric
// Programmed by Joe Sawada, Sept. 2015 - June 2016.
// Some minor modifications for usage within the Combinatorial Object Server
// by Torsten Muetze.
//---------------------------------------------------------------------------

#include<stdio.h>
#define MAX 100

// =====================
// Test if a[1..n] = 0^n
// =====================
int Zeros(int a[], int n) {
    int i;
    for (i=1; i<=n; i++) if (a[i] == 1) return 0;
    return 1;
}
// =============================
// Test if b[1..n] is a necklace
// =============================
int IsNecklace(int b[], int n) {
    int i, p=1;

    for (i=2; i<=n; i++) {
        if (b[i-p] > b[i]) return 0;
        if (b[i-p] < b[i]) p = i;
    }
    if (n % p != 0) return 0;
    return 1;
}
// ===========================================
// Necklace Successor Rules
// ===========================================
int Granddaddy(int a[], int n) {
    int i,j,b[MAX];
    
    j = 2;
    while (j<=n && a[j] == 1) j++;
    for (i=j; i<=n; i++) b[i-j+1] = a[i];
    b[n-j+2] = 0;
    for (i=2; i<j; i++) b[n-j+i+1] = a[i];
    
    if (IsNecklace(b,n)) return 1-a[1];
    return a[1];
}
// -------------------------------
int Grandmama(int a[], int n) {
    int i,j,k,b[MAX];
    
    j = 1;
    while (j<n && a[n-j+1] == 0) b[j++] = 0;
    b[j] = 1;
    k = 2;
    for (i=j+1; i<=n; i++) b[i] = a[k++];
    
    if (IsNecklace(b,n)) return 1-a[1];
    return a[1];
}
// -------------------------------
int PCR3(int a[], int n) {
    int i,b[MAX];
    
    for (i=1; i<n; i++) b[i] = a[i+1];
    b[n] = 1;
    
    if (IsNecklace(b,n)) return 1-a[1];
    return a[1];
}
// -------------------------------
int PCR4(int a[], int n) {
    int i,b[MAX];
    
    b[1] = 0;
    for (i=2; i<=n; i++) b[i] = a[i];
    
    if (IsNecklace(b,n)) return 1-a[1];
    return a[1];
}
// ===========================================
// Co-necklace Successor Rules
// ===========================================
int CCR1(int a[], int n) {
    int i,j,b[MAX],c=1;
    
    for (i=2; i<=n; i++) if (a[i] == 0) break;
    for (j=i; j<=n; j++) b[c++] = a[j];
    b[c++] = 1;
    for (j=2; j<i; j++)  b[c++] = 1-a[j];
    for (i=1; i<=n; i++) b[n+i] = 1-b[i];

    if (IsNecklace(b,2*n)) return a[1];
    return 1-a[1];
}
// -------------------------------
int CCR2(int a[], int n) {
    int i,j,b[MAX],c=1;
    
    i = n;
    while(a[i] == 0 && i >=1) i--;
    if(i == 0) i = n;
    for (j=i+1; j<=n; j++) b[c++] = 0;
    b[c++] = 1;
    for (j=2; j<=i; j++) b[c++] = 1-a[j];
    for (j=1; j<=n; j++) b[n+j] = 1-b[j];
    
    if (IsNecklace(b,2*n)) return a[1];
    return 1-a[1];
}
// -------------------------------
int CCR3(int a[], int n) {
    int i,b[MAX];
    
    for (i=1; i<n; i++) b[i] = a[i+1];
    b[n] = 0;
    for (i=1; i<=n; i++) b[n+i] = 1-b[i];
    
    if (IsNecklace(b,2*n) && !Zeros(b,n)) return a[1];
    return 1-a[1];
}
// =====================================================================
// Generate de Bruijn sequences by iteratively applying a successor rule
// =====================================================================
void DB(int rule, int n) {
    int i, new_bit, a[MAX];

    for (i=1; i<=n; i++) a[i] = 0;   // First n bits
    do {
        printf("%d", a[1]);
        switch(rule) {
            case 1: new_bit = Granddaddy(a,n); break;
            case 2: new_bit = Grandmama(a,n); break;
            case 3: new_bit = PCR3(a,n); break;
            case 4: new_bit = PCR4(a,n); break;
            case 5: new_bit = CCR1(a,n); break;
            case 6: new_bit = CCR2(a,n); break;
            case 7: new_bit = CCR3(a,n); break;
            default: break;
        }
        for (i=1; i<=n; i++) a[i] = a[i+1];
        a[n] = new_bit;
    } while (!Zeros(a,n));
    printf("\n");
}
// -------------------------------
void usage() {
    printf("Usage: db1 [n] [rule] (n>=1, 1<=rule<=7)\n");
}
// -------------------------------
int main(int argc, char **argv) {
    int i, n, rule;
     
    if (argc < 3) {
      usage();
      return 1;
    }
    sscanf(argv[1], "%d", &n);
    sscanf(argv[2], "%d", &rule);
    if ((rule < 1) || (rule > 7) || (n < 1)) {
      usage();
      return 1;
    }
    DB(rule, n);
    return 0;
}

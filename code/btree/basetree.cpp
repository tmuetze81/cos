/*
 * Copyright (c) 2023 Torsten Muetze and Namrata
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "basetree.hpp"

void BaseTree::init()
{
  left_child_.resize(n_+1, 0);
  right_child_.resize(n_+1, 0);
  parent_.resize(n_+1, 0);
}

void BaseTree::insert_leaf(int i)
{
  if (root_ == 0) {
    root_ = i;
    return;
  }
  int cur = root_;
  int prev = 0;
  while (cur != 0) {
    prev = cur;
    if (i < cur) {
      cur = left_child_[cur];
    } else {
      cur = right_child_[cur];
    }
  }
  if (i < prev) {
    left_child_[prev] = i;
    parent_[i] = prev;
  } else {
    right_child_[prev] = i;
    parent_[i] = prev;
  }
  return;
}

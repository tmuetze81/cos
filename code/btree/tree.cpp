/*
 * Copyright (c) 2023 Torsten Muetze and Namrata
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tree.hpp"
#include <algorithm>
#include <cassert>
#include <iostream>
#include <unordered_map>

Tree::Tree(int n, const std::vector<Pattern> patterns)
{
  n_ = n;
  root_ = 0;
  patterns_ = patterns;
  init_tree();
  init_bookkeeping();
}

void Tree::init_tree()
{
  init();
  for (int i = 1; i <= n_ ; i++) {  // start with an all-right tree
    insert_leaf(i);
  }
  return;
}

void Tree::init_bookkeeping()
{
  o_.resize(n_+1, Direction::up);
  s_.resize(n_+1, 0);
  for (int j=1; j<=n_; ++j) {
    s_[j] = j;
  }
}

bool Tree::contains(int node, int pnode, const Pattern &p, std::vector<std::vector<int> > &ctab)
{
  bool l_c = false;  // check if pnode is a left child
  bool r_c = false;  // check if pnode is a right child

  if (pnode == 0) {
    ctab[node][pnode] = 1;
    return true;
  }

  if (node == 0) {
    ctab[node][pnode] = 0;
    return false;
  }
  if (ctab[node][pnode] != -1) {
    // return previously computed table value
    return ctab[node][pnode];
  }

  if (std::count(p.left_child_.begin(), p.left_child_.end(), pnode)) {  // check if pnode is a left child
    l_c = true;;
  }

  if (std::count(p.right_child_.begin(), p.right_child_.end(), pnode)) {  // check if pnode is a right child
    r_c = true;;
  }

  contains(left_child_[node], p.left_child_[pnode], p, ctab);
  contains(right_child_[node], p.right_child_[pnode], p, ctab);
  contains(left_child_[node], pnode, p, ctab);
  contains(right_child_[node], pnode, p, ctab);

  if (p.e_[pnode] == Pattern::Edgetype::cont) {  // contiguous edge
    ctab[node][pnode] = ctab[left_child_[node]][p.left_child_[pnode]] and ctab[right_child_[node]][p.right_child_[pnode]];
  }

  if (p.e_[pnode] == Pattern::Edgetype::non_cont) {  // non-contiguous edge
    ctab[node][pnode] = ((ctab[left_child_[node]][p.left_child_[pnode]] and ctab[right_child_[node]][p.right_child_[pnode]])
                         or (ctab[left_child_[node]][pnode]) or (ctab[right_child_[node]][pnode]) ) ;
  }

  if (p.e_[pnode] == Pattern::Edgetype::semi_cont) {  // semi-contiguous edge
    ctab[node][pnode] = ((ctab[left_child_[node]][p.left_child_[pnode]] and ctab[right_child_[node]][p.right_child_[pnode]])
                        or ((l_c) and ctab[left_child_[node]][pnode]) or ((r_c) and  ctab[right_child_[node]][pnode]) ) ;
  }

  return ctab[node][pnode];
}

bool Tree::avoids()
{
  for (int i = 0; i < patterns_.size(); i++) {
    std::vector<std::vector<int> > ctab(n_+1, std::vector<int>(patterns_[i].n_+1, -1));  // initialize the table with -1
    if (contains(root_, patterns_[i].root_, patterns_[i], ctab)) {
      return false;
    }
  }
  return true;
}

void Tree::slide(int j)
{
  if (o_[j] == Direction::up) {
    while (true) {
      int k =  parent_[j];
      rotate(k, o_[j]);
      if (avoids()) {
        // rotate k up until the tree avoids all patterns
        break;
      }
    }
  } else {
  while (true) {
      rotate(j, o_[j]);
      if (avoids()) {
        // rotate j down until the tree avoids all patterns
        break;
      }
    }
  }
  return;
}

void Tree::rotate(int j, Direction d)
{
  if (d == Direction::down) {
    int parent = parent_[j];
    int left = left_child_[j];
    int left_right =  right_child_[left];

    if (parent != 0) {
      if (right_child_[parent] == j) {
        right_child_[parent] = left;
      } else {
        left_child_[parent] = left;
      }
    }

    if (parent != 0) {
      parent_[left] = parent;
    } else {
      parent_[left] = 0;
    }

    right_child_[left] = j;
    parent_[j] = left;
    left_child_[j] = left_right;

    if (left_right != 0) {
      parent_[left_right] = j;
    }
    if (parent == 0) {
      root_ = left;
    }
  } else {
    int parent = parent_[j];
    int right = right_child_[j];
    int right_left = left_child_[right];

    if (parent != 0) {
      if (left_child_[parent] == j) {
        left_child_[parent] = right;
      } else {
        right_child_[parent] = right;
      }
    }

    if (parent != 0) {
      parent_[right] = parent;
    } else {
      parent_[right] = 0;
    }

    left_child_[right] = j;
    parent_[j] = right;
    right_child_[j] = right_left;

    if (right_left != 0) {
      parent_[right_left] = j;
    }
    if (parent == 0) {
      root_ = right;
    }
  }
}

void Tree::aux_update(int j)
{
  assert(root_ != 0);
  s_[n_] = n_;

  if (o_[j] == Direction::up) {
    if ((parent_[j] == 0) or (j < parent_[j])) {  // if j is a root or if its value is less than its parent
      o_[j] = Direction::down;  // reverse direction
      s_[j] = s_[j-1];  // update s_ array
      s_[j-1] = j-1;
    }
  } else {
    if ((is_leaf(j)) or ((j < right_child_[j]) and left_child_[j] == 0)) {  // if j is a right leaf or if it has only a right child
      o_[j] = Direction::up;  // reverse direction
      s_[j] = s_[j-1];  // update s_ array
      s_[j-1] = j-1;
    }
  }
}

bool Tree::next()
{
  int j = s_[n_];
  if (j == 1) {
    return false;
  }
  slide(j);
  aux_update(j);
  return true;
}

void Tree::print()
{
  print_preorder(root_);
}

void Tree::print_preorder(int i)
{
  if (i == 0) {
    // base case
    return;
  }
  std::cout << i << " ";  // print vertex
  print_preorder(left_child_[i]);  // recurse on left subtree
  print_preorder(right_child_[i]);  // recurse on left subtree
}

void Tree::increase_key(int d)
{
  if (root_!= 0) {
    root_= root_ + d;
  }
  for (int i = 0; i <= n_; i++) {
    if (left_child_[i] != 0) {
      left_child_[i] = left_child_[i] + d;
    }
    if (right_child_[i] != 0) {
      right_child_[i] = right_child_[i] + d;
    }
    if (parent_[i] != 0) {
      parent_[i] = parent_[i] + d;
    }
  }
  return;
}

Tree Tree::merge_trees(const Tree &left, const Tree &right, int root, const std::vector<Pattern> patterns)
{
  int s = left.n_ + right.n_ + 1;
  Tree t(1, patterns);  // create a tree with one vertex and assign it as a root
  t.root_ = root;
  t.n_ = s;
  // reinitialize arrays to store entire tree after the merge
  t.left_child_.resize(t.n_+1, 0);
  t.right_child_.resize(t.n_+1, 0);
  t.parent_.resize(t.n_+1, 0);

  // copy values from the left tree
  t.left_child_.insert(t.left_child_.begin(), left.left_child_.begin(), left.left_child_.end() );
  t.right_child_.insert(t.right_child_.begin(), left.right_child_.begin(), left.right_child_.end() );
  t.parent_.insert( t.parent_.begin(), left.parent_.begin(), left.parent_.end() );

  // copy values from the right tree
  t.left_child_.insert(t.left_child_.begin() + left.n_ + 1, right.left_child_.begin(), right.left_child_.end() );
  t.right_child_.insert(t.right_child_.begin() + left.n_ + 1, right.right_child_.begin(), right.right_child_.end() );
  t.parent_.insert(t.parent_.begin() + left.n_ + 1, right.parent_.begin(), right.parent_.end() );

  // attach the left subtree to the root of the merged tree
  int r = left.root_;
  if (r > 0) {
    t.left_child_[root] = r;
    t.parent_[r] = root;
  }

  // attach the right subtree to the root of the merged tree
  r = right.root_;
  if (r > 0 ) {
    t.right_child_[root] = r;
    t.parent_[r] = root;
  }
  return t;
}

// create an ordered map to store previously computed binary trees with n vertices, n being the key in the map
typedef std::unordered_map<int,std::vector<Tree> > all_bt;
all_bt b;

std::vector <Tree> Tree::construct_all_trees(int n, const std::vector<Pattern> patterns)
{
  // memoization check
  auto found = b.find(n);
  if (found != b.end()) {
    return (found->second);
  }

  std::vector<Tree> list;
  if (n == 0) {
    Tree t(0, patterns);
    list.push_back(t);
  }

  for (int i = 1; i <= n; i++) {
    std::vector<Tree> leftsubtree  = construct_all_trees(i-1, patterns);  // construct all possible left subtrees of i
    std::vector<Tree> rightsubtree = construct_all_trees(n-i, patterns);  // construct all possible right subtrees of i
    // loop through all left and right subtrees of i and connect them with i as a root to create a binary tree
    for (int j = 0; j < leftsubtree.size(); j++) {
      Tree left = leftsubtree[j];
      for (int k = 0; k < rightsubtree.size(); k++) {
        Tree right = rightsubtree[k];
        right.increase_key(i);  // increase the value of each vertex of the right subtree by i
        int root = i;
        Tree t = merge_trees(left, right, root, patterns);
        list.push_back(t);  // add this tree to list
      }
    }
  }
  b.insert(all_bt::value_type(n, std::vector<Tree> (list)));
  return list;
}

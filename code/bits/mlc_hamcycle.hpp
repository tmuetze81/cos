/*
 * Copyright (c) 2018 Torsten Muetze, Jerri Nummenpalo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MLC_HAMCYCLE_HPP
#define MLC_HAMCYCLE_HPP

#include <vector>
#include "mlc_vertex.hpp"
#include "cycle.hpp"

// ####################
// The functions in this file are taken from our previous paper
// [Muetze, Nummenpalo, "A constant-time algorithm for middle levels Gray codes"]
// with some straightforward adaptions.
// ####################

class HamCycle : public AbstractCycle<Vertex> {
public:
  // Compute a Hamilton cycle in the middle levels graph G_k:=Q_{2k+1}(k,k+1)
  // as described in the paper [Muetze, Nummenpalo] in constant average
  // time per vertex. The initialization time is O(k).
  // The first four vertices of the cycle are c_{2k+1,k} = 1^{k}0^{k+1},
  // d_{2k+1,k} = 1^{k}0^{k-1}10, e_{2k+1,k} = 01^{k-1}0^{k-1}10 and
  // f_{2k+1,k} = 01^{k-1}0^{k-2}110.
  // Do not visit the first skip_start many vertices in the beginning.
  // Take a reference to a the calling AbstractCycle instance, which uses
  // the internally produced bitflip sequence (by calling two different member
  // functions of the instance, depending on the value of visit).
  explicit HamCycle(int k, long long limit, long long skip_start, AbstractCycle<std::vector<int> >* caller, bool visit);
  const std::vector<int>& get_first_vertex() const { return x0_.get_bits(); }

  // HamCycle() never calls HamCycle() (itself)
  void visit_f_visit(int j) { }
  void visit_f_log(int j) { }

private:
  AbstractCycle<std::vector<int> >* caller_;
  bool visit_;
  long long skip_start_;

  // #### auxiliary functions ####

  // Execute the given flip sequence seq on the current vertex x_, and do the necessary
  // termination checks along the way. Return value is true if termination criterion
  // is satisfied (break surrounding loop).
  bool flip_seq(const std::vector<int>& seq);
};

#endif

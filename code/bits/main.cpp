/*
 * Copyright (c) 2016 Petr Gregor, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <getopt.h>
#include "cycle.hpp"
#include "enum.hpp"
#include "math.hpp"
#include "sat.hpp"
#include "trim.hpp"

// display help
void help() {
  std::cout << "./cycle [options]   compute saturating cycle/tight enumeration from [Gregor,Muetze]" << std::endl;
  std::cout << "-h                  display this help" << std::endl;
  std::cout << "-n{2,3,...}         length of the bitstrings" << std::endl;
  std::cout << "-k{0,1,...,n}       minimum number of 1's in the bitstring" << std::endl;
  std::cout << "-l{k,k+1,...,n}     maximum number of 1's in the bitstring" << std::endl;
  std::cout << "-L{-1,0,1,2,...}    number of bitstrings to list; -1 for full cycle/enumeration*" << std::endl;
  std::cout << "-t{0,1,2}           trimmed Gray code (0*), sat. cycle (1), tight enum. (2)" << std::endl;
  std::cout << "-d{1,2}             distance-1 steps only (1*), distance-2 steps at boundaries (2) (only for -t0)" << std::endl;
  std::cout << "-s{0,1}             store and print all visited bitstrings (no=0, yes=1*)" << std::endl;
  std::cout << "-p{0,1}             print the flip positions instead of bitstrings (no=0*, yes=1)" << std::endl;
  std::cout << "examples:  ./cycle -n7 -k2 -l4 -t0" << std::endl;
  std::cout << "           ./cycle -n7 -k4 -l4 -t0 -d2" << std::endl;
  std::cout << "           ./cycle -n5 -k2 -l3 -t1 -p1" << std::endl;
  std::cout << "           ./cycle -n17 -k3 -l8 -t1 -s0" << std::endl;
}

void invalid() {
  std::cerr << "invalid combination of parameters n, k, l" << std::endl;
}

void not_implemented() {
  std::cerr << "combination of parameters n, k, l not implemented" << std::endl;
}

// print a vector of integers
inline std::ostream& operator<<(std::ostream& os, const std::vector<int>& x) {
  for (int i = 0; i < x.size(); ++i) {
    os << x[i];
  }
  return os;
}

// user-defined visit function
// x is the current vertex and pos are the positions of the bitflips since the last call
void visit_f_empty(const std::vector<int>& x, const std::vector<int>& pos) {
  // visit vertex y
}

std::vector<std::vector<int> > flip_seq_;  // bitflip sequence

void visit_f_log(const std::vector<int>& x, const std::vector<int>& pos) {
  flip_seq_.push_back(pos);
  visit_f_empty(x, pos);
}

int main(int argc, char *argv[]) {
  int n, k, l;
  bool n_set = false;  // flag whether option -n is present
  bool k_set = false;  // flag whether option -k is present
  bool l_set = false;  // flag whether option -l is present
  long long limit = -1;  // compute all vertices by default
  int type = 0;  // compute trimmed Gray code by default
  bool dist1 = true;  // distance-1 steps only, or distance-2 steps at the boundaries (relevant only for trimmed Gray code)
  bool store_vertices = true;  // store vertices by default
  bool print_flip_pos = false;  // print bitstrings by default

  // process command line options
  int c;
  while ((c = getopt(argc, argv, ":hn:k:l:L:t:d:s:p:")) != -1) {
    switch (c) {
      case 'h':
        help();
        return 0;
      case 'n':
        n = atoi(optarg);
        n_set = true;
        break;
      case 'k':
        k = atoi(optarg);
        k_set = true;
        break;
      case 'l':
        l = atoi(optarg);
        l_set = true;
        break;
      case 'L':
        limit = atoi(optarg);
        if (limit < -1) {
          std::cerr << "option -l must be followed by an integer from {-1,0,1,2,...}" << std::endl;
          return 1;
        }
        break;
      case 't':
        {
        type = atoi(optarg);
        if ((type < 0) || (type > 2)) {
          std::cerr << "option -t must be followed by 0, 1 or 2" << std::endl;
          return 1;
        }
        break;
        }
      case 'd':
        {
        int arg = atoi(optarg);
        if ((arg < 1) || (arg > 2)) {
          std::cerr << "option -d must be followed by 1 or 2" << std::endl;
          return 1;
        }
        dist1 = (arg == 1) ? true : false;
        break;
        }
      case 's':
        {
        int arg = atoi(optarg);
        if ((arg < 0) || (arg > 1)) {
          std::cerr << "option -s must be followed by 0 or 1" << std::endl;
          return 1;
        }
        store_vertices = (bool) arg;
        break;
        }
      case 'p':
        {
        int arg = atoi(optarg);
        if ((arg < 0) || (arg > 1)) {
          std::cerr << "option -p must be followed by 0 or 1" << std::endl;
          return 1;
        }
        print_flip_pos = (bool) arg;
        break;
        }
      case ':':
        std::cerr << "option -" << (char) optopt << " requires an operand" << std::endl;
        return 1;
      case '?':
        std::cerr << "unrecognized option -" << (char) optopt << std::endl;
        return 1;
    }
  }
  if ((!n_set) || (!k_set) || (!l_set)) {
    std::cerr << "options -n, -k and -l are mandatory" << std::endl;
    help();
    return 1;
  }
  // check ranges of n, k, l
  if (n < 2) {
    std::cerr << "option -n must be followed by an integer from {2,3,...}" << std::endl;
    return 1;
  }
  if ((k < 0) || (k > n)) {
    std::cerr << "option -k must be followed by an integer from {0,1,...,n}" << std::endl;
    return 1;
  }
  if ((l < k) || (l > n)) {
    std::cerr << "option -l must be followed by an integer from {k,k+1,...,n}" << std::endl;
    return 1;
  }

  std::vector<int> x;
  long long length;

  if (type == 0) {
    // compute trimmed Gray code in Q_{n,[k,l]}
    // check admissible parameter combinations
    if ((dist1 && (l - k < 2)) || (!dist1 && (((k == 0) && (l == 0)) || ((k == n) && (l == n))))) {
      invalid();
      return 1;
    }
    visit_f_t visit_f = store_vertices ? visit_f_log : visit_f_empty;
    int kp = dist1 ? k : k - 1;
    int lp = dist1 ? l : l + 1;
    long long limitp;
    // determine how many vertices we need to compute until we will be back at the start
    long long d = 0;
    if (dist1) {
      d = (k > 0 ? binom(n - 1, k - 1) : 0) + (l < n ? binom(n - 1, l) : 0);
    }
    limitp = num_vertices_Qnkl(n, k, l) - d;
    if (limit >= 0) {
      limitp = std::min(limitp, limit);
    }
    TrimmedGrayCode gc(n, kp, lp, dist1, limitp, visit_f);
    x = gc.get_first_vertex();
    length = gc.get_length();
  } else if (type == 1) {
    // compute a saturating cycle in Q_{n,[k,l]}
    // check admissible parameter combinations
    if ((k == l) || (((k == 0) && (l == 1)) || ((k == n - 1) && (l == n)))) {
      invalid();
      return 1;
    }
    if (((l - k) % 2 == 1) && (k >= 1) && (k < n / 2) && (l > (n + 1) / 2) && (l <= n-1)) {
      not_implemented();
      return 0;
    }
    visit_f_t visit_f = store_vertices ? visit_f_log : visit_f_empty;
    SatCycle sat(n, k, l, limit, visit_f);
    x = sat.get_first_vertex();
    length = sat.get_length();
  } else {
    assert(type == 2);
    // compute a tight enumeration of the vertices of Q_{n,[k,l]}
    // check admissible parameter combinations
    if ((k == l) && ((l == 0) || (l == n))) {
      invalid();
      return 1;
    }
    if (((l - k) % 2 == 1) && (k >= 1) && (k < n/2) && (l > (n+1)/2) && (l <= n-1)) {
      not_implemented();
      return 0;
    }
    visit_f_t visit_f = store_vertices ? visit_f_log : visit_f_empty;
    TightEnum tenum(n, k, l, limit, visit_f);
    x = tenum.get_first_vertex();
    length = tenum.get_length();
  }

  if (store_vertices) {
    // print vertices encountered along the cycle
    for (int j = 0; j < (int)flip_seq_.size(); ++j) {
      assert((flip_seq_[j].size() == 1) || (flip_seq_[j].size() == 2));
      int i1 = flip_seq_[j][0];
      x[i1] = 1 - x[i1];  // flip first bit
      int i2;
      if (flip_seq_[j].size() == 2) {
        i2 = flip_seq_[j][1];
        x[i2] = 1 - x[i2];  // possibly flip second bit
      }
      if (print_flip_pos) {  // print only flip positions
        if (flip_seq_[j].size() == 2) {
          std::cout << "," << i2;
        }
        std::cout << std::endl;
      } else {  // print actual bitstring
        std::cout << x << std::endl;
      }
    }
    if (limit == flip_seq_.size()) {
      std::cout << "output limit reached" << std::endl; 
    }
  }

  return 0;
}

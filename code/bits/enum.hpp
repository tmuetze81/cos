/*
 * Copyright (c) 2016 Petr Gregor, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TIGHT_HPP
#define TIGHT_HPP

#include <vector>
#include "cycle.hpp"
#include "sat.hpp"

enum cycle_type_t {
  cycle_3path,   // tight enumeration contains a 3-path
  cycle_sw2path  // tight enumeration contains a switched 2-path (see the paper for definitions)
};

// class that represents a stack item

class EnumStackItem {
public:
  // constructor for flip-items
  explicit EnumStackItem(const std::vector<int>& flip_pos) : flip_pos_(flip_pos) { type_ = flip; };
  // constructor for rec-items
  explicit EnumStackItem(cycle_type_t cycle_type, int rec_n, int rec_k, direction_t dir, int num_rots, int last_rot_size) : cycle_type_(cycle_type), rec_n_(rec_n), rec_k_(rec_k), dir_(dir), num_rots_(num_rots), last_rot_size_(last_rot_size) { type_ = recurse; };

  rec_step_type_t type_;
  // ### entries for flip-item ###
  std::vector<int> flip_pos_;  // bit position(s) to flip
  // ### entries for rec-item ###
  cycle_type_t cycle_type_;
  int rec_n_;  // recurse to dimension n
  int rec_k_;  // recurse to level [k,k+1] of Q_n
  direction_t dir_;  // traverse subpath in backward or forward direction
  int num_rots_;  // total number of recursion steps involving a right rotation
  int last_rot_size_;  // size of last recursive substring to rotate
};

// class that represents a tight enumeration
class TightEnum : public AbstractCycle<std::vector<int> > {
public:
  // Compute a tight enumeration in the cube of dimension n between levels k and l.
  explicit TightEnum(int n, int k, int l, long long limit, visit_f_t visit_f);
  const std::vector<int>& get_first_vertex() const { return x0_; }

  void visit_f_visit(int j) {
    int i = pi_[j];
    assert((i >= 0) && (i <= n_ - 1));
    x_[i] = 1 - x_[i];
    #ifndef NVISIT
    std::vector<int> pos(1, i);
    visit_f_(x_, pos);
    #endif
  }

  void visit_f_log(int j) {
    // this logging function is not needed for this part of the code
  }

private:
  std::vector<int> pi_;  // coordinate permutation

  // Compute a tight enumeration of the vertices in an even number of levels entirely above or
  // entirely below the middle level.
  void enum_even(int k, int l, bool complement);

  // Compute a tight enumeration of the vertices in levels k and k+1 below the middle level.
  // If type == cycle_3path and dir == forward, then the first four vertices of the cycle
  // are the vertices a_{n,k}, a_{n,k+1}, b_{n,k} and b_{n,k+1}.
  // If type == cycle_sw2path and dir == forward, then the first three vertices of the cycle
  // are b_{n,k}, b_{n,k+1} and a_{n,k+1}.
  // If dir == backward, then the same cycles are traversed backwards, starting at a_{n,k} or
  // b_{n,k}, and ending in b_{n,k+1}, b_{n,k}, a_{n,k+1}, a_{n,k} or a_{n,k+1}, b_{n,k+1}, b_{n,k},
  // respectively.
  // The first skip_start many vertices encountered along the cycle are neither
  // visited nor recorded.
  // Returns true if the maximum number of visited vertices is reached
  // (break the surrounding loops).
  // This is the algorithm TightEnum() implicitly mentioned in the paper.
  bool enum_2(cycle_type_t type, int k, direction_t dir, long long limit, long long skip_start);

  // Flip the given bit positions in x_, call the visit function and update length_.
  // Returns true if the maximum number of visited vertices is reached.
  bool flip_and_visit(std::vector<int>& pos);

  // compute permutations that map the 3-path
  // (c_{2k+1,k}, d_{2k+1,k+1}, e_{2k+1,k}, f_{2k+1,k}) = (1^{k}0^{k+1}, 1^{k}0^{k-1}10, 01^{k-1}0^{k-1}10, 01^{k-1}0^{k-2}110)
  // onto
  // (a_{2k+1,k}, a_{2k+1,k+1}, b_{2k+1,k}, b_{2k+1,k+1}) = (0^{k+1}1^{k}, 0^{k}1^{k+1}, 0^{k}1^{k}0, 0^{k-1}1^{k+1}0)
  // or (b_{2k+1,k+1}, b_{2k+1,k}, a_{2k+1,k+1}, a_{2k+1,k}), respectively
  void map_cdef_onto_aabb(int k, std::vector<int>& pi);
  void map_cdef_onto_bbaa(int k, std::vector<int>& pi);

  // compute permuted coordinates under right rotations of substrings
  int rot(int i, int n, int num_rots);
  void rot_perm(int n, int num_rots, int last_rot_size, std::vector<int>& pi);
};

#endif

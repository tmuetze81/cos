/*
 * Copyright (c) 2016 Petr Gregor, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <cassert>
#include <vector>
#include "trim.hpp"

TrimmedGrayCode::TrimmedGrayCode(int n, int k, int l, bool dist1, long long limit, visit_f_t visit_f) :
  AbstractCycle(n, limit, visit_f), dist1_(dist1) {
  assert((k >= -1) && (l >= k) && (l <= n+1));  // -1 <= k <= l <= n+1

  // initialize current level
  int c = k + 1;
  // initialize starting and current vertex to 1^c 0^{n-c}
  x0_.resize(n_, 0);
  for (int i = 0; i < c; ++i) {
    x0_[i] = 1;
  }
  x_ = x0_;

  length_ = 0;
  if (length_ == limit_) {  // compute no vertices at all
    return;
  }

  // auxiliary arrays explained in the paper
  std::vector<int> p(n_);
  std::vector<int> nu(n_);
  bool up;

  for (int i = 0; i < c; ++i) { p[i] = c - 1 - i; }  // initialize (p[0],...,p[c-1])
  if (c > 0) { nu[c-1] = 0; }  // initialize (nu[0],...,nu[c-1])
  // Note that arrays in C++ are 0-based, whereas the ones in the paper are 1-based,
  // so a little caution is needed when translating the algorithm from the paper to C++.

  while (true) {
    if (((c % 2 == 0) && x_[0] == 0) || ((c % 2 == 1) && (p[c-1] < n-1) && (x_[p[c-1]+1] == 0))) { up = true; } else { up = false; }
    if (up && (c < l-1)) {  // follow \Gamma_n up
      if (c % 2 == 0) {
        assert(x_[0] == 0); if (flip_and_visit(0, true)) { return; } assert(x_[0] == 1);
        ++c; p[c-1] = 0;
        if (x_[1] == 0) { nu[c-1] = c-1; } else { nu[c-1] = nu[c-2]; }
      } else {
        int i = p[c-1]; assert(x_[i+1] == 0); if (flip_and_visit(i+1, true)) { return; } assert(x_[i+1] == 1);
        ++c; p[c-1] = i; p[c-2] = i+1;
        if ((i+1 == n-1) || (x_[i+2] == 0)) { nu[c-1] = c-2; } else { nu[c-1] = nu[c-3]; }
      }
    } else if (!up && c > k+1) {  // follow \Gamma_n down
      if (c % 2 == 0) {
        assert(x_[0] == 1); if (flip_and_visit(0, true)) { return; } assert(x_[0] == 0);
        --c;
        if (x_[1] == 1) { nu[c-1] = nu[c]; }
      } else {
        int i = p[c-1]; if (i == n-1) { --i; } assert(x_[i+1] == 1); if (flip_and_visit(i+1, true)) { return; } assert(x_[i+1] == 0);
        --c;
        if (c > 0) { p[c-1] = i; nu[c-1] = c-1; }
        if ((i+2 <= n-1) && (x_[i+2] == 1)) { nu[c-2] = nu[c]; }
      }
    } else if (up && (c == l-1)) {  // follow \Gamma_{n,c} through up(x,s_{\Gamma_{n,c}}(x))
      if (c % 2 == 0) {
        int i = p[c-1]; assert(x_[i-1] == 0); if (flip_and_visit(i-1, dist1_)) { return; } assert(x_[i-1] == 1);
                        assert(x_[i] == 1);   if (flip_and_visit(i, true))     { return; } assert(x_[i] == 0);
        p[c-1] = i-1;
        assert(i+1 <= n-1);
        if (x_[i+1] == 1) { nu[c-2] = nu[c-1]; nu[c-1] = c-1; }
      } else {
        int i = p[c-1]; assert(x_[i+1] == 0); if (flip_and_visit(i+1, dist1_)) { return; } assert(x_[i+1] == 1);
                        assert(x_[i] == 1);   if (flip_and_visit(i, true))     { return; } assert(x_[i] == 0);
        p[c-1] = i+1;
        if ((i+2 <= n-1) && (x_[i+2] == 1)) { nu[c-1] = nu[c-2]; }
      }
    } else {  // follow \Gamma_{n,c} through down(x,s_{\Gamma_{n,c}}(x))
      assert(!up && (c == k+1));
      int i;
      if (x_[0] == 0) { i = 0; } else { i = p[nu[c-1]]+1; }  // i is minimal such that x[i] == 0
      if ((c+i) % 2 == 0) {
        assert(x_[i-2] == 1); if (flip_and_visit(i-2, dist1_)) { return; } assert(x_[i-2] == 0);
        assert(x_[i] == 0);   if (flip_and_visit(i, true))     { return; } assert(x_[i] == 1);
        p[nu[c-1]+1] = i-1; p[nu[c-1]] = i;
        if ((i+1 <= n-1) && (x_[i+1] == 1)) { nu[nu[c-1]+1] = nu[nu[c-1]-1]; } else { nu[nu[c-1]+1] = nu[c-1]; }
        if (i > 2) { nu[c-1] = nu[c-1]+2; }
      } else {
        int a, j;
        if (x_[0] == 0) { a = c-1; j = p[a]; } else { a = nu[c-1]-1; j = p[a]; }  // j>i is minimal such that x_[j] == 1
        if (j == n-1) {
          assert(x_[n-1] == 1); if (flip_and_visit(n-1, dist1_)) { return; } assert(x_[n-1] == 0);
          assert(x_[i] == 0);   if (flip_and_visit(i, true))     { return; } assert(x_[i] == 1);
          p[0] = i; nu[c-1] = 0;
        } else if (x_[j+1] == 0) {
          assert(x_[i-1] == 1); if (flip_and_visit(i-1, dist1_)) { return; } assert(x_[i-1] == 0);
          assert(x_[j+1] == 0); if (flip_and_visit(j+1, true))   { return; } assert(x_[j+1] == 1);
          p[nu[c-1]] = j; p[nu[c-1]-1] = j+1;
          if ((j+2 <= n-1) && (x_[j+2] == 1)) { nu[nu[c-1]] = nu[nu[c-1]-2]; } else { nu[nu[c-1]] = nu[c-1]-1; }
          if (i > 1) { nu[c-1] = nu[c-1]+1; }
        } else {
          assert(x_[j+1] == 1); if (flip_and_visit(j+1, dist1_)) { return; } assert(x_[j+1] == 0);
          assert(x_[i] == 0);   if (flip_and_visit(i, true))     { return; } assert(x_[i] == 1);
          p[a] = i; p[a-1] = j;
          if ((j+2 <= n-1) && (x_[j+2] == 1)) { nu[a-2] = nu[a]; }
          if (j == i+1) { nu[c-1] = a-1; } else { nu[a-1] = a-1; nu[c-1] = a;}
        }
      }
    }
  }
}

bool TrimmedGrayCode::flip_and_visit(int i, bool visit) {
  static std::vector<int> buffer;
  assert((0 <= i) && (i <= n_ - 1));
  x_[i] = 1 - x_[i];
  if (visit) {
    #ifndef NVISIT 
    buffer.push_back(i);
    assert((buffer.size() == 1) || (buffer.size() == 2));  // should flip one or two bits
    std::sort(buffer.begin(), buffer.end());  // want flipped coordinate positions to appear in increasing order
    visit_f_(x_, buffer);
    #endif
    buffer.clear();
    ++length_;
    if (length_ == limit_) {
      return true;
    }
  } else {
    buffer.push_back(i);
  }
  return false;
}

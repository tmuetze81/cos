/*
 * Copyright (c) 2016 Petr Gregor, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRIM_HPP
#define TRIM_HPP

#include <vector>
#include "cycle.hpp"

// class that represents a trimmed Gray code

class TrimmedGrayCode : public AbstractCycle<std::vector<int> > {
public:
  // Compute a trimmed Gray code in the cube of dimension n between levels k and l.
  explicit TrimmedGrayCode(int n, int k, int l, bool dist1, long long limit, visit_f_t visit_f);
  const std::vector<int>& get_first_vertex() const { return x0_; }

  // the trimmed Gray code algorithm never calls HamCycle()
  void visit_f_visit(int j) { }
  void visit_f_log(int j) { }

private:
  bool dist1_;  // all generated vertices have distance 1 (true) or distance 2 (false) on the boundary levels k and l

  // #### auxiliary functions ####

  // Flip the given bit in x_ (possibly call the visit function)
  // and update flip_seq_ and length_.
  // Returns true if the maximum number of visited vertices is reached.
  // If visit == false, then the bitflip position is flipped in x_,
  // but length_ is not modified.
  bool flip_and_visit(int i, bool visit);
};

#endif

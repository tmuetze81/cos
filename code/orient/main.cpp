/* 
 * Copyright (c) 2022 Ondrej Micka, Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <cctype>
#include <getopt.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>
#include <algorithm>

#include "graph.hpp"
#include "hypergraph.hpp"
#include "chordal.hpp"
#include "hyperfect.hpp"
#include "interfaces.hpp"
#include "utils.hpp"

#include "tests_chordal.hpp"
#include "tests_hyperfect.hpp"

// display help
void help() {
  std::cout << "./orient [options]   compute acyclic orientations of a chordal graph or" << std::endl;
  std::cout << "                     of a hypergraph in hyperfect elimination order" << std::endl;
  std::cout << "-h                   display this help" << std::endl;
  std::cout << "-e'u1,v1,...;u2,v2,...;...'  (hyper)edge list of the (hyper)graph:" << std::endl;
  std::cout << "                     vertices and edges separated by comma and semicolon, respectively" << std::endl;
  std::cout << "-p'v1,v2,...'        optional (hy)perfect elimination order: vertices are separated by comma;" << std::endl;
  std::cout << "                     if not specified, (H)PEO is computed upon initialization" << std::endl;
  std::cout << "-o{1,2,3}            specify output format of acyclic orientations:" << std::endl;
  std::cout << "                     1: (hyper)edge list (default)" << std::endl;
  std::cout << "                     2: adjacency matrix (unavailable for hypergraphs)" << std::endl;
  std::cout << "                     3: sequence of flipped (hyper)edges" << std::endl;
  std::cout << "-l{-1,0,1,2,...}     number of acyclic orientations to generate; -1 for all" << std::endl;
  std::cout << "-q                   quiet output" << std::endl;
  std::cout << "-c                   output number of acyclic orientations" << std::endl;
  std::cout << "examples:  ./orient -e'1,2;1,3;1,4;2,3;2,4;3,4' -c" << std::endl;
  std::cout << "           ./orient -e'1,2;1,3;1,4;1,5' -c" << std::endl;
  std::cout << "           ./orient -e'1,2;3,4;5,6' -p'6,5,2,1,3,4' -c" << std::endl;
  std::cout << "           ./orient -e'1,2;2,3;1,4;1,2,3;2,3,4' -p'1,2,3,4' -c" << std::endl;
  std::cout << "           ./orient -e'1,2;1,7;1,9;2,9;3,10;3,8;3,4;8,10;8,4;4,10;4,6;6,10;11,6;11,5;11,4;5,6;5,4' -l100" << std::endl;
  std::cout << "           ./orient -e'1,2;1,7;1,9;2,9;3,10;3,8;3,4;8,10;8,4;4,10;4,6;6,10;11,6;11,5;11,4;5,6;5,4' -q -c" << std::endl;
}

// helper function that skips over whitespaces at the current position in the stream
void drop_spaces(std::istream &instream){
  while (!instream.eof() && isspace(instream.peek())){
    instream.ignore();
  }
}

// Return postion of first non-space character in slice string[start:end] or end if there is none
int first_nonspace(const std::string &string, int start, int end){
  assert((start <= end) && "Invalid boundaries");
  assert((end <= string.size()) && "End points beyond the valid part of the string");

  for (int i = start; i < end; ++i){
    if (!isspace(string[i])) return i;
  }
  return end;
}

// Helper function that extracts a vertex name from slice string[start:end]
// Returns empty string if the slice is not valid vertex name
std::string read_vertex(const std::string &string, int start, int end){
  assert((start <= end) && "Invalid boundaries");
  assert((end <= string.size()) && "End points beyond the valid part of the string");
  // Skip initial spaces
  start = first_nonspace(string, start, end);

  // Read until you find a space or end
  int last_nonspace;
  for (last_nonspace = start; last_nonspace < end; ++last_nonspace){
    if (isspace(string[last_nonspace])) break;
  }

  // Check, that only whitespaces remain
  if (first_nonspace(string, last_nonspace, end) != end){
    return std::string();
  }

  return string.substr(start, last_nonspace - start);
}

// read in the hyperedge list from the command line and translate vertex names to integers 1,...,n
// whitespace is ignored
bool read_hyperedge_list(
  const std::string &input_string,
  int &n,
  std::vector<std::vector<int>> &hyperedge_list,
  std::vector<std::string> &vertex_names
){
  std::unordered_map<std::string, int> name_to_id_map;

  n = 0;
  vertex_names.push_back("0");  // Dummy name because of 1-indexed arrays

  // Split input to get hyperedges
  auto edge_slices = split_string(input_string, ';');
  if (edge_slices.empty()){
    std::cerr << "(Hyper)edge list must not be empty" << std::endl;
    return false;
  }

  // Now parse each edge
  for (auto &[edge_start, edge_end] : edge_slices){
    // empty string => error
    if (edge_start == edge_end){
      std::cerr << "(Hyper)edge cannot be empty" << std::endl;
      return false;
    }
    // init edge
    hyperedge_list.emplace_back();
    // Get the vertices
    auto vertex_slices = split_string(input_string, ',', edge_start, edge_end);
    // Now parse the vertices
    for (auto &[vertex_start, vertex_end] : vertex_slices){
      std::string name = read_vertex(input_string, vertex_start, vertex_end);
      if (name == "") {
        std::cerr << "Invalid vertex name." << std::endl;
        std::cerr << "Vertex name must be non-empty a may not contain whitespaces, ',' or ';'" << std::endl;
        return false;
      }
      // Get the id of vertex and push it into the edge
      int id;
      auto found = name_to_id_map.find(name);
      if (found != name_to_id_map.end()){
        // Vertex already has id
        id = found->second;
      }
      else {
        // Vertex is new a needs to get an id
        ++n;
        id = n;
        name_to_id_map[name] = id;
        vertex_names.push_back(name);
      }
      hyperedge_list.back().push_back(id);
    }
  }
  return true;
}

// Check that given list of vectors is a valid hypergraph on n vertices and return true if it is.
// Check tha edges do not contain a "self-loop" and there is no edge multiple times
bool check_hyperedge_list(const int n, const std::vector<std::vector<int>> &hyperedge_list){
  std::vector<std::vector<bool>> bitlist; // List of hyperedges as bitvectors
  
  // Check "self-loops" (and convert edges for the next test)
  for (auto &hyperedge : hyperedge_list){
    std::vector<bool> bitedge(n+1, false);
    for (auto & i : hyperedge){
      if (bitedge[i]){
        assert ( (i <= n) && (i >= 1) && "Invalid vertex id");
        std::cerr << "(hyper)edge cannot contain the same vertex twice" << std::endl;
        return false;
      }
      else {
        bitedge[i] = true;
      }
    }
    bitlist.push_back(bitedge);
  }

  // Check duplicate hyperedges (using the list of converted edges from self-loops)
  std::sort(bitlist.begin(), bitlist.end());
  for (int i = 0; i < bitlist.size() - 1; ++i){
    if (bitlist[i] == bitlist[i+1]){
      std::cerr << "no two (hyper)edges should be identical" << std::endl;
      return false;
    }
  }

  return true;
}

// read perfect elimination order from input
// whitespace is ignored
bool read_peo(const std::string &input_string, const std::vector<std::string> &vertex_names, std::vector<int> &peo){
  const int n = vertex_names.size() - 1;
  peo.push_back(0);   // dummy element since we are 1-indexed

  // Auxiliary map to make translating names to ids easier
  // NOTE: It would be more efficient to get this map from edge_list parsing, instead of building it again,
  // but wouldn't be much faster and I hate how the number of "output" arguments of function increases
  std::unordered_map<std::string, int> names_to_id;
  for (int i = 1; i <= n; ++i){
    names_to_id[vertex_names[i]] = i;
  }

  auto names_slices = split_string(input_string, ',');
  
  if (names_slices.size() != n){
    std::cerr << "PEO/HEO must contain exactly all vertices" << std::endl;
    return false;
  }

  for (auto &[start, end] : names_slices){
    std::string name = input_string.substr(start, end - start);
    auto found = names_to_id.find(name);
    
    // check the vertex is in the graph
    if (found == names_to_id.end()){
      std::cerr << "Invalid vertex name " << name << " (not present in the (hyper)graph)" << std::endl;
      return false;
    }
    
    // push vertex id to the peo list
    peo.push_back(found->second);
  }
  return true;
}

// Possible output formats
enum class Output {edge_list, adj_matrix, flip, none};

int main(int argc, char* argv[]) {
  int n;
  bool e_set = false;  // flag whether option -e is present
  long long steps = -1;  // compute all orientations by default
  bool quiet = false;  // print output by default
  int c;
  bool output_counts = false; // omit counts by default
  int quiet_dot = 10000000;  // print one dot every 10^7 orientations in quiet output mode
  bool p_set = false;   // flag whether option -p is present
  Output out_format = Output::none;   // output format

  std::vector<std::string> vertex_strings;
  std::vector<std::vector<int>> hyperedge_list;
  std::vector<std::pair<int,int>> edge_list;
  std::vector<int> peo;   // PEO or HEO, depending on the (hyper)graph input
  bool is_hypergraph;

  while ((c = getopt (argc, argv, "he:l:qcp:o:")) != -1) {
    switch (c) {
      case 'h':
        help();
        return 0;
      case 'e':
        {
        bool success = read_hyperedge_list(optarg, n, hyperedge_list, vertex_strings);
        success &= check_hyperedge_list(n, hyperedge_list);
        if (!success) {
          return 1;
        }
        e_set = true;
        break;
        }
      case 'p':
        {
        bool success = read_peo(optarg, vertex_strings, peo);
        if (!success) {
          return 1;
        }
        p_set = true;
        break;
        }
      case 'l':
        steps = atoi(optarg);
        if (steps < -1) {
          std::cerr << "option -l must be followed by an integer from {-1,0,1,2,...}" << std::endl;
          return 1;
        }
        break;
      case 'q':
        quiet = true;
        break;
      case 'c':
        output_counts = true;
        break;
      case 'o':
        {
          int out = atoi(optarg);
          switch (out) {
            case 1:
              out_format = Output::edge_list;
              break;
            case 2:
              out_format = Output::adj_matrix;
              break;
            case 3:
              out_format = Output::flip;
              break;
            default:
              std::cerr << "Invalid output option" << std::endl;
              return 1;
          }
          break;
        }
    }
  }
  if (!e_set) {
    std::cerr << "option -e is mandatory" << std::endl;
    help();
    return 1;
  }

  // Decide whether the input is simple graph or general hypergraph
  is_hypergraph = std::any_of(hyperedge_list.cbegin(), hyperedge_list.cend(), 
                              [](const std::vector<int> & hedge) {return hedge.size() != 2;});
  // If we have a simple graph, we need to transform the edges to std::pair
  if (!is_hypergraph){
    std::transform(hyperedge_list.cbegin(), hyperedge_list.cend(), std::back_inserter(edge_list),
                   [](const std::vector<int> h) {return std::pair<int, int>(h[0], h[1]);}
    );
  }

  // Default output is edge list
  if (out_format == Output::none){
    out_format = Output::edge_list;
  }

  if (is_hypergraph && (out_format == Output::adj_matrix)){
    std::cout << "adjacency matrix output option is not supported for hypergraphs" << std::endl;
    return 1;
  }

  if (DEBUG) {
    run_tests_chordal();
    run_tests_hyperfect();
  }

  int num_orientations = 0;
  Checker *ch = is_hypergraph ? 
                  static_cast<Checker *>(new Hyperfect(n, vertex_strings, hyperedge_list)) :
                  static_cast<Checker *>(new Chordal(n, vertex_strings, edge_list));
  if (!p_set){
    // We need to calculate H/PEO ourselves
    if (!ch->is()) {
      std::cerr << "(hyper)graph is not chordal/hyperfect" << std::endl;
      return 1;
    }
    peo = ch->get();
    ch->print();
  }
  else{
    // We were given H/PEO, so we check, whether it is valid
    if (!ch->check(peo)){
      std::cerr << "given PEO/HEO is invalid" << std::endl;
      return 1;
    }
  }

  if (steps == 0) {
    std::cout << "output limit reached" << std::endl;
    return 0;
  }

  Algorithm *ao = is_hypergraph ?
                    static_cast<Algorithm*>(new Acyclic_hyperorientation(n, vertex_strings, hyperedge_list, peo)):
                    static_cast<Algorithm*>(new Acyclic_orientation(n, vertex_strings, edge_list, peo));
  bool next;
  do {
    num_orientations++;
    if (!quiet) {
      std::cout << num_orientations << ": ";
      switch (out_format) {
        case Output::edge_list:
          ao->print_edge_list(std::cout);
          break;
        case Output::adj_matrix:
          ao->print_adj_matrix(std::cout);
          break;
        case Output::flip:
          // There is no flip in first step, so we print the initial orientation
          if (num_orientations == 1){
            ao->print_edge_list(std::cout);
          }
          else{
            ao->print_flip(std::cout);
          }
          break;
        default:
          assert(false && "Error, impossible output format");
      }
    } else if (num_orientations % quiet_dot == 0) {
      std::cout << "." << std::flush;
    }

    next = ao->next();

    if (next && (steps >= 0) && (num_orientations >= steps)) {
      std::cout << "output limit reached" << std::endl;
      break;
    }
  } while (next);
  if (output_counts)
  {
    if (quiet && num_orientations >= quiet_dot)
      std::cout << std::endl;
    std::cout << "number of acyclic orientations: " << num_orientations << std::endl;
  }

  // Be nice and release pointers
  delete ch;
  delete ao;

  return 0;
}

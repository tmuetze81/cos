/* 
 * Copyright (c) 2022 Ondrej Micka, Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include<vector>
#include<string>
#include<tuple>

std::vector<std::pair<std::size_t, std::size_t>>
split_string(
  const std::string &str,
  const char sep,
  std::size_t start,
  std::size_t end
) {
  std::vector<std::pair<std::size_t, std::size_t>> out;
  end = std::min(end, str.size());  // Align the end with the string

  // If the string/slice is empty, return empty list
  if (str.empty() || end <= start){
    return out;
  }

  for (auto i = start; i < end; ++i){
    // separator found => create new substring and move start
    if (str[i] == sep){
      out.emplace_back(start, i);
      start = i+1;
    }
  }
  // Now insert tha last substring
  out.emplace_back(start, end);

  return out;
}
/* 
 * Copyright (c) 2022 Ondrej Micka, Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include<vector>
#include<string>
#include<tuple>
#include<limits>
#include<iostream>

#ifdef NDEBUG
#define DEBUG 0
#else
#define DEBUG 1
#endif

template<typename T> void print_vector(const std::vector<T> &v, const std::string &sep=" "){
  for (const auto &elem : v){
    std::cout << elem << sep;
  }
  std::cout << std::endl;
}

// Split string by given separator, similarly to Python's `str.split(sep)` 
// Instead of returning list of substring, we return boundaries of the substrings.
// Optionally, split only substring str[start : min(end, str.size()) ]
// If the string/substring is empty or start > end, returns empty list
// Example:
// split_string("ab,c,,def", ',', 1, 8) ->
//  { {1, 2}, {3, 4}, {5, 5}, {6, 8} }
// split_string("bagr,kombajn", ',', 1, 1) ->
//  {}
// split_string(",a,b,") ->
//  { {0, 0}, {1, 2}, {3, 4}, {5, 5} }
std::vector<std::pair<std::size_t, std::size_t>>
split_string(
  const std::string &str,
  const char sep,
  std::size_t start = 0,
  std::size_t end = std::numeric_limits<std::size_t>::max()
);
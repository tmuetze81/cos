/* 
 * Copyright (c) 2022 Ondrej Micka, Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <ostream>
#include <vector>

struct Algorithm {
  virtual bool next() = 0;
  virtual void print_edge_list(std::ostream &out) const = 0;
  virtual void print_flip(std::ostream &out) const = 0;
  virtual void print_adj_matrix(std::ostream &out) const = 0;
  virtual void print_state() const = 0;
  virtual ~Algorithm() {};
};

struct Checker {
  virtual bool is() = 0;
  virtual bool check(const std::vector<int> &peo) = 0;
  virtual std::vector<int> get() = 0;
  virtual void print() = 0;
  virtual ~Checker() {};
};
/* 
 * Copyright (c) 2022 Ondrej Micka, Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include<vector>
#include<cassert>
#include<tuple>
#include<algorithm>

#include "graph.hpp"
#include "utils.hpp"

Acyclic_orientation::Acyclic_orientation(
    int n, 
    const std::vector<std::string> &vertex_names, 
    const std::vector<std::pair<int,int>> &edge_list,
    const std::vector<int> &peo
) : n_(n), vertex_names_(vertex_names), edge_list_(edge_list), peo_(peo) 
{
  assert((vertex_names.size() == n+1) && "Number of vertex names does not match the number of vertices");
  assert((peo.size() == n+1) && "Length of PEO does not match the number of vertices");

  // Build inverse PEO and do basic sanity checks
  oep_.resize(n+1, 0);
  for (int i = 1; i <= n_; ++i){
    assert(((peo_[i] >= 1) && (peo_[i] <= n)) && "Invalid PEO value");
    oep_[peo_[i]] = i;
  }

  init_graph();
  init_T_arrays();
  init_alpha();
  init_auxiliary();

  if (DEBUG){
    check_consistency();
  }
}

void Acyclic_orientation::init_graph()
{
  // Create adjacency matrix
  adj_matrix_.resize(n_+1, std::vector<bool>(n_+1, false));
  for (auto [i, j]: edge_list_){
    // First, do some sanity checks
    assert(((i >= 1) && (i <= n_)) && "Invalid vertex");
    assert(((j >= 1) && (j <= n_)) && "Invalid vertex");
    assert((!adj_matrix_[i][j] && !adj_matrix_[j][i]) && "Edge already exists!");
    
    // Edge goes into the vertex with larger PEO
    std::tie(i, j) = peo_sort(i, j);
    adj_matrix_[j][i] = true;
  }
}

void Acyclic_orientation::init_T_arrays() {
  T_arrays_.resize(n_+1, std::vector<int>());

  for (auto [i, j]: edge_list_){
    // Only the vertex with higher PEO gets the neighbor
    std::tie(i, j) = peo_sort(i, j);
    T_arrays_[i].push_back(j);
  }
}

void Acyclic_orientation::init_alpha(){
  alpha_.resize(n_+1, 0);
  rho_ = 0;

  int last_nonmin = oep_[1];    // first vertex with non-zero alpha_ always has value oep_[1] by definition
  for (int i = 1; i <= n_; ++i){
    int v = oep_[i];  // we follow peo order, not the "natural" order

    // vertex is smallest in its component iff its T array is empty
    if (T_arrays_[v].size() > 0){
      alpha_[v] = last_nonmin;
      last_nonmin = v;
    }
  }
  rho_ = last_nonmin;
}

void Acyclic_orientation::init_auxiliary() {
  s_.resize(n_+1, 0);
  for (int i = 1; i <= n_; ++i){
    s_[i] = i;
  }

  o_.resize(n_+1, Direction::to_source);
  t_.resize(n_+1, -1);
}

bool Acyclic_orientation::next(){
  int j = s_[rho_];
  if (peo_[j] == 1){
    return false;
  }

  // Sort neighbors, if necessary
  if (t_[j] == -1){
    std::function<bool(int, int)> cmp_less;
    if (o_[j] == Direction::to_source){
      // Vertex, which is sink among the neighbors must be first in the result
      cmp_less = [this] (int a, int b) { return adj_matrix_[b][a];};
    }
    else{
      cmp_less = [this] (int a, int b) { return adj_matrix_[a][b];};
    }
    std::sort(T_arrays_[j].begin(), T_arrays_[j].end(), cmp_less);
  }

  // Flip the correct arc
  ++t_[j];
  int i = T_arrays_[j][t_[j]];
  flip(i, j);

  if (DEBUG){
    if (o_[j] == Direction::to_source){
      assert((!adj_matrix_[i][j] && adj_matrix_[j][i]) && "Incorrect arc direction after flip");
    }
    else{
      assert((adj_matrix_[i][j] && !adj_matrix_[j][i]) && "Incorrect arc direction after flip");
    }
  }

  // Update auxiliary structures
  s_[rho_] = rho_;
  if (t_[j] == T_arrays_[j].size() - 1){
    o_[j] = reverse_dir(o_[j]);
    t_[j] = -1;
    int k = alpha_[j];
    s_[j] = s_[k];
    s_[k] = k;
  }

  if (DEBUG){
    check_consistency();
  }

  return true;
}

void Acyclic_orientation::flip(int i, int j) {
  assert((adj_matrix_[i][j] != adj_matrix_[j][i]) && "There must be exactly one arc to do the flip!");
  adj_matrix_[i][j] = !adj_matrix_[i][j];
  adj_matrix_[j][i] = !adj_matrix_[j][i];
  flipped_edge = std::pair(i,j);
}

std::pair<int,int> Acyclic_orientation::peo_sort(int i, int j) const{
  if (peo_[i] > peo_[j]){
    return std::pair(i,j);
  }
  else{
    return std::pair(j,i);
  }
}

void Acyclic_orientation::print_adj_matrix(std::ostream &out) const {
  for (int i = 1; i <= n_; ++i){
    for (int j = 1; j <= n_; ++j){
      out << adj_matrix_[peo_[i]][peo_[j]];
    }
    if (i < n_){
      out << ' ';
    }
  }
  out << std::endl;
}

void Acyclic_orientation::print_edge_list(std::ostream &out) const{
  bool start = true;
  for (auto &[u,v] : edge_list_){
    if (!start) out << "; ";
    auto [j,i] = peo_sort(u,v);
    if (adj_matrix_[j][i]){
      out << "[" << peo_[i] << "]";
    }
    else {
      out << peo_[i];
    }
    out << ",";
    if (adj_matrix_[i][j]){
      out << "[" << peo_[j] << "]";
    }
    else {
      out << peo_[j];
    }
    start = false;
  }
  out << std::endl;
}

void Acyclic_orientation::print_flip(std::ostream &out) const{
  out << peo_[flipped_edge.first] << ',' << peo_[flipped_edge.second] << std::endl;
}

void Acyclic_orientation::print_state() const {
  std::cout << "Vertex names:" << std::endl;
  print_vector(vertex_names_);
  std::cout << "PEO:" << std::endl;
  print_vector(peo_);
  std::cout << "OEP:" << std::endl;
  print_vector(oep_);
  std::cout << "Adjacency matrix:" << std::endl;
  for (auto &row : adj_matrix_){
    print_vector(row);
  }
  std::cout << "s:" << std::endl;
  print_vector(s_);
  std::cout << "o:" << std::endl;
  print_vector(o_);
  std::cout << "T:" << std::endl;
  for (int i = 0; i <= n_; ++i){
    std::cout << "  " << i << ": ";
    print_vector(T_arrays_[i]);
  }
  std::cout << "t:" << std::endl;
  print_vector(t_);
  std::cout << "rho: " << rho_ << std::endl;
  std::cout << "alpha: " << std::endl;
  print_vector(alpha_);
}

void Acyclic_orientation::print_dot(std::ostream &out) const {
  out << "graph G{" << std::endl;
  for (int i = 1; i <= n_; ++i){
    out << i << "[label=\"" << i << " (" << peo_[i] << ")\"];" << std::endl;
  }
  for (int i = 1; i <= n_; ++i){
    for (int j = 1; j <= i; ++j){
      if (adj_matrix_[i][j]){
        out << i << " -- " << j << "[dir=forward];" << std::endl;
      }
      if (adj_matrix_[j][i]){
        out << i << " -- " << j << "[dir=back];" << std::endl;
      }
    }
  }
  out << "}" << std::endl;
}

std::ostream &operator<<( std::ostream &output, const Acyclic_orientation::Direction &d){
  switch (d)
  {
  case Acyclic_orientation::Direction::to_source:
    output << '<';
    break;
  case Acyclic_orientation::Direction::to_sink:
    output << '>';
    break;
  }
  return output;
}

void Acyclic_orientation::check_consistency() const {
  check_arc_consistency();
  check_peo_consistency();
}

void Acyclic_orientation::check_arc_consistency() const{
    // Check there are no self-loops or 1-loops
    for (int i = 1; i <= n_; ++i){
        for (int j = 1; j <= i; ++j){
            assert((!adj_matrix_[i][j] || !adj_matrix_[j][i]) && "1-loop or self-loop detected!");
        }
    }

    // Check that T[i] holds only vertices with smaller PEO
    for (int i = 1; i <= n_; ++i){
        for (int j : T_arrays_[i]){
            assert((peo_[j] < peo_[i]) && "T array breaks PEO invariant");
        }
    }

    // Check that adjacency matrix and T arrays contain the same set of arc
    for (int u = 1; u <= n_; ++u){
        for (int v = 1; v <= u; ++v){
            auto [i, j] = peo_sort(u, v);   // We need new pair of variables, since swapping u, v would break the iteration
            auto &Ti = T_arrays_[i];
            if (adj_matrix_[i][j] || adj_matrix_[j][i]){
                assert((std::find(Ti.cbegin(), Ti.cend(), j) != Ti.cend()) && "Arc in adjacency matrix, but not in T array!");
            }
            else{
                assert((std::find(Ti.cbegin(), Ti.cend(), j) == Ti.cend()) && "Arc not in adjacency matrix, but in T array!");
            }
        }
    }
}

void Acyclic_orientation::check_peo_consistency() const{
  for (int i = 1; i <= n_; ++i){
    assert(((peo_[i] >= 1) && (peo_[i] <= n_)) && "Invalid peo value");
    assert(((oep_[i] >= 1) && (oep_[i] <= n_)) && "Invalid oep value");
    assert((peo_[oep_[i]] == i) && "peo and oep are not consistent!");
  }
}

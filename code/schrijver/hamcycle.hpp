#pragma once

#include "vertex.hpp"

class HamCycle {
private:
  Vertex x0_;  // starting vertex
  Vertex x_;  // current vertex
  long long limit_;  // the number of vertices to be visited
  enum class Direction {left, right};  // direction of shift to get the next vertex on the cycle

  bool quiet_;  // don't visit vertices, but only count them
  const int quiet_dot = 10000000;  // print one dot every 10^7 vertices in quiet mode

public:
  // the Hamilton cycle is built in the constructor
  // length is the number of vertices visited in total
  explicit HamCycle(const Vertex& x, long long limit, bool quiet, long long& length);
};

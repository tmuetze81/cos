#include <cstdlib>
#include <iostream>
#include <vector>
#include <getopt.h>

#include "hamcycle.hpp"

// display help
void help() {
  std::cout << "./schrijver [options]  compute a Hamilton cycle in the Schrijver graph S(n,k) or s-stable Kneser graph S(n,k,s)" << std::endl;
  std::cout << "-h                  display this help" << std::endl;
  std::cout << "-n{1,2,...}         list bitstrings of length n" << std::endl;
  std::cout << "-k{1,2,...}         weight of bitstrings" << std::endl;
  std::cout << "-s{2,3,...}         any two 1s in circular distance at least s (default=2)" << std::endl;
  std::cout << "-l{-1,0,1,2,...}    number of bitstrings to list; -1 for full cycle" << std::endl;
  std::cout << "-q                  quiet output" << std::endl;
  std::cout << "-c                  output number of vertices" << std::endl;
  std::cout << "examples:  ./schrijver -n9 -k3" << std::endl;
  std::cout << "           ./schrijver -n15 -k4 -s3 -l50" << std::endl;
  std::cout << "           ./schrijver -n45 -k8 -s4 -q -c" << std::endl;
}

void opt_n_missing() {
  std::cerr << "option -n is mandatory" << std::endl;
}

void opt_k_missing() {
  std::cerr << "option -k is mandatory" << std::endl;
}

int main(int argc, char *argv[]) {
  int n, k;
  int s = 2;  // default value
  bool n_set = false;  // flag whether option -n is present
  bool k_set = false;  // flag whether option -k is present
  bool s_set = false;  // flag whether option -s is present
  long long limit = -1;  // compute all vertices by default
  std::vector<int> x0;  // starting vertex
  bool quiet = false;  // print output by default
  bool output_counts = false;  // omit counts by default

  // process command line options
  int c;
  while ((c = getopt(argc, argv, ":hn:k:s:l:v:qc")) != -1) {
    switch (c) {
      case 'h':
        help();
        return 0;
      case 'n':
        n = atoi(optarg);
        if (n < 1) {
          std::cerr << "option -n must be followed by an integer from {1,2,...}" << std::endl;
          return 1;
        }
        n_set = true;
        break;
      case 'k':
        k = atoi(optarg);
        if (k < 1) {
          std::cerr << "option -k must be followed by an integer from {1,2,...}" << std::endl;
          return 1;
        }
        k_set = true;
        break;
      case 's':
        s = atoi(optarg);
        if (s <= 1) {
          std::cerr << "option -s must be followed by an integer from {2,3,...}" << std::endl;
          return 1;
        }
        s_set = true;
        break;
      case 'l':
        limit = atoi(optarg);
        if (limit < -1) {
          std::cerr << "option -l must be followed by an integer from {-1,0,1,2,...}" << std::endl;
          return 1;
        }
        break;
      case 'q':
        quiet = true;
        break;
      case 'c':
        output_counts = true;
        break;
      case ':':
        std::cerr << "option -" << (char) optopt << " requires an operand" << std::endl;
        return 1;
      case '?':
        std::cerr << "unrecognized option -" << (char) optopt << std::endl;
        return 1;
    }
  }
  if (!n_set) {
    opt_n_missing();
    help();
    return 1;
  }

  if (!k_set) {
    opt_k_missing();
    help();
    return 1;
  }

  if (n < s*k+1) {
    if (s_set) {
      std::cerr << "n must be greater or equal to s*k+1" << std::endl;
    } else {
      std::cerr << "n must be greater or equal to " << s << "k+1" << std::endl;
    }
  	return 1;
  }

  x0.resize(n,0);

  // set starting vertex
  for (int i=0; i<k ; i++) {
    x0[i*s] = 1;
  }

  // compute the Hamilton cycle
  Vertex x(x0, s);
  long long length;
  HamCycle hc(x, limit, quiet, length);
  if (quiet) {
    std::cout << std::endl;
  }
  if (output_counts) {
    std::cout << "number of vertices: " << length << std::endl;
  }

  return 0;
}

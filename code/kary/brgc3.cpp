/*
 * Copyright (c) 2024 Rohan Acharya, Torsten Muetze, and Francesco Verciani
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <vector>
#include "brgc.hpp"
#include "brgc3.hpp"

Brgc3::Brgc3(int n) : n_(n), brgc0_(n_-1), brgc1_(n_-1)
{
  x_.resize(n_, 0);
}

bool Brgc3::next(int &pos)
{
  if (x_[0] == 0) {
    int p;
    bool next = brgc0_.next(p);
    if (next) {
      int ps = p+1;  // shifted index
      x_[ps] = 1-x_[ps];
      pos = ps;
      return true;
    } else {
      // flip first bit to transition to second half
      x_[0] = 1;
      pos = 0; 
      return true;
    }    
  } else {
    int p;
    bool next = brgc1_.next(p);
    if (next) {
      int ps = p+2;  // shifted index
      if (ps == n_) {  // wrap around cyclically
        ps = 1;
      } 
      x_[ps] = 1-x_[ps];
      pos = ps;
      return true;
    } else {
      return false;
    }
  }
}

void Brgc3::print()
{
  for (int i=0; i<n_; i++) {
    std::cout << x_[i]; 
  }
  std::cout << std::endl;
}
/*
 * Copyright (c) 2024 Rohan Acharya, Torsten Muetze, and Francesco Verciani
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <vector>
#include "brgc.hpp"

Brgc::Brgc(int n) : n_(n)
{
  x_.resize(n_, 0);
  init_bookkeeping();
}

void Brgc::init_bookkeeping()
{
  // for a bitstring of length n, we need jumping values 1,2,...,n+1 (0 is not used)
  // and the values 2,...,n+1 correspond to the bits at position 0,...,n-1
  s_.resize(n_+2, 0);
  for (int j=1; j<=n_+1; ++j) {
    s_[j] = j;
  }
}

bool Brgc::next(int &pos)
{
  int j = s_[n_+1];
  if (j == 1) {
    return false;
  }

  x_[j-2] = 1-x_[j-2];  // jumping values are 2,...,n+1, and here we scale them to 0,...,n-1
  pos = j-2;
  
  s_[n_+1] = n_+1;
  s_[j] = s_[j-1];
  s_[j-1] = j-1;

  return true;
}

void Brgc::print()
{
  for (int i=0; i<n_; i++) {
    std::cout << x_[i]; 
  }
  std::cout << std::endl;
}
/*
 * Copyright (c) 2019 Ondrej Micka, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <iostream>
#include <vector>
#include "chains.hpp"

Chains::Chains(int n, bool full_cycle, visit_f_t visit_f, int limit) : n_(n), full_cycle_(full_cycle), visit_f_(visit_f), count_(0), limit_(limit) {
  m_ = n_/2;
  if (n_ == 2*m_) {
    chains_even();
  } else {
    chains_odd();
  }
}

bool Chains::visit() {
  if ((limit_ != -1) && (count_ == limit_)) {
    return true;
  }    
  if (full_cycle_) {
    // visit all bitstrings along the chain
    visit_f_(x_);
    count_++;
    if ((limit_ != -1) && (count_ == limit_)) {
      return true;
    }    
    if (up_) {
      int i = s_[0];
      while (i <= n_) {
        x_[i] = 1;
        visit_f_(x_);
        count_++;
        if ((limit_ != -1) && (count_ == limit_)) {
          return true;
        }
        i = s_[i];
      }
    } else {
      int i = t_[n_+1];
      while (i >= 1) {
        x_[i] = 0;
        visit_f_(x_);
        count_++;
        if ((limit_ != -1) && (count_ == limit_)) {
          return true;
        }        
        i = t_[i];
      }
    }
  } else {
    // visit only the chain
    visit_f_(c_);
    count_++;
    if ((limit_ != -1) && (count_ == limit_)) {
      return true;
    }
  }
  return false;
}

void Chains::first(int i) {
  int alpha = m_ - i + 1;  // first position of subchain
  int beta = s_[alpha];  // next * to the right of position alpha
  int gamma = s_[beta];  // next * to the right of position beta
  assert(c_[alpha] == 2);
  assert(c_[beta] == 2);

  c_[alpha] = 0;  // match two *s by 01
  c_[beta] = 1;
  p_[alpha] = beta;  // store matched positions
  p_[beta] = alpha;
  s_[alpha-1] = gamma;  // remove *s from lists
  t_[gamma] = alpha-1;
  if (up_) {
    x_[beta] = 1;
  } else {
    x_[alpha] = 0;
  }
}

void Chains::first_inv(int i) {
  int alpha = m_ - i + 1;  // first position of subchain
  int beta = p_[alpha];  // 1 matched to 0 in first position
  int gamma = s_[alpha-1];  // next * to the right of this matched pair
  assert(c_[alpha] == 0);
  assert(c_[beta] == 1);

  c_[alpha] = 2;  // unmatch 01 and replace by two *s
  c_[beta] = 2;
  s_[alpha-1] = alpha;  // insert two new *s into the lists
  s_[alpha] = beta;
  s_[beta] = gamma;
  t_[gamma] = beta;
  t_[beta] = alpha;
  t_[alpha] = alpha-1;
  if (up_) {
    x_[beta] = 0;
  } else {
    x_[alpha] = 1;
  }
}

void Chains::last(int i) {
  int alpha = n_ - m_ + i;  // last position of subchain
  int beta = t_[alpha];  // next * to the left of position alpha
  int gamma = t_[beta];  // next * to the left of position beta
  assert(c_[alpha] == 2);
  assert(c_[beta] == 2);

  c_[beta] = 0;  // match two *s by 01
  c_[alpha] = 1;
  p_[beta] = alpha;  // store matched positions
  p_[alpha] = beta;
  s_[gamma] = alpha + 1;  // remove *s from lists
  t_[alpha+1] = gamma;
  if (up_) {
    x_[alpha] = 1;
  } else {
    x_[beta] = 0;
  }
}

void Chains::last_inv(int i) {
  int alpha = n_ - m_ + i;  // last position of subchain
  int beta = p_[alpha];  // 0 matched to 1 in last position
  int gamma = t_[alpha+1];  // next * to the left this matched pair
  assert(c_[alpha] == 1);
  assert(c_[beta] == 0);

  c_[beta] = 2;  // unmatch 01 and replace by two *s
  c_[alpha] = 2;
  s_[gamma] = beta;  // insert two new *s into the lists
  s_[beta] = alpha;
  s_[alpha] = alpha + 1;
  t_[alpha+1] = alpha;
  t_[alpha] = beta;
  t_[beta] = gamma;
  if (up_) {
    x_[alpha] = 0;
  } else {
    x_[beta] = 1;
  }
}

void Chains::chains_even() {
  assert(n_ == 2*m_);

  // C1 [Initialize]
  c_.resize(n_+1,2);
  p_.resize(n_+1);
  s_.resize(n_+1);
  t_.resize(n_+2);
  s_[0] = 1;
  t_[n_+1] = n_;
  for (int i = 1; i <= n_; ++i) {
    s_[i] = i+1;
    t_[i] = i-1;
  }
  l_.resize(m_+1);
  for (int i = 1; i <= m_; ++i){
    l_[i] = 2*i;
  }
  b_.resize(m_+1,+1);
  b_[1] = -1;
  o_.resize(m_+1,+1);
  q_.resize(m_+1,'f');
  d_.resize(m_+1,0);
  for (int i = 0; i <= m_; ++i) {
    d_[i] = i;
  }
  x_.resize(n_+1,0);  // start at the all-0 string
  up_ = true;  // first chain is traversed upwards

  while (true) {
    // C2 [Visit]
    bool done = visit();
    if (done) {
      std::cout << "output limit reached" << std::endl;
      return;
    }
    up_ = !up_;  // next chain traversed in opposite direction

    // C3 [Select dimension]
    int i = d_[m_];
    if (i == 0) {
      break;
    }

    // C4 [Perform operation]
    if (q_[i] == 'l') {
      // C41 [Apply l]
      last(i);
    } else if (q_[i] == 'l'-1) {
      // C42 [Apply l^{-1}]
      last_inv(i);
    } else if (q_[i] == 'f') {
      // C43 [Apply f]
      if (s_[m_ - i + 1] <= m_ + i) {
        first(i);
      } else {
        last_inv(i+1);
        first(i);
        last(i+1);
      }
    } else {
      // C44 [Apply f^{-1}]
      if ((i == m_) || (c_[m_ - i] == '*')) {
        first_inv(i);
      } else {
        last_inv(i+1);
        first_inv(i);
        last(i+1);
      }
    }

    // C5 [Select next step]
    if ((q_[i] == 'f') || (q_[i] == 'l')) {
      l_[i] -= 2;
    } else {
      l_[i] += 2;
    }
    d_[m_] = m_;

    if (b_[i] == -1) {
      // C6 [Reached last descendant in case |C|=0]
      d_[i] = d_[i-1];
      d_[i-1] = i-1;
      l_[i] = (o_[i] == +1) ? 2 : 4;
      b_[i] = +1;
      o_[i] = -o_[i];
      q_[i] = 'f';
    } else if (q_[i] != 'f'-1) {
      // C7 [Descendants in case |C|>=2 remaining]
      if ((q_[i] == 'l') || (q_[i] == 'l'-1)) {
        q_[i] = 'f'-1;
      } else {
        q_[i] = (o_[i] == +1) ? 'l' : 'l'-1;
      }
    } else {
      // C8 [Reached last descendant in case |C|>=2]
      d_[i] = d_[i-1];
      d_[i-1] = i-1;
      int j = d_[i];
      if ((j > 0) && ((q_[j] == 'f') || (q_[j] == 'l'))) {
        l_[i] -= 2;
      } else {
        l_[i] += 2;
      }

      if (l_[i] == 0) {
        b_[i] = -1;
        q_[i] = 'f'-1;
      } else if ((l_[i] == 2) && (o_[i] == -1)) {
        b_[i] = -1;
        q_[i] = 'f';
      } else {
        q_[i] = 'f';
      }

      o_[i] = -o_[i];
    }
  }
}

void Chains::chains_odd() {
  assert(n_ == 2*m_+1);

  // C'1 [Initialize]
  c_.resize(n_+1,2);
  p_.resize(n_+1);
  s_.resize(n_+1);
  t_.resize(n_+2);
  s_[0] = 1;
  t_[n_+1] = n_;
  for (int i = 1; i <= n_; ++i) {
    s_[i] = i+1;
    t_[i] = i-1;
  }
  l_.resize(m_+1,0);
  for (int i = 1; i <= m_; ++i){
    l_[i] = 2*i+1;
  }
  b_.resize(m_+1,+1);
  b_[1] = -1;
  o_.resize(m_+1,+1);
  q_.resize(m_+1,'l');
  d_.resize(m_+1,0);
  for (int i = 0; i <= m_; ++i) {
    d_[i] = i;
  }
  x_.resize(n_+1,0);  // start at the all-0 string
  up_ = true;  // first chain is traversed upwards
  
  while (true) {
    // C'2 [Visit]
    bool done = visit();
    if (done) {
      std::cout << "output limit reached" << std::endl;
      return;
    }    

    // C'3 [Select dimension]
    int i = d_[m_];
    if (i == 0) {
      break;
    }

    if ((i < m_) || ((q_[i] != 'l'-1*'f') && (q_[i] != 'f'-1*'l'))) {
      up_ = !up_;  // next chain traversed in opposite direction
    }

    // C'4 [Perform operation]
    if (q_[i] == 'l') {
      // C'41 [Apply l]
      last(i);
    } else if (q_[i] == 'l'-1) {
      // C'42 [Apply l^{-1}]
      last_inv(i);
    } else if (q_[i] == 'f') {
      // C'43 [Apply f]
      first(i);
    } else if (q_[i] == 'f'-1) {
      // C'44 [Apply f^{-1}]
      first_inv(i);
    } else if (q_[i] == 'l'-1*'f') {
      // C'45 [Apply l^{-1}, then f]
      if (i == m_) {
        last_inv(i);
        first(i);
      } else {
        last_inv(i);
        first(i);
        first(i+1);
      }
    } else if (q_[i] == 'f'-1*'l') {
      // C'46 [Apply f^{-1}, then l]
      if (i == m_) {
        first_inv(i);
        last(i);
      } else {
        first_inv(i+1);
        first_inv(i);
        last(i);
      }
    }

    // C'5 [Select next step]
    if ((q_[i] == 'f') || (q_[i] == 'l')) {
      l_[i] -= 2;
    } else if ((q_[i] == 'f'-1) || (q_[i] == 'l'-1)) {
      l_[i] += 2;
    }
    d_[m_] = m_;

    if (b_[i] == -1) {  // |C| == 1
      if ((q_[i] == 'l') || (q_[i] == 'f'-1*'l')) {
        // C'6 [Descendants in case |C|=1 remaining]
        if (q_[i] == 'l') {
          q_[i] = 'l'-1*'f';
        } else {
          q_[i] = 'l'-1;
        }
      } else {
        // C'7 [Reached last descendant in case |C|=1]
        d_[i] = d_[i-1];
        d_[i-1] = i-1;

        int j = d_[i];
        if ((j > 0) && ((q_[j] == 'f') || (q_[j] == 'l') || (q_[j] == 'l'-1*'f'))) {
          l_[i] -= 2;
        } else {
          l_[i] += 2;
        }
        assert(l_[i] > 0);

        if (l_[i] == 1) {
          // this is the situation shown in Figure 18, bottom left the paper, from left to right
          q_[i] = 'f'-1*'l';
        } else if (l_[i] == 3) {
          if (q_[i-1] == 'l'-1) {
            b_[i] = +1;
            o_[i] = -1;
            q_[i] = 'l';
          } else {
            // this is the situation shown in Figure 18, bottom left the paper, from right to left
            q_[i] = 'l';
          }
        } else {
          assert(l_[i] == 5);
          b_[i] = +1;
          q_[i] = 'l';
          o_[i] = -o_[i];
        }
      }
    } else {  // |C|>=3
      if (q_[i] != 'l'-1) {
        // C'8 [Descendants in case |C|>=3 remaining]
        if ((q_[i] == 'f') || (q_[i] == 'f'-1)) {
          q_[i] = 'l'-1;
        } else {
          q_[i] = (o_[i] == +1) ? 'f' : 'f'-1;
        }
      } else {
        // C'9 [Reached last descendant in case |C|>=3]
        d_[i] = d_[i-1];
        d_[i-1] = i-1;

        int j = d_[i];
        if ((j > 0) && ((q_[j] == 'f') || (q_[j] == 'l') || (q_[j] == 'l'-1*'f'))){
          l_[i] -= 2;
        } else {
          l_[i] += 2;
        }
        assert(l_[i] > 0);

        if (l_[i] == 1) {
          // this is the situation shown in Figure 18, bottom right of the paper
          b_[i] = -1;
          q_[i] = 'f'-1*'l';
        } else if ((l_[i] == 3) && (o_[i] == -1)){
          // this is the situation shown in Figure 18, top left of the paper
          b_[i] = -1;
          q_[i] = 'l';
        } else {
          q_[i] = 'l';
        }
        o_[i] = -o_[i];
      }
    }
  }
}

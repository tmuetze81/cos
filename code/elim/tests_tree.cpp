/*
 * Copyright (c) 2021 Jean Cardinal, Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <vector>
#include <tuple>
#include <utility>
#include <iostream>
#include "tests_tree.hpp"
#include "tree.hpp"

struct Tests{
  static inline void expect(bool condition, const std::string &message) {
    if (!condition) {
      std::cout << "FAILED TEST" << std::endl;
      std::cout << message << std::endl;
      exit(-1);
    }
  }

  static void check_init(int n, std::vector<std::pair<int,int>> edge_list, std::vector<int> answer_oep, std::vector<int> answer_peo,
                         std::vector<int> answer_parent, std::vector<int> answer_first_child, std::vector<int> answer_last_child, std::vector<int> answer_left_sibling, std::vector<int> answer_right_sibling) {
    std::vector<std::string> dummy;
    bool success;
    Elimination_tree t = Elimination_tree(n, dummy, edge_list, success);
    if (!success) return;
    for (int i = 1; i <= t.n_; ++i) {
      expect(
        (answer_oep[i] == t.oep_[i]) && (answer_peo[i] == t.peo_[i]) && (answer_parent[i] == t.parent_[i]) && (answer_first_child[i] == t.first_child_[i]) && (answer_last_child[i] == t.last_child_[i])
        && (answer_left_sibling[i] == t.left_sibling_[i]) && (answer_right_sibling[i] == t.right_sibling_[i]),
        "wrong elimination tree"
      );
    }
  }

  static void test_init() {
    std::vector<std::tuple<int, std::vector<std::pair<int,int>>, std::vector<int>, std::vector<int>, std::vector<int>, std::vector<int>, std::vector<int>, std::vector<int>, std::vector<int>>> tests = {
      {5,{{1,2},{1,3},{1,4},{1,5}},{0,1,2,3,4,5},{0,1,2,3,4,5},{0,0,1,1,1,1},{0,2,0,0,0,0},{0,5,0,0,0,0},{0,0,0,2,3,4},{0,0,3,4,5,0}},
      {7,{{2,1},{4,1},{3,2},{6,5},{5,2},{4,7}},{0,1,2,3,6,4,5,7},{0,1,2,3,5,6,4,7},{0,0,1,2,2,4,1,6},{0,2,3,0,5,0,7,0},{0,6,4,0,5,0,7,0},{0,0,0,0,3,0,2,0},{0,0,6,4,0,0,0,0}},
      {7,{{6,5},{3,5},{1,5},{5,2},{2,4},{2,7}},{0,1,5,4,6,2,3,7},{0,1,5,6,3,2,4,7},{0,0,1,2,2,2,5,5},{0,2,3,0,0,6,0,0},{0,2,5,0,0,7,0,0},{0,0,0,0,3,4,0,6},{0,0,0,4,5,0,7,0}},
      {1,{},{0,1},{0,1},{0,0},{0,0},{0,0},{0,0},{0,0}},
      {2,{{1,2}},{0,1,2},{0,1,2},{0,0,1},{0,2,0},{0,2,0},{0,0,0},{0,0,0}},
      {4,{{1,2},{2,3},{3,1}},{},{},{},{},{},{},{}},
    };
    std::cout << "testing elimination tree initialization" << std::endl;
    for (auto test_case : tests) {
      check_init(std::get<0>(test_case), std::get<1>(test_case), std::get<2>(test_case), std::get<3>(test_case), std::get<4>(test_case), std::get<5>(test_case), std::get<6>(test_case), std::get<7>(test_case), std::get<8>(test_case));
    }
    std::cout << "OK" << std::endl;
  }

};

void run_tests_tree() {
  std::cout << "running tree algorithm tests" << std::endl;
  Tests::test_init();
  std::cout << "testing finished OK" << std::endl;
}
/*
 * Copyright (c) 2021 Jean Cardinal, Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include "tree.hpp"

Elimination_tree::Elimination_tree(int n, std::vector<std::string> &vertex_strings, std::vector<std::pair<int,int>> &edge_list, bool &is_tree) : n_(n), vertex_strings_(vertex_strings)
{
  int m = build_tree(edge_list);
  if ((m != n_-1) || !is_connected()) {
    is_tree = false;
    return;
  }

  is_tree = true;
  elimination_order();
  init_elimination_tree();
  init_neighbor_to();
  init_child_to();
  init_bookkeeping();

  if (DEBUG) {
    check_consistency();
  }
}

int Elimination_tree::build_tree(std::vector<std::pair<int,int>> &edge_list)
{
  int m = 0;  // count edges
  adjL_.resize(n_+1, std::vector<int> ());
  std::vector<std::vector<int>> adjM(n_+1, std::vector<int>(n_+1, 0));
  for (auto e : edge_list) {
    int i = e.first;
    int j = e.second;
    if ((i<1) || (i>n_) || (j<1) || (j>n_) || (adjM[i][j] == 1)) {
      // ignore invalid edge
      continue;
    }
    adjL_[i].push_back(j);
    adjL_[j].push_back(i);
    adjM[i][j] = 1;
    adjM[j][i] = 1;
    m++;
  }
  return m;
}

void Elimination_tree::dfs(int v, int &count, std::vector<int> &label, std::vector<int> &pred)
{
  count++;
  label[v] = count;
  for (auto w : adjL_[v]) {
    if (label[w] == 0) {
      pred[w] = v;
      dfs(w, count, label, pred);
    }
  }
}

bool Elimination_tree::is_connected()
{
  int count = 0;
  std::vector<int> label(n_+1, 0);
  std::vector<int> pred(n_+1, 0);
  dfs(1, count, label, pred);
  if (count == n_) {
    return true;
  } else {
    return false;
  }
}

void Elimination_tree::elimination_order()
{
  int count = 0;
  peo_.resize(n_+1, 0);
  oep_.resize(n_+1, 0);
  std::vector<int> pred(n_+1, 0);
  dfs(1, count, oep_, pred);
  // compute inverse permutation
  for (int v=1; v<=n_; ++v) {
    int i = oep_[v];
    peo_[i] = v;
  }
}

void Elimination_tree::init_elimination_tree()
{
  // initialize elimination tree based on perfect elimination order of underlying tree
  root_ = 1;
  first_child_.resize(n_+1, 0);
  last_child_.resize(n_+1, 0);
  smaller_child_.resize(n_+1, 0);
  parent_.resize(n_+1, 0);
  left_sibling_.resize(n_+1, 0);
  right_sibling_.resize(n_+1, 0);
  for (int i=1; i<=n_; ++i) {
    for (auto v : adjL_[peo_[i]]) {
      int j = oep_[v];
      if (j < i) continue;
      insert_vertex(j, i);
    }
  }
}

void Elimination_tree::insert_vertex(int i, int j)
{
  assert((i != 0) && (j != 0));
  int k = last_child_[j];
  // modify parent/child relations
  parent_[i] = j;
  last_child_[j] = i;
  if (first_child_[j] == 0) {
    first_child_[j] = i;
  }
  // modify sibling relations
  right_sibling_[k] = i;
  left_sibling_[i] = k;
  right_sibling_[i] = 0;
  // update smaller child
  if (i < j) {
    smaller_child_[j] = i;
  }
}

void Elimination_tree::delete_vertex(int i)
{
  assert(i != 0);
  // modify parent/child relations
  int j = parent_[i];
  int l = left_sibling_[i];
  int r = right_sibling_[i];
  if (first_child_[j] == i) {
    first_child_[j] = r;
  }
  if (last_child_[j] == i) {
    last_child_[j] = l;
  }
  // modify sibling relations
  right_sibling_[l] = r;
  left_sibling_[r] = l;
  // update smaller child
  if (i == smaller_child_[j]) {
    smaller_child_[j] = 0;
  }
}

int Elimination_tree::min_child(int i)
{
  assert((first_child_[i] !=0) && (last_child_[i] != 0));
  int j = first_child_[i];
  int min = j;
  while (j != 0) {
    min = (j < min) ? j : min;
    j = right_sibling_[j];
  }
  return min;
}

void Elimination_tree::init_neighbor_to()
{
  neighbor_to_.resize(n_+1, std::vector<int> (n_+1, 0));
  // run a DFS from each vertex
  for (int v=1; v<=n_; ++v) {
    int count = 0;
    std::vector<int> label(n_+1, 0);
    std::vector<int> pred(n_+1, 0);
    dfs(v, count, label, pred);
    int j = oep_[v];
    for (int w=1; w<=n_; ++w) {
      if (w == v) continue;
      int i = oep_[w];
      int k = oep_[pred[w]];
      neighbor_to_[i][j] = k;
    }
  }
}

void Elimination_tree::init_child_to()
{
  child_to_.resize(n_+1, std::vector<int> (n_+1, 0));
  for (int i=1; i<=n_; ++i) {
    int j = first_child_[i];
    while (j != 0) {
      child_to_[i][j] = j;
      j = right_sibling_[j];
    }
  }
}

void Elimination_tree::init_bookkeeping()
{
  o_.resize(n_+1, Direction::up);
  s_.resize(n_+1, 0);
  for (int j=1; j<=n_; ++j) {
    s_[j] = j;
  }
}

bool Elimination_tree::next()
{
  int j = s_[n_];
  if (j == 1) {
    return false;
  }
  // rotate vertex j in direction o_[j]
  rotate(j, o_[j]);
  if (DEBUG) {
    check_consistency();
  }

  s_[n_] = n_;
  if (o_[j] == Direction::up && (is_root(j) || (parent_[j] > j)))
  {
    o_[j] = Direction::down;
    s_[j] = s_[j-1];
    s_[j-1] = j-1;
  }
  if ((o_[j] == Direction::down) && (smaller_child_[j] == 0))
  {
    o_[j] = Direction::up;
    s_[j] = s_[j-1];
    s_[j-1] = j-1;
  }
  return true;
}

void Elimination_tree::rotate(int j, Direction d)
{
  if (d == Direction::down) {
    // rotate j down by rotating its smaller child up
    int i = smaller_child_[j];
    assert((i != 0) && (i < j) && (i == min_child(j)));
    rotate(i, Direction::up);
  } else if (d == Direction::up) {
    int i = parent_[j];
    int p = parent_[i];
    // figure out which child of j changes parent
    int np = neighbor_to_[p][i];  // np is the neighbor of p in the underlying tree towards i
    int ni = neighbor_to_[i][j];  // ni is the neighbor of i in the underlying tree towards j
    int nj = neighbor_to_[j][i];  // nj is the neighbor of j in the underlying tree towards i (the root of the tree that changes parents lies in this middle part between i and j)
    assert(child_to_[p][np] = i);
    assert(child_to_[i][ni] = j);
    int k = child_to_[j][nj];  // k is the child of j that changes parents
    // remove k from children of j and insert into children of i
    delete_vertex(j);
    if (k != 0) {
      delete_vertex(k);
      insert_vertex(k, i);
    }
    if (p == 0) {
      assert(root_ = i);
      root_ = j;
      parent_[root_] = 0;
      left_sibling_[root_] = 0;
      right_sibling_[root_] = 0;
    } else {
      delete_vertex(i);
      insert_vertex(j, p);
    }
    insert_vertex(i, j);
    child_to_[p][np] = j;
    child_to_[i][ni] = k;
    child_to_[j][nj] = i;
  }
}

void Elimination_tree::print()
{
  bool first = true;
  print_rec(root_, first);
  std::cout << std::endl;
}

void Elimination_tree::print_rec(int i, bool &first)
{
  if (!first) std::cout << " ";
  first = false;
  std::cout << i;
  int j = first_child_[i];
  while (j != 0) {
    std::cout << ".";  // print one dot for each child of the current vertex
    j = right_sibling_[j];
  }
  j = first_child_[i];
  while (j != 0) {
    print_rec(j, first);
    j = right_sibling_[j];
  }
}

void Elimination_tree::print_peo()
{
  std::cout << "perfect elimination ordering: ";
  for (int i=1; i<=n_; i++) {
    if (i>=2) std::cout << " ";
    std::cout << vertex_strings_[peo_[i]];
  }
  std::cout << std::endl;
}

void Elimination_tree::check_consistency()
{
  // check root conditions
  assert(root_ != 0);
  assert((parent_[root_] == 0) && (left_sibling_[root_] == 0) && (right_sibling_[root_] == 0));
  int count = 0;
  // check children recursively
  check_consistency_rec(root_, count);
  // check overall vertex count
  assert(n_ == count);
}

void Elimination_tree::check_consistency_rec(int i, int &count)
{
  count++;
  std::vector<int> child_to(n_+1, 0);
  if (first_child_[i] != 0) {
    int m = min_child(i);
    assert(((m > i) && (smaller_child_[i] == 0)) || ((m < i) && (smaller_child_[i] == m)));
  }
  // go through children from left to right
  int j = first_child_[i];
  int r = 0;
  while (j != 0) {
    r++;
    assert(parent_[j] == i);
    int d = neighbor_to_[i][j];
    child_to[d] = 1;
    // check child_to_ data structure
    assert(child_to_[i][d] == j);
    j = right_sibling_[j];
  }
  // go through children from right to left
  j = last_child_[i];
  int l = 0;
  while (j != 0) {
    l++;
    assert(parent_[j] == i);
    j = left_sibling_[j];
  }
  assert(r == l);
  // check child_to_ data structure
  int v = peo_[i];
  for (auto w : adjL_[v]) {
    int d = oep_[w];
    if (child_to[d] != 0) continue;
    assert(child_to_[i][d] == 0);
  }
  // check children recursively
  j = first_child_[i];
  while (j != 0) {
    check_consistency_rec(j, count);
    j = right_sibling_[j];
  }
}
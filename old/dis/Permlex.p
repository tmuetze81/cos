(*===================================================================*)
(* Pascal program for distribution from the Combinatorial Object     *)
(* Server. Generate permutations in lexicographic order. This is     *)
(* the same version used in the book "Combinatorial Generation."     *)
(* The program can be modified, translated to other languages, etc., *)
(* so long as proper acknowledgement is given (author and source).   *)  
(* Programmer: Joe Sawada, 1997.                                     *)
(* The latest version of this program may be found at the site       *)
(* http://theory.cs.uvic.ca/inf/perm/PermInfo.html                    *)
(*===================================================================*)

program Permlex (input, output);

var
	n : integer;
	i : integer;
	a : array [0..100] of integer;   /* The permutation */

procedure PrintPerm;
var j:integer;
begin

	for j:=1 to n do write(a[j]:1);
	writeln;
end; 

procedure swap(i:integer; j:integer);
var temp : integer; 
begin

	temp := a[i];
	a[i] := a[j];
	a[j] := temp;
end;

function Next : integer;
var k,j,r,s : integer;
begin

	k := n-1; 
	while a[k] > a[k+1] do k:=k-1;
	if k = 0 then Next:=0 
	else begin 
		j := n;
		while a[k] > a[j] do j:=j-1;
		swap(j,k);
		r:=n; 
		s:=k+1;
		while r>s do begin 
			swap(r,s);
			r:=r-1; 
			s:=s+1;
		end;	
		Next:=1;
		PrintPerm();
 	end;	
end;

begin

	write('Enter n: '); readln(n); 
	writeln;
	if n > 0 then begin
		for  i := 0 to n do a[i] := i;
		PrintPerm;
		while (Next = 1) do ;
		writeln;
	end;
end.

/*======================================================================*/
/* C program for distribution from the Combinatorial Object             */
/* Server. Generate binary trees in Gray code order.                    */
/* This is the same version used in the book "Combinatorial Generation."*/
/* The program can be modified, translated to other languages, etc.,    */
/* so long as proper acknowledgement is given (author and source).      */  
/* Programmer: G. Li - modified by Joe Sawada, 1997.                    */
/* The latest version of this program may be found at the site          */
/* http://www.theory.csc.uvic.ca/inf/tree/BinaryTrees.html         */
/*======================================================================*/

/* Input:  N is the number of interior nodes and k is the level
	of the leftmost leaf.  If k=0 is entered then all binary
	trees with N nodes will be generated */

#include <stdio.h>

#define  MaxSize 50

int L[MaxSize];
int N,k,i,shift;

void Print() {

	int j;
	
	for (j=1+shift; j<=2*N+shift; j++) printf("%d", L[j]);
	printf("\n");
}

void swap(int p1, int p2) {
 	int temp;
 	Print(); 
 	temp = L[p1]; L[p1]=L[p2]; L[p2]= temp;
}

void neg(int n,int k, int p);

void gen(int n,int k, int p) {

 	if (k==1) gen(n,2,p); 
	else if (1<k && k<n) { 
   		neg(n,k+1,p);
   		if (k==n-1) swap(n+1+p,n+2+p); 
		else if (k==2) swap(4+p,6+p);
   		else swap(k+2+p,2*k+3+p);
   		gen(n-1,k-1,p+2);
 	}
}

void neg(int n, int k,int p) {

 	if (k==1) neg(n,2,p); 
	else if (1<k && k<n) {
   		neg(n-1,k-1,p+2);
   		if (k==n-1) swap(n+1+p,n+2+p); 
		else if (k==2) swap(4+p,6+p);
   		else swap(k+2+p,2*k+3+p);
   		gen(n,k+1,p);
 	}
}
	
void main() {

	printf("Enter n k: ");
	scanf("%d %d", &N, &k);

	if (k<0) exit(0);
	shift = 0;
	if (k==0) {
	  	shift = 2;
	  	L[1]=1; L[3]=1; L[4]=1;
	  	L[2]=0; L[5]=0; L[6]=0;
	  	for(i=1;i<=N-2;i++) {
	   		L[6+2*i-1]=1;
	    		L[6+2*i  ]=0;
	  	}
	}
	if (k==1) {
	  	L[1]=1; L[3]=1; L[4]=1;
	  	L[2]=0; L[5]=0; L[6]=0;
	  	for(i=1;i<=N-3;i++) {
	    		L[6+2*i-1]=1;
	    		L[6+2*i  ]=0;
	  	}
	}
	if (1<k && k<N) {
	  	for(i=1;i<=k;i++) L[i]=1;
	  	L[k+1]=0; L[k+2]=1; 
	  	for(i=k+3;i<=k+2+k;i++) L[i]= 0;
	  	for(i=1;i<=N-k-1;i++) {
	    		L[2*k+2+2*i-1]=1;
	    		L[2*k+2+2*i]=0;
	 	}
	}
        if (k==N) {
	  	for(i=1;i<=N;i++) L[i]=1;
	  	for(i=N+1;i<=2*N;i++) L[i]=0;
	}
	if (k==0) gen(N+1,1,0); 
	else gen(N,k,0);
	Print();
}

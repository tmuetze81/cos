(*======================================================================*)
(* Pascal program for distribution from the Combinatorial Object        *)
(* Server. Generate combinations of a multiset in co-lex order.         *)
(* This is the same version used in the book "Combinatorial Generation."*)
(* The program can be modified, translated to other languages, etc.,    *)
(* so long as proper acknowledgement is given (author and source).      *)  
(* Programmer: Frank Ruskey.                                            *)
(* The latest version of this program may be found at the site          *)
(* http://theory.cs.uvic.ca/inf/mult/Multiset.html                       *)
(*======================================================================*)

program MultiComb( input, output );

const MAX_T = 100;

var
  num, a : array [0..MAX_T] of integer;
  n, i, k, t : integer;

procedure PrintIt;
var i : integer;
begin
  for i := 0 to t do write( a[i]:3 );  writeln;
end;

procedure gen1 ( k, t, n : integer );
var i : integer;
begin
  if k = 0 then PrintIt
  else
     for i := max(0,k-n+num[t]) to min(num[t],k) do begin
        a[t] := i;
        gen1( k-i, t-1, n-num[t] );
        a[t] := 0;
     end;
end {of gen1};

procedure gen2 ( k, t, n : integer );
var i : integer;
begin
  if k = n then PrintIt
  else
     for i := max(0,k-n+num[t]) to min(num[t],k) do begin
        a[t] := i;
        gen2( k-i, t-1, n-num[t] );
        a[t] := num[t];
     end;
end {of gen2};

begin
  n := 0;
  write('Enter t: ');
  readln(t);
  write('Enter k: ');
  readln(k);
  for i := 0 to t do begin
     write('Enter n(', i:1,'): ');
     readln( num[i] );  n := n + num[i];
  end;
  writeln;
  
  if k <= n/2 then begin
     for i := 0 to t do a[i] := 0;
     gen1( k, t, n ); 
  end else begin
     for i := 0 to t do a[i] := num[i];
     gen2( k, t, n ); 
  end;  
  writeln;
end.



(*=======================================================================*)
(* Pascal program for distribution from the Combinatorial Object Server. *)
(* Generates all score sequences of tournaments.                         *)
(* No input error checking.  Assumes 1 <= n <= MAX.                      *)
(* Algorithm is CAT (Constant Amortized Time).                           *)
(* The program can be modified, translated to other languages, etc.,     *)
(* so long as proper acknowledgement is given (author and source).       *)
(* Programmer: Frank Ruskey, 1994.                                       *)
(* The latest version of this program may be found at the site           *)
(* http://theory.cs.uvic.ca/inf/nump/ScoreSequence.html  or              *)
(* http://theory.cs.uvic.ca/dis/programs.html                             *)
(*=======================================================================*)

program Score ( input, output );

const MAX = 100;

var n,i : integer;
    C : array [0..MAX] of integer;  { C[i] = i choose 2 = i*(i-1)/2 }
    s : array [0..MAX] of integer;  { the score sequence }
    count : integer;                { number produced so far }

function max ( x,y : integer ) : integer;
begin
  if x > y then max := x else max := y;
end {of max};

procedure PrintIt ( sum : integer );
var i : integer;
begin
  count := count + 1;
  s[n] := C[n] - sum;
  for i := 1 to n do write( s[i]:3 );
  writeln;
end;
                                                      
procedure gen ( k, sum : integer );
var i : integer;
begin
  if k = n then PrintIt( sum )
  else begin
     for i := max( s[k-1], C[k]-sum ) to
              (C[n]-sum) div (n-k+1) do begin
        s[k] := i;  gen( k+1, sum+i );
     end;
  end;
end {of gen};

begin
  write( 'Enter n: ' );  readln( n );
  C[1] := 0;  s[0] := 0;
  for i := 2 to n do C[i] := C[i-1] + i-1; 
  count := 0;
  gen( 1, 0 );
end.
                                        


/*===================================================================*/
/* C program for distribution from the Combinatorial Object          */
/* Server. Generates contingency tables for multisets. This is        */
/* the same version used in the book "Combinatorial Generation."     */
/* The program can be modified, translated to other languages, etc., */
/* so long as proper acknowledgement is given (author and source).   */  
/* Programmer: Joe Sawada, 1997.                                     */
/* The latest version of this program may be found at the site       */
/* http://theory.cs.uvic.ca/inf/mult/Multiset.html                    */
/*===================================================================*/

#include <stdio.h>
#define MAX 20

int sum, check, rows, cols, count;
int table[MAX][MAX];
int k[MAX];

void PrintIt() {

	int i,j; 
	
	count++;
	printf("[%3.d]", count);
	for (i=0; i<=rows; i++) {
		for (j=0; j<=cols; j++) {
			printf( " %d",table[i][j]);
		}
		printf("\n");
		printf("     ");
	}
	printf("\n");
}

int max(int i, int j) {

	if (i>j) return(i);
	return(j);
}

int min(int i, int j) {

	if (i<j) return(i);
	return(j);
}

void Gen (int r);
void RowGen(int r, int j, int p, int n) {

	int temp,i;
	
	if (j==0) Gen(r-1);
	else {
		for (i = max(0,j-n+table[0][p]); i<= min(table[0][p],j); i++){
			table[r][p] = i;
			temp = table[0][p];
			table[0][p] = table[0][p] - i;
			RowGen(r,j-i,p-1,n-temp);
       	      		table[0][p] = temp;
       	      		table[r][p] = 0;
		}
	}
}

void Gen (int r) {
	if (r == 0) PrintIt();
	else {
		RowGen(r,k[r],cols,sum-k[r+1]);
	}
}
	
void main() {

	int i;

	printf("Enter the number of rows (0..row): ");
	scanf("%d", &rows);
	printf("Enter the number of cols (0..col): ");
	scanf("%d", &cols);
	sum = 0;
	for (i=0; i<=cols; i++) {
       		printf("  Enter column %d sum: ", i);
       		scanf("%d", &table[0][i] );
       		sum += table[0][i];
	}
	check = 0;
	for (i=0; i<=rows; i++) {
       		printf("  Enter row %d sum: ", i);
       		scanf("%d", &k[i] );
       		check += k[i];
	}
	printf("\n");
	k[rows+1] = 0;
	count = 0;
	if (check == sum) Gen(rows);
	else printf("Row sums do not equal column sums.\n");
}
	

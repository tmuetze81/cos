
(*======================================================================*)
(* Pascal program for distribution from the Combinatorial Object        *)
(* Server. Generate permutations of a multiset in Gray code order.      *)
(* This is the same version used in the book "Combinatorial Generation."*)
(* The program can be modified, translated to other languages, etc.,    *)
(* so long as proper acknowledgement is given (author and source).      *)  
(* Programmer: Frank Ruskey, 1995.                                      *)
(* The latest version of this program may be found at the site          *)
(* http://www.theory.csc.uvic.ca/inf/mult/Multiset.html            *)
(*======================================================================*)

{A direct CAT implementation of the Eades-McKay algorithm}
{modified to handle multisets.                           }

(* This program takes as input t (the number of different elements 0..t )
	followed by the number of each element n(0).. n(t) *)

program eades2 ( input, output );

var a : array [1..100] of integer;
    n,t,i,j,k,cnt,calls : integer;
    num, off, sum : array [0..20] of integer;
    dir : array [0..20] of boolean;

procedure PrintIt;
var i : integer;
begin
  cnt := cnt + 1;
  for i := 1 to sum[0] do write( a[i]:2 );  writeln;
end {of PrintIt};

procedure neg ( t,n,k : integer );  forward;
procedure gen ( t,n,k : integer );  forward;

procedure BigGen( t : integer );
begin
  if dir[t] 
     then gen( t, sum[t-1], sum[t] )
     else neg( t, sum[t-1], sum[t] );
  if t > 1 then BigGen( t-1 );
  dir[t] := not dir[t]; 
  if dir[t] then off[t] := off[t-1] + num[t-1] else off[t] := off[t-1];
end {of BigGen};

procedure swap ( t,x,y : integer );
var b,temp : integer;
begin
  if t > 1 then BigGen( t-1 );
  b := off[t-1];
  temp := a[x+b];  a[x+b] := a[y+b];  a[y+b] := temp;
  PrintIt;
end {of swap};

procedure gen (* t,n,k : integer *);  {This is the Eades-McKay algorithm}
var i: integer;
begin
  calls := calls + 1;
  if (1 < k) and (k < n) then begin
     gen( t, n-2, k-2 );  swap( t, n-1, k-1 );
     neg( t, n-2, k-1 );  swap( t,  n, n-1 );
     gen( t, n-1, k );
  end else
  if k = 1 then
     for i := n-1 downto 1 do begin
        swap( t, i, i+1 );
     end;
end {of gen};

procedure neg (* t,n,k : integer *);
var i: integer;
begin
  calls := calls + 1;
  if (1 < k) and (k < n) then begin
     neg( t, n-1, k );    swap( t,   n, n-1 );
     gen( t, n-2, k-1 );  swap( t, n-1, k-1 );
     neg( t, n-2, k-2 );  
  end else
  if k = 1 then
     for i := 1 to n-1 do begin
        swap( t, i, i+1 );
     end;
end {of neg};

begin
     write('Enter t: ');
     readln( t );
     k := 1;
     for i := 0 to t do begin
        dir[i] := true;  
	write('Enter n(', i:1,'): ');
        readln( num[i] );  
        for j := 1 to num[i] do begin
           a[k] := i;  k := k + 1;
        end;
     end;
     writeln;
     off[0] := 0;
     for i := 1 to t do off[i] := off[i-1] + num[i-1];
     dir[t+1] := true;
     sum[t] := num[t];
     for i := t-1 downto 0 do begin
        sum[i] := sum[i+1] + num[i];
     end;
     cnt := 0;  calls := 0;  
     PrintIt;
     BigGen( t );
     writeln;
end.



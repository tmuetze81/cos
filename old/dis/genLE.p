
(*=========================================================================*)
(* Pascal  program for distribution from the Combinatorial Object Server.  *)
(* Generates linear extensions in Gray code order.  This is the same       *)
(* version used in the book "Combinatorial Generation."                    *)
(* The program can be modified, translated to other languages, etc.,       *)
(* so long as proper acknowledgement is given (author and source).         *)   
(* Programmer: Joe Sawada  1997                                            *)
(* The latest version of this program may be found at the site             *)
(* http://theory.cs.uvic.ca/inf/pose/LinearExt.html                         *)
(*=========================================================================*)

(*----------------------------------------------------------
The input file for this program holds the size of the set 
followed by the partial orderings, one per line.  The end
of the file is detected by a 0 .  Here is a sample input file:

	5
	1 3
	2 4
	0 
--------------------------------------------------------*)


program genLE (input, output);

const  MAX_POINTS = 100;

var


procedure genLE( i:integer );
var
	mrb,mra,mla,x: integer;
	typical: boolean;

begin

	if i > 0 then begin
		genLE(i-1);
		mrb:= 0; typical:= false;
		while Right(b[i]) do begin
			mrb:= mrb+1;
			Move( b[i], right); genLE(i-1);
			mra:=0;
			if Right(a[i]) then begin
				typical := true;
				repeat
					mra:= mra+1
					Move(a[i], right); genLE(i-1);
				until not Right(a[i]);
			end;
			if typical then begin
				Switch(i-1); genLE(i-1);
				if odd(mrb) then mla:= mra-1 else mla:= mra+1;
				for x:=1 to mla do begin
					Move(a[i], left); genLE(i-1);
				end;
			end;
		end;
		if typical and odd(mrb) then Move(a[i], left)
		else Switch(i-1);
		genLE(i-1);
		for x:= 1 to mrb do begin
			Move(b[i], left); genLE(i-1);
		end;
	end;
end; 
	


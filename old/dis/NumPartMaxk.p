(*========================================================================*)
(* Pascal program for distribution from Frank Ruskey's                    *)
(*    Combinatorial Object Server.                                        *)
(* Generates all numerical partitions of n whose largest part is k.       *)
(* No input error checking.  Assumes 0 <= k <= n <= MAX.                  *)
(* A simple modification will generate all partitions of n (see           *)
(* comment at end of program.)                                            *)
(* Algorithm is CAT (Constant Amortized Time).                            *)
(* The program can be modified, translated to other languages, etc.,      *)
(* so long as proper acknowledgement is given (author and source).        *)
(* Programmer: Frank Ruskey, 1990.                                        *)
(* The latest version of this program may be found at the site            *)
(* http://www.theory.cs.uvic.ca/inf/nump/NumPartition.html    or     *)
(* http://www.theory.cs.uvic.ca/dis/programs.html                    *)
(*========================================================================*)
                                                                            
program NumericalPartitions ( input, output );

const MAX = 100;

var
  n,k : integer;
  p : array [0..100] of integer;

function min ( x,y : integer ) : integer;
begin
  if x < y then min := x else min := y;
end {of min};

procedure PrintIt( t : integer );
var i : integer;
begin
  for i := 1 to t do write( p[i]:2 );
  writeln;
end {of PrintIt};
                                                   
procedure P ( n,k,t : integer );
var j : integer;
begin
  p[t] := k;
  if n = k then PrintIt( t );
  for j := min( k, n-k ) downto 1 do P( n-k, j, t+1 );
end;

begin
  write( 'Enter n,k: ' );  readln( n,k );
  P( n,k,1 );
  (* NOTE: The call P( 2*n, n, 0 ) will produce all partitions of n. *)
end.                                   




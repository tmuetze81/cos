<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML><HEAD>
<TITLE>Info on Up-down Permutations</TITLE>
<BASE HREF="http://theory.cs.uvic.ca/">
<META NAME="resource-type" CONTENT="document">
<META NAME="description" CONTENT="The (Combinatorial) Object Server: A mathematical 
  tool for producin g lists of combinatorial objects.">
<META NAME="keywords" CONTENT="up-down permutation, zig-zag permutation">
<META NAME="distribution" CONTENT="global">
<META NAME="copyright" CONTENT="Frank Ruskey,
      1995, 1996, 1997, 1998, 1999, 2000, 2001 .">
<META HTTP-EQUIV="Reply-to" CONTENT="fruskey@csr.uvic.ca">
<META HTTP-EQUIV="Keywords" CONTENT="up-down permutation, zig-zag permutation, 
  alternating permutation">
</HEAD>

<A HREF=gen/perm.html>
<IMG SRC=ico/gen.gif align=left border=0 height=33 width=35></A>
<!--#include file="inc/Border.include"-->
<HR>

<H2>Information on Up-down Permutations</H2>

<P>
A permutation p<sub>1</sub> p<sub>2</sub> &#183;&#183;&#183; p<sub>n</sub>
  is an up-down permutation if p<sub>1</sub> &lt; p<sub>2</sub> &gt; p<sub>3</sub> 
  &lt; p<sub>4</sub> &gt; &#183;&#183;&#183;.
In other words, 
  <I>p<sub>i</sub></I> &lt; <I>p</I><sub><I>i</I>+1</sub> 
  if <I>i</I> is odd, and 
  <I>p<sub>i</sub></I> &gt; <I>p</I><sub><I>i</I>+1</sub>
  if <I>i</I> is even.

<P>
Other names that authors have used for these permutations are
  <I>zig-zag permutations</I> and  <I>alternating permutations</I>.
The later name is unfortunate since it is the standard term for
  the group of permutations of even parity.

<P>
The number of <I>n</I> up-down permutations for 
  <I>n</I> = 1,2,...,15, is
  1, 1, 2, 5, 16, 61, 272, 1385, 7936, 50521, 353792, 2702765,
  22368256, 199360981, 1903757312.

This is sequence 
  <A HREF="<!--#include file="inc/IntSeq.include"-->Anum=A000111">
  <B>A000111</B>(M1492)</A> in 
<!--#include file="inc/Sloane.include"-->

These numbers are known as the <B>Euler</B> numbers and have
  exponential generating function sec(<I>x</I>) + tan(<I>x</I>).
The Euler numbers are sometimes called the "up/down" numbers.

<P>
Define E(<I>n</I>,<I>k</I>) to be the number of up-down permutations for
  which p<sub>1</sub> = <I>k</I>.  
Then E(1,1) = 1, E(<I>n</I>,<I>k</I>) = 0 if <I>k</I> &gt;= <I>n</I> 
  or <I>k</I> < 1, and otherwise
  <P>E(<I>n</I>,<I>k</I>) = E(<I>n</I>,<I>k</I>+1) 
  + E(<I>n</I>-1,<I>n</I>-<I>k</I>)

<P>
The program used is from the paper: Bauslaugh and Ruskey
  <A HREF="http://csr.csc.uvic.ca/home/fruskey/Publications/">
  <I>Generating Alternating Permutations Lexicographically</I></A>,
  BIT, 30 (1990) 17-26.

<HR>

<A HREF=gen/perm.html>
<IMG SRC=ico/gen.gif align=left border=0 height=33 width=35></A>
<!--#include file="inc/Border.include"-->


<HR>
<FONT SIZE=-1>
<!--#include file="inc/Wizard.include"--><br>
<!-- This page maintained by -->
<!-- <A HREF="mailto:cos@theory.csc.uvic.ca">Dory</A>. -->
<!-- <! Dory -- the little witch, Gink -- Dory's cat> <br> -->
It was last updated <!--#echo var="LAST_MODIFIED" -->.<br>
<!--#exec cgi="inc/cos-counter/counter-nl"--><br>
<!--#include file="inc/Copyright.include"-->    
</FONT>
</BODY>
</HTML>


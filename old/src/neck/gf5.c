/* Program to enumerate all irreducible and/or primitive polynomials 
   over GF(5). 
   Gilbert Lee
*/

#include <stdio.h>
#include <stdlib.h>

typedef unsigned long long ULL;  
/* Change MAX to size of unsigned long long */
#define MAX 64
#define MAX_ITERATIONS 10000

ULL ONE = (ULL)1;
ULL TWO = (ULL)2;
ULL iter_limit = 0;

typedef struct{
  ULL top;
  ULL mid;
  ULL bot;
} Poly_GF5;
/* Each polynomial is expressed in 3 words. 
   000 = 0
   001 = 1
   010 = 2
   011 = 3
   101 = 4
*/

/* A list of primitive polynomials 
   poly_table[i] contains what x^i is equivalent to.
   E.g. given primitive polynomial x^2 + x + 2:
                                    10    2  top
        --> x^2 = 4x+3 --> [4,3] =  01  = 1  mid
				    11    3  bot
				    --     
                                    43
    so poly_table[2] = {2,1,3) 
*/


Poly_GF5 poly_table[MAX+1] = {
/*  0  */ { 0 , 0,  1 },    
/*  1  */ { 0 , 0,  1 },
/*  2  */ {(ULL)2ULL, (ULL)1ULL, (ULL)3ULL},
/*  3  */ {(ULL)0ULL, (ULL)3ULL, (ULL)1ULL},
/*  4  */ {(ULL)4ULL, (ULL)3ULL, (ULL)7ULL},
/*  5  */ {(ULL)0ULL, (ULL)1ULL, (ULL)3ULL},
/*  6  */ {(ULL)2ULL, (ULL)1ULL, (ULL)3ULL},
/*  7  */ {(ULL)0ULL, (ULL)3ULL, (ULL)1ULL},
/*  8  */ {(ULL)4ULL, (ULL)3ULL, (ULL)6ULL},
/*  9  */ {(ULL)4ULL, (ULL)3ULL, (ULL)6ULL},
/* 10  */ {(ULL)6ULL, (ULL)1ULL, (ULL)6ULL},
/* 11  */ {(ULL)0ULL, (ULL)3ULL, (ULL)1ULL},
/* 12  */ {(ULL)8ULL, (ULL)3ULL, (ULL)10ULL},
/* 13  */ {(ULL)4ULL, (ULL)3ULL, (ULL)5ULL},
/* 14  */ {(ULL)12ULL, (ULL)3ULL, (ULL)15ULL},
/* 15  */ {(ULL)4ULL, (ULL)1ULL, (ULL)5ULL},
/* 16  */ {(ULL)8ULL, (ULL)3ULL, (ULL)9ULL},
/* 17  */ {(ULL)0ULL, (ULL)7ULL, (ULL)7ULL},
/* 18  */ {(ULL)16ULL, (ULL)3ULL, (ULL)19ULL},
/* 19  */ {(ULL)8ULL, (ULL)3ULL, (ULL)11ULL},
/* 20  */ {(ULL)4ULL, (ULL)3ULL, (ULL)6ULL},
/* 21  */ {(ULL)0ULL, (ULL)1ULL, (ULL)3ULL},
/* 22  */ {(ULL)6ULL, (ULL)1ULL, (ULL)6ULL},
/* 23  */ {(ULL)4ULL, (ULL)1ULL, (ULL)6ULL},
/* 24  */ {(ULL)14ULL, (ULL)1ULL, (ULL)15ULL},
/* 25  */ {(ULL)0ULL, (ULL)11ULL, (ULL)9ULL},
/* 26  */ {(ULL)12ULL, (ULL)3ULL, (ULL)15ULL},
/* 27  */ {(ULL)0ULL, (ULL)1ULL, (ULL)3ULL},
/* 28  */ {(ULL)8ULL, (ULL)3ULL, (ULL)10ULL},
/* 29  */ {(ULL)12ULL, (ULL)1ULL, (ULL)12ULL},
/* 30  */ {(ULL)18ULL, (ULL)5ULL, (ULL)22ULL},
/* 31  */ {(ULL)0ULL, (ULL)3ULL, (ULL)1ULL},
/* 32  */ {(ULL)12ULL, (ULL)1ULL, (ULL)15ULL},
/* 33  */ {(ULL)0ULL, (ULL)7ULL, (ULL)7ULL},
/* 34  */ {(ULL)16ULL, (ULL)3ULL, (ULL)19ULL},
/* 35  */ {(ULL)12ULL, (ULL)3ULL, (ULL)13ULL},
/* 36  */ {(ULL)8ULL, (ULL)3ULL, (ULL)9ULL},
/* 37  */ {(ULL)4ULL, (ULL)3ULL, (ULL)5ULL},
/* 38  */ {(ULL)10ULL, (ULL)5ULL, (ULL)11ULL},
/* 39  */ {(ULL)0ULL, (ULL)11ULL, (ULL)9ULL},
/* 40  */ {(ULL)12ULL, (ULL)3ULL, (ULL)12ULL},
/* 41  */ {(ULL)0ULL, (ULL)1ULL, (ULL)9ULL},
/* 42  */ {(ULL)10ULL, (ULL)5ULL, (ULL)11ULL},
/* 43  */ {(ULL)0ULL, (ULL)3ULL, (ULL)1ULL},
/* 44  */ {(ULL)12ULL, (ULL)19ULL, (ULL)28ULL},
/* 45  */ {(ULL)0ULL, (ULL)7ULL, (ULL)7ULL},
/* 46  */ {(ULL)20ULL, (ULL)9ULL, (ULL)28ULL},
/* 47  */ {(ULL)28ULL, (ULL)1ULL, (ULL)29ULL},
/* 48  */ {(ULL)24ULL, (ULL)3ULL, (ULL)27ULL},
/* 49  */ {(ULL)12ULL, (ULL)1ULL, (ULL)15ULL},
/* 50  */ {(ULL)24ULL, (ULL)5ULL, (ULL)28ULL},
/* 51  */ {(ULL)12ULL, (ULL)1ULL, (ULL)12ULL},
/* 52  */ {(ULL)28ULL, (ULL)3ULL, (ULL)30ULL},
/* 53  */ {(ULL)2ULL, (ULL)13ULL, (ULL)14ULL},
/* 54  */ {(ULL)14ULL, (ULL)1ULL, (ULL)14ULL},
/* 55  */ {(ULL)2ULL, (ULL)9ULL, (ULL)3ULL},
/* 56  */ {(ULL)12ULL, (ULL)1ULL, (ULL)15ULL},
/* 57  */ {(ULL)0ULL, (ULL)1ULL, (ULL)3ULL},
/* 58  */ {(ULL)16ULL, (ULL)7ULL, (ULL)23ULL},
/* 59  */ {(ULL)10ULL, (ULL)5ULL, (ULL)14ULL},
/* 60  */ {(ULL)12ULL, (ULL)3ULL, (ULL)12ULL},
/* 61  */ {(ULL)4ULL, (ULL)3ULL, (ULL)6ULL},
/* 62  */ {(ULL)30ULL, (ULL)1ULL, (ULL)31ULL},
/* 63  */ {(ULL)4ULL, (ULL)1ULL, (ULL)5ULL},
/* 64  */ {(ULL)8ULL, (ULL)21ULL, (ULL)26ULL}};

/* Global Variables */
int N,D;
int outformat, type;
ULL maxNumber;
ULL ir_count = 0, pr_count = 0;
int printed = 0;
Poly_GF5 fiveN_1;
int a[MAX+1] = {0};

/* Printing routines */
void PrintString(Poly_GF5 a){
  int i,j;
  printf("1");
  for(i = N-1; i >= 0; i--){
    j = ((a.top&(ONE<<i))?4:0) + ((a.mid&(ONE<<i))?2:0) + ((a.bot&(ONE<<i))?1:0);
    printf(" %d", j == 5 ? 4 : j);
  }
  printf("\n");
}

void PrintCoeff(Poly_GF5 a){
  int i,j;
  printf("%d<SUB>1</SUB>", N);
  for(i = N-1; i >= 0; i--){
    j = ((a.top&(ONE<<i))?4:0) + ((a.mid&(ONE<<i))?2:0) + ((a.bot&(ONE<<i))?1:0);
    if(j) printf(" %d<SUB>%d</SUB>", i, j == 5 ? 4 : j);
  }
  printf("\n");
}

void PrintPoly(Poly_GF5 a){
  int i,j;
  printf("x<sup>%d</sup> + ", N);
  for(i = N-1; i > 0; i--){
    j = ((a.top&(ONE<<i))?4:0) + ((a.mid&(ONE<<i))?2:0) + ((a.bot&(ONE<<i))?1:0);
    if(j == 5) j--;
    if(j > 1){
      if(i != 1) printf("%dx<sup>%d</sup> + ", j, i);
      else printf("%dx + ", j);
    } else if(j == 1){
      if(i != 1) printf("x<sup>%d</sup> + ", i);
      else printf("x + ");
    }
  }
  j = ((a.top&(ONE))?4:0) + ((a.mid&(ONE))?2:0) + ((a.bot&(ONE))?1:0);
  if(j == 5) j--;
  printf("%d\n", j);
}

void PrintBit(Poly_GF5 a){
  int i;
  for(i = N-1; i >= 0; i--) printf("%d", a.top&(ONE<<i) ? 1 : 0); printf("\n");
  for(i = N-1; i >= 0; i--) printf("%d", a.mid&(ONE<<i) ? 1 : 0); printf("\n");
  for(i = N-1; i >= 0; i--) printf("%d", a.bot&(ONE<<i) ? 1 : 0); printf("\n");
}

void PrintVal(Poly_GF5 a){
  int i, j;
  ULL res = 0;
  for(i = 63; i >= 0; i--){
    res *= 5;
    j = ((a.top&(ONE<<i))?4:0) + ((a.mid&(ONE<<i))?2:0) + ((a.bot&(ONE<<i))?1:0);
    if(j == 5) j--;
    res += j;
  }
  printf("%lld\n", res);
}

/* Counting bits in O(number of bits) */
int bitcount(ULL x){
  int c = 0;
  while(x){
    c++;
    x &= x-1;
  }
  return c;
}

/* Density - (By our encoding, we count number of bits in the middle OR-ed with bottom word) */
int density(Poly_GF5 a){
  return bitcount(a.mid|a.bot);
}

/* Returns a + b (45 bit operations) */
Poly_GF5 add(Poly_GF5 a, Poly_GF5 b){
  Poly_GF5 res;

  ULL t04 = a.mid | b.top; 
  ULL t08 = a.top | b.mid;
  ULL t10 = a.top | b.top;
  ULL t12 = a.top | a.mid;
  ULL t23 = a.top & b.mid;
  ULL t31 = a.top ^ a.bot;
  ULL t37 = a.mid & b.mid;

  ULL t17 = a.mid ^ t10;
  ULL t16 = a.bot & t17;
  ULL t15 = b.top ^ t16;
  ULL t14 = b.mid ^ t15;
  ULL t13 = b.bot & t14;
  ULL t11 = t12 ^ t13;
  ULL t09 = t10 ^ t11;
  ULL t07 = t08 ^ t09;
  ULL t06 = a.mid | a.bot;
  ULL t05 = t06 & t07;
  ULL t03 = t04 ^ t05;
  ULL t02 = a.bot & b.mid;
  ULL t01 = t02 ^ t03;

  ULL t36 = a.mid ^ t37;
  ULL t35 = a.bot ^ t36;
  ULL t34 = b.top | t35;
  ULL t33 = b.top ^ t34;
  ULL t32 = t12 ^ t33;
  ULL t30 = t31 & t32;
  ULL t29 = t10 ^ t30;
  ULL t28 = t23 ^ t29;
  ULL t27 = a.top & b.bot;
  ULL t26 = t27 | t28;
  ULL t25 = a.top | b.bot;
  ULL t24 = t25 & t26;
  ULL t22 = a.top ^ t23;
  ULL t21 = t22 ^ t24;
  ULL t20 = a.top ^ t08;
  ULL t19 = t20 ^ t21;
  ULL t18 = t04 ^ t19;

  ULL t45 = a.mid ^ t20;
  ULL t44 = a.bot & t45;
  ULL t43 = b.top | t44;
  ULL t42 = b.bot & t43;
  ULL t41 = b.bot ^ t42;
  ULL t40 = t31 ^ t41;
  ULL t39 = t10 ^ t40;
  ULL t38 = t37 ^ t39;

  res.top = t01;
  res.mid = t18;
  res.bot = t38;
  return res;
}

/* Returns -1 if a < b, 0 if a == b, +1 if a > b */
int cmp(Poly_GF5 a, Poly_GF5 b){
  int i, x, y;
  for(i = N-1; i >= 0; i--){
    x = ((a.top&(ONE<<i))?4:0) + ((a.mid&(ONE<<i))?2:0) + ((a.bot&(ONE<<i))?1:0);
    y = ((b.top&(ONE<<i))?4:0) + ((b.mid&(ONE<<i))?2:0) + ((b.bot&(ONE<<i))?1:0);
    if(x < y) return -1;
    if(x > y) return 1;
  }
  return 0;
}

/* Returns a - b, (assuming a > b) */
Poly_GF5 subtract(Poly_GF5 a, Poly_GF5 b){
  int i, borrow = 0, x, y;
  Poly_GF5 r = {0,0,0};
  for(i = 0; i < N; i++){
    x = ((a.top&(ONE<<i))?4:0) + ((a.mid&(ONE<<i))?2:0) + ((a.bot&(ONE<<i))?1:0);
    if(x == 5) x--;
    x -= borrow;
    y = ((b.top&(ONE<<i))?4:0) + ((b.mid&(ONE<<i))?2:0) + ((b.bot&(ONE<<i))?1:0);
    if(y == 5) y--;
    if(x < y){
      borrow = 1;
      x += 5;
    } else {
      borrow = 0;
    }
    switch(x-y){
    case 1: r.bot |= (ONE<<i); break;
    case 2: r.mid |= (ONE<<i); break;
    case 3: r.mid |= (ONE<<i); r.bot |= (ONE<<i); break;
    case 4: r.top |= (ONE<<i); r.bot |= (ONE<<i); break;
    }
  }
  return r;
}

/* Returns a % b */
Poly_GF5 mod(Poly_GF5 a, Poly_GF5 b){
  Poly_GF5 r = {0,0,0};
  int i;
  for(i = N-1; i >= 0; i--){
    r.top <<= 1; r.mid <<= 1; r.bot <<= 1;
    if(a.top & (ONE<<i)) r.top++;
    if(a.mid & (ONE<<i)) r.mid++;
    if(a.bot & (ONE<<i)) r.bot++;
    while(cmp(b,r) <= 0){
      r = subtract(r, b);
    }
  }
  return r;
}

/* Returns the gcd of a and b (a and b treated as ternary numbers */
Poly_GF5 gcd(Poly_GF5 a, Poly_GF5 b){
  if(!b.top && !b.mid && !b.bot) return a;
  return gcd(b, mod(a, b));
}

/* Returns (a * b) mod p (where p = poly_table[N]) */
Poly_GF5 multmod(Poly_GF5 a, Poly_GF5 b){
  Poly_GF5 t = a, result = {0,0,0};
  Poly_GF5 temp;
  ULL top_bit = ONE << (N-1);
  int i,x;
  
  for(i = 0; i < N; i++){
    x = ((b.top&(ONE))?4:0) + ((b.mid&(ONE))?2:0) + ((b.bot&(ONE))?1:0);    
    if(x == 5) x--;
    switch(x){
    case 1: result = add(result, t); break;
    case 2: result = add(result, t); result = add(result, t); break;
    case 3: temp = add(t,t); result = add(result,temp); result = add(result, t); break;
    case 4: temp = add(t,t); temp = add(temp,temp); result = add(result,temp); break;
    }
    x = ((t.top&(top_bit))?4:0) + ((t.mid&(top_bit))?2:0) + ((t.bot&(top_bit))?1:0);    
    if(x == 5) x--;
    t.top = (t.top & ~top_bit) << 1;
    t.mid = (t.mid & ~top_bit) << 1;    
    t.bot = (t.bot & ~top_bit) << 1;    
    switch(x){
    case 1: t = add(t,poly_table[N]); break;
    case 2: t = add(t,poly_table[N]); t = add(t,poly_table[N]); break;
    case 3: temp = add(poly_table[N], poly_table[N]); t = add(t,temp); t = add(t,poly_table[N]); break;
    case 4: temp = add(poly_table[N], poly_table[N]); temp = add(temp,temp); t = add(t,temp); break;
    }
    b.top >>= 1;
    b.mid >>=1;
    b.bot >>= 1;
  }
  return result;
}

/* Returns (a^power) mod p (where p = poly_table[N]) */
Poly_GF5 powmod(Poly_GF5 a, Poly_GF5 power){
  Poly_GF5 t = a, t2, t4, result = {0,0,ONE};
  int x;

  while(power.top || power.mid || power.bot){
    x = ((power.top&(ONE))?4:0) + ((power.mid&(ONE))?2:0) + ((power.bot&(ONE))?1:0);    
    if(x == 5) x--;
    t2 = multmod(t,t);
    t4 = multmod(t2,t2);    
    switch(x){
    case 1: result = multmod(result, t); break;
    case 2: result = multmod(result, t2); break;
    case 3: result = multmod(result, multmod(t2,t)); break;
    case 4: result = multmod(result, t4); break;
    }
    t = multmod(t,t4);
    power.top >>= 1;
    power.mid >>= 1;
    power.bot >>= 1;
  }
  return result;
}

/* Calculates the minimum polynomial, given a necklace */
Poly_GF5 minpoly(Poly_GF5 necklace){
  Poly_GF5 root = {0,0,TWO}, result = {0,0,0}, root4;
  Poly_GF5 f[MAX]; /* f[i] contains the polynomials listing the
		      coefficient of x^i in terms of the primitive root */
  int i, j;

  f[0].top = 0;
  f[0].mid = 0;
  f[0].bot = ONE;
  for(i = 1; i < N; i++) f[i].top = f[i].mid = f[i].bot = 0;

  root = powmod(root, necklace); 

  for(i = 0; i < N; i++){
    if(i){
      root4 = multmod(root,root);
      root4 = multmod(root4,root4);
      root = multmod(root4, root);
    }
    for(j = N-1; j >= 1; j--)
      f[j] = add(f[j-1], multmod(f[j], root));
    f[0] = multmod(f[0], root);
  }

  for(i = N-1; i >= 0; i--){
    j = ((f[i].top==ONE)?4:0) + ((f[i].mid==ONE)?2:0) + ((f[i].bot==ONE)?1:0);    
    switch(j){
    case 1:
      result.bot |= ONE << i;
      break;
    case 2:
      result.mid |= ONE << i;
      break;
    case 3:
      result.mid |= ONE << i;
      result.bot |= ONE << i;
      break;
    case 5:
      result.top |= ONE << i;
      result.bot |= ONE << i;
      break;
    }
  }
   return result;
}

void OutputPoly(Poly_GF5 poly){
  switch(outformat){
  case 0: PrintBit(poly); break;
  case 1: PrintString(poly); break;
  case 2: PrintPoly(poly); break;
  }
  printed++;
}

void PrintIt(){
  int i;
  Poly_GF5 necklace = {0,0,0};
  Poly_GF5 gcdResult;
  Poly_GF5 mpoly;
  
  if(++iter_limit > MAX_ITERATIONS){
    printf("</TABLE>\n<BR>");
    if (type == 0) {
      printf( "The first %llu irreducible polynomials<BR>\n", ir_count);
      printf("<FONT COLOR=0000ff>");
    }
    printf( "The first %llu primitive polynomials\n<br><br>", pr_count );
    printf("<br><b><blink><font color=ff0000>Iteration limit exceeded !!</B><br></blink></font>\n");
    exit(0);     
  }
  
  for(i = 1; i <= N; i++){
    necklace.top <<= 1;
    necklace.mid <<= 1;
    necklace.bot <<= 1;
    switch(a[i]){
    case 1: necklace.bot++; break;
    case 2: necklace.mid++; break;
    case 3: necklace.mid++; necklace.bot++; break;
    case 4: necklace.top++; necklace.bot++; break;
    }
  }

  mpoly = minpoly(necklace);
  if(density(mpoly)+1 > D) return;

  ir_count++;  
  gcdResult = gcd(necklace, fiveN_1);

  if(gcdResult.top == 0 && gcdResult.mid == 0 && gcdResult.bot == ONE){
    printf("<TR>");
    ++pr_count;
    ++printed;
    if(outformat & 1){
      printf("<TD>");
      if(type == 0) printf("<FONT COLOR=0000ff>");
      PrintString(mpoly);
      if(type == 0) printf("</FONT>");
    }
    if(outformat & 2){
      printf("<TD>");
      if(type == 0) printf("<FONT COLOR=0000ff>");
      PrintCoeff(mpoly);
      if(type == 0) printf("</FONT>");
    } 
    if(outformat & 4){
      printf("<TD>");
      if(type == 0) printf("<FONT COLOR=0000ff>");
      PrintPoly(mpoly);
      if(type == 0) printf("</FONT>");
    }
  } else if(type == 0){
    ++printed;
    printf("<TR>");
    if(outformat & 1){
      printf("<TD>");
      PrintString(mpoly);
    } 
    if(outformat & 2){
      printf("<TD>"); 
      PrintCoeff(mpoly);
    }
    if(outformat & 4){
      printf("<TD>");
      PrintPoly(mpoly);
    }
  }
  if(maxNumber && printed >= maxNumber){
    printf("</TABLE>"); 
    exit(1);
  }
}

void Gen(int t, int p){
  int j;
  if(t > N){
    if(p == N) PrintIt();
  } else {
    a[t] = a[t-p]; 
    Gen(t+1,p);
    for(j=a[t-p]+1; j<=4; j++) {
      a[t] = j; 
      Gen(t+1,t);
    }
  }
}

void ProcessInput(int argc, char *argv[]){
  int i;
  outformat = 0;
  type = 0;
  maxNumber = 0;

  if (argc > 1) N = atoi(argv[1]);
  if (argc > 2) D = atoi(argv[2]); else D = N+1;
  if (argc > 3) outformat = atoi(argv[3]); else outformat = 1;
  if (argc > 4) type = atoi(argv[4]); 
  if (argc > 5) maxNumber = atoi(argv[5]);
  if (argc > 6 || N < 2 || N > MAX){
    fprintf(stderr,"Usage: gf5 n d format type number\n");
    fprintf(stderr,"         All params except 'n' are optional\n");
    fprintf(stderr,"         n = degree (2-%d)\n", MAX);
    fprintf(stderr,"         d = density (2-%d)\n", MAX);
    fprintf(stderr,"         format = 0,1,2 = string,ceoffs,poly\n");
    fprintf(stderr,"         type = 0/1 = all/primitive only\n");
    fprintf(stderr,"         num = max to find (0 for all)\n");
    exit( 1 );
  }
  /*
  printf( "degree      = %d\n", N );
  printf( "format      = %d\n", outformat );
  printf( "type        = %s\n", type ? "primitive" : "all" );
  printf( "num         = %lld %s\n", maxNumber, maxNumber ? "" : "(none)" );
  */
  fiveN_1.mid = 0;
  for(i = 0; i < N; i++)
    fiveN_1.top |= (ONE<<i);
  fiveN_1.bot = fiveN_1.top;
}

int main(int argc, char **argv){

  ProcessInput(argc, argv);

  printf("N = %d.  Maximum density = %d\n", N, D);
  printf("<P><TABLE BORDER=1 CELLSPACING=2 CELLPADDING=2><TR>\n");
  if (outformat & 1) {
    printf("<TH COLSPAN=1><FONT SIZE=+1>String</FONT><BR></TH>\n");
  }
  if (outformat & 2) {
    printf("<TH COLSPAN=1><FONT SIZE=+1>Coefficients</FONT><BR></TH>\n");
  }
  if (outformat & 4) {
    printf("<TH COLSPAN=1><FONT SIZE=+1>Polynomials</FONT><BR></TH>\n");
  }

  Gen(1,1);
  printf("</TABLE>\n<BR>");

  if(type == 0){
    printf("The number of irreducible polynomials = %llu<BR>\n", ir_count);
    printf("<FONT COLOR = 0000ff>");
  } 
  printf("The number of primitive polynomials   = %llu\n", pr_count);
  return 0;
}

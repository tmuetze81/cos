
/* program : labelled.c
 * purpose : generating all labelled free trees in Prufer array
 * author  : (c) Gang LI Sept. 1995
 */

#include "stdio.h"
#include "ProcessInput.c"
#include "OutputTrees.c"

#define Max 50

int
  n,        /* number of nodes in the tree */
  nn[Max-2],/* max number of each node  */
  d[Max],   /* direction  */
  a[Max];   /* Prufer array */

void gen(p)
int p;
{int i;
 if (p<n-2) gen(p+1); else
 {
   while ((a[p]==1 && d[p]==0) 
       || (a[p]==nn[p]  && d[p])) 
      {
       d[p] = 1 - d[p]; 
       if (p==1) return; else  p--;
      }
   if (d[p]) a[p]++; else a[p]--;
   PrintIt(n-2,a, a);
   if(p==n-2) gen(p);else gen(p+1);
 }
}


main(argc,argv)
int argc;
char *argv[];
{
int i,m=0,j=1;
 
  ProcessInput(argc,argv);
  if (out_format&1) outputP = 1;
  n = N;

 /* init first tree and direction array */
  for(i=1;i<=n-2;i++) {
    a[i] = 1; d[i]=1;
    nn[i] = n;
  }
 PrintIt(n-2, a, a);
 /* start to generate */

 gen(n-2);
 printf("</table><br>The total number of trees is: %d<br>\n", num);
 exit(0);
}

/* Name: rbtrees.c   
 * Purpose: Generating red-black trees in CAT 
 * Author: Richard Webber @ Oct. 1993 
 * Modified by Gang LI @ Oct. 1995
 */


#include <stdlib.h>
#include <stdio.h>
#include "ProcessInput.c"
#include "OutputTrees.c"


#define NONE (0)
#define BLACK (1)
#define RED (2)

int L[51]; /* This is for output the array using utility OutputTrees.c.*/



typedef struct RBNode RBNode;
struct RBNode {
	char colour;
	RBNode *left, *right;
	RBNode *prev, *next;
};

typedef struct RBStack RBStack;
struct RBStack {
	RBNode *head, *tail;
};

#define min(A,B) ( ((A) < (B)) ? (A) : (B) )

RBStack S;

void InitRBNode(RBNode *node);
RBNode* AllocRBNode(void);
void FreeRBNode(RBNode *node);

void InitRBStack(RBStack *S);
RBStack* AllocRBStack(void);
void FreeRBStack(RBStack *S);
void Push(RBStack *S, RBNode *node);
RBNode* Pop(RBStack *S);

void Print(RBNode *root, int i);
void ERROR(char *file, int line);
RBNode* CreateSubtree(int type);
void AddChildren(RBNode *node, int type, RBStack *S, RBNode **cur);
void AddLeaves(RBNode *node, int type);

#define min(A,B) ( ((A) < (B)) ? (A) : (B) )

void InitRBNode(RBNode *node) {
	if (node != NULL) {
		node->colour = NONE;
		node->left = node->right = node->prev = node->next =  NULL;
	}
}

RBNode* AllocRBNode(void) {
	RBNode *node = (RBNode*)malloc(sizeof(RBNode));
	InitRBNode(node);
/* printf("Alloc: %X\n",node); */
	return node;
}

void FreeRBNode(RBNode *node) {
	if (node != NULL) free(node);
}

void InitRBStack(RBStack *S) {
	if (S != NULL) {
		S->head = S->tail =  NULL;
	}
}

RBStack* AllocRBStack(void) {
	RBStack *S = (RBStack*)malloc(sizeof(RBStack));
	InitRBStack(S);
	return S;
}

void FreeRBStack(RBStack *S) {
	if (S != NULL) {
		while (S->head != NULL) FreeRBNode(Pop(S));
		free(S);
	}
}

void Push(RBStack *S, RBNode *node) {
/* printf("Push: %X\n",node); */
	if (S != NULL && node != NULL) {
		node->next = S->head;
		if (S->head != NULL)
			S->head->prev = node;
		else S->tail = node;
		S->head = node;
	}
}

RBNode* Pop(RBStack *S) {
	RBNode *node;
	if (S != NULL) {
		node = S->head;
		if (S->head != NULL) S->head = S->head->next;
		if (S->head != NULL) S->head->prev = NULL;
		if (S->tail == node) S->tail = NULL;
	} else node =  NULL;
/* printf("Pop: %X\n",node); */
	return node;
}

void Print(RBNode *root, int i) {
static int count;
	if (i) count=0; 
	if (root != NULL) {
		if (root->left != NULL || root->right != NULL) {
			if (root->colour == RED)
			      L[++count] = 2; 
			else if (root->colour == BLACK)
			      L[++count] = 1; 
			else putchar('*');
			Print(root->left,0);
			Print(root->right,0);
		} else L[++count] = 0; 
	}
}

void ERROR(char *file, int line) {
	fprintf(stderr,"Fatal Program Error in \"%s\":%d\n",file,line);
	exit(1);
}

RBNode* CreateSubtree(int type) {
	RBNode *node;
	switch (type) {
		case 2: /* Two children */
			node = AllocRBNode();
			node->colour = BLACK;
			break;
		case 3: /* Three children, left bias */
			node = AllocRBNode();
			node->colour = BLACK;
			node->left = AllocRBNode();
			node->left->colour = RED;
			break;
		case 4: /* Four children */
			node = AllocRBNode();
			node->colour = BLACK;
			node->left = AllocRBNode();
			node->left->colour = RED;
			node->right = AllocRBNode();
			node->right->colour = RED;
			break;
		case 5: /* Three children, right bias */
			node = AllocRBNode();
			node->colour = BLACK;
			node->right = AllocRBNode();
			node->right->colour = RED;
			break;
		default: /* Error */
			ERROR(__FILE__,__LINE__);
	}
/* printf("Create: %d %X\n",type,node); */
	return node;
}

void AddChildren(RBNode *node, int type, RBStack *S, RBNode **cur) {
	if (*cur == NULL)
		*cur = S->tail;
	else *cur = (*cur)->prev;
	switch (type) {
		case 2: /* Two children */
			if (*cur == NULL) ERROR(__FILE__,__LINE__);
			node->right = *cur;
			*cur = (*cur)->prev;
			if (*cur == NULL) ERROR(__FILE__,__LINE__);
			node->left = *cur;
			break;
		case 3: /* Three children, left bias */
			if (*cur == NULL) ERROR(__FILE__,__LINE__);
			node->right = *cur;
			*cur = (*cur)->prev;
			if (*cur == NULL) ERROR(__FILE__,__LINE__);
			node->left->right = *cur;
			*cur = (*cur)->prev;
			if (*cur == NULL) ERROR(__FILE__,__LINE__);
			node->left->left = *cur;
			break;
		case 4: /* Four children */
			if (*cur == NULL) ERROR(__FILE__,__LINE__);
			node->right->right = *cur;
			*cur = (*cur)->prev;
			if (*cur == NULL) ERROR(__FILE__,__LINE__);
			node->right->left = *cur;
			*cur = (*cur)->prev;
			if (*cur == NULL) ERROR(__FILE__,__LINE__);
			node->left->right = *cur;
			*cur = (*cur)->prev;
			if (*cur == NULL) ERROR(__FILE__,__LINE__);
			node->left->left = *cur;
			break;
		case 5: /* Three children, right bias */
			if (*cur == NULL) ERROR(__FILE__,__LINE__);
			node->right->right = *cur;
			*cur = (*cur)->prev;
			if (*cur == NULL) ERROR(__FILE__,__LINE__);
			node->right->left = *cur;
			*cur = (*cur)->prev;
			if (*cur == NULL) ERROR(__FILE__,__LINE__);
			node->left = *cur;
			break;
		default: /* Error */
			ERROR(__FILE__,__LINE__);
	}
}

void AddLeaves(RBNode *node, int type) {
	switch (type) {
		case 2: /* Two children */
			node->right = AllocRBNode();
			node->left = AllocRBNode();
			node->right->colour = BLACK;
			node->left->colour = BLACK;
			break;
		case 3: /* Three children, left bias */
			node->right = AllocRBNode();
			node->left->right = AllocRBNode();
			node->left->left = AllocRBNode();
			node->right->colour = BLACK;
			node->left->right->colour = BLACK;
			node->left->left->colour = BLACK;
			break;
		case 4: /* Four children */
			node->right->right = AllocRBNode();
			node->right->left = AllocRBNode();
			node->left->right = AllocRBNode();
			node->left->left = AllocRBNode();
			node->right->right->colour = BLACK;
			node->right->left->colour = BLACK;
			node->left->right->colour = BLACK;
			node->left->left->colour = BLACK;
			break;
		case 5: /* Three children, right bias */
			node->right->right = AllocRBNode();
			node->right->left = AllocRBNode();
			node->left = AllocRBNode();
			node->right->right->colour = BLACK;
			node->right->left->colour = BLACK;
			node->left->colour = BLACK;
			break;
		default: /* Error */
			ERROR(__FILE__,__LINE__);
	}
}

void /* Proto */ P(int s, int d, int p, RBNode *cur);

void Recurse(
    int type, int p_eq_d, int s, int d, int p, RBNode *cur
) {
	RBNode *node;
	node = CreateSubtree(type);
	if (!p_eq_d)
		AddChildren(node, type, &S, &cur);
	else AddLeaves(node, type);
	Push(&S, node);
	P(s, d, p, cur);
	Pop(&S);
} /* Recurse */

void P(int s, int d, int p, RBNode *cur) {
	int i;
	if (s == 1 && d == 0) {
		Print(S.head,1);  PrintIt(2*N+1,L,L);
		if (S.head->left->colour == BLACK &&
				S.head->right->colour == BLACK) {
			S.head->colour = RED;
			Print(S.head,1); PrintIt(2*N+1,L,L);
		}
	} else if (s > 4 && d >= 0) { /* A[p] = i */
		for (i = 2; i <= min(s-2,4); i++) {
			Recurse(i, p == d, s-i, d+1, p+1, cur);
			if (i == 3) Recurse(5, p == d, s-i, d+1, p+1, cur);
		}
	} else if (s == 4) {
		Recurse(2, p == d, 2, d+1, p+1, cur); /* A[p] = 2 */
		Recurse(4, p == d, d+1, 0, p+1, cur); /* A[p] = 4 */
	} else if (s >= 2) {	
		Recurse(s, p == d, d+1, 0, p+1, cur); /* A[p] = s */ 
		if (s == 3) Recurse(5, p == d, d+1, 0, p+1, cur);
	}
} /* P */

int main(int argc, char *argv[]) {
/*	int n;*/
	InitRBStack(&S);
	ProcessInput(argc, argv);
/*	printf("Enter n > "); fflush(stdout);
	scanf("%d",&n);*/

	if (out_format&1) outputL = 1;
	if (out_format&2) outputB = 1;
	if (N < 1 ) {
		printf("Sorry, <i>ni</i> must be at least 1.\n");
	} else P((N+1),0,0,NULL);
	printf("</table><p>The total number of trees is: %4d\n<br>",num);
	return 0;
}


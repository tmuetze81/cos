/********************************
 *  Program:  ComCode-Tree.c
 *  Author:  Chris Deugau
 *
 *  Description:  Using paper in IEEE Transactions on Info Theory
 *                from Oct 1967, using the recursive T(2s, q+s) to
 *                recursively generate the tree from the bottom to the top.
 *
 *  Started:  May 9th, 2005
 *
 *  Update History:
 *     May 9th - started  (CJD)
 *     May 11th - added handling for height and keys
 ********************************/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "OutputTrees.c"

#define PRECOMPUTE_SIZE 100
#define MAX_N 50
#define MAXKIDS 2

int a1[PRECOMPUTE_SIZE+1];
int p1[PRECOMPUTE_SIZE+1];
int testingOutput;
int methodCalls;
int ifCount1;
int forLoop1, forLoop2, forLoop3;

int outputArray[MAX_N+1];
int outputIDX;

Node **nodeList;
int *freeList;
int freeIDX;

/*************** STRUCTS FOR TREE **************/
Node *makeNode( void );
void levelize(Node *root, Node *x, int y);
/*********************************************/

// precompute precomputes the a1(n) and p1(n) arrays for testing what the
// first q column a one occurs for every p.
void precompute();

// recursive call to generate trees with p bottom level nodes, and q leaves
// currList will contain a list of the trees created so far
void T(int pPar, int qPar, int leftMost, int rightMost, int startOfChild);

/**********************/
void levelize(Node *root, Node *x, int level) {
  int i;

  if (x == NULL) {
    return;
  }

  if (level == 0) outputIDX = 1;
  if (level > 0) x->father = root;

  x->height = level;

  if (x->son[0] == NULL) { 
    // x is a leaf node, update output array
    outputArray[outputIDX] = level;
    outputIDX++;
  }
  else {
    levelize(x, x->son[0], level+1);
    levelize(x, x->son[1], level+1);
  }
}

/**********************/
void precompute() {
  int i;

  /* base cases for a1(n) */
  a1[1] = 1; a1[2] = 1; a1[3] = 2;
  p1[1] = 0; p1[1*2] = 1+1; p1[2*2] = 3+1;

  for (i=4; i <= PRECOMPUTE_SIZE; i++) {
    a1[i] = a1[i - 1 - a1[i-1]] + a1[i - 2 - a1[i-2]];

    // if a1[i-1] != a1[i], then we can update p1[a1[i]] = i
    if (a1[i-1] != a1[i]) {
      // i+1 gives us the first column q with a 1.
      p1[a1[i] * 2] = i+1;
    }
  }
}
/**********************/
void T(int pPar, int qPar, int leftMost, int rightMost, int startOfChild) {
  int i,j;
  int s,q,p;
  int numberOfInners;
  int newEndOfChild;

  Node *newNode;
  DimStruct dim;

  methodCalls++;

  /***** BASE CASES *****/
  if ((pPar == 1) && (qPar == 1)) {
    // combine the left and right tree into the root node, 
    // and print out the finished tree
    newNode = nodeList[freeList[freeIDX++]];

    newNode->key = 1;
    newNode->father = NULL;

    newNode->son[1] = nodeList[rightMost];
    if (rightMost > startOfChild) {
      nodeList[rightMost]->key = 0;
      nodeList[rightMost]->son[0] = NULL;
      nodeList[rightMost]->son[1] = NULL;
    }

    newNode->son[0] = nodeList[leftMost];
    if (leftMost > startOfChild) {
      nodeList[leftMost]->key = 0;
      nodeList[leftMost]->son[0] = NULL;
      nodeList[leftMost]->son[1] = NULL;
    }

    // set level #s on tree structure for printing, etc
    levelize(newNode,newNode,0);

    /********* what to print out *************/
    if (testingOutput) {
      num++; 
    }
    else {
      PrintIt(N, outputArray, (int *) newNode);
    }

    freeIDX--;
    return;
  }
  /***** END OF BASE CASES *****/

  // number of inner nodes for this level of tree
  numberOfInners = (leftMost - rightMost + 1) / 2;

  i = leftMost + 1;
  j = rightMost;

  // make next set of inner nodes
  for (; i < (leftMost + 1 + numberOfInners); i++) {
    forLoop1++;

    newNode = nodeList[freeList[freeIDX++]];
    newNode->key = 1;

    if (j > startOfChild) {
      newNode->son[1] = nodeList[j];
      nodeList[j]->key = 0;
      nodeList[j]->son[0] = NULL;
      nodeList[j]->son[1] = NULL;
      j++;
    }
    else {
      newNode->son[1] = nodeList[j++];
    }

    if (j > startOfChild) {
      newNode->son[0] = nodeList[j];
      nodeList[j]->key = 0;
      nodeList[j]->son[0] = NULL;
      nodeList[j]->son[1] = NULL;
      j++;
    }
    else {
      newNode->son[0] = nodeList[j++];
    }
  }

  newEndOfChild = freeIDX-1;

  for (; i < (leftMost + 1 + pPar); i++) {
    freeIDX++;
  }

  // make recursive call
  s = pPar / 2;
  q = qPar - s;

  for (p=s; p <= q; p++) {
    forLoop3++;

    if ((p % 2 == 0) || ((p == 1) && (q == 1))) {
      if (q >= p1[p]) {
	T(p, q, i-1, leftMost+1, newEndOfChild);
      }
    }
  }

  freeIDX = freeIDX - pPar;
}

int main(int argv, char **argc) {
  int i;

  /******** process inputs to program, set up output formats *****/
  ProcessInput(argv, argc);
  if (out_format == 101) testingOutput = 1;
  if (out_format & 1) outputCC = 1;
  if (out_format & 8) outCCGif = 1;
  /**************************************************************/

  // precompute a1 and p1 arrays to see when T(p, q) > 0
  precompute();

  // since we know there are n bottom level nodes in our tree, we know
  // that every tree will have 2n-1 total nodes.  We can make an array
  // of 2n-1 tree nodes to being with
  nodeList = (Node **) malloc(sizeof(Node *) * ((2 * N) - 1));
  freeList = (int *) malloc(sizeof(int) * ((2 * N) - 1));

  assert(nodeList);
  assert(freeList);

  for (i=0; i < (2 * N - 1); i++) {
    nodeList[i] = makeNode();   
    assert(nodeList[i]);
    freeList[i] = i;
  } 

  methodCalls = 0;
  ifCount1 = 0;
  forLoop1 = 0;
  forLoop2 = 0;
  forLoop3 = 0;

  printf("i, N, methods, if-1, for-1, for-2, for-3\n");


  for (i=2; i <= N; i=i+2) {
q    outputIDX = 0;
    freeIDX = 0;
    methodCalls = 0;
    ifCount1 = 0;
    forLoop1 = 0;
    forLoop2 = 0;
    forLoop3 = 0;

    if (N >= p1[i]) {
      T(i,N,-1,-1,-1);
      printf("%d,%d,%d,%d,%d,%d,%d\n", i, N, methodCalls, ifCount1, forLoop1, forLoop2, forLoop3);
    }
  }



  if (out_format != 101) {
    printf("</table>");
    printf("<P>The total number of trees is:  %d\n", num);
  }

  if (out_format == 101) {
        printf("Total Number of Trees:  %d\n", num);
        printf("The total number of calls:  %d\n", methodCalls);
        printf("%d,%d,%d,%d\n", N, num, methodCalls, ifCount1);
  }

  return 0;
}












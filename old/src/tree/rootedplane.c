/*===============================================*/
/* Program: Generating plane rooted trees        */
/* Input: n                                      */
/* Output: All tree sequences with n nodes       */
/* Programmer: Lara Wear                         */
/* Output binary: planerooted (Feb.1,2003)       */
/* Algorithm:  Joe Sawada, University of Victoria*/
/*	       March 22, 2002			 */
/*===============================================*/

#ifndef ROOTEDPLANE_C 
#define ROOTEDPLANE_C


#include <stdlib.h>
#include "ProcessInput.c"
#include "OutputTrees.c"
#include "LevelToParent.c"

static int a[50];
static int counter = 0;
int N; // # nodes

//void printIt();
void RootedPlane (int t, int p);

int main(int argc, char *argv[])
{
      int i;

      // Process command line args
      ProcessInput(argc,argv);
      if (out_format & 1) outputP = 1; /* main representation is parent array */
      if (out_format & 2) outputL = 1; /* alternate representation is level sequence, just like everyone else */
	  if (out_format & 8) outputPNG = 1; /* drawing the tree */
      // initialize array a to 0111... (a[0] not used)
      a[0] = 0; a[1] = 0;
      for (i = 2; i <= N; i++)
          a[i] = 1;
      if (N == 1) {
		 // PrintIt(N, &a[0], levelToParent(&a[0], N)); counter = 1;
	  	PrintIt(N, levelToParent(&a[0], N), &a[0]); counter = 1;
	  }
      else RootedPlane(2,1);
      printf("</table><P>The total number of trees is: %4d<BR>\n",counter);
      return 0;
} // main

// RootedPlane - generates rooted plane trees with N nodes
// Algorithm from ....
// inputs: t - # nodes, p - # trees for t nodes
// outputs: none

void RootedPlane (int t, int p) {
     int j;
     if (t == N) {
        if ( ((N-1)%p) == 0 ) {
	//	PrintIt(N,  &a[0], levelToParent(&a[0], N));
		PrintIt(N, levelToParent(&a[0], N), &a[0]);
			counter++;
	}
     }  // if
     else {
         for (j = a[t-p+1]; j <= a[t] + 1; j++) {
             a[t+1] = j;
             if (j == a[t-p+1])
                RootedPlane(t+1,p);
             else
                RootedPlane(t+1, t);
         } // for
     } // else
} // RootedPlane

/*
void printIt () {
     counter++;
     //cout << endl << counter << ": ";
     for (int i=0; i < N; i++) {
         cout << a[i] << " ";
     } // for
     cout << endl;
} // printIt
*/

#endif

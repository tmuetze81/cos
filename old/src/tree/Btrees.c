/* BTrees.c   Aug,22.1995
 *
 * This is a program to generate B-trees 
 */

#ifndef BTREES_C 
#define BTREES_C

#include<stdio.h>
#include "ProcessInput.c"
#include "OutputTrees.c"

int n, 
    d, 
    m, 
    m2, 
    a[100];

int min ( x, y )
int x,y;
{ if ( x<y ) return(x); else return(y); }


void P( s, d, p)
int s,d,p;
{
int i;
  if (s == 1 && d == 0) PrintIt( p-1,a,a ); else
  if (s > m && d >= 0) {
     for(i=m2;i<=min(s-m2,m);i++) {
        a[p] = i;  P( s-i, d+1, p+1 );
     };
  } else
  if (s == m) {
     if (m%2 == 0) {
        a[p] = m2;  P( m2, d+1, p+1 );
     };
     a[p] = m;  P(d+1, 0, p+1 );
  } else 
  if ((s >= m2 && d > 0) || s >= 2) {
     a[p] = s;  P( d+1, 0, p+1 );
  };   
}


main(argc, argv)
int argc;
char *argv[];
{
  int i = 0;

  /* first set all the parameters */
  n=0;
  m=0;
  ProcessInput(argc, argv);
  if (out_format&1) outputP = 1;
  if (out_format&2) outputR = 1;
  n = N; m = M;
  m2 = (m+1)/2;
  P( n, 0, 1 );
  printf("</table><P>The total number of trees is : %4d<BR>\n",num);
  exit(0);
}

#endif

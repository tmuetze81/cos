/****given a value for n and an output mode specified by ProcessInput,
as well as the number in each subset, k,
this program prints out the subsets in colex order*****/

#include <stdio.h>
#include "ProcessInput.c"

/*********globals*********/
int bitstring = 0;
int list = 0;
int bitchange = 0;
int A[MAX];

/************local functions**********/

/*prints out the generated number according to the output format*/
void printit(int b1, int b2, int b3, int b4)
{
  int i;
  int beg = 1;

  if (!LIMIT) return;
/* the bitchange is indicated for the previous line */
  // -1 indicates no change on the first string
  if (bitchange && (b1 != -1)) {
	  if (A[b1-1] == 0) {
		  printf("<td align=center>%d-%d",b1,b2);
	  } else {
		  printf("<td align=center>%d-%d",b2,b1);
	  }
	  if (b3 != -1) {
		  if (A[b3-1] == 0) {
			  printf(",&nbsp;%d-%d</td>\n",b3,b4);
		  } else {
			  printf(",&nbsp;%d-%d</td>\n",b4,b3);
		  }
	  } else {
		  printf("</td>\n");
	  }
	  printf("</tr>\n");
  }
  printf("<tr>");
  if(bitstring) {
    printf("<TD ALIGN=CENTER>");
    for(i=0;i<N;i++)
      printf("%d",A[i]);
    printf("</td>\n");
  }
  if(list) {
    printf("<TD ALIGN=CENTER>{");
    for(i=0;i<N;i++) {
      if(A[i]) {
	printf("%s%d",beg ? "" : ",",i+1);
	beg = 0;
      }
    }
    printf("}</td>\n");
  }
  if (!bitchange) {
	  printf("</tr>\n");
  }
  LIMIT--;
}

/* flips the bits that are specified */
void swap(int a, int b) {
	A[a-1] = (1-A[a-1])%2;
	A[b-1] = (1-A[b-1])%2;
}

/* initializes the first string, which the algorithm does not consider */
void init_A(int s, int t) {
	int i;
	for (i=0; i<t; i++) {
		A[i] = 1;
	}
	for (i=t; i<N; i++) {
		A[i] = 0;
	}
	printit(-1,-1,-1,-1);
}

/*recursively generates the array A*/
void coolex(int s, int t)
{
   if(!LIMIT)
      return;
   if(s>1){
	   coolex(s-1, t);
	   if (t > 1) {
	   	swap(1, t);
       }
	   swap( s+t, s+t-1 );
	   if (t > 1) {
	     printit(1,t,s+t,s+t-1);
	   } else {
		  printit(s+t,s+t-1,-1,-1);
       }
   }
   if (t > 1) {
	   coolex( s, t-1);
	   swap( t-1, s+t-1 );
	   printit(t-1,s+t-1,-1,-1);
   }
}

/***********main program********/

int main(int argc, char **argv)
{
  int i;
  int count;

  ProcessInput(argc, argv);
  count = LIMIT;

  if(out_format & 1)
    bitstring = 1;
  if(out_format & 2)
    list = 1;
  if(out_format & 4)
	  bitchange = 1;
  int s = N-K;
  int t = K;
  init_A(s,t);
  swap(1,t+1);
  printit(1,t+1,-1,-1);
  coolex(s,t);
  if (bitchange) { // finish the row gracefully
	  printf("<td align=center>%d-%d</td>\n",N,K);
  }
  printf("</TABLE><BR>Total number of combinations = %d<BR>\n",count-LIMIT);
  if(LIMIT)
     return(0);
  else
     return(-1);
}

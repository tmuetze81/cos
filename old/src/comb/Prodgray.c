/****given a value for the base of each digit in a string, and output
mode specified by ProcessInput, this program prints out the product
space in Gray code order *****/

#include <stdio.h>
#include "ProcessInput.c"

/*********globals*********/
int bitstring = 0; /*output formats*/
int bitchange = 0;
int list = 0;
int A[MAX]; /*the array of bits*/
int P[MAX]; /*the boolean array that keeps track of whether we*/
	    /*are going up or down*/

/************local functions**********/

/*prints out the generated number according to the output format*/
/*if -1 passed then the bitchange is not printed*/
void printit(bit, newvalue)
int bit, newvalue;
{
  int i;
  int j;
  int beg = 1;

  if(!LIMIT)
    return;
  printf("<TR>");
  if(bitstring) {
    printf("<TD ALIGN=CENTER>");
    for(i=0;i<N;i++)
      if (i==N-bit) printf("<B><FONT COLOR=008800>%d</FONT></B>",A[i]);
      else printf("%d",A[i]);
    printf("<BR></TD>\n");
  }
  if(list) {
    printf("<TD ALIGN=CENTER>{");
    for(i=N-1; i>=0; i--) {
      if(A[i]) {
	for(j=1; j<=A[i]; j++) {
	  printf("%s%d",beg ? "" : ",",N-i);
	  beg = 0;
        }
      }
    }
    printf("}<BR></TD>\n");
  }
  if(bitchange) {
    if(bit!=-1) {
      printf("<TD ALIGN=CENTER>(%d,%d)<BR></TD>\n",bit,newvalue);
    }
    else {
      printf("<TD ALIGN=CENTER><BR></TD>\n");
    }
  }
  printf("</TR>");
  LIMIT--; /*count down the number of printable objects*/
}

/*recursively generates the array A*/
/*in Gray code order*/
void prod(n)
int n;
{
  int i;

  if((n<N)&&(LIMIT)) {
    prod(n+1);
    if(P[n]) {
      for(i=1;i<lambda[n+1];i++) {
        printit(N-n,i);
        A[n] = i;
        prod(n+1);
      }
    }
    else {
      for(i=lambda[n+1]-2;i>=0;i--) {
        printit(N-n,i);
        A[n] = i;
        prod(n+1);
      }
    }
    P[n] = (1-P[n])%2;
  }
}

/***********main program********/

int main(int argc, char **argv)
{
  int i;
  int count;

  ProcessInput(argc, argv);
  count = LIMIT;

  if(out_format & 1)
    bitstring = 1;
  if(out_format & 2)
    list = 1;
  if(out_format & 4)
    bitchange = 1;

/*initializing the arrays*/
  for(i=0;i<N;i++)
    A[i] = 0;
  for(i=0;i<N;i++)
    P[i] = 1;

  prod(0);
  printit(-1);
  printf("</TABLE><BR>Total number of products = %d<BR>\n",count-LIMIT);
  if(LIMIT)
    return(0);
  else
    return(-1);
}

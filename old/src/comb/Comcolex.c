/****given a value for n and an output mode specified by ProcessInput,
as well as the number in each subset, k,
this program prints out the subsets in colex order*****/

#include <stdio.h>
#include "ProcessInput.c"

/*********globals*********/
int bitstring = 0;
int list = 0;
int A[MAX];

/************local functions**********/

/*prints out the generated number according to the output format*/
void printit()
{
  int i;
  int beg = 1;

  printf("<TR>");
  if(bitstring) {
    printf("<TD ALIGN=CENTER>");
    for(i=0;i<N;i++)
      printf("%d",A[i]);
    printf("<BR></TD>\n");
  }
  if(list) {
    printf("<TD ALIGN=CENTER>{");
    for(i=N-1;i>=0;i--) {
      if(A[i]) {
	printf("%s%d",beg ? "" : ",",N-i);
	beg = 0;
      }
    }
    printf("}<BR></TD>\n");
  }
  printf("</TR>");
  LIMIT--;
}

/*recursively generates the array A*/
void Colex(n,k)
int n,k;
{
   if(!LIMIT)
      return;
   if(n==0)
      printit();
   else {
      if(k<n) {
	 A[n-1] = 0;
         Colex(n-1,k);
      }
      if(k>0) {
	 A[n-1] =1;
	 Colex(n-1,k-1);
      }
   }
}

/***********main program********/

int main(int argc, char **argv)
{
  int i;
  int count;

  ProcessInput(argc, argv);
  count = LIMIT;

  if(out_format & 1)
    bitstring = 1;
  if(out_format & 2)
    list = 1;

  Colex(N,K);
  printf("</TABLE><BR>Total number of combinations = %d<BR>\n",count-LIMIT);
  if(LIMIT)
     return(0);
  else
     return(-1);
}

/* except.c - Exceptions */

/* Copyright 1991, Kenny Wong and Frank Ruskey. All Rights Reserved.
 *
 * This program is free software. You may use this software, and copy,
 * redistribute, and/or modify it under the terms of version 2 of the 
 * GNU General Public License. This and all modified works may not be
 * sold for profit. Please do not remove this notice or the original 
 * copyright notices.
 *
 * This software is provided "as is" and without any express or implied
 * warranties, including, without limitation, any implied warranties
 * of merchantability or fitness for a particular purpose. You bear all
 * risk as to the quality and performance of this software.
 *
 * You should have received a copy of the GNU General Public License
 * with this software (see the file COPYING); if not, write to the
 * Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Please report bugs, fixes, applications, modifications, and requests to:
 *
 * Kenny Wong or Frank Ruskey
 * Department of Computer Science, University of Victoria
 * PO Box 3055, Victoria, BC, Canada, V8W 3P6
 * Internet: kenw@csr.UVic.CA or fruskey@csr.UVic.CA
 *
 * All correspondence should include your name, address, phone number,
 * and preferably an electronic mail address.
 */

#include <stdio.h>
#include <errno.h>

#include "genle.h"
#include "except.h"

/* application error messages */
static char *sAppErrors[] = {
	"Okay",
	"Oh oh",
	"Bad format",
	"Too many elements",
	"Too few elements"
	/* add error messages here */
};

#define kMAXEXCEPTIONLEVEL	4

static int sSysExceptionLevel = 0;
static int sAppExceptionLevel = 0;


void RaiseSysException( p, e )
int p;
void (*e)(/* int x */);
{
	char errmsg[kMAXFILENAMELEN*2+2];

	if (sSysExceptionLevel > kMAXEXCEPTIONLEVEL) return;
	sSysExceptionLevel++;

	/* call the handler */
	if (e) {
	    (*e)( errno ); /* system errors are +ve */
	}

	(void)sprintf( errmsg, "%s: %s", gProgname, gFilename );
	perror( errmsg );

	if (p == kEXIT) Quit( errno );

	sSysExceptionLevel--;
}

void RaiseAppException( x, p, e )
int x;
int p;
void (*e)(/* int x */);
{
	if (sAppExceptionLevel > kMAXEXCEPTIONLEVEL) return;
	sAppExceptionLevel++;

	/* call the handler */
	if (e) {
	    (*e)( -x ); /* app errors are -ve */
	}

	(void)fprintf( stderr, "%s: %s: %s\n", 
	    gProgname, gFilename, sAppErrors[x] );

	if (p == kEXIT) Quit( -x );

	sAppExceptionLevel--;
}

.TH GENLE 1 "15 September 1991"
.UC 4


.SH NAME
genle \- 
generate linear extensions of partially ordered sets


.SH SYNOPSIS
.B genle
[
.B \-c
|
.B \-g
|
.B \-p
|
.B \-t
]
.I posetfile ...


.SH DESCRIPTION
For each
.I posetfile,
.B genle
generates all the linear extensions of 
the partially ordered set (poset) described
by the file. 
Each linear extension is generated exactly
once.
(Linear extensions are 
also known as topological sortings.)
.PP
If the poset contains no cover relations 
(all N elements are unrelated),
then the set of all linear extensions is the 
same as the set of N! permutations of
N elements.
.PP
The
.B genle
implementation is based on an algorithm
by Gara Pruesse and Frank Ruskey which
generates all the linear extensions of a
poset in constant amortized time.
That is, the total running time is
some constant times the total number of
linear extensions (plus a small amount of
preprocessing).
The next linear extension is produced
by using only adjacent transpositions on
the elements of the current linear
extension.
Moreover, successive linear extensions
differ by at most two adjacent transpositions.
.PP
The 
.B genle
program can count the total number of linear 
extensions.
Counting is often faster than generating
since not all linear extensions need to
be produced to be included in the final count.
Counting is the default option.
.PP
Generation normally does not produce output;
there is a separate option for printing the
linear extensions.
.PP
The
.B genle
program can produce for each ordered pair 
of elements (x,y), the count of linear 
extensions in which x appears before y. 
The results are presented in a table.
The computation of this table is also done
in constant amortized time, with a small
amount of postprocessing.
.PP
Timings of the main processing are always provided
and directed to the standard error stream.
.PP
It is more efficient to specify a series of poset
files for processing instead of running the
.B genle
program repeatedly.


.SH OPTIONS
.TP
.B \-c
Count the total number of linear extensions.
.TP
.B \-g
Generate all the linear extensions without output.
This is only useful for evaluating the speed of 
the implementation.
.TP
.B \-p
Print all the linear extensions as they are 
generated.
.TP
.B \-t
Tabulate, in matrix form, the number of linear 
extensions in which one element appears before another.
Write the total number of linear extensions
and also the best pair (x,y) which comes closest
to having x before y in half the linear extensions
(along with the actual ratio).
.PP
Options cannot be combined (e.g., 
.B \-c 
.B "\-p" );
the most recent one specified overrides
earlier ones.
However, a different option can be given for each
.I posetfile
specified.


.SH USAGE
A 
.I posetfile
is an ASCII text file and specifies a single poset.
The poset must be labelled in breadth\-first\-search
order starting from the minimal elements.
The first line in the file contains the number of elements;
following lines contain pairs of integers specifying
the cover relations.
(Cover relations correspond to the edges of the Hasse
diagram of the poset;
additional relations in the transitive closure do
not affect the algorithm.)
A line which contains a pair of zeroes marks the
end of the relations.
After this, the file may contain anything (e.g.,
a note describing the poset).
.PP
Consider the following poset, which has been given
the proper breadth\-first\-search numbering.

.nf
7  8  9 10 11 12
| /| /| /| /| /|
|/ |/ |/ |/ |/ |
1  2  3  4  5  6
.fi

This poset is described by a file containing:

.nf
12
1 7
1 8
2 8
2 9
3 9
3 10
4 10
4 11
5 11
5 12
6 12
0 0
-- fence poset on 12 elements and 11 cover relations
.fi


.SH DIAGNOSTICS
The exit status is normally 0.
System exceptions return a positive
exit status (e.g., could not open the
.I posetfile).
Program exceptions return a negative
exit status.
.TP
.BI "Usage: genle [\-c|\-g|\-p|\-t] " posetfile \fR.\|.\|.
Invalid options were specified on the command line.
Exit status becomes \-1.
.TP
.B Bad format
The 
.I posetfile
is corrupted or in the wrong format.
Exit status becomes \-2.
.TP
.IB posetfile ": Too many elements"
The number of elements in the poset exceeds 254.
Exit status becomes \-3.
.TP
.IB posetfile ": Too few elements"
The number of elements in the poset is non-positive.
Exit status becomes \-4.


.SH SEE ALSO
Please report bugs, fixes, applications, modifications, and requests to:

.nf
Kenny Wong or Frank Ruskey
Department of Computer Science
University of Victoria
PO Box 3055, Victoria, BC, Canada, V8W 3P6
Internet: kenw@csr.UVic.CA or fruskey@csr.UVic.CA
.fi

All correspondence should include your name, address, phone number,
and preferably an electronic mail address.


.SH BUGS
A signed integer overflow may occur if too many
linear extensions are counted.

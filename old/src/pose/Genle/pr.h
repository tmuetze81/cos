/* pr.h - Pruesse and Ruskey Algorithm */

/* Copyright 1991, Kenny Wong and Frank Ruskey. All Rights Reserved.
 *
 * This program is free software. You may use this software, and copy,
 * redistribute, and/or modify it under the terms of version 2 of the 
 * GNU General Public License. This and all modified works may not be
 * sold for profit. Please do not remove this notice or the original 
 * copyright notices.
 *
 * This software is provided "as is" and without any express or implied
 * warranties, including, without limitation, any implied warranties
 * of merchantability or fitness for a particular purpose. You bear all
 * risk as to the quality and performance of this software.
 *
 * You should have received a copy of the GNU General Public License
 * with this software (see the file COPYING); if not, write to the
 * Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Please report bugs, fixes, applications, modifications, and requests to:
 *
 * Kenny Wong or Frank Ruskey
 * Department of Computer Science, University of Victoria
 * PO Box 3055, Victoria, BC, Canada, V8W 3P6
 * Internet: kenw@csr.UVic.CA or fruskey@csr.UVic.CA
 *
 * All correspondence should include your name, address, phone number,
 * and preferably an electronic mail address.
 */

/* initialize module */
extern void PruesseRuskeyInit();

/* prepare for new poset */
extern void PruesseRuskeyBegin();

/* generate and call uGen hook */
extern void PruesseRuskeyGen();

/* print linear extensions */
extern void PruesseRuskeyPrint();

/* count linear extensions */
extern void PruesseRuskeyCount();

/* compute P(x<y) table */
extern void PruesseRuskeyTable();

/* cleanup after processing */
extern void PruesseRuskeyEnd();

/* shutdown module */
extern void PruesseRuskeyQuit();

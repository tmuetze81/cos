#!/usr/bin/perl
$filename = "data";
$limit = 40;

$counter = 1;
#zigzag();
&two_chain();
#&star();
#&one_chain();

sub zigzag{
    open(RESULT, "> zigzag");
    print RESULT "0 $limit 0 15\n";
    print RESULT "1\n";
    $x = $limit -1;
    print RESULT "$x\n";
    close(RESULT);
    while($counter <= $limit){
	open(DATA, "> $filename");
	print DATA "$counter\n";
	$flag = 1;
	$i = 1;
	while($i < $limit){
	    $j = $i + 1;
	    if($flag % 2 == 1){
		print DATA "$i $j\n";
	    }
	    else{
		print DATA "$j $i\n";
	    }
	    $i++;
	    $flag++;
	}
	print DATA "1\n";
	$i = 2;
	while($i < $counter){
	    print DATA "0\n";
	    $i++;
	}
	print DATA "1\n";       
	close(DATA);
	system "gang zigzag";
	$counter ++;
    }
}

sub star{
    open(RESULT, "> star");
    print RESULT "0 $limit 0 15\n";
    print RESULT "1\n";
    $x = $limit -1;
    print RESULT "$x\n";
    close(RESULT);
    while($counter <= $limit){
	open(DATA, "> $filename");
	print DATA "$counter\n";
	$i = 2;
	while($i <= $counter){
	    print DATA "1 $i\n";
	    $i++;
	}
	if($counter % 2 == 0){print DATA "1\n";}
	else { print DATA "0\n";}
	$i = 2;
	while($i <= $counter){
	    print DATA "1\n";
	    $i++;
	}
	close(DATA);
	system "gang star";
	$counter ++;
    }
}

sub two_chain{
     open(RESULT, "> two_chain");
    print RESULT "0 $limit 0 20\n";
    print RESULT "1\n";
    $x = ($limit - 1)/2;
    print RESULT "$x\n";
    close(RESULT);
    while($counter <= $limit){
	open(DATA, "> $filename");
	if($counter % 2 == 1){
	    print DATA "$counter\n";
	$i = 1; $j = 2;
	while($j <= $limit){
	    print DATA "$i $j\n";
	    $i = $j;
	    $j += 2;
	}
	$i = 1; $j = 3;
	while($j <= $limit){
	    print DATA "$i $j\n";
	    $i = $j;
	    $j += 2;
	}
	$i = 1;
	while($i < $limit -1){
	    print DATA "0\n";
	    $i += 1;
	}
	print DATA "1\n";
	print DATA "1\n";
	close(DATA);
	system "gang two_chain";
    }
	$counter ++;
    }
 }

sub one_chain{
    open(RESULT, "> one_chain");
    print RESULT "0 $limit 0 15\n";
    print RESULT "1\n";
    $x = $limit -1;
    print RESULT "$x\n";
    close(RESULT);
    while($counter <= $limit){
	open(DATA, "> $filename");
	print DATA "$counter\n";
	$i = 1;
	while($i < $counter){
	    $j = $i + 1;
	    print DATA "$i $j\n";
	    $i++;
	}
	print DATA "1\n";
	$i = 2;
	while($i < $counter){
	    print DATA "0\n";
	    $i++;
	}
	print DATA "1\n";
	close(DATA);
	system "gang one_chain";
	$counter ++;
    }
}


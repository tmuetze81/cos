/* Generate all solutions to the n Queens problem */
/* -n num    the number of queens             */
/* -P        generate permutations            */
/* -B        generate the chessboard          */
/* -l num    max number of objects generated  */
/* programmer: Frank Ruskey, Sept. 13, 1995   */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LIMIT_ERROR     -1
#define TRUE             1
#define FALSE            0
#define ICON

int  x[100], a[100], b[100], c[100];
int  count, N, i;
int  perm_out, board_out, LIMIT;

void ProcessInput (int argc, char *argv[]) {  int i;
   perm_out = board_out = FALSE;
   N = 0;  
   for (i=1; i<argc; i++) {
       if (strcmp(argv[i], "-n") == 0) {
          if (i+1 < argc) { N = atoi(argv[++i]); }
       }
       else if (strcmp(argv[i], "-P") == 0) perm_out = TRUE;
       else if (strcmp(argv[i], "-B") == 0) board_out = TRUE;
       else if (strcmp(argv[i], "-l") == 0) {
          if (i+1 < argc) { LIMIT = atoi(argv[++i]); }
       }
       else printf("Spurious input data: %s\n", argv[i]);
   }
}

void PrintIt ( ) { int i,j;
  if (count <= LIMIT) {
     ++count;
     printf("<TR>\n");
     if (perm_out) {
        printf("<TD ALIGN=CENTER>");
        for (i=1;i<=N;++i) printf("%d ",x[i] );  printf("<BR>\n");
        printf("</TD>");
     }
     if (board_out) {
        printf("<TD>");
        printf("<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>\n");
        for (j=1;j<=N;++j) {
           printf("<TR>\n");
           for (i=1;i<=N;++i) {  
             printf("<TD>");
             if (x[j]==i && ((i+j)%2)==0) printf("<IMG SRC=ico/wQueen.gif>");
             if (x[j]==i && ((i+j)%2)==1) printf("<IMG SRC=ico/wQueenRed.gif>");
             if (!(x[j]==i) && ((i+j)%2)==0) printf("<IMG SRC=ico/green.gif>");
             if (!(x[j]==i) && ((i+j)%2)==1) printf("<IMG SRC=ico/red.gif>");
             printf("</TD>\n");
           }
           printf("</TR>\n");
        }
        printf("</TABLE></TD>\n");
     }
     printf("</TR>\n");
  } else exit(LIMIT_ERROR);
}

void queen ( int col ) { int row;
  if (col > N) PrintIt( );
  else {
    for (row=1; row<=N; ++row) 
       if (a[row] && b[row+col] && c[row-col+N]) {
          x[col] = row;
          a[row] = b[row+col] = c[row-col+N] = FALSE;
          queen( col+1 );
          a[row] = b[row+col] = c[row-col+N] = TRUE;
       }
  }
}

int main (int argc, char *argv[] ) {
  ProcessInput( argc, argv );
  x[0] = count = 0;
  for (i=0; i<=2*N; ++i) a[i] = b[i] = c[i] = TRUE;
  queen( 1 );
  printf("</TABLE>\n");
  printf("Solutions output = %d", count );
  return (0);
}



/*======================================================================*/
/* C program for distribution from the Combinatorial Object             */
/* Server. Generate permutations of a multiset in Gray code order.      */
/* This is the same version used in the book "Combinatorial Generation."*/
/* The program can be modified, translated to other languages, etc.,    */
/* so long as proper acknowledgement is given (author and source).      */  
/* Programmer: Frank Ruskey, 1995.                                      */
/* Programmer: Joe Sawada, 1997 (translation to C).                     */
/* The latest version of this program may be found at the site          */
/* http://www.theory.csc.uvic.ca/inf/mult/Multiset.html            */
/*======================================================================*/

// A direct CAT implementation of the Eades-McKay algorithm
// modified to handle multisets.                           

// This program takes as input t (the number of different elements 0..t )
//	followed by the number of each element n(0).. n(t)  

int n,t,cnt,calls;
int a[100], num[20], sum[20], off[20];
int dir[20];

void PrintIt() {

	int i;
	cnt++;
	/*printf("%d. ", cnt);*/
	for(i=1; i<= sum[0]; i++) printf("%d ", a[i]);
	printf("\n");
}

void neg( int t, int n, int k);
void gen( int t, int n, int k);

void BigGen(int t) {
	if (dir[t]) {
		gen(t,sum[t-1],sum[t]);
	}
	else {
		neg(t,sum[t-1],sum[t]);
	}
	
	if (t>1)  {
		BigGen(t-1);
	}
	dir[t] = (dir[t] + 1)%2;
	if (dir[t]) off[t] = off[t-1] + num[t-1];
	else off[t] = off[t-1];
}
	
void swap(int t, int x, int y) {
	int b,temp;
	if (t>1) BigGen(t-1);
	b = off[t-1];
	temp = a[x+b];
	a[x+b] = a[y+b];
	a[y+b] = temp;
	PrintIt();

}
	
void gen( int t, int n, int k) { // This is the Eades-McKay algorithm
	int i;
	calls++;
	if ((1<k) && (k<n)) {
     		gen( t, n-2, k-2 );  swap( t, n-1, k-1 );
     		neg( t, n-2, k-1 );  swap( t,  n, n-1 );
     		gen( t, n-1, k );
	}
	else if (k == 1) {
		for (i=n-1; i>= 1; i--) swap(t,i,i+1);
	}
}
		
void neg( int t, int n, int k) {
	int i;
	calls++;
	if ((1<k) && (k<n)) {
     		neg( t, n-1, k );    swap( t,  n, n-1 );
     		gen( t, n-2, k-1 );  swap( t, n-1, k-1 );
     		neg( t, n-2, k-2 );  
	}
	else if (k == 1) {
		for (i=1; i<=n-1; i++) swap(t,i,i+1);
	}
}

int main() {

	int i,j,k;
	scanf("%d", &t);
     	k = 1;
     	for (i=0; i<=t; i++) {
        	dir[i] = 1;  
		scanf("%d", &num[i]);
		for(j=1; j<= num[i]; j++) {
			a[k] = i; k++;
		}
	}
     	off[0] = 0;
	for(i=1; i<=t; i++) off[i] = off[i-1] + num[i-1];
	dir[t+1] = 1;
	sum[t] = num[t];
	for(i=t-1; i>=0; i--)  sum[i] = sum[i+1] + num[i];

     	cnt = 0;  calls = 0;  
     	PrintIt();
     	BigGen( t );

	return 0;
}



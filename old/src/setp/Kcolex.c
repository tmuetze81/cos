/*Given a value for n and an output mode specified by ProcessInput,
as well as the number of blocks, k:
this program prints out the set partitions in pseudo-colex order.*****/

#include <stdio.h>
#include "../ProcessInput.c"

#define ICON printf("ico/");

/********globals*******************/
int bitstring = 0;
int list = 0;
int chess = 0;
int A[MAX];

/**********local functions***************/
/*prints out the generated number according to the output format*/
void printit()
{
  int i,j;
  int beg, pos;

  if(!LIMIT)
    return;
  printf("<TR>");
  if(bitstring) {
    printf("<TD ALIGN=CENTER>");
    for (i=0; i<N; i++)
      printf("%d",A[i]);
    printf("<BR></TD>\n");
  }
  if (list) {
    printf("<TD ALIGN=CENTER>{");
    for (j=0; j<N; j++) {
      beg = 1;
      for (i=0; i<N; i++) {
        if (A[i]==j) {
          printf("%s%d",beg&&j ? "},{" : "",i+1);
          beg = 0;
        }
      }
    }
    printf("}<BR></TD>\n");
  }
  if (chess) {
    printf("<TD ALIGN=CENTER>");
    printf("<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>\n");
    for (j=0; j<N; j++) {
      printf("<TR>");
      pos = -1;
      for (i=j-1; i>=0 && pos==-1; i--)
        if (A[i]==A[j]) pos = i;
      for (i=0; i<j; i++) {
        printf("<TD><IMG SRC="); ICON
        if (i==pos) printf("greenRook.gif></TD>\n");
        else printf("green.gif></TD>\n");
      }
      printf("<TD><IMG SRC="); ICON printf("red.gif></TD></TR>\n");
    }
    printf("</TABLE></TD>\n");
  }
  LIMIT--; /*count down the number of printable objects*/
}

void gen(int n, int k)
{
  int i;

  if (!LIMIT) return;
  if (n==k) {printit(); return; }

  for (i=0; i<k; i++) {
    A[n-1] = i;
    gen(n-1,k);
  }
  if (k>1) 
    gen(n-1,k-1);
  A[n-1] = n-1;
}

/***************main program********************/
int main(int argc, char **argv)
{
  int i;
  int count;

  ProcessInput(argc, argv);
  count = LIMIT;  
  if(out_format & 1)
    bitstring = 1;
  if(out_format & 2)
    list = 1;
  if(out_format & 8)
    chess = 1;

  /*initialize the array A*/
  for (i=0; i<N; i++) 
    A[i] = i;
  
  gen(N,K);
  printf("</TABLE><BR>Total number of partitions = %d<BR>\n",count-LIMIT);
  if(LIMIT)
    return(0);
  else
    return(-1);
}

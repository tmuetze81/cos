/*
 ProcessInput.c

 Header file to include with all programs called from cgi scripts.
 program parses command line arguments into correct variables.

 Here's what goes where:

 COMMAND LINE                global variable (declared in this file)

 -n                          N
 -k                          K
 -l                          lambda
 -L                          LIMIT
 -o                          out_format
 -m                          M
 -u                          U
 -P                          PID
 -r                          restriction for stamp foldings

  Note that these variables are declared here and needn't be
  redefined.

  */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

/* Arbitrary constant - max size of array */
#define MAX 200

/* Output formats */
#define OUT_ONELINE	1
#define OUT_CYCLE	2
#define OUT_TRANS	4
#define OUT_CHESS	8
#define OUT_INVERSE	16
#define OUT_TABLEAU_INVOL	32
#define OUT_TABLEAU	64
#define OUT_DIAGRAM	128

/* Restriction format for Stamp folding */
#define STAMP_ALL		0
#define STAMP_INCREASING	1
#define STAMP_UNLABELLED	2
#define STAMP_MEANDER		3
#define STAMP_CMEANDER		4


/* Here go the global variable declarations */

int N;  /* Corresponds to -n */
int K;  /*                -k */
int lambda[MAX];  /*      -l */
int LIMIT;        /*      -L */
int M;            /*      -m */
int out_format;   /*      -o */
int U;            /*      -u */
int PID;          /*      -P */
int stamp_restrict; /*    -r */

/* Our function prototype     */

void ProcessInput(int argc, char **argv);
int ProcessList(char *list);

/* ProcessInput is the procedure that does all the work here.  If an error
   is encountered during processing, it barfs with error level 1 */

void ProcessInput(int argc, char **argv) {
    int count=1;

    while(count < argc) {
       switch(argv[count++][1]) {
        case 'n' : N = atoi(argv[count++]);
                    break;
        case 'k' : K = atoi(argv[count++]);
                    break;
        case 'L' : LIMIT = atoi(argv[count++]);
                    break;
        case 'm' : M = atoi(argv[count++]);
                    break;
        case 'o' : out_format = atoi(argv[count++]);
                    break;
        case 'u' : U = atoi(argv[count++]);
                    break;
        case 'l' : ProcessList(argv[count++]);
                    break;
        case 'P' : PID = atoi(argv[count++]);
                    break;
        case 'r' : stamp_restrict = atoi(argv[count++]); 
                    break;
        default : printf("\nError Processing input %s\n", argv[count-2]);
                    exit(1);
       }
    }
}

/*
 * process_list returns 0 if an error was encountered during list processing.
 * If no errors occured, the it returns 1
 *
 * The list is terminated with a -1
 *
 */

int ProcessList(char *list)
{
    /* list MUST be comma delimited.  Otherwise we crash and burn */

    char strNum[MAX];
    char *ptrNum;
    int  nNum;

    int  nLambda=1; /* will be used to index lambda array */

    lambda[1] = -1;  /* Initial value of lambda array */
    while(*list)   /* As long as there's input... */
        {
            ptrNum = strNum; /* For array manipulation.  Nasty 'C' tricks. */
            while(*list && (*list != ','))  *ptrNum++ = *list++;
            *ptrNum = 0; /* ASCIIZ terminator */
            nNum = atoi(strNum); /* Convert to integer and check range */
            if (nNum < 0)  return 0; /* terminate with error condition */
            lambda[nLambda++] = nNum;
            if(*list) list++; /* Advance list pointer if we're not at end */
        }
    lambda[nLambda] = -1; /* Terminator */
    return 1;
}

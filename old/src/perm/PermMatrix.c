/*
 * PermMatrix
 * ----------
 * 
 * This little set of routines take in a permuation array,
 * A[0]...A[n-1], and display a chessboard corresponding to that
 * permutation.  GIFS are stolen from some chess site.
 *
 */


#include <stdio.h>


/*
 * Here are all the GIFs that will used in the generation of this
 * here wonderful chessbored.
 */

const char *blackSquare = "[B ]";
const char *whiteSquare = "[W ]";
const char *blackRook = "[BR]";
const char *whiteRook = "[WR]";

/* Here are some nice macros I use.  May be defined somewhere else; I dunno */

#define odd(x)  ((x) % 2)
#define even(x) !odd(x)

/* Local function prototypes */

void PrintBoard(int perm[], int n);

/* Testostomundo kOdeZ */

main() {
    int n;
    int A[25];
    int i;

    printf("Enter n :");
    scanf("%d",&n);
    fflush(stdin);
    
    for(i=0; i<n; i++) {
	printf("Enter A[%d] :",i);
	scanf("%d",&A[i]);
    }

    printf("\n\nThe Mettrix Bored is :\n\n");
    PrintBoard(A, n);
}

void PrintBoard(int Perm[], int n) {
    int row, col;

    for(row = 0; row < n; row++) {
	for(col = 0; col < n; col++) {
	    if (Perm[row] == col) {
		if(odd(row+col))
		    printf(blackRook);
		else
		    printf(whiteRook);
	    } else {
		if(odd(row+col))
                    printf(blackSquare);
                else
                    printf(whiteSquare);
	    }
	}
	printf("\n");
    }
}
	    
		

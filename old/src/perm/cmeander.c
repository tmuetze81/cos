#include <stdio.h>
#include <stdlib.h>
#include "ProcessInput.c"
#include "commonio.h"
#include "gdstamp.c"

#define TRUE 1
#define FALSE 0
#define odd(x) ( (x)%2==1 ? TRUE : FALSE )
#define LIMIT_ERROR -1

#define MAX_SIZE 200

void PrintIt();
void PrintBoard(int perm[], int n);
void init_tab( int );
void insert_tab( int );
void tableaux( int );
void tableaux2 ( int );
int NN;

struct stamp *first;
struct stamp *last;
struct stamp *one;
struct stamp *n;

int stamp_array[2*MAX_SIZE];
int second_array[2*MAX_SIZE];

int count = 0;

void GenStamp(struct stamp *latest, int parity);   /* Recursively generates
						      stamps using a stamp of
						      one size smaller.
						      Parity is the parity of
						      'latest' */
void AddStamp(struct stamp *current, struct stamp *new);

void RemoveStamp(struct stamp *new);

int LastStamp(struct stamp *latest);

int PerfRight(struct stamp *latest);	/* ensures the odd perforation of 
				      the latest stamp goes rightwards */

void ResetPerf(struct stamp *latest, int reset);

void DisplayStamp();

int ends_free();

int main(int argc, char *argv[])
	{
	struct perforation *temp;
	ProcessInput(argc, argv);
	NN = N;

	/* initialize the data structures */
	first = (struct stamp *) malloc(sizeof(struct stamp));
	last = first;
	one = first;		/* pointer to one end of strip */
	n = first;		/* pointer to other end (in case N = 1) */
	first->value = 1;
	first->prev = 0;
	first->next = 0;

	temp = (struct perforation *) malloc(sizeof(struct perforation));
	temp->left_value = 0;
	temp->right_value = 1;
	temp->left_link = 0;
	temp->right_link = first;

	first->even_perf = 0;
	first->odd_perf = temp;

	GenStamp(first, 1);

	free (temp);
	free (first);

	printf("\n</TABLE><P> Closed meanders = %d", count);

	exit(0);
	}

void GenStamp(struct stamp *latest, int parity)
	{
	struct stamp *temp = 0;		/* points to new stamp */
	struct perforation *new_perf = 0;	/* points to new
perforation */
	struct stamp *current = 0;	/* points to a current stamp */
	int flag = 0;			/* a flag for flow control */

	/* variables used to adjust the perforation (see below) */
	struct stamp *temp_stamp = 0;
	struct perforation *temp_perf = 0;
	int temp_value;
	int reset;			/* has perforation been reset? */


	if (latest->value == 2*N - 1)
		{
		if (!ends_free()) return;
		reset = LastStamp(latest);
		DisplayStamp();
		ResetPerf(latest, reset);
		return;
		}

	/* make new stamp */
	temp = (struct stamp *) malloc(sizeof(struct stamp));
	temp->value = latest->value + 1;
	temp->prev = 0;
	temp->next = 0;
	temp->odd_perf = 0;
	temp->even_perf = 0;

	if (temp->value == 2*N - 1)
		n = temp;	/* pointer to one end of strip */

	/* make new perforation */
	new_perf = (struct perforation *) malloc(sizeof(struct perforation));
	new_perf->right_value = temp->value;
	new_perf->left_value = 0;

	/* connect stamp to perforation */
	if (parity == 0)
		{
		temp->odd_perf = new_perf;
		temp->even_perf = 0;
		}
	else
		{
		temp->even_perf = new_perf;
		temp->odd_perf = 0;
		}
	new_perf->right_link = temp;
	new_perf->left_link = 0;

	/* connect new stamp to peforation of 'latest' */
	if ( parity == 1 )			/* odd */
		{
		(latest->odd_perf)->left_link = temp;
		(latest->odd_perf)->left_value = temp->value;
		temp->odd_perf = latest->odd_perf;
		}
	else					/* even */
		{
		(latest->even_perf)->left_link = temp;
		(latest->even_perf)->left_value = temp->value;
		temp->even_perf = latest->even_perf;
		}

	/* go left */
	current = latest;
	while (current != 0)
		{
		if (parity == 1)
			{
			flag = 0;
			if ( ((current->odd_perf)->right_value ==
				current->value) && (current != latest))
				{
				current = (current->odd_perf)->left_link;
				flag = 1;
				}
			if ( ((current->odd_perf)->left_value ==
				current->value) && (current != latest) &&
				!flag)
				{
				break;
				}
			}
		else
			{
			flag = 0;
			if (current->even_perf != 0)
			 {
			 if ( ((current->even_perf)->right_value ==
				current->value) && (current != latest))
				{
				current = (current->even_perf)->left_link;
				flag = 1;
				}
			 if ( ((current->even_perf)->left_value ==
				current->value) && (current != latest) &&
				!flag)
				{
				break;
				}
			 }
			}
		AddStamp(current, temp);	
		GenStamp(temp, (parity + 1) % 2);
		RemoveStamp(temp);
		current = current->prev;
		}

	/* adjust the perforation */
	if ( parity == 1 )			/* odd */
		{
		temp_perf = latest->odd_perf;
		}
	else					/* even */
		{
		temp_perf = latest->even_perf;
		}
	temp_value = temp_perf->left_value;           		/* swap */
	temp_perf->left_value = temp_perf->right_value;         /* values */
	temp_perf->right_value = temp_value;
	temp_stamp = temp_perf->left_link;                    	/* swap */
	temp_perf->left_link = temp_perf->right_link;		/* links */
	temp_perf->right_link = temp_stamp;

	/* go right */
	current = latest;
	while (current != 0)
	       {
	       current = current->next;
	       AddStamp(current, temp);
	       GenStamp(temp, (parity + 1) % 2);
	       RemoveStamp(temp);
	       if (current != 0)
		   {
		if (parity == 1)
			{
			flag = 0;
			if ((current->odd_perf)->left_value ==
				current->value)
				{
				current = (current->odd_perf)->right_link;
				flag = 1;
				}
			if ( ((current->odd_perf)->right_value ==
				current->value) && !flag )
				{
				break;
				}
			}
		else
			{
			flag = 0;
			if (current->even_perf != 0)
			 {
			 if ((current->even_perf)->left_value ==
				current->value)
				{
				current = (current->even_perf)->right_link;
				flag = 1;
				}
			 if ( ((current->even_perf)->right_value ==
				current->value) && !flag )
				{
				break;
				}
			 }
			}
		   }
		}

	/* remove the extra stamp and extra perforation */
	if (parity == 1)
		{
		temp_perf = temp->odd_perf;
		}
	else
		{
		temp_perf = temp->even_perf;
		}
	if (temp_perf->left_value == temp->value)
		{
		temp_perf->left_value = 0;
		temp_perf->left_link = 0;
		}
	else
		{
		temp_perf->right_value = temp_perf->left_value;
		temp_perf->right_link = temp_perf->left_link;
		temp_perf->left_value = 0;
		temp_perf->left_link = 0;
		}
	free (temp);
	free (new_perf);
	}

void AddStamp(struct stamp *current, struct stamp *new)
	{
	struct stamp *temp = 0;

	if (current == first)
		{
		new->next = first;
		new->prev = 0;
		current->prev = new;
		first = new;
		}
	else if (current == 0)	/* new must go last */
		{
		new->next = 0;
		new->prev = last;
		last->next = new;
		last = new;
		}
	else     		/* normal case */
		{
		temp = current;
		temp = temp->prev;
		new->next = temp->next;
		new->prev = temp;
		(temp->next)->prev = new;
		temp->next = new;
		}
	}

void RemoveStamp(struct stamp *new)
	{
	struct stamp *before;
	struct stamp *after;

	if (new == first)
		{
		after = new->next;
		after->prev = 0;
		first = after;
		new->next = 0;
		new->prev = 0;
		}
	else if (new == last)
		{
		before = new->prev;
		before->next = 0;
		last = before;
		new->next = 0;
		new->prev = 0;
		}
	else			/* normal case */
		{
		before = new->prev;
		after = new->next;
		before->next = after;
		after->prev = before;
		new->next = 0;
		new->prev = 0;
		}
	}

int LastStamp(struct stamp *latest)
	{
	struct stamp *last_stamp;
	struct perforation *last_perf;
	int reset;			/* has the perf been reset? */

	reset = PerfRight(latest);	/* ensure the expected orientation
					of the perforation involved */

	/* make a stamp structure for the last stamp */
	last_stamp = (struct stamp *) malloc(sizeof(struct stamp));
	last_stamp->value = 2*N;
	last_stamp->prev = 0;
	last_stamp->next = 0;	

	last_perf = (struct perforation *) malloc(sizeof(struct
perforation));
	last_perf->right_value = 2*N;
	last_perf->left_value = 1;
	last_perf->right_link = last_stamp;
	last_perf->left_link = one;

	last_stamp->even_perf = last_perf;
	last_stamp->odd_perf = latest->odd_perf;
	one->even_perf = last_perf;
	(latest->odd_perf)->right_value = 2*N;
	(latest->odd_perf)->right_link = last_stamp;

	AddStamp(0, last_stamp);	

	return reset;
	}

int PerfRight(struct stamp *latest)
	{
	int temp_value;
	struct perforation *temp_perf;
	struct stamp *temp_stamp;

	/* orientation is already rightwards */
	if ((latest->odd_perf)->left_value == latest->value)
		return 0;

	/* orientation must be switched */
	temp_perf = latest->odd_perf;
	temp_value = temp_perf->left_value;		/* swap */
	temp_perf->left_value = temp_perf->right_value;	/* values */
	temp_perf->right_value = temp_value;
	temp_stamp = temp_perf->left_link;		/* swap */
	temp_perf->left_link = temp_perf->right_link;	/* links */
	temp_perf->right_link = temp_stamp;

	return 1;
	}

void ResetPerf(struct stamp *latest, int reset)
	{
	struct stamp *final;		/* the stamp labelled 2*N */
	struct perforation *newest;	/* the perf connecting w/ final */

	/* first, remove the perf connecting stamps 1 and 2N */
	newest = one->even_perf;
	one->even_perf = 0;
	(newest->right_link)->even_perf = 0;
	free (newest);

	newest = last->odd_perf;
	if (reset)
		{
		newest->right_value = newest->left_value;
		newest->right_link = newest->left_link;
		newest->left_value = 0;
		newest->left_link = 0;
		}
	else
		{
		newest->right_value = 0;
		newest->right_link = 0;
		}

	final = last;		/* need to deep pointer to this stamp */
	free(last->even_perf);
	RemoveStamp(last);
	free(final);
	}

void DisplayStamp()
	{
	struct stamp *temp;
	int i;
	Pi[0] = 2*N + 1;
	iP[0] = 2*N + 1;
	temp = first;
	for (i = 1; (i <= 2*N) && (temp != 0); i++)
		{
		Pi[i] = temp->value;
		iP[temp->value] = i;
		temp = temp->next;
		}
	NN = 2*N;
	PrintIt();
	}

int ends_free()
	{
	struct stamp *temp;
	struct perforation *direction;

	/* the '1' stamp must be free (not parenthesized by a perf) in the
	   even direction */
	temp = one;
	while (temp != last)
		{
		temp = temp->next;
		if (((2*N - 1) % 2 == 0) && (temp == n))
			continue;
		if ((temp->even_perf)->right_value == temp->value)
			return 0;
		if ((temp->even_perf)->left_value == temp->value)
			temp = (temp->even_perf)->right_link;
		}

	/* the 'n' stamp must be free in the odd direction if 2*N is odd
	   in the even direction if 2*N is even */
	temp = n;
	while (temp != last)
		{
		temp = temp->next;
		if (((2*N - 1) % 2 == 0) && (temp == one))
			continue;

		/* set 'direction' according to odd or even */
		if ((2*N - 1) % 2 == 1)
			direction = temp->odd_perf;
		else
			direction = temp->even_perf;

		if (direction->right_value == temp->value)
			return 0;
		if (direction->left_value == temp->value)
			temp = direction->right_link;
		}

	return 1;
	}


/*  from  perm.c
	   Provide routines for I/O of permutations

           Added KoDeZ to add commas if N > 10 
*/


void PrintIt() {
    int i;
    
    ++count;
    if(count > LIMIT) exit (-1); /* Do this if we exceed max allowable */

    printf("<TR>");
    if (out_format & 1)
	{   printf("<TD ALIGN=CENTER>");
	    for(i=1; i<NN; i++) printf("%d, ",Pi[i]); /* one line */
	    printf("%d",Pi[NN]);
            printf("<BR></TD>");
	}

    if (out_format & 16)
	{   printf("<TD ALIGN=CENTER>");
	    for(i=1; i<=NN; i++) iP[Pi[i]] = i;
	    for(i=1; i<NN; i++) printf("%d, ",iP[i]); /* one line */
	    printf("%d",iP[NN]);
	    printf("<BR></TD>");
	}

    if (out_format & 2) {
	printf("<TD ALIGN=CENTER>");
	PrintCycle();
	printf("<BR></TD>");
    }

    if (out_format & 8) {
	printf("<TD ALIGN=CENTER>");
	PrintBoard(Pi, NN);
	printf("<BR></TD>");
    }

   if(out_format & 64) {
	printf("<TD ALIGN=CENTER>\n");
	printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");
	printf("<TR>\n");
	printf("<TD ALIGN=CENTER>\n");
	tableaux(NN);
	printf("</TD>\n");
	printf("<TD ALIGN=CENTER>\n");
	tableaux2(NN);
	printf("</TD></TR></TABLE>\n");
	printf("</TD>\n");
    }
    if (out_format & 128) {
	printf("\n");
	gdStampFolding(first, NN, 2);
    }
    printf("</TR>");

}

void PrintCycle(void) {
    int j,k;
    int a[NN];
    for (k=0; k<=NN;k++) a[k] = 1;

    k=1;

    while( k <= NN) {
	printf("(");
	j=k;
	do {
	    a[j] = 0;
            if (Pi[j] != k) {
	       printf("%d, ",j);
	    } else {
               printf("%d",j);
            }
	    j = Pi[j];
	} while (j != k);
	printf(")");
	while ((!a[k]) && (k<=NN)) k++;
    }
}
	
    

/*
 * PermMatrix
 * ----------
 * 
 * This little set of routines take in a permuation array,
 * A[0]...A[n-1], and display a chessboard corresponding to that
 * permutation.  GIFS are stolen from some chess site.
 *
 */


/*
 * Here are all the GIFs that will used in the generation of this
 * here wonderful chessbored.
 */

const char *blackSquare = "<IMG SRC =ico/red.gif>";
const char *whiteSquare = "<IMG SRC =ico/green.gif>";
const char *blackRook = "<IMG SRC =ico/redRook.gif>";
const char *whiteRook = "<IMG SRC =ico/greenRook.gif>";

/* Here are some nice macros I use.  May be defined somewhere else; I dunno */

#define even(x) !odd(x)

void PrintBoard(int Perm[], int n) {
    int row, col;

    printf("<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>\n");
    for(row = 1; row <= n; row++) {
	printf("<TR>\n");
	for(col = 1; col <= n; col++) {
	    printf("<TD>");
	    if (Perm[row] == col) {
		if(odd(row+col))
		    printf(blackRook);
		else
		    printf(whiteRook);
	    } else {
		if(odd(row+col))
                    printf(blackSquare);
                else
                    printf(whiteSquare);
	    }
	    printf("</TD>");
	}
	printf("</TR>\n");
    }
    printf("</TABLE>\n");
}
	
	    
		


#define INFINITY 999

int Tab[MAX][MAX];


void init_tab( int n ) 
{
    int i, j;

    for (i=0; i< n; i++) 
        for(j=0; j< n; j++) Tab[i][j] = INFINITY;

    for (i=0; i< n; i++) 
	Tab[0][i] = 0;

    for (i=0; i< n; i++)
	Tab[i][0] = 0;

}

void tableaux( int n ) {
int i,j;

init_tab(MAX);
for (i=1; i<=n; i++) insert_tab(Pi[i]);

printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");

for (i=1; Tab[i][1]!= INFINITY && i<=n; i++) {
    printf("<TR>");
    for (j=1; Tab[i][j] != INFINITY && j <= n; j++)
      printf("<TD>%d</TD>",Tab[i][j]);
    printf("</TR>\n");
}

printf("</TABLE>");

}

void tableaux2( int n ) {
int i,j;

init_tab(MAX);
for(i=1; i<=NN; i++) iP[Pi[i]] = i;
for (i=1; i<=n; i++) insert_tab(iP[i]);

printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");

for (i=1; Tab[i][1]!= INFINITY && i<=n; i++) {
    printf("<TR>");
    for (j=1; Tab[i][j] != INFINITY && j <= n; j++)
      printf("<TD>%d</TD>",Tab[i][j]);
    printf("</TR>\n");
}

printf("</TABLE>");

}

void insert_tab(int ex) 
{
   int i = 0; 
   int j;
   int x[MAX];

   x[1] = ex;  

   for (j = 1; j< MAX; j++)    
       if (Tab[1][j] == INFINITY) 
	   break;
	   	 
 do {
      i++;
      while (x[i] < Tab[i][j-1]) j--; 
      x[i+1] = Tab[i][j];             
      Tab[i][j] = x[i];               
  } while (x[i+1] != INFINITY);
  
}


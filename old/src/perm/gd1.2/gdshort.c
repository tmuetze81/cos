#include <stdio.h>
#include "gd.h"

int main(int argc, char *argv[])
	{
	gdImagePtr im;
	int black, white;
	FILE *out;

	im = gdImageCreate(100, 100);
	black = gdImageColorAllocate(im, 0, 0, 0);
	white = gdImageColorAllocate(im, 255, 255, 255);
	gdImageFilledRectangle(im, 25, 25, 74, 74, white);
	out = fopen("test.gif", "wb");
	gdImageGif(im, out);
	fclose(out);
	gdImageDestroy(im);
	return 0;
	}

#!/usr/bin/perl

# This file called "e_part.pl.cgi"
# Written and copyrighted by Frank Ruskey and Scott Lausch, March 1998.
# Some code taken from COS, but virtually everything re-written for ECOS/AMOF;
# in particular, there are no calls to C programs, everything done
# in PERL.
# Not to be distributed without permission.
# "Ownership": Frank Ruskey

require "e_common.pl";

$nLIMIT = 20;
@p = ();
@coins = (200, 100, 25, 10, 5, 1);
$num_coins = @coins;	# $num_coins becomes length of @coins


sub min
	{
	local ($first, $second) = @_;
	$first < $second ? $first : $second;
	}

sub max
	{
	local ($first, $second) = @_;
	$first < $second ? $second : $first;
	}

# prints a Ferrer's diagram as an HTML table
sub PrintFerrer
	{
	local($KK) = @_;
	local($i, $j);

	printf "<TABLE BORDER=1 CELLPADDING=0 CELSPACING=0>\n";
	for ($i = ($M == 0 ? 2 : 1); $i <= $KK; ++$i)
		{
		print "<TR>";
		for ($j = 1; $j <= $p[$i]; ++$j)
			{
			printf "<TD height=14 width=14><IMG SRC=Ico/redball.gif height=14 width=14></TD>\n";
			}
		print "</TR>";
		}
	print "</TABLE>";
	}

# displays an HTML table of coins for the partition.  No more than 4
# pennies will be displayed.
sub PrintCoins
	{
	local($KK) = @_;
	local($i, $j, $c, $value, $penny_limit, $snipped, $marked );
	local(%inventory, %columns); # for reorganizing the info about the
				     # parts

	$snipped = 0;	# false
	$marked = 0;
	$penny_limit = 4;

	for ($i = 0; $i <= $num_coins; $i++)
		{
		$inventory{$coins[$i]} = 0;
		$columns{$coins[$i]} = 0;
		}
	$inventory{'max'} = 0;
	$c = 1;		# column index
	for ($i = ($M == 0 ? 2 : 1); $i <= $KK;)
		{
		$value = $p[$i];
		$j = 0;
		while (($p[$i] == $value)&&($i <= $KK))
			{
			$i++;
			$j++;
			}
		if (($value == 1) && ($j > $penny_limit))
			{
			$inventory{$value} = $penny_limit;
			$snipped = 1;	# true
			}
		else
			{
			$inventory{$value} = $j;
			}
		if ($inventory{'max'} < $inventory{$value})
			{
			$inventory{'max'} = $inventory{$value};
			}
		$columns{$value} = $c++;
		}

	print "<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>";

	while ($inventory{'max'} > 0)
		# print a row in the table	
		{
		printf "\n<TR>";
		for ($i = 0; $i < $num_coins; $i++)
			{
			if ($columns{$coins[$i]} > 0)
				{
				if ($inventory{$coins[$i]} > 0)
					{
					print"<TD height=30 width=30>";
					print "<IMG SRC=Ico/coin$i.gif height=30 width=30>";
					printf "\n";
					$inventory{$coins[$i]}--;
					}
				else
					{
					if ($coins[$i] == 1 && !$marked && $snipped)
						{
						printf "<TD align=center>:\n";
						$marked = 1;
						}
					else
						{
						print "<TD>";
						}
					}
				print "</TD>";
				}
			}
		print "</TR>";
		$inventory{'max'}--;
		}
	if ($snipped && !$marked)
		{
		printf "\n<TR>";
		for ($i = 1; $i < $columns{1}; $i++)
			{
			print "<TD></TD>"
			}
		printf "<TD align=center>:</TD></TR>";
		}

	print "</TABLE>";
	}

sub PrintIt
	{
	local ($KK) = @_;
	local($i);

	if ($count == $LIMIT) { $retval = $LIMITerror; return; }
	++$count;
	printf "<TR>\n";
	if ($outformat & 1)
		{
		printf "<TD ALIGN=CENTER>";
		for ($i = ($M == 0 ? 2 : 1); $i <= $KK - 1; $i++)
			{
			printf (" %2d +", $p[$i]);
			}
		printf (" %2d", $p[$i]);
		printf "<BR></TD>\n";
		}
	if ($outformat & 2)
		{
		if ($input {'program'} eq "partreg")
			{
			printf "<TD ALIGN=CENTER>";
			&PrintFerrer($KK);
			}
		else
			{
			printf "<TD ALIGN=CENTER>";
			&PrintCoins($KK);
			}
		}
	}

sub PartA
	{
	local ($n, $k, $t) = @_;
	local ($j);

	if ($retvalue) {return;}
	$p[$t] = $k;
	if ( $n == $k || $k == 1)
		{
		&PrintIt ($t + $n - $k);
		if ($retval == $LIMITerror) {return;}
		}
	else
		{
		for ($j = 1; $j <= &min ($k, $n - $k); $j++)
			{
			&PartA ($n - $k, $j, $t + 1);
			}
		}
	$p[$t] = 1;
	}

sub Part
	{
	local ($n, $k, $s, $t) = @_;
	local ($j, $lower, $temp);

	if ($retvalue) {return;}
	$p[$t] = $s;
	if ( $k == 1 || $n == $k)
		{
		&PrintIt ($K);
		}
	else
		{
		if ( int($n - $s) % int($k - 1) > 0)
			{
			$lower = &max ( 1, 1 + int(($n - $s)/($k - 1)));
			}
		else
			{
			$lower = &max (1, int(($n - $s)/($k - 1)));
			}
		for ($j = $lower; $j <= &min ( $s, $n - $s - $k + 2); $j++)
			{
			&Part ( $n - $s, $k - 1, $j, $t + 1);
			}
		}
	$p[$t] = 1;
	}

# a routine to select the next lower valid coin value
sub SelectDown
	{
	local ($first, $second) = @_;
	local ($i, $size);
	for ($i = 0; $i < $num_coins; $i++)
		{
		$size = $coins[$i];
		if ($size <= &min($first, $second))
			{
			return $coins[$i];
			}
		}	
	}

sub CoinA
	{
	local ($n, $k, $t) = @_;
	local ($j, $low_value); # $low_value is used to restrict penny
				# generation
	
	if ($retvalue) {return;}
	$p[$t] = $k;
	if ( $n == $k || $k == 1)
		{
		&PrintIt ($t + $n - $k);
		}
	else
		{
		$j = &SelectDown($k, $n - $k);
		if ($K < 3)
			{
			$low_value = 1;
			}
		else
			{
			if ($n - $k > $K)
				{
				$low_value = 5;
				}			
			}
		while ($j >= $low_value)
			{
			&CoinA ($n - $k, $j, $t + 1);
			$j--;
			if ($j == 0) {last;}
			$j = &SelectDown($j, $j);
			};
		}
	$p[$t] = 1;
	}


sub GenPart
	{
	local ($N, $K, $M) = @_;
	local ($i);
	
	if ($N != 0)
		{
		for ($i = 1; $i <= $N + 1; $i++)
			{
			$p[$i] = 1;
			}
		$count = 0;
		if ($K == 0)
			{
			if ($M == 0)
				{
				&PartA ( 2*$N, $N, 1);
				}
			else
				{
				&PartA ($N, $M, 1);
				}
			}
		else
			{
			if ($M == 0)
				{
				&Part ( 2*$N, ++$K, $N, 1);
				}
			else
				{
				if ( $N - $M >= $K - 1)
					{
					&Part ($N, $K, $M, 1);
					}
				}
			}
		}
	else
		{
		print "Sorry.  N equals zero.  Goodbye";
		}
	}

sub GenCoin
	{
	local ($N, $K, $M) = @_;
	local ($i);
	
	if ($N != 0)
		{
		for ($i = 1; $i <= $N + 1; $i++)
			{
			$p[$i] = 1;
			}
		$count = 0;
		if ($M == 0)
			{
			&CoinA ( 2*$N, $N, 1);
			}
		else
			{
			&CoinA ($N, &SelectDown($M, $M), 1);
			}
		}
	else
		{
		printf "Sorry.  N equals zero.  Goodbye\n";
		}
	}


MAIN: 
{
# Read in all the variables set by the form
   if (&ReadParse(*input)) {
      &ProcessForm;
   }
   &CloseDocument( 'part' );
}

sub PrintTableHeaders {
   &TableSetup;
   if ($input{'output1'}) { &ColumnLabel( "Numbers" ); }
   if ($input{'output2'}) { &ColumnLabel( "Pictures" ); }
   print "</TR><BR>";
}


sub StdOutput {
   &TableSetup;
   if ($input{'output1'}) 
	{
        print "<TH COLSPAN=1><FONT SIZE=+1>$_[0]<BR></FONT>"; 
	}
   if ($input{'output2'}) { &ColumnLabel( $_[1] ); }
   print "</TR>";
}



sub nError
	{
	&ErrorMessage('An <I>n</I> parameter must be specified');
	}

sub ProcessForm
	{
	&OutputHeader ('Partitions Output', 'part' ); # from common.pl

	$outformat = 0;
	if ($input{'output1'}) { $outformat = 1; }
	if ($input{'output2'}) { $outformat = $outformat | 2; }
	if ($outformat == 0) { &OutError; return; }

	if (!($input{'n'}))
		{ 
     		&ErrorMessage( "Parameter <I>n</I> must be specified!" );
		return;
		}
	$N = $input{'n'};	$K = $input{'k'}; $M = $input{'m'};
	if (int($input{'n'}) eq 0) {&nError; return;}	
	if ($input{'program'} eq "partreg")
		{
		if ($N > 50) { &TooBig('n', 50); return; }
		}
	elsif ($input{'program'} eq "partcoin")
		{
		if ($N > 800) { &TooBig('n', 800); return; }
		}
	if ($N < 1) { &TooSmall('n', 0); return; }
	if ($K > $N) { &TooBig('k',"<I>n</I> = $N"); return; }
	if ($M > $N) { &TooBig('m',"<I>n</I> = $N"); return; }
	$retval = $count = 0;

	if ($input{'program'} eq "partreg")
		{
		print"<P>Output in lexicographic order.<P>";
		if ($outformat & 1)
			{
			if ($outformat & 2)
				{
				&StdOutput('Numbers', 'Pictures');
				}
			else
				{
				&StdOutput('Numbers');
				}
			}
		else
			{
			&StdOutput('Pictures');
			}
		&GenPart($N, $K, $M);
		}
	elsif ($input{'program'} eq "partcoin")
		{
		print "<P>Output in reverse lexicographic order.<P>";
		if ($outformat & 1)
			{
			if ($outformat & 2)
				{
				&StdOutput('Numbers', 'Pictures');
				}
			else
				{
				&StdOutput('Numbers');
				}
			}
		else
			{
			&StdOutput('Pictures');
			}
		&GenCoin($N, $K, $M);
		}
	if ($retval) { &LimitError; }
	print "</TABLE><P>Partitions generated = $count\n";
	}





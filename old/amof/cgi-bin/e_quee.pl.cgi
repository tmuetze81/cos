#!/usr/bin/perl

# This file called "e_quee.pl.cgi"
# Written by Frank Ruskey, June 1996.
# Some code taken from COS, but most everything re-written for ECOS/AMOF;
# in particular, there are no calls to C programs, everything done
# in PERL (but this makes solutions for n > 7 slower.)
# Not to be distributed without permission.
# "Ownership": Frank Ruskey 

require "e_common.pl";

$FALSE = 0;
$TRUE  = 1;
$nLIMIT = 9; # need to insert commas if this becomes greater than 9.
$boardLIMIT = 10;
$boardLIMITerror = -2;

MAIN:
{
# Read in all the variables set by the form
  if (&ReadParse(*input)) {
    &ProcessForm;
  }
  &CloseDocument( 'quee' );
}

sub PrintIt {
  local($i);  local($j);
  if ($count == $textLIMIT) { $retval = $LIMITerror; return; };
  if ($count == $boardLIMIT && $board_out) { $retval = $boardLIMITerror; return; }
  ++$count;
  print "<TR>\n";
  if ($perm_out) {
     print "<TD ALIGN=CENTER>";
     for ($i=1;$i<=$N;++$i) { print $x[$i]; }  print "<BR>\n"; 
     print "</TD>";
  }
  if ($board_out) {
     print "<TD>";
     print "<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>\n";
     for ($j=1;$j<=$N;++$j) {
        print "<TR>\n";
        for ($i=1;$i<=$N;++$i) {  
           print "<TD height=30 width=30>";
           if ($x[$j]==$i && (($i+$j)%2)==0) 
              { print "<IMG SRC=Ico/wQueen.gif height=30 width=30>"; }
           if ($x[$j]==$i && (($i+$j)%2)==1) 
              { print "<IMG SRC=Ico/wQueenR.gif height=30 width=30>"; }
           if (!($x[$j]==$i) && (($i+$j)%2)==0) 
              { print "<IMG SRC=Ico/green.gif height=30 width=30>"; }
           if (!($x[$j]==$i) && (($i+$j)%2)==1)
              { print "<IMG SRC=Ico/red.gif height=30 width=30>"; }
           print "</TD>\n";
        }
        print "</TR>\n";
     }
     print "</TABLE></TD>";
  }
}

sub queen {
  local($col) = $_[0];
  local($row);
  if ($col > $N) { &PrintIt; }
  else {
     # for ($row=1; $row<=$N; ++$row) {
     foreach $row (1..$N) {
        if ($a[$row] && $b[$row+$col] && $c[$row-$col+$N]) {
           $x[$col] = $row;
           $a[$row] = $b[$row+$col] = $c[$row-$col+$N] = $FALSE;
           &queen( $col+1 );
           $a[$row] = $b[$row+$col] = $c[$row-$col+$N] = $TRUE;
     }  }
  }
}

sub ProcessForm {

  &OutputHeader('Queens Problem Output','quee');  # from common.pl

  if ($input{'program'} eq "Queens") {
     $N = int($input{'n'});
     $textLIMIT = int($input{'textLIMIT'});
     if ($textLIMIT > $LIMIT) { $textLIMIT = $LIMIT; }  # might want to warn user.
     if (int($input{'boardLIMIT'}) < $boardLIMIT) { $boardLIMIT = $input{'boardLIMIT'}; }
     print "Solutions to the queens problem for <I>n</I> = $input{'n'}.<BR><P>\n";
     if (int($input{'n'}) > $nLIMIT) { &TooBig('n',$nLIMIT); return; }
     if (int($input{'n'}) < 1) { &TooSmall('n',1); return; }
     &TableSetup;
     $perm_out  = $input{'output1'};
     $board_out = $input{'output2'};
     if (!$perm_out && !$board_out) { &OutError;  return; }
     if ($perm_out)  { &ColumnLabel( "Permutation" ); }
     if ($board_out) { &ColumnLabel( "Chessboard" ); }
     $x[0] = $count = 0;
     for ($i=0; $i<=2*$N; ++$i) { $a[$i] = $b[$i] = $c[$i] = $TRUE; }
     &queen( 1 );
     print "</TABLE>\n";
     if ($retval == $LIMITerror) {
        if ($count == $LIMIT) { &LimitError; } else {
        print "<P>Limit of $textLIMIT text solutions reached.\n"; }
     }
     if ($retval == $boardLIMITerror) { 
        print "<P>Limit of $boardLIMIT chessboard solutions reached.\n"; }
  } 

  print "<P>Queens solutions = $count<BR>\n";
}


#!/usr/bin/perl

# This file called "e_subs.pl.cgi"
# Written by Frank Ruskey, June 1996.
# Some code taken from COS, but most everything re-written for ECOS/AMOF.
# Not to be distributed without permission.
# "Ownership": Frank Ruskey 
 
require "e_common.pl";

MAIN: 
{
# Read in all the variables set by the form
   if (&ReadParse(*input)) {
      &ProcessForm;
   }
   &CloseDocument( 'subs' );
}

sub Towers {
    local($i,$p0,$p1,$p2);
    # int peg0[MAXLAYERS], peg1[MAXLAYERS], peg2[MAXLAYERS];
 
    for ($i = 0; $i <= $N; $i++) {
        $peg0[$i] = $peg1[$i] = $peg2[$i] = -1;
    }
 
    $p0 = $p1 = $p2 = 0;
    for ($i = ($N-1); $i >= 0; $i--) {
        SWITCH: { 
           if ($pos[$i] == 0) { $peg0[$p0] = $i;  $p0++;  last SWITCH }
           if ($pos[$i] == 1) { $peg1[$p1] = $i;  $p1++;  last SWITCH }
           if ($pos[$i] == 2) { $peg2[$p2] = $i;  $p2++;  last SWITCH }
        }
    }
 
    print "<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0>\n";
    for ($i = $N; $i >= 0; $i--) {
        print "<tr>\n";
        print "<td height=7 width=85><img src='Ico/disk",$peg0[$i]+1,".gif' height=7 width=85></td>";
        print "<td height=7 width=85><img src='Ico/disk",$peg1[$i]+1,".gif' height=7 width=85></td>";
        print "<td height=7 width=85><img src='Ico/disk",$peg2[$i]+1,".gif' height=7 width=85></td>";
        print "</tr>\n";
    }
    print "</TABLE>\n";
}
 
sub PrintIt {
   local($p) = $_[0];
   local($i);
   local($num);
   if ($count == $LIMIT) { $retval = $LIMITerror; return; };
   ++$count;
   print "<TR>";
   if ($input{'output1'}) {
      print "<TD ALIGN=CENTER>";
      for ($i=1; $i<=$N; $i++) { print $A[$i]; }
      print "<BR></TD>\n";
   }
   if ($input{'output2'}) {
      print "<TD ALIGN=CENTER>";
      print "{";
      $num = 0;
      for ($i=$N; $i>=1; $i--) {
         if ($A[$i]) {
            print $N-$i+1;  ++$num;
            if ($num < $p) { print "," }
         }
      }
      print "}<BR></TD>\n";
   }
   if ($input{'output3'}) {
      print "<TD ALIGN=CENTER>";
      if ($bit) { print $N-$bit+1; }  
      print "<BR></TD>\n";
   }
   if ($input{'output4'}) {
      print "<TD ALIGN=CENTER>";
      &Towers;
      if ($bit==$N) {
         $pos[0] = ($pos[0]+$inc)%3;
      } else {
         if (($pos[$N-$bit] = ($pos[$N-$bit]+1)%3)==$pos[0])
            { $pos[$N-$bit] = ($pos[$N-$bit]+1)%3; }
      }
      # print "<BR></TD>\n";
      print "</TD>\n";
   }
   print "</TR>";
}

sub GenLex {
   local($k) = $_[0];
   local($p) = $_[1];  # p is the number of 1's in A
   if ($retval) { return; }
   if ($k > $N) { &PrintIt($p); 
   } else {
      $A[$k] = 0;  
      &GenLex( $k+1, $p );
      $A[$k] = 1;  
      &GenLex( $k+1, $p+1 );
   }
}
 
sub GenGray {
   local($k) = $_[0];
   local($p) = $_[1];  # p is the number of 1's in A
   if ($retval) { return; }
   $ones = $p;
   if ($k <= $N) { 
      $A[$k] = 0;  
      &GenGray( $k+1,  $p  );  
      $bit = $k;
      &PrintIt( $ones );
      $A[$k] = 1;              # $ones++;
      &NegGray( $k+1, $p+1 );  # $ones--;
   }
}
 
sub NegGray {
   local($k) = $_[0];
   local($p) = $_[1];  # p is the number of 1's in A
   if ($retval) { return; }
   $ones = $p;
   if ($k <= $N) {  
      $A[$k] = 1;              # $ones++; 
      &GenGray( $k+1, $p+1 );  # $ones--;
      $bit = $k;              
      &PrintIt( $ones );
      $A[$k] = 0;            
      &NegGray( $k+1, $p   );  
   }
}
 
sub StdOutput {
   print "<P>";
   &TableSetup;
   if ($input{'output1'}) {
   print "<TH COLSPAN=1><FONT SIZE=+1>$_[0]<BR></FONT><I>n</I>...321</TH>"; }
   if ($input{'output2'}) { &ColumnLabel( $_[1] ); }
   if ($input{'output3'}) { &ColumnLabel( $_[2] ); }
   if ($input{'output4'}) { &ColumnLabel( $_[3] ); }
   print "</TR>";
}

sub kError {
  &ErrorMessage('A k parameter must be specified');
 }

sub ProcessForm {

  &OutputHeader('Subsets Output','subs');  # from common.pl

  $outformat = 0;
  if ($input{'output1'}) { $outformat = 1; }
  if ($input{'output2'}) { $outformat = $outformat | 2; }
  if ($input{'output3'}) { $outformat = $outformat | 4; }
  if ($input{'output4'}) { $outformat = $outformat | 8; }
  if ($outformat == 0 ) { &OutError; return;  }

  if (int($input{'n'}) > 20) { &TooBig('n',20);  return; }
  if (int($input{'n'}) < 1) { &TooSmall('n',1);  return; }
  $N = $input{'n'};
  $retval = 0;
 
  if ($input{'program'} eq "sublex") {
      print "Subsets of <I>n</I> = $input{'n'} in lex order.";
      if ($input{'output3'}) { 
         &OutputIgnored('Bit Change');
         $input{'output3'} = '0'; }
      if ($input{'output4'}) 
         { &OutputIgnored('Towers');
         $input{'output4'} = '0'; }
      $outformat = $outformat & 3;
      &StdOutput('Bitstring','Subsets');
      &GenLex( 1, 0 );

   } elsif ($input{'program'} eq "subgray") {
      print "Subsets of <I>n</I> = $input{'n'} in Gray code order.<P>";
      if (($input{'output4'}) && ($input{'n'} > 8)) {
      print "The Towers of Hanoi must have n <= 8.<P>"; 
      $input{'output4'} = '0'; $outformat = $outformat & 7; }
      # if ($N%2 == 0) { $inc = 1; } else { $inc = 2; }
      $inc = 1 + $N%2;
      for ($i=0; $i<=$N; ++$i) { $pos[$i] = 0; $A[$i] = 0; }
      &StdOutput('Bitstring','Subsets','Bitchanges','Towers of Hanoi');
      $bit = $N;  $ones = 0;
      &GenGray( 1, 0 );
      $bit = 0;
      &PrintIt( 1 );
   }

   print "</TABLE>\n";
   if ($retval) { &LimitError; }
   print "<P>Subsets generated = $count";
}


#!/usr/bin/perl

# This file called "e_mulp.pl.cgi"
# Written and copyrighted by Frank Ruskey and Scott Lausch, March 1998
# Some code taken from COS, but virtually everything re-written for ECOS/AMOF;
# in particular, there are no calls to C programs, everything done
# in PERL.
# Not to be distributed without permission.
# "Ownership": Frank Ruskey

require "e_common.pl";

$outformat;
@pi;
@inputs;
@outarray;
$numinputs;
$N;
$done;

sub PrintIt
	{
	local($i);

	if ($count == $LIMIT) { $retval = $LIMITerror; $done = 1; return; }
	++$count;
	printf "<TR>\n";
	if ($outformat & 1)
		{
		printf "<TD ALIGN=CENTER>";
		for ($i = $N; $i > 0; $i--)
			{
			printf "$outarray[$pi[$i]]";
			}
		printf "<BR></TD>\n";
		}
	if ($outformat & 2)
		{
		printf "<TD ALIGN=CENTER>";
		for ($i = $N; $i > 0; $i--)
			{
			if ($pi[$i] == 0)
				{
				printf "<IMG SRC=Ico/blueball.gif height=14 width=14>";
				}			
			elsif ($pi[$i] == 1)
				{
				printf "<IMG SRC=Ico/redball.gif height=14 width=14>";
				}
			elsif ($pi[$i] == 2)
				{
				printf "<IMG SRC=Ico/greenbal.gif height=14 width=14>";
				}
			elsif ($pi[$i] == 3)
				{
				printf "<IMG SRC=Ico/orangeba.gif height=14 width=14>";
				}
			else
				{
				printf "<IMG SRC=Ico/newhibal.gif height=14 width=14>";
				}
			}
		printf "<BR></TD>\n";
		}
	}

sub PermMult
	{
	local($n) = @_;  # $n is position in @pi

	local($j);

	if ($done) { return; }

	if ($inputs[0] == $n)
		{
		&PrintIt;
		if ($done) { return; }
		}
	else
		{
		for ($j = 0; $j < $numinputs; $j++)	# for each input
			{
			if ($inputs[$j] > 0)
				{
				$pi[$n] = $j;
				$inputs[$j]--;
				PermMult($n - 1);
				if ($done) { return; }
				$inputs[$j]++;
				$pi[$n] = 0;
				}
			}
		}
	}

	
sub GenMulp
	{
	local($i);

	for ($i = 0; $i < $N; $i++)
		{
		$pi[$i] = 0;
		}	
	PermMult($N);
	}


MAIN: 
{
# Read in all the variables set by the form
   if (&ReadParse(*input)) {
      &ProcessForm;
   }
   &CloseDocument( 'fibo' );
}

sub PrintTableHeaders {
   &TableSetup;
   if ($input{'output1'}) { &ColumnLabel( "Symbols" ); }
   if ($input{'output2'}) { &ColumnLabel( "Coloured Balls" ); }
   print "</TR><BR>";
}


sub StdOutput {
   &TableSetup;
   if ($input{'output1'}) 
	{
	print "<TH COLSPAN=1><FONT SIZE=+1>$_[0]<BR></FONT>"; 
	}
   if ($input{'output2'}) { &ColumnLabel( $_[1] ); }
   print "</TR>";
}



sub nError
	{
	&ErrorMessage('An <I>n</I> parameter must be specified');
	}

sub ProcessForm
	{
	&OutputHeader ('Permutations of a Multiset Output', 'mulp' ); # from common.pl

	$outformat = 0;
	if ($input{'output1'}) { $outformat = 1; }
	if ($input{'output2'}) { $outformat = $outformat | 2; }
	if ($outformat == 0) { &OutError; return; }

	# place n1 - n5 in an array @inputs
	@inputs = ($input{'n1'}, $input{'n2'}, $input{'n3'}, $input{'n4'}, $input{'n5'});
	$N = 0;
	for ($i = 0; $i < 5; $i++)
		{
		$N += $inputs[$i];
		}
	$numinputs = @inputs;
	if (!$N)
		{ 
		&ErrorMessage( "At least one parameter <I>n<sub>i</sub></I> must be specified!" );
		return;
		}
	if ($N > 20)
		{
		&ErrorMessage( "The <I>n<sub>i</sub></I>'s add to more than 20.  This is too large." );
		return;
		}
	if ($input{'in1'})
		{
		$outarray[0] = $input{'in1'};
		}
	else
		{
		$outarray[0] = 1;
		}
	if ($input{'in2'})
		{
		$outarray[1] = $input{'in2'};
		}
	else
		{
		$outarray[1] = 2;
		}
	if ($input{'in3'})
		{
		$outarray[2] = $input{'in3'};
		}
	else
		{
		$outarray[2] = 3;
		}
	if ($input{'in4'})
		{
		$outarray[3] = $input{'in4'};
		}
	else
		{
		$outarray[3] = 4;
		}
	if ($input{'in5'})
		{
		$outarray[4] = $input{'in5'};
		}
	else
		{
		$outarray[4] = 5;
		}
	$retval = $count = 0;

	print"<P>Output in lexicographic order.<P>";
	&StdOutput('Symbols', 'Coloured Balls');
	$done = 0; # false
	&GenMulp;
	if ($retval) { &LimitError; }
	print "</TABLE><P>Permutations of a multiset generated = $count\n";
	}





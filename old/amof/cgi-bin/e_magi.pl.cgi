#!/usr/bin/perl

# This file called "e_magi.pl.cgi"
# Written and copyrighted by Frank Ruskey and Scott Lausch, March 1998
# Some code taken from COS, but virtually everything re-written for ECOS/AMOF;
# This PERL script calls a C program, which does the generation of Magic
# squares.
# Not to be distributed without permission.
# "Ownership": Frank Ruskey

require "e_common.pl";

$LIMIT = 10;
$N = $M = $Max = 0;

MAIN: 
{
# Read in all the variables set by the form
   if (&ReadParse(*input)) {
      &ProcessForm;
   }
   &CloseDocument( 'magi' );
}

sub LimError {
   local($M) = @_;
   print "</TABLE>";
   &ErrorHeader;
   print "You asked for more objects than the current limit of $M.<BR>\n";
   &ErrorTrailer;
}

sub PrintTableHeaders {
   &TableSetup;
   &ColumnLabel( "Magic Square" ); 
   print "</TR><BR>";
}


sub StdOutput {
   &TableSetup;
   print "<TH COLSPAN=1><FONT SIZE=+1>$_[0]<BR></FONT>"; 
   print "</TR>";
}

sub nError { &ErrorMessage('An <I>n</I> parameter must be specified'); }

sub TwoError { &ErrorMessage('There are no <math>2 x 2</math> magic squares'); }

sub ProcessForm	{
	local($Max);

	&OutputHeader ('Magic Squares Output', 'magi' ); # from common.pl

	if (!($input{'n'})) { 
		&ErrorMessage( "Parameter <I>n</I> must be specified!" );
		return;
	}
	$N = $input{'n'};

	if (int($input{'n'}) eq 0) { &nError; return; }   
	if (int($input{'n'}) eq 2) { &TwoError; return; }
	if ($N > 10) { &TooBig('n', 10); return; }
	if ($N < 1) { &TooSmall('n', 0); return; }
	if (int($input{'max'}) > $LIMIT) { $Max = $LIMIT; }
	else { $Max = int($input{'max'}); }
	
	$retval = $count = 0;
	&StdOutput('Magic Squares');
	$retval = system "./msquares $input{'n'} $Max";

	if ($retval) { &LimError($Max); }
	if ($N == 1 || $N == 3) { $count = 1; }
	elsif ($N < 1 || $N == 2 || $N > 10) { $count = 0; }
	else { $count = $Max; }

	print "</TABLE><P>Magic squares generated = $count\n";
}





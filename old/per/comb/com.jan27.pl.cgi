#!/usr/bin/perl

require "../common.pl";

MAIN: 
{
$LIMIT=500;
# Read in all the variables set by the form
  if (&ReadParse(*input)) {
    &ProcessForm;
  }
}

sub ExtraNs {
  for ($i=1; $i<=6; $i++) {
    if ($nval[$i]) {
      &ExtraParam("n($i)",$nval[$i]); } }
}

sub StdOutput {
   if ($outformat == 0 ) { &OutError; }
   &TableSetup;
  if ($input{'output1'}) {
    print "<TH COLSPAN=1><FONT SIZE=\"+1\">$_[0]<BR></FONT><I>n</I>...321</TH>"; }
  if ($input{'output2'}) { &ColumnLabel( $_[1] ); }
  if ($input{'output3'}) { &ColumnLabel( $_[2] ); }
  if ($input{'output4'}) { &ColumnLabel( $_[3] ); }
  print "</TR>";
}

sub MkProd {
  $prod = $input{'n1'}.",";
  $prod = $prod.$input{'n2'}.",";
  $prod = $prod.$input{'n3'}.",";
  $prod = $prod.$input{'n4'}.",";
  $prod = $prod.$input{'n5'}.",";
  $prod = $prod.$input{'n6'};
  print "<BR>";
  $beg = 0;
  for ($i=1; ($nval[$i] != 0) && ($nval[$i] ne ""); $i++) {
    if (!$beg) {
      print "<I>n($i)</I> = $nval[$i]"; $beg = 1; }
    else {
      print ", <I>n($i)</I> = $nval[$i]"; }}
  $input{'n'} = $i-1;
  print ".<P>";
  for ($i; $i<=6; $i++) {
    if ($nval[$i]) {
      &ExtraParam("n($i)",$nval[$i]); } }
}

sub OutError {
  &ErrorMessage('The output has not been specified or is not applicable');
}

sub kError {
  &ErrorMessage('A k parameter must be specified');
 }

sub ProcessForm {

  $x = $input{'program'};
  if (($x eq "sublex") || ($x eq "subgray") || ($x eq "prodlex") || ($x eq "prodgray") )	{
    &OutputHeader('Subsets and Combinations Output','comb/SubsetInfo');  # from common.pl
  }
  else {
    &OutputHeader('Subsets and Combinations Output','comb/CombinationsInfo');  # from common.pl
  }

  $outformat = 0;
  if ($input{'output1'}) { $outformat = 1; }
  if ($input{'output2'}) { $outformat = $outformat | 2; }
  if ($input{'output3'}) { $outformat = $outformat | 4; }
  if ($input{'output4'}) { $outformat = $outformat | 8; }

  $nval[1] = $input{'n1'};
  $nval[2] = $input{'n2'};
  $nval[3] = $input{'n3'};
  $nval[4] = $input{'n4'};
  $nval[5] = $input{'n5'};
  $nval[6] = $input{'n6'};
  
 if ($input{'program'} eq "sublex") {
    print "Subsets of <I>n</I> = $input{'n'} in lex order";
    if ($input{'k'}) { print ", with size limit <I>k</I> = $input{'k'}.<P>"; }
    else { $input{'k'} = $input{'n'}; print ".<P>"; }
    &ExtraNs;  
    if (int($input{'n'}) > 50) { &TooBig('n',50); exit(0); }
    if (int($input{'n'}) < 1) { &TooSmall('n',1); exit(0); }
    if ($input{'output3'}) { 
      &OutputIgnored('Bit Change');
      $input{'output3'} = '0'; }
    if ($input{'output4'}) 
      { &OutputIgnored('Towers');
      $input{'output4'} = '0'; }
    $outformat = $outformat & 3;
    &StdOutput('Bitstring','Subsets');
    $retval = system "../../bin/comb/sublex -n $input{'n'} -o $outformat -L $LIMIT -k $input{'k'}";
    if ($retval) { &LimitError; }
    &MoreInfo('comb/SubsetInfo');

}elsif ($input{'program'} eq "subgray") {
    print "Subsets of <I>n</I> = $input{'n'} in Gray code order.<P>";
    &ExtraParam('k',$input{'k'}) if ($input{'k'});
    &ExtraNs;
    if (int($input{'n'}) > 50) { &TooBig('n',50); exit(0); }
    if (int($input{'n'}) < 1) { &TooSmall('n',1); exit(0); }
    if (($input{'output4'}) && ($input{'n'} > 8)) {
      print "The Towers of Hanoi must have n <= 8.<P>"; 
      $input{'output4'} = '0'; $outformat = $outformat & 7; }
    &StdOutput('Bitstring','Subsets','Bitchanges','Towers of Hanoi');
    $retval = system "../../bin/comb/subgray -n $input{'n'} -o $outformat -L $LIMIT";
    if ($retval) { &LimitError; }
    &MoreInfo('comb/SubsetInfo');

}elsif ($input{'program'} eq "prodlex") {
    print "Product Spaces in Lex Order.";
    if (($input{'n1'} eq "")||($input{'n1'} == 0)) 
      { &TooSmall('n1',1); exit(0); }
    else { 
      &MkProd; }
    &ExtraParam('n',$input{'n'}) if ($input{'n'});
    &ExtraParam('k',$input{'k'}) if ($input{'k'});
    if ($input{'output3'}) { &OutputIgnored('Bit Change');
      $input{'output3'} = '0'; }
    if ($input{'output4'}) { 
      &OutputIgnored('Towers');
      $input{'output4'} = '0'; }
    $outformat = $outformat & 3;
    &StdOutput('String','Multisets');
    $retval = system "../../bin/comb/prodlex -n $input{'n'} -l $prod -o $outformat -L $LIMIT";
    if ($retval) { &LimitError; }
    &MoreInfo('comb/SubsetInfo');

}elsif ($input{'program'} eq "prodgray") {
    print "Product Spaces in Gray code Order.<BR>";
    if (($input{'n1'} eq "")||($input{'n1'} == 0)) 
      { &TooSmall('n1',1); exit(0); }
    else { 
      &MkProd; }
    &ExtraParam('n',$input{'n'}) if ($input{'n'});
    &ExtraParam('k',$input{'k'}) if ($input{'k'});
    if ($input{'output4'}) { &OutputIgnored('Towers');
      $input{'output4'} = '0'; }
    $outformat = $outformat & 7;
    &StdOutput('String','Multisets','(Digit,Value)');
    $retval = system "../../bin/comb/prodgray -n $input{'n'} -l $prod -o $outformat -L $LIMIT";
    if ($retval) { &LimitError; }
    &MoreInfo('comb/SubsetInfo');

}elsif ($input{'program'} eq "comlex") {
    print "Combinations of (<I>n,k</I>) = ($input{'n'},$input{'k'}) in lex order.<P>";
    &ExtraNs;
    if ($input{'output3'}) { &OutputIgnored('Bit Change');
      $input{'output3'} = '0'; }
    if ($input{'output4'}) { &OutputIgnored('Towers');
      $input{'output4'} = '0'; }
    $outformat = $outformat & 3;
    if (int($input{'n'}) > 50) { &TooBig('n',50); exit(0); }
    if (int($input{'n'}) < 1) { &TooSmall('n',1); exit(0); }
    if(int($input{'k'}) eq 0) { 
      &kError; }
    &StdOutput('Bitstring','Subsets');
    $retval = system "../../bin/comb/comlex -n $input{'n'} -o $outformat -L $LIMIT -k $input{'k'}";
    if ($retval) { &LimitError; }
    &MoreInfo('comb/CombinationsInfo');

}elsif ($input{'program'} eq "comcolex") {
    print "Combinations of (<I>n,k</I>) = ($input{'n'},$input{'k'}) in colex order.<P>";
    &ExtraNs;
    if ($input{'output3'}) { &OutputIgnored('Bit Changes');
      $input{'output3'} = '0'; }
    if ($input{'output4'}) { &OutputIgnored('Towers');
      $input{'output4'} = '0'; }
    $outformat = $outformat & 3;
    if (int($input{'n'}) > 50) { &TooBig('n',50); exit(0); }
    if (int($input{'n'}) < 1) { &TooSmall('n',1); exit(0); }
    if(int($input{'k'}) eq 0) { 
      &kError; }
    &StdOutput('Bitstring','Subsets');
    $retval = system "../../bin/comb/comcolex -n $input{'n'} -o $outformat -L $LIMIT -k $input{'k'}";
    if ($retval) { &LimitError; }
    &MoreInfo('comb/CombinationsInfo');

}elsif ($input{'program'} eq "comtran") {
    print "Combinations of (<I>n,k</I>) = ($input{'n'},$input{'k'}) by Transpositions.<P>";
    &ExtraNs;
    if ($input{'output4'}) { &OutputIgnored('Towers');
      $input{'output4'} = '0'; }
    $outformat = $outformat & 7;
    if (int($input{'n'}) > 50) { &TooBig('n',50); exit(0); }
    if (int($input{'n'}) < 1) { &TooSmall('n',1); exit(0); }
    if(int($input{'k'}) eq 0) { 
      &kError; }
    &StdOutput('Bitstring','Subsets','Bitchanges');
    $retval = system "../../bin/comb/comtran -n $input{'n'} -k $input{'k'} -o $outformat -L $LIMIT";
    if ($retval) { &LimitError; }
    &MoreInfo('comb/CombinationsInfo');

}elsif ($input{'program'} eq "comadj") {
    print "Combinations of (<I>n,k</I>) = ($input{'n'},$input{'k'}) by 
      Adjacent Transpositions.<P>";
    &ExtraNs;
    if ($input{'output4'}) { &OutputIgnored('Towers');
      $input{'output4'} = '0'; }
    $outformat = $outformat & 7;
    if (int($input{'n'}) > 50) { &TooBig('n',50); exit(0); }
    if (int($input{'n'}) < 1) { &TooSmall('n',1); exit(0); }
    if(int($input{'k'}) eq 0) { 
      &kError; }
    if
(($input{'n'}!=$input{'k'}+1)&&($input{'k'}!=1)&&(($input{'n'}%2==1)||($input{'k'}%2==0))){
      &ErrorHeader;
      print "The following must apply:<BR>";
      print "n=k+1 or k=1 or n even, k odd";
      exit(0); }
    &StdOutput('Bitstring','Subsets','Bitchanges');
    $retval = system "../../bin/comb/comadj -n $input{'n'} -k $input{'k'} -o $outformat -L $LIMIT";
    if ($retval) { &LimitError; }
    &MoreInfo('comb/CombinationsInfo');

}else {

# Print the header
  print &PrintHeader;
  print "<html><head>\n";
  print "<title>Subsets or Combinations Output</title>\n";
  print "</head>\n<body>\n";
  print "<h1>Subsets or Combinations Output</h1>\n";
  print "The program you selected was: $input{'program'}<p>\n";
  print "The values of the variables entered were:<br>\n";
  print "n = $input{'n'}<br> k = $input{'k'}<p>\n";
  print "The output types selected were:<ul>\n";
  if($input{'output1'}) { print "<li>$input{'output1'}"; }
  if($input{'output2'}) { print "<li>$input{'output2'}"; }
  if($input{'output3'}) { print "<li>$input{'output3'}"; }
  if($input{'output4'}) { print "<li>$input{'output4'}"; }
  if($input{'output5'}) { print "<li>$input{'output5'}"; }
  if($input{'output6'}) { print "<li>$input{'output6'}"; }
  print "</ul><p><hr>";
}
# Close the document cleanly.
  print "</body></html>\n";
}



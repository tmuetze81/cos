#!/usr/bin/perl

require "../common.pl";

select((select(STDOUT), $| = 1)[$[]);
select((select(STDERR), $| = 1)[$[]);

$PERMBINARY="../../bin/perm/Perm";
$BINDIR="../../bin/perm";


 MAIN:
{
# Read in all the variables set by the form
  if (&ReadParse(*input)) {
    &ProcessForm;
  }
}


sub PrintTableHeaders {
     print "<P>\n";
     print "<TABLE BORDER=1 CELLSPACING=2 CELLPADDING=2><TR>\n";
     if ($input{'out_oneline'})  {
        print '<TH COLSPAN=1><FONT SIZE="+1">One-Line</FONT><BR></TH>', "\n";
     }
     if ($input{'out_inverse'})  {
        print '<TH COLSPAN=1><FONT SIZE="+1">Inverse</FONT><BR></TH>', "\n";
     }
     if ($input{'out_cycle'}) {
        print '<TH COLSPAN=1><FONT SIZE="+1">Cycle</FONT><BR></TH>', "\n";
     }
     if ($input{'out_chess'}) {
        print '<TH COLSPAN=1><FONT SIZE="+1">Chess Board</FONT><BR></TH>', "\n";
     }
     if ($input{'out_tableau'}) {
        print '<TH COLSPAN=1><FONT SIZE="+1">Tableaux</FONT><BR></TH>', "\n";
     }
     if ($input{'out_trans'} and ($input{'program'} eq "Transp")) {
        print '<TH COLSPAN=1><FONT SIZE="+1">Transposition</FONT><BR></TH>', "\n";
     }
     if ($input{'out_diagram'} and ($input{'program'} eq "StampFolding"))  {
        print '<TH COLSPAN=1><FONT SIZE="+1">Picture</FONT><BR></TH>', "\n";
     }
     print "</TR>\n";
}

sub ExtraParamNi {
  &ExtraParam('n1',$input{'n1'}) if ($input{'n1'});
  &ExtraParam('n2',$input{'n2'}) if ($input{'n2'});
  &ExtraParam('n3',$input{'n3'}) if ($input{'n3'});
  &ExtraParam('n4',$input{'n4'}) if ($input{'n4'});
  &ExtraParam('n5',$input{'n5'}) if ($input{'n5'});
  &ExtraParam('n6',$input{'n6'}) if ($input{'n6'});
}

sub ProcessForm {

  # Verify that n is less than 50
  if($input{'n'} > 50) {
      printf("<HR>\n");
      &ErrorMessage("<I>n</I> must be less than 50.<BR>\n");
      printf("<HR>\n");
      printf("</BODY></HTML>\n");
      exit(1);
  }

  $outformat = 0;
  if ($input{'out_oneline'}) { $outformat = 1; }
  if ($input{'out_cycle'}) { $outformat = $outformat | 2; }
  if ($input{'out_trans'}) { $outformat = $outformat | 4; }
  if ($input{'out_chess'}) { $outformat = $outformat | 8; }
  if ($input{'out_inverse'}) { $outformat = $outformat | 16; }
  if ($input{'out_diagram'}) { $outformat = $outformat | 128; }
  if ($input{'out_tableau'}) {
    if ($input{'program'} eq "Invol") {
      $outformat = $outformat | 32;
    }
    else {
      $outformat = $outformat | 64;
    }
  }
  if ($outformat eq 0) { $outformat = 1; }

  if ($input{'program'} eq "Transp") {
     &OutputHeader('Permutations Output','perm/PermInfo');  # from common.pl
     print "Permutations by transpositions for <I>n</I> = $input{'n'}.<P>";
     &ExtraParam('k',$input{'k'}) if ($input{'k'});
     &ExtraParamNi();
     &PrintTableHeaders;
     $retval = system "$BINDIR/Perm -n $input{'n'} -o $outformat -L $LIMIT";
     print "</TABLE>";
     if (int($retval/256)) { &LimitError; }
     &WorkingMoreInfo('perm/PermInfo');

  } elsif ($input{'program'} eq "Invol") {
     &OutputHeader('Permutations Output','perm/Involutions');  # from common.pl
     print "Involutions for <I>n</I> = $input{'n'}.<P>";
     &ExtraParam('k',$input{'k'}) if ($input{'k'});
     &ExtraParamNi();
     if ($input{'output3'}) { &OutputIgnored("Transposition");  $input{'output3'} = ''; }     if ($input{'n'}) {
        &PrintTableHeaders();
        $retval = system "$BINDIR/invols -n $input{'n'} -o $outformat -L $LIMIT";
        print "</TABLE>";
        if (int($retval/256)) { &LimitError; }
     }
     &MoreInfo('perm/Involutions');

  } elsif ($input{'program'} eq "Derange") {
     &OutputHeader('Permutations Output','perm/Derangements');  # from common.pl
     print "Derangements for <I>n</I> = $input{'n'}";
     if ($input{'k'}) { print " and <I>k</I> = $input{'k'}.<P>"; } else { print ".<P>"; }     if ($input{'output3'}) { &OutputIgnored("Transposition");  $input{'output3'} = ''; }     &ExtraParamNi();
     if ($input{'n'}) {
        &PrintTableHeaders();
        if($input{'k'}) {
            $retval = system "$BINDIR/derange -o $outformat -n $input{'n'} -k $input{'k'} -L $LIMIT"; } else {
                $retval = system "$BINDIR/derange -o $outformat -n $input{'n'} -L $LIMIT"; }
        print "</TABLE>";
        if (int($retval/256)) { &LimitError; }
     }
     &MoreInfo('perm/Derangements');

  } elsif ($input{'program'} eq "Unrest") {
     &OutputHeader('Permutations Output','perm/PermInfo');  # from common.pl
            print "Unrestricted permutations for <I>n</I> = $input{'n'}.<P>";
            if ($input{'n'}) {
                &PrintTableHeaders();
                $retval = system "$BINDIR/kperms -n $input{'n'} -k $input{'n'} -o $outformat -L $LIMIT";
                print "</TABLE>";
                if (int($retval/256)) { &LimitError; }
            }
            &MoreInfo('perm/PermInfo');

  } elsif ($input{'program'} eq "maxlen") {
     &OutputHeader('Permutations Output','perm/MaxLen');  # from common.pl
            print "Permutations for <I>n</I> = $input{'n'}";
            print " and maximum cycle length <I>k</I> = $input{'k'}.<P>";
            if (($input{'n'}) && ($input{'k'})) {
                &PrintTableHeaders();
                $retval = system "$BINDIR/maxlen -n $input{'n'} -k $input{'k'} -o $outformat -L $LIMIT";
                print "</TABLE>";
                if (int($retval/256)) { &LimitError; }
            } else {
		print "Requires values for N and K";
	    }
            &MoreInfo('perm/MaxLen');

  } elsif ($input{'program'} eq "kcycles") {
     &OutputHeader('Permutations Output','perm/KCycles');  # from common.pl
            print "Permutations for <I>n</I> = $input{'n'}";
            print " with exactly $input{'k'} cycles.<P>";
            if (($input{'n'}) && ($input{'k'})) {
                &PrintTableHeaders();
                $retval = system "$BINDIR/kcycles -n $input{'n'} -k $input{'k'} -o $outformat -L $LIMIT";
                print "</TABLE>";
               if (int($retval/256)) { &LimitError; }
            } else {
		print "Requires values for N and K";
	    }
            &MoreInfo('perm/KCycles');

  } elsif ($input{'program'} eq "inversions") {
     &OutputHeader('Permutations Output','perm/Inversion');  # from common.pl
            print "Permutations for <I>n</I> = $input{'n'}";
            print " with $input{'k'} inversions.<P>";
            if (($input{'n'}) && ($input{'k'})) {
                &PrintTableHeaders();
                $retval = system "$BINDIR/inversions $input{'n'} $input{'k'} $outformat $LIMIT";
                print "</TABLE>";
                if (int($retval/256)) { &LimitError; }
            }
            &MoreInfo('perm/Inversion');

  } elsif ($input{'program'} eq "index") {
     &OutputHeader('Permutations Output','perm/Inversion');  # from common.pl
            print "Permutations for <I>n</I> = $input{'n'}";
            print " with index $input{'k'}.<P>";
            if (($input{'n'}) && ($input{'k'})) {
                &PrintTableHeaders();
                $retval = system "$BINDIR/index $input{'n'} $input{'k'} $outformat $LIMIT";
                print "</TABLE>";
                if (int($retval/256)) { &LimitError; }
            } else {
		print "Requires values for N and K";
	    }
            &MoreInfo('perm/Inversion');

  } elsif ($input{'program'} eq "lrmaxima") {
     &OutputHeader('Permutations Output','perm/LRMaxima');  # from common.pl
            print "Permutations for <I>n</I> = $input{'n'}";
            print " with exactly $input{'k'} left-to-right maxima.<P>";
            if (($input{'n'}) && ($input{'k'})) {
                &PrintTableHeaders();
                $retval = system "$BINDIR/lrmaxima -n $input{'n'} -k $input{'k'} -o $outformat -L $LIMIT";
                print "</TABLE>";
                if (int($retval/256)) { &LimitError; }
            } else {
		print "Requires values for N and K";
	    }
            &MoreInfo('perm/LRMaxima');

  } 
  elsif ($input{'program'} eq "StampFolding") {
    &OutputHeader('Stamp Foldings Output','perm/StampFolding');  # from common.pl
    if ($input{'n'})   {
      $restriction = 0;  ## default
      if ($input{'restriction'} eq "all")    {
        print "Stamp Foldings for <I>n</I> = $input{'n'}.<P>\n";
        $restriction = 0;
      }
      elsif ($input{'restriction'} eq "increasing")   {
        print "Stamp Foldings for <I>n</I> = $input{'n'} with one symmetry removed.<P>\n";
        $restriction = 1;
      }
      elsif ($input{'restriction'} eq "unlabelled")   {
        print "Stamp Foldings for <I>n</I> = $input{'n'} with both symmetries removed. <P>\n";
        $restriction = 2;
      }
      elsif ($input{'restriction'} eq "meanders")   {
        print "Meanders for <I>n</I> = $input{'n'}.<P>\n";
        $restriction = 3;
      }
      elsif ($input{'restriction'} eq "closed meanders")   {
        print "Closed meanders for <I>n</I> = ", $input{'n'}, ".  Each meander will be of length ",  $input{'n'} * 2, "<P>\n";
        $restriction = 4;
      }
      &PrintTableHeaders();
      $retval = system "$BINDIR/stamp-2.0 -n $input{'n'} -k $input{'n'} -o $outformat -L $LIMIT -P $$ -r $restriction";
      print "</TABLE>\n";
     if (int($retval/256)) { &LimitError; }
    }
    else {
      &ErrorMessage('Specify "n" for this type of object!');
    }
    &MoreInfo('perm/StampFolding');
  } 
  elsif ($input{'program'} eq "FirstGenocchi")  {
     &OutputHeader('Genocchi Sequences Output','perm/GenocchiInfo');  #from common.pl
            $length = $input{'n'}*2;
            print "Genocchi sequences (first kind) of length $length.<P>";
            &PrintTableHeaders();
            $retval = system "$BINDIR/genocchi -n $length -k $length -o $outformat -L $LIMIT";
            print "</TABLE>";
            if (int($retval/256)) { &LimitError; }
            &MoreInfo('perm/GenocchiInfo');

   } elsif ($input{'program'} eq "SecondGenocchi")  {
      &OutputHeader('Genocchi Sequences Output','perm/GenocchiInfo'); #from common.pl
            $length = $input{'n'}*2;
            print "Genocchi sequences (second kind) of length $length.<P>";
            &PrintTableHeaders();
            $retval = system "$BINDIR/genocch2 -n $length -k $length -o $outformat -L $LIMIT";
            print "</TABLE>";
            if (int($retval/256)) { &LimitError; }
            &MoreInfo('perm/GenocchiInfo');

  } elsif ($input{'program'} eq "kperms") {
     &OutputHeader('Permutations Output','perm/PermInfo');  # from common.pl
            print "$input{'k'} permutations of [$input{'n'}].<P>";
            if ($input{'output2'}) {
               &ErrorMessage("Can only output in 1-line notation!"); }
            if (($input{'n'}) && ($input{'k'})) {
                print "<TABLE BORDER=1 CELLSPACING=2 CELLPADDING=2><TR>\n";
                print '<TH COLSPAN=1><FONT SIZE="+1">One-line<BR></FONT></TH>';
                $retval = system "$BINDIR/kperms -n $input{'n'} -k $input{'k'} -o 1 -L $LIMIT";
                print "</TABLE>";
                if (int($retval/256)) { &LimitError; }
            } else {
                &ErrorMessage("Specify N and K for this type of object!"); }
            &MoreInfo('perm/PermInfo');

  } elsif ($input{'program'} eq "cyclestruct") {
     &OutputHeader('Permutations Output','perm/CycleStruct');  # from common.pl
            # build command-line version of cycle structure
            $parmsok = 0; # assume paramters are NOT ok
            $tempsum = 0;  # This will be used to ensure that the cycle
                           # structure is valid
            if ($input{'n1'} eq "") { $input{'n1'} = 0; }
            if ($input{'n2'} eq "") { $input{'n2'} = 0; }
            if ($input{'n3'} eq "") { $input{'n3'} = 0; }
            if ($input{'n4'} eq "") { $input{'n4'} = 0; }
            if ($input{'n5'} eq "") { $input{'n5'} = 0; }
            if ($input{'n6'} eq "") { $input{'n6'} = 0; }

            $tempsum = $input{'n1'} + 2*$input{'n2'} + 3*$input{'n3'};
            $tempsum = $tempsum + 4*$input{'n4'} + 5*$input{'n5'} + 6*$input{'n6'};

            print "Permutations for <I>n</I> = $input{'n'}, and cyle structure ";
            print "<I>n(1) = $input{'n1'}, ";
            print "n(2) = $input{'n2'}, ";
            print "n(3) = $input{'n3'}, ";
            print "n(4) = $input{'n4'}, ";
            print "n(5) = $input{'n5'}, ";
            print "n(6)</I> = $input{'n6'}<P>";

            if (($tempsum == $input{'n'}) && ($input{'n'})) { $parmsok = 1; }
            if ($parmsok) {
                $cyclestruct = $input{'n1'}.",";
                $cyclestruct = $cyclestruct.$input{'n2'}.",";
                $cyclestruct = $cyclestruct.$input{'n3'}.",";
                $cyclestruct = $cyclestruct.$input{'n4'}.",";
                $cyclestruct = $cyclestruct.$input{'n5'}.",";
                $cyclestruct = $cyclestruct.$input{'n6'};
                &PrintTableHeaders();
                $retval = system "$BINDIR/cyclestruct -n $input{'n'} -l $cyclestruct -o $outformat -L $LIMIT";
#               print "</TABLE>";
                if (int($retval/256)) { &LimitError; }
            } else {
                &ErrorMessage("Bad Cycle Structure Specified!");
                &ErrorMessage("Your cycle structure results in a permutation of length $tempsum");
            }
            &MoreInfo('perm/CycleStruct');
  }

# Close the document cleanly.
        print "</body></html>\n";
}


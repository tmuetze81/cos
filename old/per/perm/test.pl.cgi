#!/usr/bin/perl

require "../common.pl";

$BinDir = "../../bin/perm";
$PosetFILE = "PosetBinTree.temp";
$MAXn = 50;

MAIN: 
{
# Read in all the variables set by the form
  &ProcessForm;
}

sub InitializePosetFILE {
     open( FILE, ">$PosetFILE" ) || die "Can't open $PosetFILE:$!\n";
     $first = 2*$input{'n'};
     print FILE "$first\n";
     for ($i=1;$i<=$input{'n'};++$i) {
         $first = 2*$i-1;  $second = 2*$i;
         print FILE "$first  $second\n";
     }
     for ($i=1; $i<$input{'n'};++$i) {
         $first = 2*$i-1;  $second = 2*$i+1;
         print FILE "$first  $second\n";
     }
     for ($i=1; $i<$input{'n'};++$i) {
         $first = 2*$i;  $second = 2*$i+2;
         print FILE "$first  $second\n";
     }
     print FILE "0\n";
     close( FILE );
}

sub PrintTableHeaders {
     print "<P>";
     print "<TABLE BORDER=1 CELLSPACING=2 CELLPADDING=2><TR>\n";
     if ($input{'output1'})  {
        print "<TH COLSPAN=1><FONT SIZE=+1>One-Line<BR></FONT></TH>";
     }
     if ($input{'output6'})  {
        print "<TH COLSPAN=1><FONT SIZE=+1>Inverse<BR></FONT></TH>";
     }
     if ($input{'output2'}) {
        print "<TH COLSPAN=1><FONT SIZE=+1>Cycle<BR></FONT></TH>";
     }
     if ($input{'output4'}) {
        print "<TH COLSPAN=1><FONT SIZE=+1>Chess Board<BR></FONT></TH>";
     }
     if ($input{'output5'}) {
        print "<TH COLSPAN=1><FONT SIZE=+1>Tableaux<BR></FONT></TH>";
     }
     if ($input{'output3'} && $input{'program'} eq "Transp") {
        print "<TH COLSPAN=1><FONT SIZE=+1>Transposition<BR></FONT></TH>";
     }
     print "</TR><BR>";
 
 
}

sub ProcessForm {

    &OutputHeader('Rooted Trees Output','tree/RootedTree');  # from common.pl
    print "A diagram";

    $retval = system "OutputStamps";
    $retval = system "chmod a+r test_1.gif";
    print "<IMG SRC=http://sue.csc.uvic.ca/per/perm/test_1.gif>";
  
  &CloseDocument;
}


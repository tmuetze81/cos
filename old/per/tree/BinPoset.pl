#!/usr/bin/perl

require "../common.pl";

$paren = $ARGV[0];
$balls = $ARGV[1];
$lines = 0;

while (<STDIN>) {
  $x = $_;
  $x =~ s#<|>|T|D|B|R|/##g;
  @result = split(' ',$x);
  grep( ($_ %= 2) && 0, @result );  # from PERL book, pg 221.
  print "<TR>";
  if ($paren) {
     print "<TD ALIGN=CENTER>";
     for ($i=0; $i<@result; ++$i ) { print "$result[$i]"; }
     print "<BR></TD>\n";
  }
  if ($balls) {
     print "<TD ALIGN=CENTER>";
     for ($i=0; $i<@result; ++$i ) {
        if ($result[$i] == 1) { 
           print "<IMG SRC=ico/tiny.blackball.gif>"; 
        } else {
           print "<IMG SRC=ico/tiny.whiteball.gif>"; 
        };
     }
     print "<BR></TD>\n";
  }
  print "</TR>\n";
  if (++$lines >= $LIMIT) {
     print "</TABLE>";
     exit($LIMITerror); 
  }
}

print "</TABLE>\n<P>Trees generated = $lines<BR>\n";


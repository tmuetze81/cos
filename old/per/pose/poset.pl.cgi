#!/usr/bin/perl  
# !/usr/bin/perl5 -w

require "../common.pl";

$BinDir = "/theory/www/bin/pose";
$MaxN   = 100;

&InitIO;

MAIN: 
{
  if (&ReadParse(*input)) {
    &ProcessForm;
  }
}

sub check_filt {

  open (POSET, "./data.new");
  open (STDIN);
  while (<POSET>){
    @pair = split;
    if($pair[$0] eq "BAD") { 
	print "ERROR: Invalid Poset.";
    	&WorkingMoreInfo( 'pose/LinearExt' );
	exit(1);
    }
  }

}


sub ProcessForm {

  if ($input{'program'} eq "LexExtensions") {
     &OutputHeader( 'Poset Output', 'pose/LinearExt' );
     if  ($input{'output1'} eq "Standard") {$format="standard";}
     if ($format eq "") { &ErrorMessage('The output has not been specified or is not applicable'); Cleanup('pose/LinearExt');}
     if (int($input{'n'}) > $MaxN) { &TooBig('n',$MaxN);  Cleanup('pose/LinearExt'); }
     &parseposet;
     # system "$BinDir/poset";
     
     system "$BinDir/filt";
     &check_filt;
     print "Linear extensions in \"lexicographic\" order.<BR>\n";
     $retval = system "$BinDir/VRextension data.new | /usr/bin/perl Permute2.pl $format";	
     if ($retval) { &LimitError; }
     &WorkingMoreInfo( 'pose/LinearExt' );

  } elsif ($input{'program'} eq "LexIdeals") {
     &OutputHeader( 'Poset Output', 'pose/Ideals' );
     if (int($input{'n'}) > 100) { &TooBig('n',100); Cleanup('pose/LinearExt'); }
     &parseposet;
     system "$BinDir/filt";
     &check_filt;
     $retval = system "$BinDir/genIdeal -g poset.temp";
     &WorkingMoreInfo( 'pose/Ideals' );

  } elsif ($input{'program'} eq "GrayExtensions") {
     &OutputHeader( 'Poset Output', 'pose/LinearExt' );
     if  ($input{'output1'} eq "Standard") {$format="standard";}
     if  ($input{'output2'} eq "Gray") {$format = $format."gray"; }
     if ($format eq "") { &ErrorMessage('The output has not been specified or is not applicable'); Cleanup('pose/LinearExt');}
     if (int($input{'n'}) > 100) { &TooBig('n',100); Cleanup('pose/LinearExt'); }
     &parseposet;
     system "$BinDir/filt";
     &check_filt;
     print "Linear extensions in Gray code order.<BR>\n";
     $retval = system "$BinDir/genle -p data.new | /usr/bin/perl Permute2.pl $format";
     if ($retval) { &LimitError; }
     &WorkingMoreInfo( 'pose/LinearExt' );

  } elsif ($input{'program'} eq "CountExtensions") {
     &OutputHeader( 'Poset Output', 'pose/LinearExt' );
     if (int($input{'n'}) > 100) { &TooBig('n',100); Cleanup('pose/LinearExt'); }
     &parseposet;
     system "$BinDir/filt";
     &check_filt;
     $retval = system "$BinDir/genle -c data.new";
     &WorkingMoreInfo( 'pose/LinearExt' );

  } elsif ($input{'program'} eq "ProbTable") {
     &OutputHeader( 'Poset Output', 'pose/LinearExt' );
     if (int($input{'n'}) > 100) { &TooBig('n',100); Cleanup('pose/LinearExt'); }
     &parseposet;
     system "$BinDir/filt";
     &check_filt;
     print "Table of Prob(<I>x</I>&lt;<I>y</I>)*<I>e</I>(P).<BR>\n";
     $retval = system "$BinDir/genle -t data.new | /usr/bin/perl PermuteTable.pl";
     &WorkingMoreInfo( 'pose/LinearExt' );

  }  elsif ($input{'program'} eq "GrayIdeals") {
     &OutputHeader( 'Poset Output', 'pose/Ideals' );
     if (int($input{'n'}) > 100) { &TooBig('n',100); Cleanup('pose/LinearExt'); }
     &parseposet;
     system "$BinDir/filt";
     &check_filt;
     if($input{'output2'} eq "Gray"){
	 $retval = system "$BinDir/IdealGrey -g poset.temp";
      }else{
	   $retval = system "$BinDir/IdealGrey -s poset.temp";}
     &WorkingMoreInfo( 'pose/Ideals' );

  } elsif ($input{'program'} eq "CountIdeals") {
     &OutputHeader( 'Poset Output', 'pose/Ideals' );
     if (int($input{'n'}) > 100) { &TooBig('n',100); Cleanup('pose/LinearExt'); }
     &parseposet;
     system "$BinDir/filt";
     &check_filt;
     $retval = system "$BinDir/genIdeal -c poset.temp";
     &WorkingMoreInfo( 'pose/Ideals' );
  }

  print "</BODY></HTML>\n";
}


###########################################################################
#The following was written by Jeremy Schwartzentruber (jschwart@csr.uvic.ca)
#and is specific to the output for Linear Extensions in "lex" order.
#Aug. 28, 1995
###########################################################################

sub InitIO {
  $oldhandle = select(STDOUT);
  $| = 1;
  select($oldhandle);

  $oldhandle = select(STDERR);
  $| = 1;
  select($oldhandle);
}

sub gettoke {
  local($toke);
  if ($firsttime) {
     $nextline = $input{'relations'};
     $firsttime = 0;
  }
  if (!$nextline) {
     return '';
  } else {
     ($toke) = $nextline =~ /^\s*(\{|>|<|\}|\,|[^<>\{\{\}\,\s]+)\s*/;
     $nextline = $';
     return $toke;
  }
}

sub store {
  local($first, $second) = @_;
  if ($first > $n || $second > $n) {
      print "All nodes must be less than or equal to N\n";
     &WorkingMoreInfo( 'pose/LinearExt' );
      exit;
  }
  print FILE "$first $second\n";
#	print ("First is $first, second is $second\n");
}

sub badposet {
	print "ERROR: Invalid Poset.\n";
}

sub startposet {
  $file = "./poset.temp";
  open(FILE, ">$file") || die "Can't open $file:$!\n";
  print FILE "$n\n";
  $firstint='';
  $secondint='';
  $lastint='';
  $newpair=1;
}

sub finishpair {
	$secondint=$lastint;
	&store($firstint, $secondint);
	$firstint='';
	$secondint='';
	$lastint='';
	$newpair=1;
}

sub finishposet {
	print FILE "0\n";
	close(FILE);
}

sub less {
	$firstint=$lastint;
}

sub symbol {
	if ($newpair) {
		($lastint) = @_;
		$newpair=0;
	} else {
		($lastint) = @_;
		&finishpair;
	}
}

sub parseposet {
	$nextline = '';
	$n = $input{'n'} + 0;
	if ($n <= 0) {
		print "N must be an integer greater than zero!\n";
     		&WorkingMoreInfo( 'pose/LinearExt' );
		exit;
	}
	$firsttime = 1;

	$_ = &gettoke;
	if (/^\{$/) {
		&startposet;
	} else {
		print "List must start with a '{'\n\n";
     		&WorkingMoreInfo( 'pose/LinearExt' );
		exit;
	}
	while ($_=&gettoke) {
#		print "<br>TOKEN: |$_|\n";
		PARSE: {
			if (/^\}$/) { &finishposet; }
			elsif (/^\,$/) {
				if (!$newpair) { &finishpair; }
			}
			elsif (/^<$/)  { 
				# Extra if statement added to check for
				# bad data of the form { 1<2, <3} -Joe
				if ($newpair) {
					print "Bad input. \n\n";
     					&WorkingMoreInfo( 'pose/LinearExt' );
					exit;
				}
				else { &less; }
			}
			else {&symbol($_); }
		}
	}
}


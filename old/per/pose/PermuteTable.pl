#!/usr/bin/perl
#
# This program performs a tranformation on the output of the genle
# program using the transformation rules in the transform.log file.
#

require "../common.pl";

open (RULES, "./transform.log");
open (STDIN);
$n = 0;
while (<RULES>) {
   @pair = split;
   $node[++$n] = $pair[0];
   $reorder[$pair[0]] = $n;
#  $node[$pair[1]] = $pair[0];
}
print "n = $n<BR>";
$returnval = 0;
$row = 1;
print "<P><TABLE BORDER=1 CELLSPACING=2 CELLPADDING=2><TR>\n";
print "<TH align=right><I>y</I> = \n";
for ($i = 1; $i <= $n; ++$i) { print "<TH>$i</TH>\n"; } 

# Read in lines and store in table 
while (<>) {
	push @table , [split]
}

# Re-order and display the output
for($row=1; $row<=$n; ++$row) {

       print "<TR><TH ALIGN=RIGHT>";
       if ($row == 1) { print"<I>x</I> = $row"; }
       else { print "$row"; }
       for($i=1; $i <= $n; ++$i) {
          print "<TD ALIGN=RIGHT>$table[$reorder[$row]-1][$reorder[$i]-1] ";
       }	
       print "<BR></TD></TR>\n";
}

print "</TABLE>";
print "<P>Total extensions = <I>e</I>(P) = $table[$n][3]<BR>\n";
print "A best pair = (<I>x</I>,<I>y</I>) = ($node[$table[$n][0]],$node[$table[$n][1]])<BR>\n";
print "For best pair Prob(<I>x</I>&lt;<I>y</I>) = ";
print "$table[$n][2]/$table[$n][3] = $table[$n][4]<BR>\n";

close (RULES);
close (STDIN);
$returnval; # return value  


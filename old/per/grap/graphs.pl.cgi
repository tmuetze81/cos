#!/usr/bin/perl

require "../common.pl";

$BinDir="../../bin/grap";
$GraphBIN = "$BinDir/makeg";    # source in Nauty directory
$BGraphBIN = "$BinDir/makebg";  #

MAIN: 
{
# Read in all the variables set by the form
  if (&ReadParse(*input)) {
    &ProcessForm;
  }
}

sub ProcessForm {
  &OutputHeader('Graphs Output','grap/GraphInfo');
  
  $xx = "";
  $Gtype = $input{'graph_type'}; 
  if ($Gtype eq 'BicG' || $Gtype eq 'ConnBicG') { $two_color = 1 }
  else { $two_color = 0 }
  
  print "All ";
  if (   $Gtype eq 'ConnG' || $Gtype eq 'ConnBipG' || 
         $Gtype eq 'ConnTrifG' || $Gtype eq 'ConnBicG') {
     print "connected ";  $xx = $xx.' -c';
  }
  if ($Gtype eq "TriG" || $Gtype eq "ConnTrifG") {
     print "triangle-free ";  $xx = $xx.' -t';
  }
  if ($Gtype eq 'BipG' || $Gtype eq 'ConnBipG') {
     print "bipartite ";  $xx = $xx.' -b';
  }
  if ($two_color) { print "bicoloured ";  $xx = $xx.''; }
  print " graphs with <I>n</I> = $input{'n'} vertices";
  if ($two_color) {
     print " in one partite set and <I>m</I> = $input{'m'} vertices in";
     print " the other partite set"; 
  }
  if ($input{'d'}) { 
     print ", and max degree <I>d</I> = $input{'d'}"; 
     $xx = $xx." -d$input{'d'}";
  } 
  print ", and edge counts satisfying ";
  $xx = $xx." $input{'n'} "; 
  if ($input{'graph_type'} eq 'BicG' || $input{'graph_type'} eq 'ConnBicG') {
     $xx = $xx." $input{'m'} ";
  }
  if ($input{'mine'}) {
     print "$input{'mine'}";  $xx = $xx." $input{'mine'} ";
  }
  else { print "0"; }
  print " &lt;= |<I>E</I>| &lt;= ";
  if ($input{'mine'}) {
     print "$input{'maxe'}.<BR>";  $xx = $xx." $input{'maxe'} ";
  }
  elsif ($two_color) {
     $num = int(($input{'n'})*($input{'m'})); print "$num."; }
  else { $num = int(($input{'n'}-1)*$input{'n'}/2); print "$num."; }

  print "<P>";
  &TableSetup;
    
  if ($input{'output1'}) { $oo =     ' 1'; 
     &ColumnLabel( "Squashed" ); 
  } else { $oo =     ' 0'; }
  if ($input{'output2'}) { $oo = $oo.' 1';  
     &ColumnLabel( "Adj. List" ); 
  } else { $oo = $oo.' 0'; }
  if ($input{'output3'}) { $oo = $oo.' 1';  
     &ColumnLabel( "Matrix" ); 
  } else { $oo = $oo.' 0'; }
  
  if ($two_color) {
     $oo = $input{'n'}+$input{'m'}.$oo;
     $retval = system "$BGraphBIN $xx | ./process.pl $oo"; 
  } else { 
     $retval = system "$GraphBIN $xx | ./process.pl $input{'n'} $oo";
  }
  &LimitError if ($retval);
  &WorkingMoreInfo('grap/GraphInfo');
  print "</BODY></HTML>\n";
}


#!/usr/bin/perl

rename $ARGV[0].'.html', $ARGV[0].'.hold';
$name = $ARGV[0].'.html';
open (IN_FILE, $ARGV[0].".hold") || die "couldn't open input file\n";
open (OUT_FILE, ">$name") || die "couldn't open output file\n";
while ($_ = <IN_FILE>) {
   $_ =~ s/<TD align=right><FONT SIZE=-2>/<TD align=right><FONT SIZE=-1>/;
   $_ =~ s#http://ejc.math.gatech.edu:8080/Journal/journalhome.html#http://www.combinatorics.org/#;
   $_ =~ s#<!-- BASE=http://theory.cs.uvic.ca/venn/ -->\n##;
   $_ =~ s/3 \(1996\)/4 (1997)/;
   $_ =~ s/\(to appear in\)/<!-- [to appear in] -->/;
   $_ =~ s/<TITLE>The Venn Diagram Page/<TITLE>Survey of Venn Diagrams/;
   $_ =~ s/Ruskey, 1996./Ruskey, 1996, 1997./o;
   $_ =~ s/The Venn Diagram Page:/Venn Diagram Survey:/;
   $_ =~ s/<FONT SIZE=-1>The Venn Diagram Page/<FONT SIZE=-1>Venn Diagram Survey/;
   print;
   print OUT_FILE $_;
}
close IN_FILE;
close OUT_FILE;


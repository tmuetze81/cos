<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>

<TITLE>The Venn Diagram Page -- Symmetric Diagrams</TITLE>

<META NAME="resource-type" CONTENT="document">
<META NAME="description" CONTENT="The Venn Diagram Page: combinatorial facts, beautiful figures, n=3 and beyond. This page is about symmetric Venn diagrams.">
<META NAME="keywords" CONTENT="Venn diagram, hypercube, symmetric, binomial coefficient, Gray codes, middle-two-levels problem">
<META NAME="distribution" CONTENT="global">
<META NAME="copyright" CONTENT="Frank Ruskey, 1996.">
<META HTTP-EQUIV="Reply-to" CONTENT="fruskey@csr.uvic.ca">

<!-- BASE=http://theory.cs.uvic.ca/venn/ -->
</HEAD>

<BODY BGCOLOR="#FFFFFF">
 
<TABLE width=100%>
<TR><TD align=left>
<A HREF=VennEJC.html><IMG border=0 SRC=gifs/smallV3plain.gif></A>
<TD align=right><FONT SIZE=-1>
(to appear in) 
<A HREF=http://ejc.math.gatech.edu:8080/Journal/journalhome.html>
<FONT SIZE=-1>T</FONT>HE <FONT SIZE=-1>E</FONT>LECTRONIC
<FONT SIZE=-1>J</FONT>OURNAL OF <FONT SIZE=-1>C</FONT>OMBINATORICS</A>
4 (1997), DS #5.
</FONT>
</TR></TABLE>
 
<HR NOSHADE>
 
<CENTER>
<IMG ALIGN=TOP SRC=gifs/VictoriaIcon.gif>
<IMG ALIGN=TOP SRC=gifs/VictoriaIcon.gif>
<H1 ALIGN=CENTER>
<FONT SIZE=-1>The Venn Diagram Page</FONT><BR>
Symmetric Diagrams
</H1>
</CENTER>

<CENTER>
[<A HREF="#symmetric">symmetric diagrams</A> |
 <A HREF="#gray">Gray codes</A> ]
</CENTER>

<P>
<HR width="60%">

<A NAME="symmetric">
<H2>Symmetric Venn diagrams</H2>

<IMG SRC=gifs/ellipsevenn.gif ALIGN=RIGHT>
Here we (re-)show a Venn diagram made from 5
  congruent ellipses.
The regions are colored according to the number
  of ellipses in which they are contained: grey = 0, yellow = 1,
  red = 2, blue = 3, green = 4, and black = 5.
Note that the number of regions colored with a given color corresponds to
  the appropriate binomial coefficient: #(grey) = #(black) = 1,
  #(yellow) = #(green) = 5, and #(red) = #(blue) = 10.
 
<P>
This diagram has a very a pleasing symmetry, namely
  an <I>n</I>-fold rotational symmetry.
Such diagrams are said to be <EM>symmetric</EM>.
This simply means that there is a point <I>x</I> about which
  the diagrams may be rotated by 2<I>i</I><B>pi</B>/<I>n</I> 
  and remain invariant, for <I>i</I>=0,1,...,<I>n</I>-1.
Any symmetric Venn diagram must be made from congruent curves.

<P>
A symmetric diagram need not be simple.
Diagram <A HREF=gifs/venn3c-01-02.gif>#3.1</A>
  of Class 1 in the list of all Venn diagrams for <I>n</I>=3
  is also a symmetric diagram, but it is not simple.

<H4><I>n</I> = 5</H4>

<P>
The ellipse diagram above has a 5-fold rotational symmetry.
It is the only simple symmetric Venn diagram for <I>n</I>=5.
There are non-simple symmetric diagrams for <I>n</I>=5
  (<A HREF=howcirc.ps>postscript version</A> (6K) ),
  but their exact number is unknown.

<P>
Rick Mabry observes that the link of five knots formed by
  weaving the Venn diagram shown above is a Brunnian link ---
  the removal of any knot causes the link to fall apart.
Mabry has created a beautiful rendering of the link made from 
  "ice cream cone curves" 
  (<A HREF=MabryIce.ps>postscript versions</A> (96K) ).
This diagram serves as an illustration of a symmetric 
  Brunnian link<sup><A HREF=VennFootEJC.html#brunnian>7</A></sup> 
  of order 5. 
(The usual depiction of the <A HREF=borromean.html>Borromean rings</A>
  is a symmetric Brunnian link of order 3.)
This Brunnian link is attributed to C. van de Walle by Stewart
  [<A HREF=VennRefs.html#St>St</A>, pg. 106].

<H4><I>n</I> = 7</H4>

<P>
Referring to the case <I>n</I>=7, Gr&uuml;nbaum [Gr75,p.19] writes:
  "at present it seems likely that no such diagram exists."
However, Gr&uuml;nbaum found examples of such diagrams 
  [<A HREF="VennRefs.html#Gr92b">Gr92b</A>] and
  in 1992 additional examples were also discovered by Anthony 
  Edwards and independently by Carla Savage and Peter Winkler.
One of Gr&uuml;nbaum's examples is a remarkable <A HREF=gifs/GrunSymm7.gif>
  non-monotone symmetric Venn diagram</A>.
There are at least <A HREF=nonsymm7.list>17 other symmetric Venn diagrams</A>
  for <I>n</I>=7 (these are all monotone).

<P>
<B>Theorem.</B>
  Symmetric Venn diagrams can exist only if <I>n</I> is a prime number.

<P>
This explains why we have only been discussing symmetric Venn diagrams for
  the values 3,5, and 7.
To understand why the theorem is true, imagine the diagram to be split evenly
  into <I>n</I> sectors (pieces of pie).
The <I>C</I>(<I>n</I>,<I>k</I>)<sup>
  <A HREF=VennFootEJC.html#binomial>1</A></sup> 
  different regions representing the <I>k</I>-subsets 
  must be evenly distributed in each of the <I>n</I> sectors.
It thus follows that <I>n</I> divides <I>C</I>(<I>n</I>,<I>k</I>) 
  for each <I>k</I> = 1,2,...,<I>n-1</I>.
This divisibility restriction implies that <I>n</I> must be prime,
  via a theorem of Leibnitz [<A HREF=VennRefs.html#Ed96>Ed96</A>].

<P>
However, no one has ever discovered a symmetric Venn diagram for
  any prime larger than 7, and it remains a tantalizing puzzle
  to find one for <I>n</I>=11 or prove that none exists.
Below we summarize what is know about symmetric Venn diagrams for
  <I>n</I>=7.

<H3>Representations of symmetric Venn diagrams</H3>

<H4>The Gr&uuml;nbaum encoding</H4>

The Gr&uuml;nbaum encoding of a simple symmetric Venn diagram consists of four
  <I>n</I>-ary strings, call them <B>w</B>, <B>x</B>, <B>y</B>, 
  <B>z</B>, each of length (2<sup><I>n+1</I></SUP>-4)/<I>n</I>.
String <B>w</B> is obtained by starting with the outermost curve,
  following it in a clockwise direction, recording its intersections
  with the other curves, labelling them as they are first encountered,
  starting at 1.
The other strings are obtained by going counter-clockwise and/or  
  starting with one of the innermost curves.
(Actually, Gr&uuml;nbaum proposed ordering the curves by their 
  clock-wise appearance on the outer face; this amounts to a renumbering.)
For the 3 circle diagram <B>w</B> = <B>x</B> = <B>y</B> = <B>z</B> = 1212.
Each string of the encoding is a 
  <A HREF=http://theory.cs.uvic.ca/inf/setp/SetPartitions.html#RG>restricted 
  growth string</A>, meaning that each number is at most one greater than
  the values that precede it, with the first value being 0 (except that
  we take the first value to be 1).
For the 5 ellipse diagram at the top of this page, the strings of the 
  Gr&uuml;nbaum encoding are
  <B>w</B> = <B>z</B> = 123214121432 and 
  <B>x</B> = <B>y</B> = 123414341214.
Of course, we really only need two of the strings, one starting at the
  inside and one at the outside, since <B>w</B> can be inferred from
  <B>x</B>, and <B>y</B> from <B>z</B> -- but it's convenient to have
  all four, particularly when checking a diagram by hand.
In fact, only one string is required, since, given <B>x</B>, the starting
  point of an inner string can be determined as the unique location where
  all curves have been encountered an odd number of times (thus implying that
  we're on the inside of all curves).

<!--
It's clear that two diagrams with different encodings will have different
representations, but it is the case that different diagrams will always
have different encodings?  Also, have we lost something by going from
the numbering of curves on the outer face to the RG function?
-->

<H3>Symmetric Venn diagrams with polar symmetry</H3>

<P>
A symmetric Venn diagram has <EM>polar symmetry</EM> if it is
  invariant under "polar flips".
Think of the diagram as being projected stereographically onto a sphere 
  with the axis running thru the point of symmetry, and then project
  the diagram back onto the plane from the antipodal point on the
  axis.
If the corresponding Venn graphs are isomorphic as plane graphs,
  then the diagram has polar symmetry.
The unique symmetric Venn diagram for <I>n</I> = 5 has polar symmetry.

<P>
For <I>n</I>=7, all simple monotone symmetric Venn diagrams with polar symmetry 
  are known; there are six of them, and they are listed below.
The first five were discovered by Anthony Edwards using a combination of hand 
  checking and computer searches
  [<A HREF=VennRefs.html#Ed96>Ed96</A>].  
He named them after the cities in which they were discovered and we
  follow his naming convention (but also number them P1-P6).
Gr&uuml;nbaum [<A HREF=VennRefs.html#Gr92b>Gr92b</A>], 
  and Savage and Winkler independently discovered "Adelaide".
An exhaustive computer search by Frank Ruskey uncovered another, 
  "Victoria", that had been overlooked.

<H4>Symmetric monotone Venn diagrams with polar symmetry:</H4>

<UL>
<LI><B>Adelaide (P1):</B>
  <A HREF=Adelaide.html>Info</A>,
  <A HREF=gifs/Adelaide.gif> black-and-white </A>, 
  <A HREF=gifs/AdelaideGreen.gif> one curve colored </A>,
  <A HREF=gifs/AdelaideColor.gif> symmetric coloring </A>,
  <A HREF=gifs/AdelaideRain.gif> rainbow coloring </A>, 
  <A HREF=gifs/AdelaideEdwards.gif> Edwards' rendering (149K)</A>.
<LI><B>Hamilton (P2):</B>
  <A HREF=gifs/Hamilton.gif> black-and-white </A>, 
  <A HREF=gifs/HamiltonGreen.gif> one curve colored </A>,
  <A HREF=gifs/HamiltonColor.gif> symmetric coloring </A>,
  <A HREF=gifs/HamiltonRain.gif> rainbow coloring </A>. 
<LI><B>Massey (P3):</B> 
  <A HREF=gifs/Massey.gif> black-and-white </A>, 
  <A HREF=gifs/MasseyGreen.gif> one curve colored </A>,
  <A HREF=gifs/MasseyColor.gif> symmetric coloring </A>,
  <A HREF=gifs/MasseyRain.gif> rainbow coloring </A>. 
<LI><B>Victoria (P4):</B> 
  <A HREF=gifs/Victoria.gif> black-and-white </A>, 
  <A HREF=gifs/VictoriaGreen.gif> one curve colored </A>,
  <A HREF=gifs/VictoriaColor.gif> symmetric coloring </A>,
  <A HREF=gifs/VictoriaRain.gif> rainbow coloring </A>, 
  <A HREF=gifs/VictoriaWild.gif> alternate coloring </A>.
<LI><B>Palmerston North (P5):</B>
  <A HREF=gifs/Palmerston.gif> black-and-white </A>, 
  <A HREF=gifs/PalmerstonGreen.gif> one curve colored </A>,
  <A HREF=gifs/PalmerstonColor.gif> symmetric coloring </A>,
  <A HREF=gifs/PalmerstonRain.gif> rainbow coloring </A>. 
<LI><B>Manawatu (P6):</B>
  <A HREF=gifs/Manawatu.gif> black-and-white </A>, 
  <A HREF=gifs/ManawatuGreen.gif> one curve colored </A>,
  <A HREF=gifs/ManawatuColor.gif> symmetric coloring </A>,
  <A HREF=gifs/ManawatuRain.gif> rainbow coloring </A>. 
</UL>

<H4>Symmetric monotone Venn diagrams without polar symmetry:</H4>

There are 23 known monotone symmetric Venn diagrams and 17 of
  these do not have polar symmetry.

<UL>
  <LI><A HREF=nonpolar.html>A list</A> of the 17 known monotone 
  symmetric Venn diagrams without polar symmetry.  
  We believe that these are all of them.
<LI><A HREF=gifs/scan-venn4.gif>A symmetric diagram whose 
  curves are 5-gons</A>.  This diagram is from
  [<A HREF=VennRefs#Gr92b>Gr92b</A>] and is M2 on our list.
<LI><A HREF=nonsymm7.list>A list</A> of all 23 known monotone
  Venn diagrams (includes the six with polar symmetry).
</UL>

<H4>Symmetric non-monotone Venn diagrams:</H4>

There are no known symmetric non-monotone Venn diagrams with
  polar symmetry, but there are 33 known that do not have
  polar symmetry.

<UL>
<LI>Gr&uuml;nbaum's non-monotone symmetric diagram, in 
  <A HREF=gifs/GrunSymm7.gif>black-and-white</A> (scanned from 
  [<A HREF=VennRefs.html#Gr92b>Gr92b</A>]) and 
  in <A HREF=gifs/GrunSymm7color.gif>color</A>.
<LI><A HREF=gifs/NonNeck7.gif>Another non-monotone symmetric 
  Venn diagram</A>; this one discovered by Chow and Ruskey.  A
  <A HREF=gifs/NonNeck7color.gif>colored rendering</A>.
<LI>A <A HREF=NonMonotoneEJC.html>list</A> of the Gr&uuml;nbaum 
    representations of the 33 known non-monotone symmetric Venn diagrams.
  We believe that these are all of them.  
  None have polar symmetry.
  Gr&uuml;nbaum's example shown above is number 27 in the list and 
    the other one shown above is number 33.
<LI>Tutte embeddings of diagrams 
  <A HREF=gifs/7set_1.gif>N1</A>, 
  <A HREF=gifs/7set_2.gif>N2</A>, 
  <A HREF=gifs/7set_3.gif>N3</A>, 
  <A HREF=gifs/7set_4.gif>N4</A>, 
  <A HREF=gifs/7set_5.gif>N5</A>, 
  <A HREF=gifs/7set_7.gif>N7</A> from the above list.
<LI>Colored Tutte embeddings of diagrams 
  <A HREF=gifs/7set_1col.gif>N1</A> and  
  <A HREF=gifs/7set_7col.gif>N7</A> from the above list.
</UL>

<P>
<HR>

<A NAME="gray">
<H2>Venn Diagrams and Gray Codes</H2>

<P>
There are many relations between Venn diagrams and (combinatorial)
  Gray codes.
Some of these relations have not much to do with symmetry but
  some do.

<P>
<H3>A Phase Shifted Version of Edwards' Construction 
  and Counting in Binary</H3>
 
Suppose that we rotate the <I>j</I>th curve in Edwards' general
  construction by <B>pi</B>/<I>j</I> radians.
The result is still a Venn diagram, but it is no longer simple.
The vertical line disappears into the horizontal line but the other
  curves remain distinct.
 
<UL>
<LI><I>n</I> = 2:
  <A HREF=gifs/rot3.gif>black and white</A>,
  <A HREF=gifs/rot3c.gif>colored</A>.
<LI><I>n</I> = 3:
  <A HREF=gifs/rot4.gif>black and white</A>,
  <A HREF=gifs/rot4c.gif>colored</A>.
<LI><I>n</I> = 4:
  <A HREF=gifs/rot5.gif>black and white</A>,
  <A HREF=gifs/rot5c.gif>colored</A>.
<LI><I>n</I> = 5:
  <A HREF=gifs/rot6.gif>black and white</A>,
  <A HREF=gifs/rot6c.gif>colored</A>.
<LI><I>n</I> = 6:
  <A HREF=gifs/rot7.gif>black and white</A>,
  <A HREF=gifs/rot7c.gif>colored</A>.
<LI><A HREF=gifs/anim-rot.gif>animated</A> for <I>n</I>=2..6.
</UL>
 
Aside from being attractive, the rotated version illustrates an interesting
connection between the binary reflected Gray code and counting in binary.
The binary reflected Gray code arises an a Hamilton path
  following two circles (<A HREF=VennGray.html#gray>picture</A>) in the Venn
  graph of Edwards original construction.
In the rotated version, with a minor modification, these same two circles
  may be traversed to count in binary (<A HREF=VennGray.html#count>picture</A>).
 
<P>
Edwards produced templates for making a rotatable 
  Venn diagram; templates which we have scanned:
  <A HREF=gifs/scan-ed1.gif>page 1</A>,
  <A HREF=gifs/scan-ed2.gif>page 2</A>,
  <A HREF=gifs/scan-ed3.gif>page 3</A>,
  <A HREF=gifs/scan-ed4.gif>page 4</A>.

<H3>Symmetric Venn diagrams and the Middle Two Levels problem</H3>

<P>
For odd <I>n</I> = 2<I>k</I>+1 the <EM>middle two levels problem</EM>
  is to find a Hamilton path in the subgraph of the <I>n</I>-cube
  induced by those vertices corresponding to <I>k</I>-sets and
  (<I>k</I>+1)-sets.
This is a well-known problem about which several papers have been
  written.
Some symmetric Venn diagrams have embedded in them a solution to the
  middle two levels problem.
For <I>n</I>=7 consider the diagrams 
  <A HREF="gifs/AdelaideColor.gif">Adelaide</A> and 
  <A HREF="gifs/HamiltonColor.gif">Hamilton</A>
  with the symmetric coloring.
The middle two levels correspond to the middle two necklaces,
  the ones colored yellow (they also have diagonal line shading
  for those with black-and-white browsers).  
Note the zig-zag pattern that they follow, alternating between
  <I>k</I>-sets and (<I>k</I>+1)-sets.
This is exactly a solution to the middle two levels problem. 

<P>
It might be hoped that a general construction of symmetric Venn diagrams
  would give solutions to the middle two levels problem for <I>n</I>
  prime.
However, a general construction of symmetric Venn diagrams seems very
  difficult, given that we can't find a single instance for
  <I>n</I>=11, so perhaps it's better to go the other way around.
Maybe known solutions to the middle two levels problem for small values
  can give us insight into the construction of symmetric Venn
  diagrams!

<H3>Symmetric Gray Codes?</H3>

Venn diagrams can lead to a highly symmetric Gray codes, if we exclude
  the bitstring 0<sup><I>n</I></sup> and 1<sup><I>n</I></sup>.
The idea is to find a Hamilton cycle that also exhibits a <I>n</I>-fold
  rotational symmetry, like that shown in the figure below.

<P><IMG SRC="gifs/ellipse2ham.gif">

<P>
Starting at the set {1}, and writing the bitstring representation of each
  subset we are lead to the table shown below (read down).  
Note that: (a) each of the 5 blocks is a right shift of the preceding block,
  (b) the columns are cyclicly equivalent (under a right shift of 6 bits), and
  (c) each block contains a representative of each non-trivial necklace of black 
  and white beads.
Such Gray codes are obviously balanced in the sense of 
  Bhat and Savage [<A HREF=VennRefs.html#BS>BS</A>]; that is, each column has
  the same number of bit changes.
It is easy to find symmetric Gray codes for <I>n</I>=7 using any of the Venn
  diagrams discussed earlier on this page.

<P>
<TABLE border=1 ALIGN=CENTER WIDTH="80%">
<TR>
<TD>10000
<TD>01000
<TD>00100
<TD>00010
<TD>00001
<TR>
<TD>10100
<TD>01010
<TD>00101
<TD>10010
<TD>01001
<TR>
<TD>11100
<TD>01110
<TD>00111
<TD>10011
<TD>11001
<TR>
<TD>11110
<TD>01111
<TD>10111
<TD>11011
<TD>11101
<TR>
<TD>11010
<TD>01101
<TD>10110
<TD>01011
<TD>10101
<TR>
<TD>11000
<TD>01100
<TD>00110
<TD>00011
<TD>10001
<TR>
<TH>Block 1
<TH>Block 2
<TH>Block 3
<TH>Block 4
<TH>Block 5
</TABLE>


<P>
<HR NOSHADE>

<TABLE width=100%>
<TR><TD align=left>
<A HREF=VennEJC.html><IMG border=0 SRC=gifs/smallV3plain.gif></A>
<TD align=right><FONT SIZE=-2>
(to appear in) 
<A HREF=http://ejc.math.gatech.edu:8080/Journal/journalhome.html>
<FONT SIZE=-1>T</FONT>HE <FONT SIZE=-1>E</FONT>LECTRONIC
<FONT SIZE=-1>J</FONT>OURNAL OF <FONT SIZE=-1>C</FONT>OMBINATORICS</A>
3 (1996), DS #5.
</FONT>
</TR></TABLE>
 
</BODY>
</HTML>


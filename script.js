// set up navigation menu on the left
// each category is a category name plus a list of objects
// each object is a triple with identifier, menu entry and page title
var items = [["index", "Home", "The Combinatorial Object Server"],
             ["Permutations", [["perm", "Permutations", "Permutations"],
                               ["multiperm", "Multiset permutations", "Multiset permutations"],
                               ["cperm", "Colored permutations", "Colored permutations"],
                               ["meander", "Meanders", "Meanders and stamp foldings"],
                               ["jump", "Pattern avoidance", "Pattern-avoiding permutations"],
                               ["btree", "Binary trees", "Binary trees"],
                               ["rect", "Rectangulations", "Rectangulations"],
                               ["elim", "Elimination trees", "Elimination trees"],
                               ["orient", "Acyclic orientations", "Acyclic orientations"]]],
             ["Bitstrings", [["bits", "Bitstrings", "Bitstrings"],
                             ["brgc", "BRGC", "Sublists of the BRGC"],
                             ["comb", "Combinations", "Combinations"],
                             ["schrijver", "Schrijver graphs", "Schrijver and s-stable Kneser graphs"],
                             ["middle", "Middle levels", "Middle levels"],
                             ["chains", "Symmetric chains", "Symmetric chains"],
                             ["dyck", "Lattice paths", "Lattice paths"]]],
             ["Graphs", [["nauty", "Graphs", "Graphs (nauty)"],
                         ["plantri", "Planar graphs", "Planar graphs (plantri)"],
                         ["fullgen", "Fullerenes", "Fullerenes (fullgen)"],
                         ["kary", "K-ary trees", "K-ary trees"],
                         ["tree", "Plane trees", "Rooted and free plane trees"],
                         ["span", "Spanning trees", "Spanning trees"]]],
             ["part", "Partitions", "Integer and set partitions"],
             ["necklace", "Necklaces", "Necklaces and Lyndon words"],
             ["bruijn", "De Bruijn sequences", "De Bruijn sequences"],
             ["sgroup", "Semigroups", "Numerical semigroups"]];

// compute menu item string for given menu entry
function menu_item(it, active) {
  var active_string = "";
  if (active) {
    document.title = it[2];
    active_string = "class='active' ";
  }
  return "<li class='menu_item'><a " + active_string + "href='" + it[0] + "'>" + it[1] + "</a></li>";
}

// set up menu
var my_obj = document.getElementById("nav0").getAttribute("class");
var nav_string = "<ul id='menu'>";
var cat = 0;
var active_cat = -1;
for (var i = 0; i < items.length; i++) {
  var it = items[i];
  if (it.length == 2) {  // category entry
  // each category gets a running id and also the <span> inside, which will later hold the up/down triangle for unfolding
    nav_string += "<li class='menu_item dropdown_category' id='cat" + cat + "'><a href='javascript:;'>" + it[0] + "<span id='fold" + cat + "'></span></a></li>";
    nav_string += "<li class='dropdown_container'><ul class='submenu'>";  // start submenu
    for (var j = 0; j < it[1].length; j++) {
      var active = (my_obj == it[1][j][0]);
      if (active) {
        active_cat = cat;  // remember active category (need to unfold)
      }
      nav_string += menu_item(it[1][j], active);
    }
    nav_string += "</ul>";
    cat++;
  } else {  // object entry
    var active = (my_obj == it[0]);
    nav_string += menu_item(it, active);
  }
}
nav_string += "</ul>";
document.getElementById("nav0").innerHTML = nav_string;

// store menu state persistently
if (sessionStorage.getItem("menu") == null) {
  sessionStorage.setItem("menu", 0);
}

var triangle_up = "&#9652";
var triangle_down = "&#9662";

// define behavior of foldable menu blocks
var dropdown = document.getElementsByClassName("dropdown_category");
for (var i = 0; i < dropdown.length; i++) {
  // set initial menu state
  if (parseInt(sessionStorage.getItem("menu")) & (1 << i)) {
    // unfold previously opened menus
    document.getElementById("fold" + i).innerHTML = triangle_up + "&nbsp;";
    dropdown[i].nextElementSibling.style.display = "block";
  } else {
    document.getElementById("fold" + i).innerHTML = triangle_down + "&nbsp;";
  }
  if (i == active_cat) {
    // unfold currently active menu (e.g., upon redirects from other sites)
    document.getElementById("fold" + i).innerHTML = triangle_up + "&nbsp;";
    dropdown[i].nextElementSibling.style.display = "block";
    sessionStorage.setItem("menu", parseInt(sessionStorage.getItem("menu")) | (1 << i));
  }
  // set event listener
  dropdown[i].addEventListener("click", function() {
    var id = parseInt(this.id.substring(3));  // ignore first three letters 'cat' of identifier
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "block") {
      document.getElementById("fold" + id).innerHTML = triangle_down + "&nbsp;";
      dropdownContent.style.display = "none";
    } else {
      document.getElementById("fold" + id).innerHTML = triangle_up + "&nbsp;";
      dropdownContent.style.display = "block";
    }
    // remember menu change
    sessionStorage.setItem("menu", parseInt(sessionStorage.getItem("menu")) ^ (1 << id));
  });
}

// make section headings collapsible
var heads = document.getElementsByClassName("collapse_heading");
for (var i = 0; i < heads.length; i++) {
  var folded = (heads[i].dataset.folded == "true");
  var sec = heads[i].nextElementSibling;
  if (folded) {
    heads[i].innerHTML = triangle_down + " " + heads[i].innerHTML;
    sec.style.maxHeight = null;
  } else {
    heads[i].innerHTML = triangle_up + " " + heads[i].innerHTML;
    sec.style.maxHeight = sec.scrollHeight + "px";
  }
  heads[i].addEventListener("click", function() {
    var sec = this.nextElementSibling;
    if (sec.style.maxHeight){
      this.innerHTML = triangle_down + " " + this.innerHTML.slice(2);
      sec.style.maxHeight = null;
    } else {
      this.innerHTML = triangle_up + " " + this.innerHTML.slice(2);
      sec.style.maxHeight = sec.scrollHeight + "px";
    }
  });
}

// enlarge output generation window on first generation
var enlarge_output = (function() {
  var executed = false;
  return function() {
    if (!executed) {
      var selfmt = document.getElementById("fmt");
      if (selfmt == null) {
        return;  // output format unknown, output window height unchanged
      }
      fmt = selfmt.options[selfmt.selectedIndex].value;
      if (fmt == 2) {
        return;  // file output, output window height unchanged
      }
      // graphical or textual output, so output window height must be enlarged
      var divout = document.getElementById("output");
      divout.style.height = "500px";
      executed = true;
    }
  };
})();

// update output formatting buttons
function output_buttons()
{
  var selfmt = document.getElementById("fmt");
  if (selfmt == null) {
    return;
  }
  fmt = selfmt.options[selfmt.selectedIndex].value;
  var opt = document.getElementById("options");
  if (fmt == 0) {  // graphical output, show options
    opt.style.display = "";
  } else if (fmt == 1) {  // textual output, hide options
    opt.style.display = "none";
  }  // else file output, no change
}

// switch numbers of computed objects on or off
function numbering()
{
  var checked = document.getElementById("numbering").checked;
  var iframe = document.getElementById("consoleframe");
  var numbers = iframe.contentWindow.document.getElementsByClassName("num");
  for (var i = 0; i < numbers.length; i++) {
    if (checked) {
      numbers[i].style.display = "";
    } else {
      numbers[i].style.display = "none";
    }
  }
}

// switch graphical output represenation of objects on or off
function graphics()
{
  var checked = document.getElementById("graphics").checked;
  var iframe = document.getElementById("consoleframe");
  var strings = iframe.contentWindow.document.getElementsByClassName("txt");
  var graphics = iframe.contentWindow.document.getElementsByClassName("gfx");
  for (var i = 0; i < strings.length; i++) {
    if (checked) {
      strings[i].style.display = "none";
      graphics[i].style.display = "";
    } else {
      strings[i].style.display = "";
      graphics[i].style.display = "none";
    }
  }
}

// display total number of objects generated, and indicate if output limit was reached
function set_total() {
   // read out total from output console
   var iframe = document.getElementById("consoleframe");
   var total_frame = iframe.contentWindow.document.getElementById("total");
   var total = document.getElementById("total");

   if (total_frame != null) {
     // set total number of objects, suffixed by '+' if limit reached
     var limit = (total_frame.dataset.limit == "true");
     total.textContent = "Total: " + total_frame.dataset.value + (limit ? "+" : "");
   } else {
     total.textContent = "";
   }
}

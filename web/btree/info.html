We consider binary trees with vertex set $[n]:=\{1,\ldots,n\}$ whose vertex labels are given by the <span class="def">search tree property</span>, i.e., for any vertex $i$, all vertices in the left subtree are smaller than $i$, and all vertices in the right subtree are larger than $i$.
This allows encoding a binary tree $T$ with $n$ vertices as a permutation $\tau(T)$ of length $n$, by recording the vertices of $T$ in a <span class="def">preorder traversal</span>, i.e., we first record the root of $T$, followed by recursively recording vertices in the left subtree of the root, followed by recursively recording vertices in the right subtree of the root.
For the tree $T$ shown in the figure, the corresponding permutation is $\tau(T)=(6,5,2,1,3,4,8,7,9,10)$.
It is well known that this mapping $\tau$ is a bijection between binary trees with vertex set $[n]$ and permutations of length $n$ that avoid the pattern $231$ (see <a href="jump">pattern-avoiding permutations</a>).
<p>

<svg width="200" height="170">
<style> .l { stroke:black; stroke-width:2; } .c {stroke:black; fill:white; } </style>
<!--<rect x="0" y="0" width="180" height="180" fill="red"/>-->
<line class="l" x1="90" y1="15" x2="65" y2="50"/>
<line class="l" x1="65" y1="50" x2="40" y2="85"/>
<line class="l" x1="40" y1="85" x2="15" y2="120"/>
<line class="l" x1="90" y1="15" x2="115" y2="50"/>
<line class="l" x1="115" y1="50" x2="140" y2="85"/>
<line class="l" x1="140" y1="85" x2="165" y2="120"/>
<line class="l" x1="40" y1="85" x2="65" y2="120"/>
<line class="l" x1="65" y1="120" x2="90" y2="155"/>
<line class="l" x1="115" y1="50" x2="90" y2="85"/>
<circle class="c" cx="90" cy="15" r="12"/>
<circle class="c" cx="65" cy="50" r="12"/>
<circle class="c" cx="115" cy="50" r="12"/>
<circle class="c" cx="40" cy="85" r="12"/>
<circle class="c" cx="90" cy="85" r="12"/>
<circle class="c" cx="140" cy="85" r="12"/>
<circle class="c" cx="15" cy="120" r="12"/>
<circle class="c" cx="65" cy="120" r="12"/>
<circle class="c" cx="165" cy="120" r="12"/>
<circle class="c" cx="90" cy="155" r="12"/>
<text x="90" y="16" alignment-baseline="middle" text-anchor="middle">6</text>
<text x="65" y="51" alignment-baseline="middle" text-anchor="middle">5</text>
<text x="115" y="51" alignment-baseline="middle" text-anchor="middle">8</text>
<text x="40" y="86" alignment-baseline="middle" text-anchor="middle">2</text>
<text x="90" y="86" alignment-baseline="middle" text-anchor="middle">7</text>
<text x="140" y="86" alignment-baseline="middle" text-anchor="middle">9</text>
<text x="15" y="121" alignment-baseline="middle" text-anchor="middle">1</text>
<text x="65" y="121" alignment-baseline="middle" text-anchor="middle">3</text>
<text x="165" y="121" alignment-baseline="middle" text-anchor="middle">10</text>
<text x="90" y="156" alignment-baseline="middle" text-anchor="middle">4</text>
<text x="15" y="25">T</text>
</svg>
<p>

There are two natural notions of pattern containment in binary trees&nbsp;[Row10, DPTW12].
A <span class="def">tree pattern</span> is a pair $(P,e)$, where $P$ is a binary tree with vertex set $[k]$, and $e$ is a mapping that assigns to every non-root vertex $i$ of $P$ a number $e(i)\in \{0,1\}$, and the value $e(i)$ determines how the relationship between $i$ and its parent vertex $p(i)$ in $P$ translates into an occurrence of the pattern $P$ in a host tree $T$ with vertex set $[n]$.
Formally, we say that $T$ <span class="def">contains</span> $P$, if there is an injection $f:[k]\rightarrow [n]$ of the vertices of $P$ into the vertices of $T$ satisfying the following conditions:
<ul class="enum">
<li>
For every edge $(i,p(i))$ of $P$ with $e(i)=1$ (drawn solid in the figure below), we have that $f(i)$ is a child of $f(p(i))$ in $T$.
Specifically, if $i$ is the left child of $p(i)$, then $f(i)$ is the left child of $f(p(i))$, whereas if $i$ is the right child of $p(i)$ then $f(i)$ is the right child of $f(p(i))$.
<li>
For every edge $(i,p(i))$ of $P$ with $e(i)=0$ (drawn dashed in the figure below), we have that $f(i)$ is a descendant of $f(p(i))$ in $T$.
Specifically, if $i$ is the left child of $p(i)$, then $f(i)$ is a left descendant of $f(p(i))$, whereas if $i$ is the right child of $p(i)$ then $f(i)$ is a right descendant of $f(p(i))$.
</ul>
If $T$ does not contain $(P,e)$, then we say that $T$ <span class="def">avoids</span> $P$.
<p>

<svg width="220" height="170">
<style> .ls { stroke:black; stroke-width:3; } .ld { stroke:black; stroke-dasharray:3 2; stroke-width:3; } .c {stroke:black; fill:white; } </style>
<!--<rect x="0" y="0" width="220" height="170" fill="red"/>-->
<line class="ld" x1="110" y1="15" x2="75" y2="60"/>
<line class="ld" x1="75" y1="60" x2="40" y2="105"/>
<line class="ls" x1="110" y1="15" x2="145" y2="60"/>
<line class="ls" x1="75" y1="60" x2="110" y2="105"/>
<circle class="c" cx="110" cy="15" r="12"/>
<circle class="c" cx="75" cy="60" r="12"/>
<circle class="c" cx="145" cy="60" r="12"/>
<circle class="c" cx="40" cy="105" r="12"/>
<circle class="c" cx="110" cy="105" r="12"/>
<text x="110" y="16" alignment-baseline="middle" text-anchor="middle">4</text>
<text x="75" y="61" alignment-baseline="middle" text-anchor="middle">2</text>
<text x="145" y="61" alignment-baseline="middle" text-anchor="middle">5</text>
<text x="40" y="106" alignment-baseline="middle" text-anchor="middle">1</text>
<text x="110" y="106" alignment-baseline="middle" text-anchor="middle">3</text>
<text x="38" y="38">e(2)=0</text>
<text x="135" y="38">e(5)=1</text>
<text x="5" y="83">e(1)=0</text>
<text x="102" y="86">e(3)=1</text>
<text x="5" y="15">(P,e)</text>
</svg>

<svg width="250" height="170">
<style> .l { stroke:black; stroke-width:2; } .c {stroke:black; fill:white; } .f {stroke:black; fill:red; }  </style>
<!--<rect x="0" y="0" width="250" height="180" fill="red"/>-->
<line class="l" x1="90" y1="15" x2="65" y2="50"/>
<line class="l" x1="65" y1="50" x2="40" y2="85"/>
<line class="l" x1="40" y1="85" x2="15" y2="120"/>
<line class="l" x1="90" y1="15" x2="115" y2="50"/>
<line class="l" x1="115" y1="50" x2="140" y2="85"/>
<line class="l" x1="140" y1="85" x2="165" y2="120"/>
<line class="l" x1="40" y1="85" x2="65" y2="120"/>
<line class="l" x1="65" y1="120" x2="90" y2="155"/>
<line class="l" x1="115" y1="50" x2="90" y2="85"/>
<circle class="f" cx="90" cy="15" r="12"/>
<circle class="c" cx="65" cy="50" r="12"/>
<circle class="f" cx="115" cy="50" r="12"/>
<circle class="f" cx="40" cy="85" r="12"/>
<circle class="c" cx="90" cy="85" r="12"/>
<circle class="c" cx="140" cy="85" r="12"/>
<circle class="f" cx="15" cy="120" r="12"/>
<circle class="f" cx="65" cy="120" r="12"/>
<circle class="c" cx="165" cy="120" r="12"/>
<circle class="c" cx="90" cy="155" r="12"/>
<text x="90" y="16" alignment-baseline="middle" text-anchor="middle">6</text>
<text x="65" y="51" alignment-baseline="middle" text-anchor="middle">5</text>
<text x="115" y="51" alignment-baseline="middle" text-anchor="middle">8</text>
<text x="40" y="86" alignment-baseline="middle" text-anchor="middle">2</text>
<text x="90" y="86" alignment-baseline="middle" text-anchor="middle">7</text>
<text x="140" y="86" alignment-baseline="middle" text-anchor="middle">9</text>
<text x="15" y="121" alignment-baseline="middle" text-anchor="middle">1</text>
<text x="65" y="121" alignment-baseline="middle" text-anchor="middle">3</text>
<text x="165" y="121" alignment-baseline="middle" text-anchor="middle">10</text>
<text x="90" y="156" alignment-baseline="middle" text-anchor="middle">4</text>
<text x="125" y="15">T contains (P,e)</text>
</svg>
<p>

The figure shows an occurrence of the pattern $(P,e)$ in the tree $T$ from before.
On the other hand, $T$ avoids the pattern $(P,e')$ that is obtained from modifying $(P,e)$ by changing $e(2)=0$ to $e'(2):=1$ (i.e., the edge $(2,4)$ in $P$ is solid instead of dashed).
<p>

The algorithm running on this website generates all binary trees with $n$ vertices avoiding certain tree patterns (possibly none).
Each pattern $(P,e)$ is specified by the preorder permutation $\tau(P)$ of length $n$, and the 0/1-string of length $n-1$ of all $e$-values of the non-root vertices of $P$, in the order of the preorder permutation $\tau(P)$, separated by comma.
For example, the pattern $(P,e)$ from before is encoded as $\tau(P),e(2)e(1)e(3)e(5)=42135,0011$.
Multiple patterns are separated by semicolon.
In graphical output mode, each generated binary tree $T$ is printed as a bracketing expression $\beta(T)$, defined recursively as $\beta(T):=$<span style='color:red;'>[</span>$\beta(L(T))$,$\beta(R(T))$<span style='color:blue;'>]</span>, where $L(T)$ and $R(T)$ are the left and right subtree of the root of $T$.
In textual output mode, $T$ is printed as the preorder permutation $\tau(T)$.
The tree $T$ in the figure above is printed as $\beta(T)=$<style>.r{color:red;} .b{color:blue;}</style><span class='r'>[</span><span class='r'>[</span><span class='r'>[</span><span class='r'>[</span> , <span class='b'>]</span>,<span class='r'>[</span> ,<span class='r'>[</span> , <span class='b'>]</span><span class='b'>]</span><span class='b'>]</span>, <span class='b'>]</span>,<span class='r'>[</span><span class='r'>[</span> , <span class='b'>]</span>,<span class='r'>[</span> ,<span class='r'>[</span> , <span class='b'>]</span><span class='b'>]</span><span class='b'>]</span><span class='b'>]</span> and $\tau(T)=6\,5\,2\,1\,3\,4\,8\,7\,9\,10$.
<p>

An algorithm for generating all binary trees by tree rotations was proposed by Lucas, Roelants van Baronaigien, and Ruskey&nbsp;[LRvBR93].
Williams&nbsp;[Wil13] discovered an amazingly simple description of this method in terms of a greedy algorithm: Start with the all-right tree, and then repeatedly perform a tree rotation involving the largest possible vertex that creates a previously unvisited tree.
The paper&nbsp;[GMN23] proposes a generalization of Williams' algorithm, which allows specifying certain forbidden tree patterns that satisfy some mild constraints.
A pattern satisfying these constraints is called <span class="def">friendly</span> (see the paper for definitions).
If all specified tree patterns are friendly, then our generator uses the algorithm from [GMN23], and otherwise the trees are generated by a brute-force method (with a stricter limit on the number of vertices $n$, which can be disabled in the downloaded source code).
The binary reflected Gray code is a classical ordering of all bitstrings of length&nbsp;$n$ where successive strings differ by a single bit flip&nbsp;[Gra53].
The listings have the recursive definition $\Gamma_n = \Gamma_{n-1}\cdot 0, ~\Gamma_{n-1}^R\cdot 1,$ where $\Gamma^R$ denotes the listing $\Gamma$ in reverse order and $\Gamma_1 = 0,1$.

<p>
Natural representations of many combinatorial objects appear as 2-Gray codes when listed in BRGC order.
A large class of such languages are known as flip-swap languages.
A <span class="def">flip-swap language</span> (with respect to 1) is a subset $S$ of the length&nbsp;$n$ bitstrings such that $S \cup \{0^n\}$ is closed under two operations (where applicable):
(1) flipping the leftmost 1, and (2) swapping the leftmost 1 with the bit to its right.
Examples include:
<ul class="enum">
<li>bitstrings</li>
<li>necklaces</li>
<li>Lyndon words</li>
<li>prenecklaces</li>
<li>pseudo-necklaces</li>
<li>bitstrings &lt; their reversal</li>
<li>bitstrings &le; their reversal (neckties)</li>
<li>bitstrings with weight &le; $k$</li>
<li>bitstrings with forbidden $10^k$</li>
<li>bitstrings with forbidden prefix $\beta=1\beta'$</li>
<li>bitstrings &lt; their complemented reversal</li>
<li>bitstrings &le; their complemented reversal</li>
<li>bitstrings &le; &beta;</li>
<li>bitstrings with &le; $k$ inversions with respect to $0^*1^*$</li>
<li>bitstrings with &le; $k$ transpositions with respect to $0^*1^*$</li>
<li>0-prefix normal words</li>
<li>left factors of $k$-ary Dyck words</li>
<li>feasible solutions to 0-1 knapsack problems</li>
</ul>
where <a href="necklace">necklaces and their relatives</a> are defined with respect to a lexicographically smallest representative.
Each of the above languages includes the string $0^n$ except for Lyndon words and bitstrings that are strictly less than their reversal.

<p>
Flip-swap languages (with respect to 0) can be defined similarly by interchanging the roles of&nbsp;0 and&nbsp;1.
They appear as 2-Gray codes in BRGC ordering when the roles of&nbsp;0 and&nbsp;1 are also interchanged in the definition of $\Gamma$.
Examples include:
<ul class="enum">
<li>bitstrings</li>
<li>necklaces</li>
<li>aperiodic necklaces</li>
<li>prenecklaces</li>
<li>pseudo-necklaces</li>
<li>bitstrings &gt; their reversal</li>
<li>bitstrings &ge;  their reversal</li>
<li>bitstrings with weight &ge; $k$</li>
<li>bitstrings with forbidden $01^k$</li>
<li>bitstrings with forbidden prefix $\beta=0\beta'$</li>
<li>bitstrings &gt; their complemented reversal</li>
<li>bitstrings &ge; their complemented reversal</li>
<li>bitstrings &ge; &beta;</li>
<li>bitstrings with &le; $k$ inversions with respect to $1^*0^*$</li>
<li>bitstrings with &le; $k$ transpositions with respect to $1^*0^*$</li>
<li>1-prefix normal words</li>
</ul>
where <a href="necklace">necklaces and their relatives</a> are defined with respect to a lexicographically largest representative.
Each of the above languages includes the string $1^n$ except for aperiodic necklaces and bitstrings that are strictly greater than their reversal.

<p>
Each of these languages can be generated in $O(n)$ time per generated bitstring by applying a successor rule approach, with the exception of prefix normal words.
By considering a recursive approach, most listings can be implemented in $O(1)$-amortized time including necklaces and Lyndon words&nbsp;[SWW17].
For more details on flip-swap languages, see&nbsp;[SWW22].
For information specifically related to necklaces and Lyndon words see&nbsp;[SWW17, Vaj08].
Generating bitstrings whose weight range is bounded from below and above is possible with our <a href="bits">trimmed BRGC algorithms</a>.

<!--
<p>
A number of other interesting objects appear as 3-Gray codes when listed in BRGC order including:
<ul>
<li>integer partitions</li>
<li>unlabeled necklaces</li>
<li>conecklaces</li>
<li>Fibonacci words</li>
<li>Lucas words</li>
<li>bitstrings with forbidden $\beta=1\beta'$</li>
</ul>
-->
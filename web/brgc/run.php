<?php
  if (isset($_GET["t"]) && isset($_GET["n"]) && isset($_GET["fmt"]) && isset($_GET["num"]) && isset($_GET["gfx"])) {
    $type = intval($_GET["t"]);
    $n = intval($_GET["n"]);
    $k = intval($_GET["k"]);
    $beta = trim($_GET["b"]);
    $weights = trim($_GET["w"]);
    $fmt = intval($_GET["fmt"]);

    $cmd = "code/brgc/brgc" . " " . $type
                            . " " . min($n, 20)  /* hard-wired maximum bitstring length */
                            . " " . min($k, 20)  /* hard-wired maximum bitstring length */
                            . " " . strval(limit($fmt));  /* hard-wired maximum output */

    // beta
    if (($type == 10 || $type == 50 || $type == 13 || $type == 53)) $cmd = $cmd . " " . $beta;

    // capacity and weights
    $valid = preg_match("/^[0-9\s]*$/", $weights);  // string of numerical characters and whitespace
    if ($type == 18) {
      $cmd = $cmd . " " . intval($_GET["c"]);
      $w = array_map('intval', preg_split("/\s+/", $weights));  // split at whitespace
      $cmd = $cmd . " " . $weights;
      // check if weights are sorted decreasingly
      for ($i=1; $i < sizeof($w); $i++) {
        if ($w[$i] > $w[$i-1]) {
          $valid = false;
        }
      }
    }

    $cmd = $cmd .  " 2>&1"  /* this command redirects stderr to stdout */
                . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */

    // input validation
    if ($type == 0) echo "select an object type";
    else if ($n <= 0 || $n > 20) echo "length n must be between 1 and 20";
    // range of k
    else if (($type == 8 || $type == 9 || $type == 14 || $type == 15 ||  $type == 17 ||$type == 48 || $type == 49 || $type == 54 || $type == 55 || $type == 57 )  && ($k < 0 || $k > $n)) echo "k must be between 0 and n";
    else if (($type == 17  || $type == 57) && $k == 0) echo "k must be greater than 0";
    // beta
    else if (($type == 10 || $type == 50 || $type == 13 || $type == 53) && (!preg_match("/^[01]*$/", $beta))) echo "beta must be a binary string";
    else if (($type == 13 || $type == 53) && strlen($beta) != $n) echo "beta must have length n";
    else if (($type == 10 || $type == 50) && strlen($beta) > $n) echo "beta must have length at most n";
    else if (($type == 10 && $beta[0] == 0)) echo "beta must begin with 1";
    else if (($type == 50 && $beta[0] == 1)) echo "beta must begin with 0";
    // weights
    else if ($type == 18 && !$valid) echo "the weights must be integers in decreasing order";
    else if ($type == 18 && sizeof($w) != $n) echo "there must be n weights in decreasing order";
    else {
      exec($cmd, $result);
      $num = filter_var($_GET["num"], FILTER_VALIDATE_BOOLEAN);
      $gfx = filter_var($_GET["gfx"], FILTER_VALIDATE_BOOLEAN);
      output($result, $fmt, "string_to_bits", 0, "svg_bits", 0, "svg_wheel", "bits_col", $num, $gfx);
    }
  }
?>
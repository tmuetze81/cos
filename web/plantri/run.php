<?php
  if (isset($_GET["n"]) && isset($_GET["t"]) && isset($_GET["o"]) && isset($_GET["d"]) && isset($_GET["graph"]) && isset($_GET["fmt"])) {
    $n = min(intval($_GET["n"]), 20);  // hard-wired maximum number of vertices
    $cmopts = array(array("-c3m3", "-m5", "-c5", "-m5c4", "-m5c3x", "-m5c4x", "-m4", "-c4", "-m4c3x", "-c2", "-c2x", "-c1", "-c1x", "-c1t", "-c1tx"),
                    array("-c3", "-c4", "-c3x"),
                    array("-c3m3", "-c2", "-c2x", "-c2m2", "-c2xm2"),
                    array("-c3m3", "-c2m2", "-c2", "-c4"));
    $topts = array("-b", "-p", "-bp", "-P", "-q", "-A");
    $copts = array("-c1", "-c1x", "-c2", "-c2x", "-c3", "-c4");
    $t = $_GET["t"];
    $cm = 0;
    if (isset($_GET["cm"])) {
      $cm = intval($_GET["cm"]);
    }
    $cmopt = "";
    $topt = "";
    switch ($t) {
      case 0:                                                       $cmopt = " " . $cmopts[0][$cm]; break;  
      case 1: $topt = " " . $topts[0];                              $cmopt = " " . $cmopts[1][$cm]; break;
      case 2: $topt = " " . $topts[1];                                                              break;
      case 3: $topt = " " . $topts[2];                                                              break;
      case 4: $topt = " " . $topts[3] . strval(intval($_GET["P"])); $cmopt = " " . $cmopts[2][$cm]; break;
      case 5: $topt = " " . $topts[4];                              $cmopt = " " . $cmopts[3][$cm]; break;
      case 6: $topt = " " . $topts[5];                                                              break;        
    }
    $copt = "";
    if (isset($_GET["c"])) {
      $copt = " " . $copts[intval($_GET["c"])];
    }
    $m = "";
    if (isset($_GET["m"])) {
      $m = " -m" . strval(intval($_GET["m"]));    
    }
    $erange = "";
    if (isset($_GET["mine"]) || isset($_GET["maxe"])) {
      $from = "";
      if (isset($_GET["mine"])) {
        $from = strval(intval($_GET["mine"]));
      }
      $to = "";
      if (isset($_GET["maxe"])) {
        $to = strval(intval($_GET["maxe"]));
      }
      $erange = " -e" . $from . ":" . $to;
    }
    $f = "";
    if (isset($_GET["f"])) {
      $f = " -f" . strval(intval($_GET["f"]));
    }
    $o = filter_var($_GET["o"], FILTER_VALIDATE_BOOLEAN);
    $d = filter_var($_GET["d"], FILTER_VALIDATE_BOOLEAN);
    $graph = intval($_GET["graph"]);
    $G = "";
    switch ($graph) {
      case 1: $G = " -g"; break;
      case 2: $G = " -s"; break;
      case 3: $G = " -a -F3"; break;
      case 4: $G = " -a -F4"; break;
      case 5: $G = " -a -F5"; break;
    }
    $fmt = intval($_GET["fmt"]);
    // run plantri
    $cmd = "code/plantri/plantri"
           . $topt . $cmopt . $copt . $m . $erange . $f
           . ($o ? " -o" : "")
           . ($d ? " -d" : "")
           . $G
           . " -L" . strval(limit($fmt))  /* hard-wired maximum number of graphs */
           . " " . strval($n)
           . " 2>&1"  /* this command redirects stderr to stdout */
           . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */
    exec($cmd, $result);
    output($result, $fmt, 0, 0, 0, 0, 0, 0, false, false);
  }
?>
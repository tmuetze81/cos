<?php
  if (isset($_GET["n"]) && isset($_GET["a"]) && isset($_GET["fmt"]) && isset($_GET["num"]) && isset($_GET["gfx"])) {
    $fmt = intval($_GET["fmt"]);
    $algo = intval($_GET["a"]);
    // the meaning of the parameter -v is slightly different in the program knuth (a==1)
    if (isset($_GET["v"])) {
      $start = escapeshellarg($_GET["v"]);
      // prepend extra first bit so that number of 0s matches number of 1s (n+1 occurences of each)
      if ($algo == 1) {
        $num0s = substr_count($start, '0');
        $num1s = substr_count($start, '1');
        if ($num0s < $num1s) {
          $start = '0' . $start;
        } else {
          $start = '1' . $start;
        }
      }
    } 
    $cmd = ($algo == 0 ? "code/middle/middle" : "code/knuth/knuth -i")  /* suppress first bit in the output of Knuth code */
                             . " -n" . min(intval($_GET["n"]), 15)  /* hard-wired maximum bitstring length */
                             . " -l" . strval(limit($fmt))  /* hard-wired maximum number of bitstrings */
                             . (isset($_GET["s"]) ? " -s" . escapeshellarg($_GET["s"]) : "")
                             . (isset($_GET["v"]) ? " -v" . $start : "")
                             . " 2>&1"  /* this command redirects stderr to stdout */
                             . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */
    exec($cmd, $result);
    $num = filter_var($_GET["num"], FILTER_VALIDATE_BOOLEAN);
    $gfx = filter_var($_GET["gfx"], FILTER_VALIDATE_BOOLEAN);
    output($result, $fmt, "string_to_bits", 0, "svg_bits", 0, "svg_wheel", "bits_col", $num, $gfx);
  }
?>


An <span class="def">elimination tree</span> of a connected graph&nbsp;$G$ is a rooted tree whose root is a vertex&nbsp;$v$ of&nbsp;$G$, and the subtrees of&nbsp;$v$ are obtained by recursing on the connected components of the graph obtained by removing&nbsp;$v$ from&nbsp;$G$.
The children of a vertex in an elimination tree are unordered.
An <span class="def">elimination forest</span> of a graph&nbsp;$G$ (not necessarily connected) is the disjoint union of elimination trees, one for each connected component of&nbsp;$G$.
<p>

The figure below shows a graph&nbsp;$G$ on vertices $a,b,c,d,e,f$ (left) and one of its elimination trees $T$ (right), obtained by removing the vertices in the order $d,a,c,b,e,f$.
This graph can be used as input by selecting "example G" above.
There may be several elimination orders corresponding to the same elimination tree.
For example, removing the vertices in the order $d,f,a,c,e,b$ yields the same elimination tree.
Formally, the set of elimination orders corresponding to an elimination tree is the set of linear extensions of the poset whose Hasse diagram is the tree.
<p>
  
<svg width="200" height="120">
<style> .l { stroke:black; stroke-width:2; } .c {stroke:black; fill:white; } </style>
<!--<rect x="0" y="0" width="200" height="120" fill="red"/>-->
<line class="l" x1="70" y1="15" x2="15" y2="60"/>
<line class="l" x1="70" y1="15" x2="70" y2="105"/>
<line class="l" x1="70" y1="15" x2="125" y2="15"/>
<line class="l" x1="125" y1="15" x2="180" y2="15"/>
<line class="l" x1="70" y1="105" x2="15" y2="60"/>
<line class="l" x1="70" y1="105" x2="125" y2="105"/>
<line class="l" x1="70" y1="105" x2="125" y2="105"/>
<circle class="c" cx="15" cy="60" r="12"/>
<circle class="c" cx="70" cy="15" r="12"/>
<circle class="c" cx="70" cy="105" r="12"/>
<circle class="c" cx="125" cy="15" r="12"/>
<circle class="c" cx="125" cy="105" r="12"/>
<circle class="c" cx="180" cy="15" r="12"/>
<text x="70" y="16" alignment-baseline="middle" text-anchor="middle">a</text>
<text x="15" y="61" alignment-baseline="middle" text-anchor="middle">b</text>
<text x="70" y="106" alignment-baseline="middle" text-anchor="middle">c</text>
<text x="125" y="16" alignment-baseline="middle" text-anchor="middle">d</text>
<text x="125" y="106" alignment-baseline="middle" text-anchor="middle">e</text>
<text x="180" y="16" alignment-baseline="middle" text-anchor="middle">f</text>
<text x="15" y="15" alignment-baseline="middle" text-anchor="middle">G</text>
</svg>

<svg width="110" height="165">
<style> .l { stroke:black; stroke-width:2; } .c {stroke:black; fill:white; } </style>
<!--<rect x="0" y="0" width="110" height="165" fill="red"/>-->
<line class="l" x1="55" y1="15" x2="35" y2="60"/>
<line class="l" x1="55" y1="15" x2="75" y2="60"/>
<line class="l" x1="75" y1="60" x2="75" y2="105"/>
<line class="l" x1="75" y1="105" x2="55" y2="150"/>
<line class="l" x1="75" y1="105" x2="95" y2="150"/>
<circle class="c" cx="55" cy="15" r="12"/>
<circle class="c" cx="35" cy="60" r="12"/>
<circle class="c" cx="75" cy="105" r="12"/>
<circle class="c" cx="55" cy="150" r="12"/>
<circle class="c" cx="95" cy="150" r="12"/>
<circle class="c" cx="75" cy="60" r="12"/>
<text x="55" y="16" alignment-baseline="middle" text-anchor="middle">d</text>
<text x="75" y="61" alignment-baseline="middle" text-anchor="middle">a</text>
<text x="75" y="106" alignment-baseline="middle" text-anchor="middle">c</text>
<text x="55" y="151" alignment-baseline="middle" text-anchor="middle">b</text>
<text x="95" y="151" alignment-baseline="middle" text-anchor="middle">e</text>
<text x="35" y="61" alignment-baseline="middle" text-anchor="middle">f</text>
<text x="15" y="15" alignment-baseline="middle" text-anchor="middle">T</text>
</svg>
<p>  

By choosing suitable families of graphs&nbsp;$G$, elimination forests encode many different combinatorial objects, and tree rotations in the elimination trees correspond to natural minimum change operations on those objects:
<ul class="enum">
<li class="enum">If $G$ is a path labeled $1,\ldots,n$ between its two end vertices, then its elimination trees correspond to binary trees. The distinction between left and right child of a vertex in an elimination tree is given by the smaller and larger labels. Elimination tree rotations correspond to classical rotations in binary trees.</li>
<li class="enum">If $G$ is a complete graph on $n$ vertices, then its elimination trees are paths, which can be interpreted as permutations, by reading off the vertices of the elimination tree from the root to the leaf of the path. Elimination tree rotations correspond to adjacent transpositions in the permutations.</li>
<li class="enum">If $G$ is a star with center $c$ and leaves $1,\ldots,n$, then its elimination trees are brooms, and reading off the vertices of the broom starting at the root and ending at the parent of&nbsp;$c$ yields partial permutations, i.e., linear orderings of all subsets of&nbsp;$\{1,\ldots,n\}$. Moreover, elimination tree rotations correspond to adjacent transpositions or deletions and insertions of a trailing element in a partial permutation.</li>
<li class="enum">If $G$ is a disjoint union of $n$ edges&nbsp;$\{i,n+i\}$, then its elimination forests correspond to bitstrings of length&nbsp;$n$, where the value of the $i$th bit is 0 if in the elimination forest the edge&nbsp;$\{i,n+i\}$ is rooted at&nbsp;$i$ and the bit is 1 if the edge is rooted at&nbsp;$n+i$. Elimination tree rotations correspond to flipping a single bit in the bitstrings.</li>
<li class="enum">If $G$ is a disjoint union of $n$ edges and a complete graph on $n$ vertices, then its elimination forest can be interpreted as signed permutations: The elimination tree of the complete graph determines the permutation, and the elimination trees for the edges determine the signs. Elimination tree rotations correspond to adjacent transpositions in the permutations (without changing the signs), or to sign reversals of a single entry in the permutation.
</li>
</ul>

A graph&nbsp;$G$ is <span class="def">chordal</span> if every induced cycle has length three.
Subclasses of chordal graphs include trees (in particular, paths and stars), complete graphs, $k$-trees, interval graphs, and split graphs.
In particular, they include the five classes of graphs mentioned before in the examples.
A <span class="def">perfect elimination ordering</span> of a chordal graph is an ordering of the vertices such that each vertex forms a clique with the neighbors that appear before it in the ordering.
For example, a perfect elimination ordering of the graph&nbsp;$G$ shown in the figure above is $a,d,c,b,f,e$.
<p>

It was shown in&nbsp;[MP15] that if $G$ has at least two edges, then all its elimination forests can be ordered cyclically so that any two consecutive elimination forests differ in a single tree rotation.
These orderings of elimination forests correspond to Hamilton cycles on the skeleta of polytopes known as <span class="def">graph associahedra</span> [CD06], [Pos09].
In the paper&nbsp;[CMM21] we propose an efficient algorithm for computing such an ordering for the case when $G$ is chordal (albeit the ordering is not necessarily cyclic).
The algorithm running on this website implements this algorithm, and it first outputs the perfect elimination ordering that is used for labeling the vertices of the input graph.
In the graphical output, the elimination trees are rotated to save space, with the root on the left and the tree growing to the right.
In the textual output, each elimination tree is traversed in DFS order, printing each vertex label followed by a sequence of dots corresponding to the number of children of that vertex in the elimination tree.
For example, the elimination tree shown above with the perfect elimination ordering $a,d,c,b,f,e$ is printed as 2.. 5 1. 3.. 4 6 (it appears at position 156 in the output for "example G")
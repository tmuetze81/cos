<?php
  if (isset($_GET["g"]) && isset($_GET["fmt"]) && isset($_GET["num"]) && isset($_GET["gfx"])) {
    $fmt = intval($_GET["fmt"]);
    $cmd = "code/elim/elim"
                             . " -e" . escapeshellarg($_GET["g"])
                             . " -l" . strval(limit($fmt))  /* hard-wired maximum number of elimination forests */
                             . " 2>&1"  /* this command redirects stderr to stdout */
                             . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */
    exec($cmd, $result);
    $num = filter_var($_GET["num"], FILTER_VALIDATE_BOOLEAN);
    $gfx = filter_var($_GET["gfx"], FILTER_VALIDATE_BOOLEAN);
    output($result, $fmt, "string_to_elim", 0, "svg_elim", 0, 0, 0, $num, $gfx);
  }
?>

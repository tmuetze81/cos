<html>
<?php include "head.html"; ?>

<?php include "web/limits.php"; ?>
<?php $object = $_GET["obj"]; ?>

<body>

<div id="left">
<?php include "logo.html"; ?>
<nav id="nav0" class="<?php echo $object; ?>"></nav>
</div>

<div id="header">
<span id="title">The Combinatorial Object Server</span>
</div>
  
<div id="algo">

<!-- algorithm input -->
<?php include "web/" . $object . "/header.html"; ?>
<div id="input">
<?php include "web/" . $object . "/run.html"; ?>
</div>

<!-- algorithm output -->
<div id="output">
<span class="h2">Output</span>&nbsp;
<span id="options">
<input type="checkbox" id="numbering" onclick="numbering()" checked/> numbering
<input type="checkbox" id="graphics" onclick="graphics()" checked/> graphics
</span><br>
<iframe id="consoleframe" src="<?php echo "gen.php?obj=$object"; ?>"></iframe>
<span id="total"></span>
</div>

<!-- collapsible subsections -->
<?php
  $sections = ["Object info", "Enumeration (OEIS)", "Download source code", "References"];
  $files = ["info.html", "oeis.html", "download.html", "refs.html"];
  for ($i = 0; $i < sizeof($sections); $i++) {
    $file = "web/$object/$files[$i]";
    if (file_exists($file)) {
      /* Initially, all subsections are folded, and the first one (object info) is unfolded upon completion of the Mathjax rendering. */
      /* This is to get the vertical spacing of the subsections during folding/unfolding correct. */
      echo "<h2 class='collapse_heading' data-folded='true'>" . $sections[$i] . "</h2>\n";
      echo "<div class='collapse_section'>\n";
      include $file;
      echo "</div>\n\n";
    }
  } 
?>
</div>

<script src="script.js"></script>
<script> output_buttons(); </script>

</body>
</html>
